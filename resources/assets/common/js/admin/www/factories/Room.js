app.factory('Room', ['$http', 'BASE_API_URL', 'BASE_URL', 'Session', 'Promise',
    function ($http, BASE_API_URL, BASE_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'room/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();

            var beds = [];
            var images = [];

            angular.forEach(model.beds, function (value) {
                beds.push(value.id);
            });

            //Image
            angular.forEach(model.images, function (value) {
                images.push({
                    title: value.title,
                    alt: value.alt,
                    caption: value.caption,
                    description: value.description,
                    url: (!angular.isUndefined(value.url) && value.url) ? value.url.replace(BASE_URL, '') : null
                });
            });

            var data = {
                product_id: model.product_id,
                title: model.title,
                description: model.description,
                adult: model.adult,
                children: model.children,
                max: model.max,
                footage: model.footage,
                price: model.price,
                option: model.option,
                policy: model.policy,
                bed: beds,
                room_facility: model.room_facility,
                room_image: images
            };
            var config = {};

            $http.post(BASE_API_URL + 'room/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();

            var beds = [];
            var images = [];

            angular.forEach(model.beds, function (value) {
                beds.push(value.id);
            });

            //Image
            angular.forEach(model.images, function (value) {
                images.push({
                    id: (!angular.isUndefined(value.id) && value.id) ? value.id : null,
                    title: value.title,
                    alt: value.alt,
                    caption: value.caption,
                    description: value.description,
                    url: (!angular.isUndefined(value.url) && value.url) ? value.url.replace(BASE_URL, '') : null
                });
            });

            var data = {
                id: model.id,
                product_id: model.product_id,
                title: model.title,
                description: model.description,
                adult: model.adult,
                children: model.children,
                max: model.max,
                footage: model.footage,
                price: model.price,
                option: model.option,
                policy: model.policy,
                bed: beds,
                room_image: images,
                room_facility: model.room_facility
            };
            var config = {};

            $http.post(BASE_API_URL + 'room/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'room/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getRoomListByAgentId = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'room/getRoomListByAgentId/' + id + '?token=' + Session.token, data, config)
                .then(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);