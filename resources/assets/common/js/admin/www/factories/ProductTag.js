app.factory('ProductTag', ['$http', 'BASE_API_URL', 'Session', 'Promise',
    function ($http, BASE_API_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'productTag/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();
            var data = {
                agent_id: model.agent_id,
                name: model.name,
                slug: model.slug
            };
            var config = {};

            $http.post(BASE_API_URL + 'productTag/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();
            var data = {
                id: model.id,
                agent_id: model.agent_id,
                name: model.name,
                slug: model.slug
            };
            var config = {};

            $http.post(BASE_API_URL + 'productTag/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'productTag/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getProductTagByName = function (agentId, name) {
            var def = Promise.defer();
            var data = {
                params: {
                    agent_id: agentId,
                    name: name
                }
            };
            var config = {};

            $http.get(BASE_API_URL + 'productTag/getProductTagByName?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getSlug = function (agentId, name) {
            var def = Promise.defer();
            var data = {
                params: {
                    agent_id: agentId,
                    name: name
                }
            };
            var config = {};

            $http.get(BASE_API_URL + 'productTag/getSlug?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);