app.factory('Product', ['$http', '$filter', 'BASE_API_URL', 'BASE_URL', 'Session', 'Promise',
    function ($http, $filter, BASE_API_URL, BASE_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'product/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();

            var images = [];
            var videos = [];
            var wayPoints = [];
            var includes = [];
            var itineraries = [];

            //Image
            angular.forEach(model.images, function (value) {
                images.push({
                    title: value.title,
                    alt: value.alt,
                    caption: value.caption,
                    description: value.description,
                    url: (!angular.isUndefined(value.url) && value.url) ? value.url.replace(BASE_URL, '') : null
                });
            });

            //Video
            angular.forEach(model.videos, function (value) {
                videos.push({
                    title: value.title,
                    description: value.description,
                    url: (!angular.isUndefined(value.url) && value.url) ? value.url.replace(BASE_URL, '') : null
                });
            });

            //Way Point
            angular.forEach(model.product_way_point, function (value) {
                wayPoints.push({
                    title: value.title,
                    address: value.address,
                    lat: value.lat,
                    lng: value.lng,
                    order: value.order
                });
            });

            //Include
            angular.forEach(model.product_include, function (value) {
                includes.push({
                    title: value.title,
                    sub_title: value.sub_title,
                    description: value.description,
                    order: value.order
                });
            });

            //Itinerary
            angular.forEach(model.product_itinerary, function (value) {
                itineraries.push({
                    title: value.title,
                    sub_title: value.sub_title,
                    description: value.description,
                    order: value.order
                });
            });

            var data = {
                product_type_id: model.product_type_id,
                product_category_id: model.product_category_id,
                property_type_id: (!angular.isUndefined(model.property_type_id) && model.property_type_id) ? model.property_type_id : null,
                agent_id: model.agent_id,
                publish: $filter('date')(model.publish, 'yyyy-MM-dd HH:mm:ss'),
                title: model.title,
                slug: model.slug,
                overview: model.overview,
                description: model.description,
                featured_image_url: (!angular.isUndefined(model.featured_image_url) && model.featured_image_url) ? model.featured_image_url.replace(BASE_URL, '') : null,
                featured_video_url: (!angular.isUndefined(model.featured_video_url) && model.featured_video_url) ? model.featured_video_url.replace(BASE_URL, '') : null,
                include_brochure_url: (!angular.isUndefined(model.include_brochure_url) && model.include_brochure_url) ? model.include_brochure_url.replace(BASE_URL, '') : null,
                itinerary_brochure_url: (!angular.isUndefined(model.itinerary_brochure_url) && model.itinerary_brochure_url) ? model.itinerary_brochure_url.replace(BASE_URL, '') : null,
                lat: model.lat,
                lng: model.lng,
                address: model.address,
                duration: model.duration,
                min_age: model.min_age,
                max_age: model.max_age,
                price: (!angular.isUndefined(model.price)) ? model.price : null,
                grade: model.grade,
                discount: model.discount,
                status: model.status,
                product_tag: model.product_tag,
                product_facility: model.product_facility,
                product_image: images,
                product_video: videos,
                product_way_point: wayPoints,
                product_include: includes,
                product_itinerary: itineraries
            };

            var config = {};

            $http.post(BASE_API_URL + 'product/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();

            var images = [];
            var videos = [];
            var wayPoints = [];
            var includes = [];
            var itineraries = [];

            //Image
            angular.forEach(model.images, function (value) {
                images.push({
                    id: (!angular.isUndefined(value.id) && value.id) ? value.id : null,
                    title: value.title,
                    alt: value.alt,
                    caption: value.caption,
                    description: value.description,
                    url: (!angular.isUndefined(value.url) && value.url) ? value.url.replace(BASE_URL, '') : null
                });
            });

            //Video
            angular.forEach(model.videos, function (value) {
                videos.push({
                    id: (!angular.isUndefined(value.id) && value.id) ? value.id : null,
                    title: value.title,
                    description: value.description,
                    url: (!angular.isUndefined(value.url) && value.url) ? value.url.replace(BASE_URL, '') : null
                });
            });

            //Way Point
            angular.forEach(model.product_way_point, function (value) {
                wayPoints.push({
                    id: (!angular.isUndefined(value.id) && value.id) ? value.id : null,
                    title: value.title,
                    address: value.address,
                    lat: value.lat,
                    lng: value.lng,
                    order: value.order
                });
            });

            //Include
            angular.forEach(model.product_include, function (value) {
                includes.push({
                    title: value.title,
                    sub_title: value.sub_title,
                    description: value.description,
                    order: value.order
                });
            });

            //Itinerary
            angular.forEach(model.product_itinerary, function (value) {
                itineraries.push({
                    title: value.title,
                    sub_title: value.sub_title,
                    description: value.description,
                    order: value.order
                });
            });

            var data = {
                id: model.id,
                product_type_id: model.product_type_id,
                product_category_id: model.product_category_id,
                property_type_id: (!angular.isUndefined(model.property_type_id) && model.property_type_id) ? model.property_type_id : null,
                agent_id: model.agent_id,
                title: model.title,
                slug: model.slug,
                overview: model.overview,
                description: model.description,
                featured_image_url: (!angular.isUndefined(model.featured_image_url) && model.featured_image_url) ? model.featured_image_url.replace(BASE_URL, '') : null,
                featured_video_url: (!angular.isUndefined(model.featured_video_url) && model.featured_video_url) ? model.featured_video_url.replace(BASE_URL, '') : null,
                include_brochure_url: (!angular.isUndefined(model.include_brochure_url) && model.include_brochure_url) ? model.include_brochure_url.replace(BASE_URL, '') : null,
                itinerary_brochure_url: (!angular.isUndefined(model.itinerary_brochure_url) && model.itinerary_brochure_url) ? model.itinerary_brochure_url.replace(BASE_URL, '') : null,
                lat: model.lat,
                lng: model.lng,
                address: model.address,
                duration: model.duration,
                min_age: model.min_age,
                max_age: model.max_age,
                price: model.price,
                grade: model.grade,
                discount: model.discount,
                status: model.status,
                product_tag: model.product_tag,
                product_facility: model.product_facility,
                product_image: images,
                product_video: videos,
                product_way_point: wayPoints,
                product_include: includes,
                product_itinerary: itineraries
            };

            var config = {};

            $http.post(BASE_API_URL + 'product/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'product/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getSlug = function (agentId, title) {
            var def = Promise.defer();
            var data = {
                params: {
                    agent_id: agentId,
                    title: title
                }
            };
            var config = {};

            $http.get(BASE_API_URL + 'product/getSlug?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);