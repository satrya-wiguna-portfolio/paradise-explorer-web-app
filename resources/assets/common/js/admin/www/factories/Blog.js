app.factory('Blog', ['$http', '$filter', 'BASE_API_URL', 'BASE_URL', 'Session', 'Promise',
    function ($http, $filter, BASE_API_URL, BASE_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'blog/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();
            var data = {
                blog_category: model.blog_category,
                blog_tag: model.blog_tag,
                user_id: model.user_id,
                publish: $filter('date')(model.publish, 'yyyy-MM-dd HH:mm:ss'),
                title: model.title,
                slug: model.slug,
                contents: model.contents,
                featured_image_url: (!angular.isUndefined(model.featured_image_url) && model.featured_image_url) ? model.featured_image_url.replace(BASE_URL, '') : null,
                featured_video_url: (!angular.isUndefined(model.featured_video_url) && model.featured_video_url) ? model.featured_video_url.replace(BASE_URL, '') : null,
                status: model.status
            };
            var config = {};

            $http.post(BASE_API_URL + 'blog/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();
            var data = {
                id: model.id,
                blog_category: model.blog_category,
                blog_tag: model.blog_tag,
                title: model.title,
                slug: model.slug,
                contents: model.contents,
                featured_image_url: (!angular.isUndefined(model.featured_image_url) && model.featured_image_url) ? model.featured_image_url.replace(BASE_URL, '') : null,
                featured_video_url: (!angular.isUndefined(model.featured_video_url) && model.featured_video_url) ? model.featured_video_url.replace(BASE_URL, '') : null,
                status: model.status
            };
            var config = {};

            $http.post(BASE_API_URL + 'blog/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'blog/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getSlug = function (title) {
            var def = Promise.defer();
            var data = {
                params: {
                    title: title
                }
            };
            var config = {};

            $http.get(BASE_API_URL + 'blog/getSlug?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);