app.factory('Agent', ['$http', 'BASE_API_URL', 'Session', 'Promise',
    function ($http, BASE_API_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'agent/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();
            var data = {
                user_id: (!angular.isUndefined(model.user_id)) ? model.user_id : null,
                agent_type_id: model.agent_type_id,
                title: model.title,
                first_name: model.first_name,
                last_name: model.last_name,
                gender: model.gender,
                company: model.company,
                address: model.address,
                country_id: model.country_id,
                state_id: model.state_id,
                city: model.city,
                zip: model.zip,
                image_url: (!angular.isUndefined(model.image_url)) ? model.image_url : null,
                image_file: (!angular.isUndefined(model.image_file)) ? model.image_file : null,
                phone: model.phone,
                fax: model.fax
            };

            if (!angular.isUndefined(model.user)) {
                data.user = {
                    email: model.user.email,
                    password: model.user.password,
                    password_confirmation: model.user.password_confirmation,
                    handphone: model.user.handphone
                }

                if (!angular.isUndefined(model.user.role)) {
                    data.user.role = {
                        id: model.user.role.id
                    }
                }
            }

            var config = {};

            $http.post(BASE_API_URL + 'agent/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();
            var data = {
                id: model.id,
                user_id: model.user_id,
                agent_type_id: model.agent_type_id,
                title: model.title,
                first_name: model.first_name,
                last_name: model.last_name,
                gender: model.gender,
                company: model.company,
                address: model.address,
                country_id: model.country_id,
                state_id: model.state_id,
                city: model.city,
                zip: model.zip,
                image_url: (!angular.isUndefined(model.image_url)) ? model.image_url : null,
                image_file: (!angular.isUndefined(model.image_file)) ? model.image_file : null,
                phone: model.phone,
                fax: model.fax
            };
            var config = {};

            $http.post(BASE_API_URL + 'agent/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'agent/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getAgentByUserId = function (userId) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'agent/getAgentByUserId/' + userId + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);