app.factory('RoomAllotment', ['$http', 'BASE_API_URL', 'Session', 'Promise',
    function ($http, BASE_API_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'roomAllotment/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();
            var data = {
                room_id: model.room_id,
                from: (!angular.isUndefined(model.datePicker.startDate)) ? moment(model.datePicker.startDate).format('YYYY-MM-DD') : moment(model.datePicker).format('YYYY-MM-DD'),
                to: (!angular.isUndefined(model.datePicker.endDate)) ? moment(model.datePicker.endDate).format('YYYY-MM-DD') : null,
                allotment: model.allotment,
                lock: model.lock,
                sold: model.sold,
                balance: model.balance,
                price: model.price,
                status: model.status
            };

            var config = {};

            $http.post(BASE_API_URL + 'roomAllotment/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();
            var data = {
                id: model.id,
                room_id: model.room_id,
                date: moment(model.datePicker).format('YYYY-MM-DD'),
                allotment: model.allotment,
                lock: model.lock,
                sold: model.sold,
                balance: model.balance,
                price: model.price,
                status: model.status
            };

            var config = {};

            $http.post(BASE_API_URL + 'roomAllotment/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'roomAllotment/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);