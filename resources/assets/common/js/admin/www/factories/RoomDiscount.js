app.factory('RoomDiscount', ['$http', 'BASE_API_URL', 'Session', 'Promise',
    function ($http, BASE_API_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'roomDiscount/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();
            var data = {
                room_id: model.room_id,
                open_date: moment(model.datePicker.startDate).format('YYYY-MM-DD'),
                close_date: moment(model.datePicker.endDate).format('YYYY-MM-DD'),
                discount: model.discount,
                status: model.status
            };
            var config = {};

            $http.post(BASE_API_URL + 'roomDiscount/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();
            var data = {
                id: model.id,
                room_id: model.room_id,
                open_date: moment(model.datePicker.startDate).format('YYYY-MM-DD'),
                close_date: moment(model.datePicker.endDate).format('YYYY-MM-DD'),
                discount: model.discount,
                status: model.status
            };
            var config = {};

            $http.post(BASE_API_URL + 'roomDiscount/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'roomDiscount/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);