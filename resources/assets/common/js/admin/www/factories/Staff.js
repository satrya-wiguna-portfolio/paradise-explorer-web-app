app.factory('Staff', ['$http', 'BASE_API_URL', 'Session', 'Promise',
    function ($http, BASE_API_URL, Session, Promise) {
        var promiseService = {};

        promiseService.getDetail = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'staff/getDetail/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.create = function (model) {
            var def = Promise.defer();
            var data = {
                user_id: (!angular.isUndefined(model.user_id)) ? model.user_id : null,
                nik: model.nik,
                title: model.title,
                first_name: model.first_name,
                last_name: model.last_name,
                gender: model.gender,
                address: model.address,
                province_id: model.province_id,
                regency_id: model.regency_id,
                district_id: model.district_id,
                village: model.village,
                zip: model.zip,
                image_url: (!angular.isUndefined(model.image_url)) ? model.image_url : null,
                image_file: (!angular.isUndefined(model.image_file)) ? model.image_file : null,
                phone: model.phone
            };

            if (!angular.isUndefined(model.user)) {
                data.user = {
                    email: model.user.email,
                    password: model.user.password,
                    password_confirmation: model.user.password_confirmation,
                    handphone: model.user.handphone
                }

                if (!angular.isUndefined(model.user.role)) {
                    data.user.role = {
                        id: model.user.role.id
                    }
                }
            }

            var config = {};

            $http.post(BASE_API_URL + 'staff/create?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.update = function (model) {
            var def = Promise.defer();
            var data = {
                id: model.id,
                user_id: model.user_id,
                nik: model.nik,
                title: model.title,
                first_name: model.first_name,
                last_name: model.last_name,
                gender: model.gender,
                address: model.address,
                province_id: model.province_id,
                regency_id: model.regency_id,
                district_id: model.district_id,
                village: model.village,
                zip: model.zip,
                image_url: (!angular.isUndefined(model.image_url)) ? model.image_url : null,
                image_file: (!angular.isUndefined(model.image_file)) ? model.image_file : null,
                phone: model.phone
            };
            var config = {};

            $http.post(BASE_API_URL + 'staff/update?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.delete = function (id) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'staff/delete/' + id + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        promiseService.getStaffByUserId = function (userId) {
            var def = Promise.defer();
            var data = {};
            var config = {};

            $http.get(BASE_API_URL + 'staff/getStaffByUserId/' + userId + '?token=' + Session.token, data, config)
                .success(function (response) {
                    def.resolve(response);
                }, function (error) {
                    def.reject(error);
                });

            return def.promise;
        }

        return promiseService;
    }]);