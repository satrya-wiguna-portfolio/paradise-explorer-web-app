app.controller('StaffController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'Staff', 'Province', 'Regency', 'District', 'Role', 'Session', 'Notification', 'BASE_API_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'BASE_URL', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, DTOptionsBuilder, DTColumnBuilder, Urls, Staff, Province, Regency, District, Role, Session, Notification, BASE_API_URL, AUTH_EVENTS, LOADING_EVENTS, BASE_URL, OTHER_EVENTS) {
        var filterProvinceOption = {
            placeholder: "- Filter by Province -",
            allowClear: true
        };

        var filterRegencyOption = {
            placeholder: "- Filter by Regency -",
            allowClear: true
        };

        var filterDistrictOption = {
            placeholder: "- Filter by District -",
            allowClear: true
        };

        var titleOption = {
            placeholder: "- Please Search -",
            minimumResultsForSearch: -1
        };

        var provinceOption = {
            placeholder: "- Please Search -"
        };

        var regencyOption = {
            placeholder: "- Please Search -"
        };

        var districtOption = {
            placeholder: "- Please Search -"
        };

        var roleOption = {
            placeholder: "- Please Search -"
        }

        $scope.dtInstance = {};
        $scope.data = {
            gender: 'male'
        };
        $scope.titles = [
            {
                id: 'mr',
                name: 'Mr.'
            },
            {
                id: 'mrs',
                name: 'Mrs.'
            }
        ];

        //List
        $scope.provinceId = 0;
        $scope.regencyId = 0;
        $scope.districtId = 0;
        $scope.userAlready = false;

        setInterval(function() {
            $scope.$apply()
        }, 500);

        Province.getProvinceList()
            .success(function (response) {
                $scope.provinces = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        Role.getRoleList()
            .success(function (response) {
                $scope.roles = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $scope.$watch("data.province_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                Regency.getRegencyListByProvince(newValue)
                    .success(function (response) {
                        $scope.regencies = response.data.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }

            if (newValue == '') {
                delete $scope.regencies;
                delete $scope.districts;
            }

            if (newValue != 1) {
                $('.nav-tabs a[data-target="#general"]').tab('show');
            }
        });

        $scope.$watch("data.regency_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                District.getDistrictListByRegency(newValue)
                    .success(function (response) {
                        $scope.districts = response.data.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }

            if (newValue == '') {
                delete $scope.districts;
            }

            if (newValue != 1) {
                $('.nav-tabs a[data-target="#general"]').tab('show');
            }
        });

        $scope.edit = function (id) {
            $location.path('/staff/edit/' + id);
        };

        $scope.delete = function (id) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                Staff.delete(id).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.add = function () {
            $location.url('/staff/add');
        };

        $scope.cancel = function () {
            $location.path('/staff');
        };

        $scope.create = function (staff) {
            $scope.loading = true;

            if (staff.phone.$valid === false) {
                $scope.loading = false;

                return false;
            }

            if (!angular.isUndefined(staff.handphone)) {
                if (staff.handphone.$valid === false) {
                    $scope.loading = false;

                    return false;
                }
            }

            Staff.create($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/staff');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.update = function (staff) {
            $scope.loading = true;

            if (staff.phone.$valid === false) {
                $scope.loading = false;

                return false;
            }

            Staff.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/staff');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.detail = function (id) {
            Staff.getDetail(id)
                .success(function (response) {
                    $scope.staff = response.dto;

                    if (!$scope.staff.image_url) {
                        $scope.staff.image_url = './assets/common/images/admin/avatar.jpg';
                    }
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.user = function (id) {
            $location.path('/user/edit/' + id);
        };

        $scope.getRegencyListByProvince = function (province_id) {
            delete $scope.data.regency_id;
            $scope.initializeRegency();

            Regency.getRegencyListByProvince(province_id)
                .success(function (response) {
                    $scope.regencies = response.data.dto;

                    delete($scope.districts);
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.getDistrictListByRegency = function (regency_id) {
            delete $scope.data.district_id;
            $scope.initializeDistrict();

            District.getDistrictListByRegency(regency_id)
                .success(function (response) {
                    $scope.districts = response.data.dto;
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('email').withTitle('Email').withOption('name', 'users.email'),
            DTColumnBuilder.newColumn('title').withTitle('Title').withOption('name', 'staffs.title'),
            DTColumnBuilder.newColumn('first_name').withTitle('First Name').withOption('name', 'staffs.first_name'),
            DTColumnBuilder.newColumn('last_name').withTitle('Last Name').withOption('name', 'staffs.last_name'),
            DTColumnBuilder.newColumn('gender').withTitle('Gender').withOption('name', 'staffs.gender'),
            DTColumnBuilder.newColumn('province').withTitle('Province').withOption('name', 'provinces.name'),
            DTColumnBuilder.newColumn('regency').withTitle('Regency').withOption('name', 'regencies.name'),
            DTColumnBuilder.newColumn('district').withTitle('District').withOption('name', 'districts.name'),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#view-detail" ng-click="detail(' + data.id + ')">' +
                    '<i class="fa fa-eye"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-primary btn-sm" ng-click="user(' + data.user_id + ')">' +
                    '<i class="fa fa-key"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(' + data.id + ')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'staff/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('fnServerParams', function (aoData) {
                aoData.province_id = ($scope.provinceId != 0) ? $scope.provinceId : null;
                aoData.regency_id = ($scope.regencyId != 0) ? $scope.regencyId : null;
                aoData.district_id = ($scope.districtId != 0) ? $scope.districtId : null;
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTable = function () {
            $scope.districtId = $scope.data.district_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTableAndFilterDistrict = function () {
            delete $scope.data.district_id;

            $scope.districtId = 0;
            $scope.regencyId = $scope.data.regency_id;
            $scope.provinceId = $scope.data.province_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTableAndFilterRegencyAndFilterDistrict = function () {
            delete $scope.data.district_id;
            delete $scope.data.regency_id;

            $scope.districtId = 0;
            $scope.regencyId = 0;
            $scope.provinceId = $scope.data.province_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.initializeFilterProvince = function () {
            $('#filterSelectProvince').select2(filterProvinceOption);
        };

        $scope.initializeFilterRegency = function () {
            $('#filterSelectRegency').select2(filterRegencyOption);
        };

        $scope.initializeFilterDistrict = function () {
            $('#filterSelectDistrict').select2(filterDistrictOption);
        };

        $scope.initializeUser = function () {
            $('#selectUser').select2({
                placeholder: "- Please Search -",
                minimumInputLength: 2,
                ajax: {
                    url: BASE_API_URL + 'user/getUserByEmail?token=' + Session.token,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            email: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.dto, function (item) {
                                return {
                                    text: item.email,
                                    id: item.id
                                }
                            })
                        }
                    },
                    cache: true
                }
            });
        };

        $scope.initializeTitle = function () {
            $('#selectTitle').select2(titleOption);
        };

        $scope.initializeProvince = function () {
            $('#selectProvince').select2(provinceOption);
        };

        $scope.initializeRegency = function () {
            $('#selectRegency').select2(regencyOption);
        };

        $scope.initializeDistrict = function () {
            $('#selectDistrict').select2(districtOption);
        };

        $scope.initializeRole = function () {
            $('#selectRole').select2(roleOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'add')) {
            var image = new Slim(document.getElementById('inputImage'), {
                label: 'Drop image here',
                size: '512,512',
                ratio: '1:1',
                didSave: function (data) {
                    $scope.data.image_file = data;
                },
                didRemove: function () {
                    delete $scope.data.image_file;
                },
                statusFileType: 'File type support (.jpg, .png, .gif)'
            });
        }

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            var image = new Slim(document.getElementById('inputImage'), {
                label: 'Drop image here',
                size: '512,512',
                ratio: '1:1',
                didSave: function (data) {
                    $scope.data.image_file = data;
                },
                didRemove: function () {
                    delete $scope.data.image_file;
                },
                statusFileType: 'File type support (.jpg, .png, .gif)'
            });

            $scope.$watch('data', function (newValues) {
                if (!angular.isUndefined(newValues)) {
                    Regency.getRegencyListByProvince(newValues.province_id)
                        .success(function (response) {
                            $scope.regencies = response.data.dto;
                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });

                    District.getDistrictListByRegency(newValues.regency_id)
                        .success(function (response) {
                            $scope.districts = response.data.dto;
                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });
                }
            });

            Staff.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;
                    console.log($scope.data);

                    if ($scope.data.image_url !== null)
                        image.load(BASE_URL + $scope.data.image_url);

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        }

    }
]);
