app.controller('MemberController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'Member', 'Country', 'State', 'Role', 'Session', 'Notification', 'BASE_API_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'BASE_URL', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, DTOptionsBuilder, DTColumnBuilder, Urls, Member, Country, State, Role, Session, Notification, BASE_API_URL, AUTH_EVENTS, LOADING_EVENTS, BASE_URL, OTHER_EVENTS) {
        var filterCountryOption = {
            placeholder: "- Filter by Country -",
            allowClear: true
        };

        var filterStateOption = {
            placeholder: "- Filter by State -",
            allowClear: true
        };

        var titleOption = {
            placeholder: "- Please Search -",
            minimumResultsForSearch: -1
        };

        var countryOption = {
            placeholder: "- Please Search -"
        };

        var stateOption = {
            placeholder: "- Please Search -"
        };

        var roleOption = {
            placeholder: "- Please Search -"
        };

        $scope.dtInstance = {};
        $scope.data = {
            gender: 'male'
        };
        $scope.titles = [
            {
                id: 'mr',
                name: 'Mr.'
            },
            {
                id: 'mrs',
                name: 'Mrs.'
            }
        ];

        //List
        $scope.countryId = 0;
        $scope.stateId = 0;
        $scope.userAlready = false;

        setInterval(function() {
            $scope.$apply()
        }, 500);

        Country.getCountryList()
            .success(function (response) {
                $scope.countries = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        Role.getRoleList()
            .success(function (response) {
                $scope.roles = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $scope.$watch("data.country_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                State.getStateListByCountry(newValue)
                    .success(function (response) {
                        $scope.states = response.data.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }

            if (newValue == '') {
                delete $scope.states;
            }

            if (newValue != 1) {
                $('.nav-tabs a[data-target="#general"]').tab('show');
            }
        });

        $scope.edit = function (id) {
            $location.path('/member/edit/' + id);
        };

        $scope.delete = function (id) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                Member.delete(id).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.add = function () {
            $location.url('/member/add');
        };

        $scope.cancel = function () {
            $location.path('/member');
        };

        $scope.create = function (member) {
            $scope.loading = true;

            if (member.phone.$valid === false) {
                $scope.loading = false;

                return false;
            }

            if (!angular.isUndefined(member.handphone)) {
                if (member.handphone.$valid === false) {
                    $scope.loading = false;

                    return false;
                }
            }

            Member.create($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/member');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });

        };

        $scope.update = function (member) {
            $scope.loading = true;

            if (member.phone.$valid === false) {
                $scope.loading = false;

                return false;
            }

            Member.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/member');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.user = function (id) {
            $location.path('/user/edit/' + id);
        };

        $scope.detail = function (id) {
            Member.getDetail(id)
                .success(function (response) {
                    $scope.member = response.dto;

                    if (!$scope.member.image_url) {
                        $scope.member.image_url = './assets/common/images/admin/avatar.jpg';
                    }
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.getStateListByCountry = function (country_id) {
            delete $scope.data.state_id;
            $scope.initializeState();

            State.getStateListByCountry(country_id)
                .success(function (response) {
                    $scope.states = response.data.dto;
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('email').withTitle('Email').withOption('name', 'users.email'),
            DTColumnBuilder.newColumn('title').withTitle('Title').withOption('name', 'members.title'),
            DTColumnBuilder.newColumn('first_name').withTitle('First Name').withOption('name', 'members.first_name'),
            DTColumnBuilder.newColumn('last_name').withTitle('Last Name').withOption('name', 'members.last_name'),
            DTColumnBuilder.newColumn('gender').withTitle('Gender').withOption('name', 'members.gender'),
            DTColumnBuilder.newColumn('country').withTitle('Country').withOption('name', 'countries.name'),
            DTColumnBuilder.newColumn('state').withTitle('State').withOption('name', 'states.name'),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#view-detail" ng-click="detail(' + data.id + ')">' +
                    '<i class="fa fa-eye"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-primary btn-sm" ng-click="user(' + data.user_id + ')">' +
                    '<i class="fa fa-key"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(' + data.id + ')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'member/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('fnServerParams', function (aoData) {
                aoData.country_id = ($scope.countryId != 0) ? $scope.countryId : null;
                aoData.state_id = ($scope.stateId != 0) ? $scope.stateId : null;
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTable = function () {
            $scope.stateId = $scope.data.state_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTableAndFilterState = function () {
            delete $scope.data.state_id;

            $scope.stateId = 0;
            $scope.countryId = $scope.data.country_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.initializeFilterCountry = function () {
            $('#filterSelectCountry').select2(filterCountryOption);
        };

        $scope.initializeFilterState = function () {
            $('#filterSelectState').select2(filterStateOption);
        };

        $scope.initializeUser = function () {
            $('#selectUser').select2({
                placeholder: "- Please Search -",
                minimumInputLength: 2,
                ajax: {
                    url: BASE_API_URL + 'user/getUserByEmail?token=' + Session.token,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            email: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.dto, function (item) {
                                return {
                                    text: item.email,
                                    id: item.id
                                }
                            })
                        }
                    },
                    cache: true
                }
            });
        };

        $scope.initializeTitle = function () {
            $('#selectTitle').select2(titleOption);
        };

        $scope.initializeCountry = function () {
            $('#selectCountry').select2(countryOption);
        };

        $scope.initializeState = function () {
            $('#selectState').select2(stateOption);
        };

        $scope.initializeRole = function () {
            $('#selectRole').select2(roleOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'add')) {
            var image = new Slim(document.getElementById('inputImage'), {
                label: 'Drop image here',
                size: '512,512',
                ratio: '1:1',
                didSave: function (data) {
                    $scope.data.image_file = data;
                },
                didRemove: function () {
                    delete $scope.data.image_file;
                },
                statusFileType: 'File type support (.jpg, .png, .gif)'
            });
        }

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            var image = new Slim(document.getElementById('inputImage'), {
                label: 'Drop image here',
                size: '512,512',
                ratio: '1:1',
                didSave: function (data) {
                    $scope.data.image_file = data;
                },
                didRemove: function () {
                    delete $scope.data.image_file;
                },
                statusFileType: 'File type support (.jpg, .png, .gif)'
            });

            $scope.$watch('data', function (newValues) {
                if (!angular.isUndefined(newValues)) {
                    State.getStateListByCountry(newValues.country_id)
                        .success(function (response) {
                            $scope.states = response.data.dto;
                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });
                }
            });

            Member.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;

                    if ($scope.data.image_url !== null)
                        image.load(BASE_URL + $scope.data.image_url);

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        }
    }
]);
