app.controller('CurrencyController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'Currency', 'Session', 'Notification', 'BASE_API_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, $timeout, DTOptionsBuilder, DTColumnBuilder, Urls, Currency, Session, Notification, BASE_API_URL, AUTH_EVENTS, LOADING_EVENTS, OTHER_EVENTS) {
        $scope.dtInstance = {};

        setInterval(function() {
            $scope.$apply()
        }, 500);

        $scope.edit = function (id) {
            $location.path('/currency/edit/' + id);
        };

        $scope.delete = function (code) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                Currency.delete(code).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.add = function () {
            $location.url('/currency/add');
        };

        $scope.sync = function () {
            swal({
                title: "Syncronize the currencies?",
                text: "It's will take syncronize all currencies exchange",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, syncronize!",
                closeOnConfirm: true
            }, function () {
                $timeout(function () {
                    $scope.syncronising = true;

                    Currency.sync($scope.data)
                        .success(function (response) {
                            console.log(response);

                            $scope.syncronising = false;

                            $rootScope.$broadcast(OTHER_EVENTS.messages, {
                                type: response._messages[0].type,
                                text: response._messages[0].text
                            });

                            if ($scope.data) {
                                $location.path('/currency');
                            } else {
                                $scope.dtInstance._renderer.rerender();
                            }
                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });
                }, 1000);
            });
        };

        $scope.cancel = function () {
            $location.path('/currency');
        };

        $scope.create = function () {
            $scope.loading = true;

            Currency.create($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/currency');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.update = function () {
            $scope.loading = true;

            Currency.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/currency');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('name').withTitle('Name').withOption('name', 'name'),
            DTColumnBuilder.newColumn('code').withTitle('Code').withOption('name', 'code'),
            DTColumnBuilder.newColumn('symbol').withTitle('Symbol').withOption('name', 'symbol'),
            DTColumnBuilder.newColumn('format').withTitle('Format').withOption('name', 'format'),
            DTColumnBuilder.newColumn('exchange_rate').withTitle('Exchange Rate').withOption('name', 'exchange_rate'),
            DTColumnBuilder.newColumn(null).withTitle('Active').notSortable().renderWith(function (data) {
                var active = '';

                switch (data.active) {
                    case 0:
                        active = '<strong>Disable</strong>';
                        break;

                    case 1:
                        active = '<strong>Enable</strong>';
                        break;
                }

                return active;

            }),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(\'' + data.code + '\')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'currency/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            console.log($scope.dtInstance);
            $scope.dtInstance._renderer.rerender();
        };

        if (Urls.getActionUrl($location.absUrl(), 'add')) {}

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            Currency.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        }
    }
]);
