app.controller('UserController', ['$scope', '$rootScope', '$location', '$http', '$routeParams', 'User', 'Role', 'Country', 'State', 'Staff', 'Agent', 'Session', 'Urls', 'BASE_URL', 'BASE_API_URL', 'AUTH_EVENTS',
    function ($scope, $rootScope, $location, $http, $routeParams, User, Role, Country, State, Staff, Agent, Session, Urls, BASE_URL, BASE_API_URL, AUTH_EVENTS) {
        var countryOption = {
            placeholder: "- Please Search -"
        };

        var stateOption = {
            placeholder: "- Please Search -"
        };

        var roleOption = {
            placeholder: "- Please Search -",
            minimumResultsForSearch: -1
        };

        $scope.session = Session;
        $scope.data = {};
        $scope.data_profile_view = {};
        $scope.data_profile_create_or_update = {};

        setInterval(function() {
            $scope.$apply()
        }, 500);

        Role.getRoleList()
            .success(function (response) {
                $scope.roles = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        Country.getCountryList()
            .success(function (response) {
                $scope.countries = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $scope.$watch("data_profile_create_or_update.country_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                State.getStateListByCountry(newValue)
                    .success(function (response) {
                        $scope.states = response.data.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }

            if (newValue == '') {
                delete $scope.states;
            }

            if (newValue != 1) {
                $('.nav-tabs a[data-target="#general"]').tab('show');
            }
        });

        $scope.update = function () {
            $scope.loading = true;

            User.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $location.path(Urls.previous.url);
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.createOrUpdateProfile = function () {
            $scope.loading = true;

            if (angular.isUndefined($scope.data_profile_create_or_update.phone)) {
                $scope.loading = false;

                return false;
            }

            switch ($scope.data.role[0].id) {
                case 1:
                case 2:
                    if ($scope.data_profile_create_or_update.id) {
                        Staff.update($scope.data_profile_create_or_update)
                            .success(function (response) {
                                console.log(response);

                                $scope.loading = false;

                                $rootScope.$broadcast(OTHER_EVENTS.messages, {
                                    type: response._messages[0].type,
                                    text: response._messages[0].text
                                });

                                $location.path('/staff');
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });
                    } else {
                        Staff.create($scope.data_profile_create_or_update)
                            .success(function (response) {
                                console.log(response);

                                $scope.loading = false;

                                $rootScope.$broadcast(OTHER_EVENTS.messages, {
                                    type: response._messages[0].type,
                                    text: response._messages[0].text
                                });

                                $location.path('/staff');
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });
                    }

                    break;

                case 3:
                    if ($scope.data_profile_create_or_update.id) {
                        Agent.update($scope.data_profile_create_or_update)
                            .success(function (response) {
                                console.log(response);

                                $scope.loading = false;

                                $rootScope.$broadcast(OTHER_EVENTS.messages, {
                                    type: response._messages[0].type,
                                    text: response._messages[0].text
                                });

                                $location.path('/agent');
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });
                    } else {
                        Agent.create($scope.data_profile_create_or_update)
                            .success(function (response) {
                                console.log(response);

                                $scope.loading = false;

                                $rootScope.$broadcast(OTHER_EVENTS.messages, {
                                    type: response._messages[0].type,
                                    text: response._messages[0].text
                                });

                                $location.path('/agent');
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });
                    }

                    break;

            }
        };

        $scope.updatePassword = function () {
            $scope.loading = true;

            if (angular.isUndefined($scope.data.handphone)) {
                $scope.loading = false;

                return false;
            }

            User.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $location.path(Urls.previous.url);
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.cancel = function () {
            $location.path(Urls.previous.url);
        };

        $scope.changePasswordModal = function (userId) {
            User.getDetail(userId)
                .success(function (response) {
                    $scope.data = response.dto;

                    console.log($scope.data);

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.editProfileModal = function (roleId, userId) {
            var image = new Slim(document.getElementById('inputImage'), {
                label: 'Drop image here',
                size: '512,512',
                ratio: '1:1',
                didSave: function (data) {
                    $scope.data_profile_create_or_update.image_file = data;
                },
                didRemove: function () {
                    delete $scope.data_profile_create_or_update.image_file;
                },
                statusFileType: 'File type support (.jpg, .png, .gif)'
            });

            $scope.data_profile_create_or_update.gender = "male";

            $scope.$watch('data_profile_create_or_update', function (newValues) {
                if (!angular.isUndefined(newValues)) {
                    State.getStateListByCountry(newValues.country_id)
                        .success(function (response) {
                            $scope.states = response.data.dto;
                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });
                }
            });

            User.getUserProfile(roleId, userId)
                .success(function (response) {
                    $scope.data = response.dto;

                    if ($scope.data.staff) {
                        angular.extend($scope.data_profile_create_or_update, $scope.data.staff);

                        if ($scope.data_profile_create_or_update.image_url != null)
                            image.load(BASE_URL + $scope.data_profile_create_or_update.image_url);
                    }

                    if ($scope.data.agent) {
                        angular.extend($scope.data_profile_create_or_update, $scope.data.agent);

                        if ($scope.data_profile_create_or_update.image_url != null)
                            image.load(BASE_URL + $scope.data_profile_create_or_update.image_url);
                    }

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        $scope.getStateListByCountry = function (country_id) {
            delete $scope.data_profile.state_id;
            $scope.initializeState();

            State.getStateListByCountry(country_id)
                .success(function (response) {
                    $scope.states = response.data.dto;
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.initializeCountry = function () {
            $('#selectCountry').select2(countryOption);
        };

        $scope.initializeState = function () {
            $('#selectState').select2(stateOption);
        };

        $scope.initializeRole = function () {
            $('#validation-role').select2(roleOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            User.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;

                    console.log($scope.data);

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        if (Urls.getActionUrl($location.absUrl(), 'profile')) {
            User.getUserProfile($scope.session.roleId, $scope.session.userId)
                .success(function (response) {
                    $scope.data = response.dto;

                    if ($scope.data.staff) {
                        angular.extend($scope.data_profile_view, $scope.data.staff);
                    }

                    if ($scope.data.agent) {
                        angular.extend($scope.data_profile_view, $scope.data.agent);
                    }

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };
    }
]);
