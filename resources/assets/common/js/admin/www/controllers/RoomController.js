app.controller('RoomController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'Room', 'Agent', 'Product', 'Bed', 'Facility', 'Session', 'FileManager', 'Notification', 'BASE_API_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, $filter, DTOptionsBuilder, DTColumnBuilder, Urls, Room, Agent, Product, Bed, Facility, Session, FileManager, Notification, BASE_API_URL, AUTH_EVENTS, LOADING_EVENTS, OTHER_EVENTS) {
        var filterAgentOption = {
            placeholder: "- Filter by Agent -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        agent_type_id: 2,
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var filterProductOption = {
            placeholder: "- Filter by Product -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'product/getProductByTitle?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        agent_id: ($scope.agentId != 0) ? $scope.agentId : -1,
                        product_type_id: 2,
                        title: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var agentOption = {
            placeholder: "- Please Search -",
            minimumInputLength: 2,
            cache: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        agent_type_id: 2,
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var productOption = {
            placeholder: "- Please Search -",
            minimumInputLength: 2,
            cache: true,
            ajax: {
                url: BASE_API_URL + 'product/getProductByTitle?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        agent_id: $scope.data.product.agent_id,
                        product_type_id: 2,
                        title: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var bedOption = {
            placeholder: "- Please Choose -",
            minimumResultsForSearch: -1
        };

        $scope.dtInstance = {};
        $scope.data = {
            adult: 1,
            children: 1,
            max: 2,
            footage: 0,
            price: 0
        };

        $scope.session = Session;

        $scope.agentId = 0;
        $scope.productId = 0;
        $scope.customMax = false;

        $scope.beds = [];
        $scope.images = [];

        $scope.tinymceBasicOptions = {
            height: 200,
            menubar: false,
            skin: 'lightgray',
            theme: 'modern',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            image_advtab: true,
            relative_urls: false
        };

        //Folder
        $scope.folderPath = 'room';
        $scope.subFolderPath = ($scope.session.role == 'super' || $scope.session.role == 'admin') ? 0 : $scope.session.userId;

        setInterval(function() {
            $scope.$apply()
        }, 500);

        FileManager.getAccessKey($scope.session.email)
            .success(function (response) {
                $scope.fileManager = response.dto;

                FileManager.setFolder($scope.folderPath)
                    .success(function (response) {
                        console.log(response);

                        FileManager.setSubFolder($scope.folderPath, $scope.subFolderPath)
                            .success(function (response) {
                                console.log(response);
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        Agent.getAgentByUserId($scope.session.userId)
            .success(function (response) {
                $scope.agent = response.dto;
                $scope.agentId = (!Array.isArray($scope.agent)) ? $scope.agent.id : 0;

                if (!angular.isUndefined($scope.dtInstance._renderer)) {
                    $scope.dtInstance._renderer.rerender();
                }

            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $scope.$watch("data.product.agent_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                if (Urls.getActionUrl($location.absUrl(), 'add')) {
                    //Set bed to empty
                    $scope.beds = [];
                }

                Bed.getBedListByAgentId(newValue)
                    .success(function (response) {
                        $scope.items = response.data.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });

                Agent.getDetail(newValue)
                    .success(function (response) {
                        agentOption.initSelection = function (element, callback) {
                            var data = {
                                id: response.dto.id,
                                text: response.dto.company
                            };

                            callback(data);
                        };

                        $('#validation-agent_id').select2(agentOption);
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        });

        $scope.$watch("data.product_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                Product.getDetail(newValue)
                    .success(function (response) {
                        productOption.initSelection = function (element, callback) {
                            var data = {
                                id: response.dto.id,
                                text: response.dto.title
                            };

                            callback(data);
                        };

                        $('#validation-product_id').select2(productOption);
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        });

        $scope.$watchGroup(['data.adult', 'data.children'], function (newValues) {
            if (!$scope.customMax) {
                $scope.data.max = newValues[0] + newValues[1];
            }

            $scope.total = newValues[0] + newValues[1];
        });

        $scope.$watch("customMax", function (newValues) {
            if (!newValues) {
                $scope.data.max = $scope.data.adult + $scope.data.children;
            }

            $scope.total = $scope.data.adult + $scope.data.children;
        });

        $scope.$watch("beds", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.beds = newValue;
            }
        });

        $scope.$watch("images", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.images = newValue;
            }
        });

        $scope.$watch("_images", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                $scope.images.push({
                    url: newValue,
                    title: null,
                    caption: null,
                    alt: null,
                    description: null
                });

                $('#validation-image').val('');
            }
        });

        $scope.detailImage = function (index) {
            $scope.indexImage = index;
        };

        $scope.edit = function (id) {
            $location.path('/room/edit/' + id);
        };

        $scope.delete = function (id) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                Room.delete(id).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.add = function () {
            $location.url('/room/add');
        };

        $scope.cancel = function () {
            $location.path('/room');
        };

        $scope.create = function () {
            $scope.loading = true;

            Room.create($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/room');

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.update = function () {
            $scope.loading = true;

            var roomFacility = [];

            angular.forEach($scope.data.room_facility, function (value) {
                if (value.id) {
                    roomFacility.push(value.id);
                } else {
                    roomFacility.push(value);
                }
            });

            $scope.data.room_facility = roomFacility;

            Room.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/room');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.addBed = function () {
            $scope.beds.push({
                bed_id: null
            });
        };

        $scope.removeBed = function (index) {
            $scope.beds.splice(index, 1);
        };

        $scope.removeImage = function (index) {
            $scope.images.splice(index, 1);
        };

        $scope.setImage = function () {
            $('#set-image').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('product_id').withTitle('Product ID').withOption('name', 'product_id').notVisible(),
            DTColumnBuilder.newColumn('title').withTitle('Title').withOption('name', 'title'),
            DTColumnBuilder.newColumn('adult').withTitle('Adult').withOption('name', 'adult'),
            DTColumnBuilder.newColumn('children').withTitle('Children').withOption('name', 'children'),
            DTColumnBuilder.newColumn(null).withTitle('Price').withOption('name', 'price').renderWith(function (data) {
                return 'Rp ' + $filter('number')(data.price);
            }),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(' + data.id + ')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'room/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('fnServerParams', function (aoData) {
                aoData.agent_id = ($scope.agentId != 0) ? $scope.agentId : null;
                aoData.product_id = ($scope.productId != 0) ? $scope.productId : null;
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('fnDrawCallback', function (oSettings) {
                var api = this.api();
                var rows = api.rows({
                    page:'current'
                }).nodes();
                var last = null;

                api.column(0, {
                    page:'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group" style="background-color: #2a68bd; color: #ffffff;"><td colspan="6">' +
                            '<strong>' + oSettings.aoData[i]._aData.product.title + '</strong></td></tr>'
                        );
                        last = group;
                    }
                });
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshProduct = function () {
            delete $scope.data.product_id;
        };

        $scope.refreshTable = function () {
            $scope.agentId = $scope.data.product.agent_id;
            $scope.productId = $scope.data.product_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshFilterOfProductAndTable = function () {
            delete $scope.data.product_id;

            $scope.productId = 0;
            $scope.agentId = $scope.data.product.agent_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshRoomFacility = function (name) {
            Facility.getFacilityByName(name)
                .success(function (response) {
                    $scope.roomFacilities = response.dto;

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.initializeFilterAgent = function () {
            $('#filter-agent_id').select2(filterAgentOption);
        };

        $scope.initializeFilterProduct = function () {
            $('#filter-product_id').select2(filterProductOption);
        };

        $scope.initializeAgent = function () {
            $('#validation-agent_id').select2(agentOption).on("select2:select", function (e) {
                $("#validation-product_id").val(null).trigger("change");
            });
        };

        $scope.initializeProduct = function () {
            $('#validation-product_id').select2(productOption);
        };

        $scope.initializeBed = function () {
            $('.select2-bed').select2(bedOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'add')) {
            if ($scope.session.role != 'super' || $scope.session.role != 'admin') {
                Agent.getAgentByUserId($scope.session.userId)
                    .success(function (response) {
                        $scope.agent = response.dto;
                        $scope.data.product.agent_id = (!Array.isArray($scope.data.product.agent_id)) ? $scope.agent.id : 0;

                        Bed.getBedListByAgentId(response.dto.id)
                            .success(function (response) {
                                $scope.items = response.data.dto;

                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        }

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            Room.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;

                    Bed.getBedListByAgentId(response.dto.product.agent_id)
                        .success(function (response) {
                            $scope.items = response.data.dto;

                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });

                    $scope.data.adult = parseInt(response.dto.adult);
                    $scope.data.children = parseInt(response.dto.children);
                    $scope.data.max = parseInt(response.dto.max);

                    if ($scope.data.max != ($scope.data.adult + $scope.data.children)) {
                        $scope.customMax = true;
                    }

                    $scope.data.footage = parseInt(response.dto.footage);
                    $scope.data.price = parseInt(response.dto.price);

                    //Bed
                    var beds = [];

                    angular.forEach(response.dto.bed, function (value) {
                        beds.push({
                            id: value.id
                        });
                    });

                    $scope.beds = beds;

                    //Image
                    $scope.images = response.dto.image;

                    //Room Facility
                    var roomFacility = [];

                    angular.forEach(response.dto.room_facility, function (value) {
                        roomFacility.push({
                            id: value.id,
                            facility: value.facility
                        });
                    });

                    $scope.data.room_facility = roomFacility;
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        }
    }
]);
