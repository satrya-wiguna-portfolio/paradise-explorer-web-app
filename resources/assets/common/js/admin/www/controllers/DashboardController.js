app.controller('DashboardController', ['$scope', '$rootScope', '$location', 'Session', 'User', 'OTHER_EVENTS', 'AUTH_EVENTS',
    function($scope, $rootScope, $location, Session, User, OTHER_EVENTS, AUTH_EVENTS) {
        $scope.session = Session;

        setInterval(function() {
            $scope.$apply()
        }, 500);

        User.getUserInfo($scope.session.userId)
            .success(function (response) {
                $scope.userInfo = response.dto;

                $rootScope.$broadcast(OTHER_EVENTS.userInfo, $scope.userInfo);
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });
    }
]);