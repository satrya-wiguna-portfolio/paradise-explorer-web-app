app.controller('ProductController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', '$uibModal', '$timeout', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'Product', 'ProductType', 'ProductCategory', 'Agent', 'ProductTag', 'Facility', 'PropertyType', 'Session', 'FileManager', 'Notification', 'BASE_API_URL', 'BASE_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, $uibModal, $timeout, $filter, DTOptionsBuilder, DTColumnBuilder, Urls, Product, ProductType, ProductCategory, Agent, ProductTag, Facility, PropertyType, Session, FileManager, Notification, BASE_API_URL, BASE_URL, AUTH_EVENTS, LOADING_EVENTS, OTHER_EVENTS) {
        var filterAgentOption = {
            placeholder: "- Filter by Agent -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var filterProductTypeOption = {
            placeholder: "- Filter by Product Type -",
            allowClear: true
        };

        var filterProductCategoryOption = {
            placeholder: "- Filter by Product Category -",
            allowClear: true
        };

        var productCategoryOption = {
            placeholder: "- Please Choose -",
            minimumResultsForSearch: -1
        };

        var propertyTypeOption = {
            placeholder: "- Please Choose -"
        };

        var agentOption = {
            placeholder: "- Please Search -",
            minimumInputLength: 2,
            cache: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var prefixOption = {
            placeholder: "- Please Choose -",
            minimumResultsForSearch: -1
        };

        $scope.session = Session;

        $scope.dtInstance = {};
        $scope.data = {
            publish: new Date(),
            lat: -8.340539,
            lng: 115.091951,
            discount: 0,
            min_age: 17,
            max_age: 40,
            duration: 1,
            duration_covert: null,
            status: 0
        };
        $scope.featuredVideoExternal = {
            prefix: null,
            code: null
        };
        $scope.videoGalleryExternal = {
            prefix: null,
            code: null
        };
        $scope.videoChannels = [{
            name: 'Youtube Video Channel',
            prefix: 'https://www.youtube.com/embed/'
        }, {
            name: 'Vimeo Video Channel',
            prefix: 'https://www.vimeo.com/'
        }];
        $scope.tinymceFullOptions = {
            height: 400,
            plugins: 'advlist autolink link image lists charmap preview table textcolor code responsivefilemanager pagebreak',
            toolbar: 'insertfile undo redo | styleselect table code | sizeselect | bold italic |  fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager',
            skin: 'lightgray',
            theme: 'modern',
            pagebreak_separator: "<!--more-->",
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            image_advtab: true,
            relative_urls: false,
            external_filemanager_path: BASE_URL + '/filemanager/',
            filemanager_title: "File Manager",
            external_plugins: {
                "filemanager": BASE_URL + '/filemanager/plugin.min.js'
            }
        };
        $scope.tinymceBasicOptions = {
            height: 200,
            menubar: false,
            skin: 'lightgray',
            theme: 'modern',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            image_advtab: true,
            relative_urls: false
        };

        //List
        $scope.agentId = 0;
        $scope.productTypeId = 0;
        $scope.productCategoryId = 0;

        //Folder
        $scope.folderPath = 'product';
        $scope.subFolderPath = ($scope.session.role == 'super' || $scope.session.role == 'admin') ? 0 : $scope.session.userId;
        $scope.tinyMceFull = false;

        $scope.video = {
            external: false
        };
        $scope.video_gallery = {
            external: false
        };

        //Image Gallery, Video Gallery & Way Point
        $scope.images = [];
        $scope.videos = [];
        $scope.wayPoints = [];
        $scope.includes = [];
        $scope.itineraries = [];

        $scope.renderlocation = false;
        $scope.age = {
            all: false
        };

        setInterval(function() {
            $scope.$apply()
        }, 500);

        FileManager.getAccessKey($scope.session.email)
            .success(function (response) {
                $scope.fileManager = response.dto;
                $scope.tinymceFullOptions.filemanager_access_key = $scope.fileManager.akey;
                $scope.tinyMceFull = true;

                FileManager.setFolder($scope.folderPath)
                    .success(function (response) {
                        console.log(response);

                        FileManager.setSubFolder($scope.folderPath, $scope.subFolderPath)
                            .success(function (response) {
                                console.log(response);
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        Agent.getAgentByUserId($scope.session.userId)
            .success(function (response) {
                $scope.agent = response.dto;
                $scope.agentId = (!Array.isArray($scope.agent)) ? $scope.agent.id : 0;

                if (!angular.isUndefined($scope.dtInstance._renderer)) {
                    $scope.dtInstance._renderer.rerender();
                }

            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        ProductType.getProductTypeList()
            .success(function (response) {
                $scope.productTypes = response.data.dto;

            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        PropertyType.getPropertyTypeList()
            .success(function (response) {
                $scope.propertyTypes = response.data.dto;
            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $timeout(function () {
            $scope.renderlocation = true;
        }, 1000);

        $scope.$watch("data.agent_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                if (Urls.getActionUrl($location.absUrl(), 'add')) {
                    delete $scope.data.product_tag;

                }

                Agent.getDetail(newValue)
                    .success(function (response) {
                        agentOption.initSelection = function (element, callback) {
                            var data = {
                                id: response.dto.id,
                                text: response.dto.company
                            };

                            callback(data);
                        };

                        $('#validation-agent_id').select2(agentOption);
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        });

        $scope.$watch("data.product_type_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                ProductCategory.getProductCategoryList(newValue)
                    .success(function (response) {
                        $scope.productCategories = response.data.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }

            if (newValue == '') {
                delete $scope.productCategories;
            }

            if (newValue != 1) {
                $('.nav-tabs a[data-target="#general"]').tab('show');
            }
        });

        $scope.$watch("data.duration", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                $scope.durationConvert(newValue);
            }
        });

        $scope.$watch("images", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.images = newValue;
            }
        });

        $scope.$watch("_images", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                $scope.images.push({
                    url: newValue,
                    title: null,
                    caption: null,
                    alt: null,
                    description: null
                });

                $('#validation-image').val('');
            }
        });

        $scope.$watch("videos", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.videos = newValue;
            }
        });

        $scope.$watch("_videos", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                $scope.videos.push({
                    url: newValue,
                    title: null,
                    description: null
                });

                $('#validation-video').val('');
            }
        });

        $scope.$watch("wayPoints", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.product_way_point = newValue;
            }
        });

        $scope.$watch("includes", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.product_include = newValue;
            }
        });

        $scope.$watch("itineraries", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                $scope.data.product_itinerary = newValue;
            }
        });

        $scope.$watchCollection("age.all", function (newValue) {
            if (newValue) {
                $scope.data.min_age = 1;
                $scope.data.max_age = 70;
            }
        });

        $scope.durationConvert = function (hours) {
            var duration_convert = '';
            var units = {
                "year": 24 * 365,
                "month": 24 * 30,
                "week": 24 * 7,
                "day": 24,
                "hour": 1
            }

            for (var key in units) {
                if (units.hasOwnProperty(key)) {
                    switch (key) {
                        case "year":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Years</strong></i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Year</strong></i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        case "month":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Months</strong></i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Month</strong></i>&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        case "week":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Weeks</strong></i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Week</strong></i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        case "day":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Days</strong></i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i><strong>Day</strong></i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        default:
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.round(hours / units[key]) + '&nbsp;<i><strong>Hours</strong></i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.round(hours / units[key]) + '&nbsp;<i><strong>Hour</strong></i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;
                    }
                }
            }

            $scope.data.duration_convert = duration_convert;
        };

        $scope.getPositionLocation = function (event) {
            $scope.data.lat = event.latLng.lat();
            $scope.data.lng = event.latLng.lng();
        };

        $scope.detailImage = function (index) {
            $scope.indexImage = index;
        };

        $scope.detailVideo = function (index) {
            $scope.indexVideo = index;
        };

        $scope.openWayPoint = function (index) {
            $scope.indexWayPoint = index;

            var wayPointInstance = $uibModal.open({
                templateUrl: 'wayPoint.html',
                controller: 'ModalWayPointController',
                size: 'lg',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });

            wayPointInstance.result.then(function (selectedItem) {
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openProductTag = function () {
            var blogTagInstance = $uibModal.open({
                templateUrl: 'productTag.html',
                controller: 'ModalProductTagController',
                size: 'lg',
                backdrop: 'static',
                keyboard: false
            });

            blogTagInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.addWayPoint = function () {
            var order = $scope.wayPoints.length + 1;

            $scope.wayPoints.push({
                title: null,
                address: null,
                lat: $scope.data.lat,
                lng: $scope.data.lng,
                order: order
            });
        };

        $scope.addInclude = function () {
            var order = $scope.includes.length + 1;

            $scope.includes.push({
                title: null,
                sub_title: null,
                description: null,
                order: order
            });
        };

        $scope.addItinerary = function () {
            var order = $scope.itineraries.length + 1;

            $scope.itineraries.push({
                title: null,
                sub_title: null,
                description: null,
                order: order
            });
        };

        $scope.removeImage = function (index) {
            $scope.images.splice(index, 1);
        };

        $scope.removeVideo = function (index) {
            $scope.videos.splice(index, 1);
        };

        $scope.removeWayPoint = function (index) {
            $scope.wayPoints.splice(index, 1);
        };

        $scope.removeInclude = function (index) {
            $scope.includes.splice(index, 1);
        };

        $scope.removeItinerary = function (index) {
            $scope.itineraries.splice(index, 1);
        };

        $scope.edit = function (id) {
            $location.path('/product/edit/' + id);
        };

        $scope.delete = function (id) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                Product.delete(id).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.add = function () {
            $location.url('/product/add');
        };

        $scope.cancel = function () {
            $location.path('/product');
        };

        $scope.create = function () {
            $scope.loading = true;

            //If product type is hotel it will set duration equal 0
            if ($scope.data.product_type_id == 2) {
                $scope.data.duration = 0;
                $scope.data.max_age = 0;
                $scope.data.min_age = 0;
            }

            Product.create($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/product');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.update = function () {
            $scope.loading = true;

            var productTag = [];

            angular.forEach($scope.data.product_tag, function (value) {
                if (value.id) {
                    productTag.push(value.id);
                } else {
                    productTag.push(value);
                }
            });

            $scope.data.product_tag = productTag;

            var productFacility = [];

            angular.forEach($scope.data.product_facility, function (value) {
                if (value.id) {
                    productFacility.push(value.id);
                } else {
                    productFacility.push(value);
                }
            });

            $scope.data.product_facility = productFacility;

            //If product type is hotel it will set duration equal 0
            if ($scope.data.product_type_id == 2) {
                $scope.data.duration = 0;
                $scope.data.max_age = 0;
                $scope.data.min_age = 0;
            }

            Product.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/product');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.placeMarker = function() {
            var loc = this.getPlace().geometry.location;
            $scope.data.lat = loc.lat();
            $scope.data.lng = loc.lng();
        };

        $scope.insertFeaturedVideoExternal = function (url) {
            $('#validation-featured_video').val(url);

            $('#validation-featured_video_prefix').val(null).trigger("change");
            $('#validation-featured_video_prefix').val('');
            $('#validation-featured_video_code').val('');

            $timeout(function () {
                $scope.featuredVideoExternal = {
                    prefix: null,
                    code: null
                };
            }, 1000);

            $('.modal').modal('hide');
        };

        $scope.insertVideoGalleryExternal = function (url) {
            $scope.videos.push({
                url: url,
                title: null,
                description: null
            });

            $('#validation-video_gallery_prefix').val(null).trigger("change");
            $('#validation-video_gallery_prefix').val('');
            $('#validation-video_gallery_code').val('');

            $timeout(function () {
                $scope.videoGalleryExternal = {
                    prefix: null,
                    code: null
                };
            }, 1000);

            $('.modal').modal('hide');
        };

        $scope.isVideoGalleryExternal = function (url) {
            if (url.indexOf('youtube') > 0 || url.indexOf('vimeo') > 0) {
                return true;
            }

            return false;
        };

        $scope.setImage = function () {
            $('#set-image').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

        };

        $scope.setVideo = function () {
            $('#set-video').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

        };

        $scope.setFeaturedImage = function () {
            $('#set-featured_image').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

            $('#image-featured_image').hide();
            $('#remove-featured_image').hide();
            $('#set-featured_image').show();

        };

        $scope.setFeaturedVideo = function () {
            $('#set-featured_video').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

            $('#control-featured_video').hide();
            $('#control-featured_video_external').hide();
            $('#remove-featured_video').hide();
            $('#set-featured_video').show();
            $('#set-featured_video_external').show();
        };

        $scope.setIncludeBrochure = function () {
            $('#set-include_brochure').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

            $('#label-include_brochure').hide();
            $('#remove-include_brochure').hide();
            $('#set-include_brochure').show();
        };

        $scope.setItineraryBrochure = function () {
            $('#set-itinerary_brochure').fancyboxPlus({
                'width': 1024,
                'height': 768,
                'type': 'iframe',
                'autoScale': false
            });

            $('#label-itinerary_brochure').hide();
            $('#remove-itinerary_brochure').hide();
            $('#set-itinerary_brochure').show();
        };

        $scope.removeFeaturedImage = function () {
            $('#image-featured_image').removeAttr('src');
            $('#validation-featured_image').val('');

            $('#image-featured_image').hide();
            $('#remove-featured_image').hide();
            $('#set-featured_image').show();

            delete $scope.data.featured_image_url;
        };

        $scope.removeFeaturedVideo = function () {
            $('#control-featured_video').removeAttr('src');
            $('#validation-featured_video').val('');

            $('#control-featured_video').hide();
            $('#control-featured_video_external').hide();
            $('#remove-featured_video').hide();
            $('#set-featured_video').show();
            $('#set-featured_video_external').show();

            delete $scope.data.featured_video_url;
        };

        $scope.removeIncludeBrochure = function () {
            $('#validation-include_brochure').val('');

            $('#label-include_brochure').hide();
            $('#remove-include_brochure').hide();
            $('#set-include_brochure').show();

            delete $scope.data.include_brochure_url;
        };

        $scope.removeItineraryBrochure = function () {
            $('#validation-itinerary_brochure').val('');

            $('#label-itinerary_brochure').hide();
            $('#remove-itinerary_brochure').hide();
            $('#set-itinerary_brochure').show();

            delete $scope.data.itinerary_brochure_url;
        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('title').withTitle('Title').withOption('name', 'title'),
            DTColumnBuilder.newColumn('agent.company').withTitle('Agent').withOption('name', 'agents.company'),
            DTColumnBuilder.newColumn('product_type.type').withTitle('Product Type').withOption('name', 'product_types.type'),
            DTColumnBuilder.newColumn(null).withTitle('Product Category').notSortable().renderWith(function (data) {
                var product_categories = '';

                if (data.product_category != null) {
                    product_categories = data.product_category.category;
                }

                return product_categories
            }),
            DTColumnBuilder.newColumn('publish').withTitle('Publish').withOption('name', 'publish'),
            DTColumnBuilder.newColumn(null).withTitle('Status').notSortable().renderWith(function (data) {
                var status = '';

                switch (data.status) {
                    case 0:
                        status = '<strong>Draft</strong>';
                        break;

                    case 1:
                        status = '<strong>Published</strong>';
                        break;

                    case 2:
                        status = '<strong>Pending</strong>';
                        break;
                }

                return status;

            }),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(' + data.id + ')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'product/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('fnServerParams', function (aoData) {
                aoData.agent_id = ($scope.agentId != 0) ? $scope.agentId : null;
                aoData.product_type_id = ($scope.productTypeId != 0) ? $scope.productTypeId : null;
                aoData.product_category_id = ($scope.productCategoryId != 0) ? $scope.productCategoryId : null;
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTable = function () {
            $scope.agentId = $scope.data.agent_id;
            $scope.productCategoryId = $scope.data.product_category_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTableAndFilterProductCategory = function () {
            delete $scope.data.product_category_id;

            $scope.productCategoryId = 0;
            $scope.productTypeId = $scope.data.product_type_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshProductTag = function (name) {
            if (!angular.isUndefined($scope.data.agent_id)) {
                ProductTag.getProductTagByName($scope.data.agent_id, name)
                    .success(function (response) {
                        $scope.productTags = response.dto;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        };

        $scope.refreshProductFacility = function (name) {
            Facility.getFacilityByName(name)
                .success(function (response) {
                    $scope.productFacilities = response.dto;

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.initializeItineraryBrochure = function () {
            $('#validation-itinerary_brochure').observe_field(1, function () {
                $('#validation-itinerary_brochure').trigger('input');

                if (this.value != '') {
                    $('#label-itinerary_brochure').show();
                    $('#remove-itinerary_brochure').show();
                    $('#set-itinerary_brochure').hide();
                }
            });
        };

        $scope.initializePropertyType = function () {
            $('#validation-property_type').select2(propertyTypeOption);
        };

        $scope.initializeProductCategory = function () {
            $('#validation-product_category').select2(productCategoryOption);
        };

        $scope.initializeAgent = function () {
            $('#validation-agent_id').select2(agentOption);
        };

        $scope.initializePrefix = function () {
            $('#validation-featured_video_prefix').select2(prefixOption);
            $('#validation-video_gallery_prefix').select2(prefixOption);
        };

        $scope.initializeFilterAgent = function () {
            $('#filter-agent_id').select2(filterAgentOption);
        };

        $scope.initializeFilterProductType = function () {
            $('#filter-product_type_id').select2(filterProductTypeOption);
        };

        $scope.initializeFilterProductCategory = function () {
            $('#filter-product_category_id').select2(filterProductCategoryOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'add')) {
            $scope.data.product_type_id = 1;

            if ($scope.session.role != 'super' || $scope.session.role != 'admin') {
                Agent.getAgentByUserId($scope.session.userId)
                    .success(function (response) {
                        $scope.agent = response.dto;
                        $scope.data.agent_id = (!Array.isArray($scope.data.agent_id)) ? $scope.agent.id : 0;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }

            $scope.getSlug = function (title) {
                if (!angular.isUndefined($scope.data.agent_id)) {
                    $scope.loadingSlug = true;

                    Product.getSlug($scope.data.agent_id, title)
                        .success(function (response) {
                            console.log(response);

                            $scope.loadingSlug = false;

                            $scope.data.slug = response.dto.slug;
                        })
                        .error(function (xhr, error, thrown) {
                            console.log(xhr);
                            console.log(error);
                            console.log(thrown);

                            if (xhr.status == 401) {
                                $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                            }
                        });
                }
            };

        }

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            Product.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;

                    console.log($scope.data);

                    //Publish
                    $scope.data.publish = moment(response.dto.publish).format('YYYY-MM-DD HH:mm A');

                    //If duration equal with 0 it will set to 1
                    if ($scope.data.duration == 0) {
                        $scope.data.duration = 1;
                        $scope.data.max_age = 17;
                        $scope.data.min_age = 40;
                    }

                    $scope.durationConvert($scope.data.duration);

                    var featuredVideoUrl = String($scope.data.featured_video_url);

                    if (featuredVideoUrl.indexOf('youtube') > 0 || featuredVideoUrl.indexOf('vimeo') > 0) {
                        $scope.video.external = true;
                    }

                    //Product Tag
                    var productTag = [];

                    angular.forEach(response.dto.product_tag, function (value) {
                        productTag.push({
                            id: value.id,
                            name: value.name
                        });
                    });

                    $scope.data.product_tag = productTag;

                    //Product Facility
                    var productFacility = [];

                    angular.forEach(response.dto.product_facility, function (value) {
                        productFacility.push({
                            id: value.id,
                            facility: value.facility
                        });
                    });

                    $scope.data.product_facility = productFacility;

                    //Image & Video
                    $scope.images = response.dto.product_image;
                    $scope.videos = response.dto.product_video;

                    //Product Way Point
                    angular.forEach(response.dto.product_way_point, function (value) {
                        value.order = parseInt(value.order);
                    });

                    $scope.wayPoints = response.dto.product_way_point;

                    //Product Include
                    angular.forEach(response.dto.product_include, function (value) {
                        value.order = parseInt(value.order);
                    });

                    $scope.includes = response.dto.product_include;

                    //Product Itinerary
                    angular.forEach(response.dto.product_itinerary, function (value) {
                        value.order = parseInt(value.order);
                    });

                    $scope.itineraries = response.dto.product_itinerary;

                    if ((response.dto.min_age == 1) && (response.dto.max_age == 70)) {
                        $scope.age.all = true;
                    }

                    $scope.title = response.dto.title;
                    $scope.slug = response.dto.slug;

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });

            $scope.getSlug = function (title) {
                if (!angular.isUndefined($scope.data.agent_id)) {
                    $scope.loadingSlug = true;

                    if ($filter('spaceLess')($scope.data.title) != $filter('spaceLess')($scope.title)) {
                        Product.getSlug($scope.data.agent_id, title)
                            .success(function (response) {
                                console.log(response);

                                $scope.loadingSlug = false;

                                $scope.data.slug = response.dto.slug;
                            })
                            .error(function (xhr, error, thrown) {
                                console.log(xhr);
                                console.log(error);
                                console.log(thrown);

                                if (xhr.status == 401) {
                                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                                }
                            });

                    } else {
                        $scope.data.slug = $scope.slug;

                        $scope.loadingSlug = false;
                    }
                }
            };
        }

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        }
    }
]);

app.controller('ModalWayPointController', ['$scope', '$uibModalInstance', '$timeout',
    function ($scope, $uibModalInstance, $timeout) {
        $scope.renderWayPoint = false;

        $timeout(function () {
            $scope.renderWayPoint = true;
        }, 1000);

        $scope.getPositionWayPoint = function (event) {
            $scope.wayPoints[$scope.indexWayPoint].lat = event.latLng.lat();
            $scope.wayPoints[$scope.indexWayPoint].lng = event.latLng.lng();
        };

        $scope.closeWayPoint = function () {
            $uibModalInstance.close();
        };

        $scope.$watch("autoCompleteDetails", function (newValue) {
            if (!angular.isUndefined(newValue)) {
                var loc = newValue.geometry.location;

                $scope.wayPoints[$scope.indexWayPoint].lat = loc.lat();
                $scope.wayPoints[$scope.indexWayPoint].lng = loc.lng();
            }
        });
    }
]);

app.controller('ModalProductTagController', ['$scope', '$uibModalInstance', '$filter', '$timeout', 'ProductTag', 'Agent', 'Session', 'AUTH_EVENTS', 'BASE_API_URL',
    function ($scope, $uibModalInstance, $filter, $timeout, ProductTag, Agent, Session, AUTH_EVENTS, BASE_API_URL) {
        var agentOption  = {
            placeholder: "- Please Search -",
            minimumInputLength: 2,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        }

        $scope.data = {};

        $scope.session = Session;

        if ($scope.session.role != 'super' || $scope.session.role != 'admin') {
            Agent.getAgentByUserId($scope.session.userId)
                .success(function (response) {
                    $scope.agent = response.dto;
                    $scope.data.agent_id = (!Array.isArray($scope.data.agent_id)) ? $scope.agent.id : 0;

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        $scope.create = function () {
            $scope.loading = true;

            ProductTag.create($scope.data)
                .success(function (response) {
                    $scope.loading = false;

                    if (response._messages) {
                        if (response._messages[0].type == 'error') {
                            swal("Error", response._messages[0].text, "error");
                        }

                        if (response._messages[0].type == 'success') {
                            swal({
                                title: 'Success!',
                                text: response._messages[0].text,
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonText: 'Add again',
                                cancelButtonText: 'No, cancel'
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    if ($scope.session.role == 'super' || $scope.session.role == 'admin') {
                                        $('#validation-tag-agent_id').val(null).trigger("change");
                                        $scope.data.agent_id = null;
                                    }

                                    $('#validation-tag-name').val('');
                                    $('#label-slug').text('');

                                    $scope.data.name = null;
                                    $scope.data.slug = null;
                                } else {
                                    $scope.closeProductTag();
                                }
                            });
                        }
                    }
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.getSlug = function (name) {
            if (!angular.isUndefined($scope.data.agent_id)) {
                $scope.loadingSlug = true;

                ProductTag.getSlug($scope.data.agent_id, name)
                    .success(function (response) {
                        console.log(response);

                        $scope.loadingSlug = false;

                        $scope.data.slug = response.dto.slug;
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        };

        $scope.closeProductTag = function () {
            $uibModalInstance.close();
        };

        $scope.initializeAgent = function () {
            $('#validation-tag-agent_id').select2(agentOption);
        };
    }
]);

