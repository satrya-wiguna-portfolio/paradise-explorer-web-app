app.controller('ErrorController', ['$scope', '$rootScope', '$location',
    function($scope, $rootScope, $location) {
        $rootScope.hideLeftMenu = true;
        $rootScope.hideTopMenu = true;

        setInterval(function() {
            $scope.$apply()
        }, 500);

        $scope.backToHome = function () {
            $location.path('/dashboard');
        }
    }
]);