app.controller('RoomDiscountController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'Agent', 'Product', 'Room', 'RoomDiscount', 'Session', 'Notification', 'BASE_API_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, $timeout, DTOptionsBuilder, DTColumnBuilder, Urls, Agent, Product, Room, RoomDiscount, Session, Notification, BASE_API_URL, AUTH_EVENTS, LOADING_EVENTS, OTHER_EVENTS) {
        var filterAgentOption = {
            placeholder: "- Filter by Agent -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        agent_type_id: 2,
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var filterProductOption = {
            placeholder: "- Filter by Product -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'product/getProductByTitle/?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        agent_id: ($scope.agentId != 0) ? $scope.agentId : -1,
                        title: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var filterRoomOption = {
            placeholder: "- Filter by Room -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'room/getRoomByTitle/?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        agent_id: ($scope.agentId != 0) ? $scope.agentId : -1,
                        product_id: ($scope.productId != 0) ? $scope.productId : -1,
                        title: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var agentOption = {
            placeholder: "- Please Search -",
            minimumInputLength: 2,
            cache: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        agent_type_id: 2,
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var productOption = {
            placeholder: "- Please Choose -",
            minimumInputLength: 2,
            cache: true,
            ajax: {
                url: BASE_API_URL + 'product/getProductByTitle/?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        agent_id: (!angular.isUndefined($scope.data.agent_id)) ? $scope.data.agent_id : -1,
                        title: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        var roomOption = {
            placeholder: "- Please Choose -",
            minimumInputLength: 2,
            cache: true,
            ajax: {
                url: BASE_API_URL + 'room/getRoomByTitle/?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        agent_id: (!angular.isUndefined($scope.data.agent_id)) ? $scope.data.agent_id : -1,
                        product_id: (!angular.isUndefined($scope.data.product_id)) ? $scope.data.product_id : -1,
                        title: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        $scope.dtInstance = {};
        $scope.data = {
            datePicker: {
                startDate: null,
                endDate: null
            },
            datePickerOption: {},
            status: 1
        };

        $scope.session = Session;

        $scope.agentId = 0;
        $scope.productId = 0;
        $scope.roomId = 0;
        $scope.datePicker = {
            startDate: null,
            endDate: null
        };
        $scope.datePickerOption = {
            opens: 'left',
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()]
            },
            eventHandlers: {
                'apply.daterangepicker': function (event, picker) {
                    $scope.dtInstance._renderer.rerender();
                },
                'cancel.daterangepicker': function (event, picker) {
                    $scope.datePicker = {
                        startDate: null,
                        endDate: null
                    };

                    $scope.dtInstance._renderer.rerender();
                }
            }
        };

        setInterval(function() {
            $scope.$apply()
        }, 500);

        Agent.getAgentByUserId($scope.session.userId)
            .success(function (response) {
                $scope.agent = response.dto;
                $scope.agentId = (!Array.isArray($scope.agent)) ? $scope.agent.id : 0;

                if (!angular.isUndefined($scope.dtInstance._renderer)) {
                    $scope.dtInstance._renderer.rerender();
                }

            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $scope.$watch("data.agent_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                Agent.getDetail(newValue)
                    .success(function (response) {
                        agentOption.initSelection = function (element, callback) {
                            var data = {
                                id: response.dto.id,
                                text: response.dto.company
                            };

                            callback(data);
                        };

                        $('#validation-agent_id').select2(agentOption);
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        });

        $scope.$watch("data.product_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                Product.getDetail(newValue)
                    .success(function (response) {
                        productOption.initSelection = function (element, callback) {
                            var data = {
                                id: response.dto.id,
                                text: response.dto.title
                            };

                            callback(data);
                        };

                        $('#validation-product_id').select2(productOption);
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        });

        $scope.$watch("data.room_id", function (newValue) {
            if (!angular.isUndefined(newValue) && newValue) {
                Room.getDetail(newValue)
                    .success(function (response) {
                        roomOption.initSelection = function (element, callback) {
                            var data = {
                                id: response.dto.id,
                                text: response.dto.title
                            };

                            callback(data);
                        };

                        $('#validation-room_id').select2(roomOption);
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        });

        $scope.edit = function (id) {
            $location.path('/roomDiscount/edit/' + id);
        };

        $scope.delete = function (id) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                RoomDiscount.delete(id).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.add = function () {
            $location.url('/roomDiscount/add');
        };

        $scope.cancel = function () {
            $location.path('/roomDiscount');
        };

        $scope.refresh = function () {
            $('#calendar').fullCalendar('refetchEvents');
        };

        $scope.create = function () {
            $scope.loading = true;

            RoomDiscount.create($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/roomDiscount');

                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.update = function () {
            console.log($scope.data);

            $scope.loading = true;

            RoomDiscount.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/roomDiscount');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('room_id').withTitle('Room ID').withOption('name', 'room_id').notVisible(),
            DTColumnBuilder.newColumn('open_date').withTitle('Open Date').withOption('name', 'open_date'),
            DTColumnBuilder.newColumn('close_date').withTitle('Close Date').withOption('name', 'close_date'),
            DTColumnBuilder.newColumn('room').withTitle('Room').withOption('name', 'room'),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(' + data.id + ')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'roomDiscount/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('searching', false)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('fnServerParams', function (aoData) {
                aoData.agent_id = ($scope.agentId != 0) ? $scope.agentId : null;
                aoData.product_id = ($scope.productId != 0) ? $scope.productId : null;
                aoData.room_id = ($scope.roomId != 0) ? $scope.roomId : null;
                aoData.open_date = ($scope.datePicker.startDate != null) ? $scope.datePicker.startDate.format('YYYY-MM-DD') : null;
                aoData.close_date = ($scope.datePicker.endDate != null) ? $scope.datePicker.endDate.format('YYYY-MM-DD') : null;
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('fnDrawCallback', function (oSettings) {
                var api = this.api();
                var rows = api.rows({
                    page:'current'
                }).nodes();
                var last = null;

                api.column(0, {
                    page:'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group" style="background-color: #2a68bd; color: #ffffff;"><td colspan="4">' +
                            '<a href="#/roomDiscount/calendar/' + group + '" style="color: #ffffff;"><strong>' + oSettings.aoData[i]._aData.room + '</strong></a></td></tr>'
                        );
                        last = group;
                    }
                });
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshProductAndRoom = function () {
            delete $scope.data.product_id;
            delete $scope.data.room_id;
        };

        $scope.refreshRoom = function () {
            delete $scope.data.room_id;
        };

        $scope.refreshTableAndFilterProductAndFilterRoom = function () {
            delete $scope.data.product_id;
            delete $scope.data.room_id;

            $scope.agentId = $scope.data.agent_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTableAndFilterRoom = function () {
            delete $scope.data.room_id;

            $scope.productId = $scope.data.product_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTable = function () {
            $scope.roomId = $scope.data.room_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.initializeFilterAgent = function () {
            $('#filter-agent_id').select2(filterAgentOption).on("select2:unselect", function (e) {
                $("#filter-product_id").val(null).trigger("change");
                $("#filter-room_id").val(null).trigger("change");
            });
        };

        $scope.initializeFilterProduct = function () {
            $('#filter-product_id').select2(filterProductOption).on("select2:unselect", function (e) {
                $("#filter-room_id").val(null).trigger("change");
            });
        };

        $scope.initializeFilterRoom = function () {
            $('#filter-room_id').select2(filterRoomOption);
        };

        $scope.initializeAgent = function () {
            $('#validation-agent_id').select2(agentOption).on("select2:select", function (e) {
                $("#validation-product_id").val(null).trigger("change");
                $("#validation-room_id").val(null).trigger("change");
            });
        };

        $scope.initializeProduct = function () {
            $('#validation-product_id').select2(productOption).on("select2:select", function (e) {
                $("#validation-room_id").val(null).trigger("change");
            });
        };

        $scope.initializeRoom = function () {
            $('#validation-room_id').select2(roomOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'add')) {
            $scope.data.datePickerOption.minDate = moment(new Date()).format('YYYY/MM/DD');

            if ($scope.session.role != 'super' || $scope.session.role != 'admin') {
                Agent.getAgentByUserId($scope.session.userId)
                    .success(function (response) {
                        $scope.agent = response.dto;
                        $scope.data.agent_id = (!Array.isArray($scope.data.agent_id)) ? $scope.agent.id : 0;

                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            }
        }

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            RoomDiscount.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;

                    var open = moment(response.dto.open_date).format('YYYY-MM-DD');

                    if (moment(new Date()).isAfter(open)) {
                        $scope.data.datePickerOption = {
                            minDate: moment(response.dto.open_date).format('YYYY/MM/DD')
                        };
                    } else {
                        $scope.data.datePickerOption = {
                            minDate: moment(new Date()).format('YYYY/MM/DD')
                        };
                    }

                    $scope.data.datePicker = {
                        startDate: open,
                        endDate: moment(response.dto.close_date).format('YYYY-MM-DD')
                    };

                    console.log($scope.data);
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        }

        if (Urls.getActionUrl($location.absUrl(), 'calendar')) {
            Room.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;

                    console.log($scope.data);
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev, next',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay'
                },
                buttonIcons: {
                    prev: 'none fa fa-arrow-left',
                    next: 'none fa fa-arrow-right',
                    prevYear: 'none fa fa-arrow-left',
                    nextYear: 'none fa fa-arrow-right'
                },
                defaultDate: moment(new Date()),
                editable: false,
                selectable: true,
                eventLimit: true,
                eventSources: [{
                    url: BASE_API_URL + 'roomDiscount/getByRoomId/' + $routeParams.id + '/?token=' + Session.token,
                    type: 'GET',
                    error: function () {
                        alert('there was an error while fetching events!');
                    },
                    className: 'fc-event-success'
                }],
                eventClick: function(calEvent, jsEvent, view) {
                    if (!$(this).hasClass('event-clicked')) {
                        $('.fc-event').removeClass('event-clicked');
                        $(this).addClass('event-clicked');
                    }
                }
            });
        }

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        }
    }
]);