app.controller('ProductCommentController', ['$scope', '$rootScope', '$location', '$http', '$compile', '$routeParams', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', 'Urls', 'ProductComment','Agent',  'Session', 'Notification', 'BASE_API_URL', 'AUTH_EVENTS', 'LOADING_EVENTS', 'OTHER_EVENTS',
    function ($scope, $rootScope, $location, $http, $compile, $routeParams, $filter, DTOptionsBuilder, DTColumnBuilder, Urls, ProductComment, Agent, Session, Notification, BASE_API_URL, AUTH_EVENTS, LOADING_EVENTS, OTHER_EVENTS) {
        var filterAgentOption = {
            placeholder: "- Filter by Agent -",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: BASE_API_URL + 'agent/getAgentByTypeAndCompany?token=' + Session.token,
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function (params) {
                    return {
                        company: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.dto, function (item) {
                            return {
                                text: item.company,
                                id: item.id
                            }
                        })
                    }
                }
            }
        };

        $scope.dtInstance = {};
        $scope.data = {};

        $scope.session = Session;
        $scope.agentId = 0;

        setInterval(function() {
            $scope.$apply()
        }, 500);

        Agent.getAgentByUserId($scope.session.userId)
            .success(function (response) {
                $scope.agent = response.dto;
                $scope.agentId = (!Array.isArray($scope.agent)) ? $scope.agent.id : 0;

                if (!angular.isUndefined($scope.dtInstance._renderer)) {
                    $scope.dtInstance._renderer.rerender();
                }

            })
            .error(function (xhr, error, thrown) {
                console.log(xhr);
                console.log(error);
                console.log(thrown);

                if (xhr.status == 401) {
                    $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                }
            });

        $scope.edit = function (id) {
            $location.path('/productComment/edit/' + id);
        };

        $scope.delete = function (id) {
            swal({
                title: "Are you sure?",
                text: "This item will be remove",
                showCancelButton: true,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes, delete!",
                closeOnConfirm: true
            }, function () {
                ProductComment.delete(id).success(function (response) {
                        console.log(response);

                        $rootScope.$broadcast(OTHER_EVENTS.messages, {
                            type: response._messages[0].type,
                            text: response._messages[0].text
                        });

                        $scope.reloadData();
                    })
                    .error(function (xhr, error, thrown) {
                        console.log(xhr);
                        console.log(error);
                        console.log(thrown);

                        if (xhr.status == 401) {
                            $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        }
                    });
            });
        };

        $scope.cancel = function () {
            $location.path('/productComment');
        };

        $scope.update = function () {
            $scope.loading = true;

            ProductComment.update($scope.data)
                .success(function (response) {
                    console.log(response);

                    $scope.loading = false;

                    $rootScope.$broadcast(OTHER_EVENTS.messages, {
                        type: response._messages[0].type,
                        text: response._messages[0].text
                    });

                    $location.path('/productComment');
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('title').withTitle('Product Title').withOption('name', 'title'),
            DTColumnBuilder.newColumn('comment').withTitle('Comment').withOption('name', 'comment'),
            DTColumnBuilder.newColumn(null).withTitle('Status').notSortable().renderWith(function (data) {
                var status = '';

                switch (data.status) {
                    case 0:
                        status = '<strong>Pendding</strong>';
                        break;

                    case 1:
                        status = '<strong>Approved</strong>';
                        break;
                }

                return status;

            }),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().renderWith(function (data) {
                return '<button class="btn btn-warning btn-sm" ng-click="edit(' + data.id + ')">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '&nbsp;' +
                    '<button class="btn btn-danger btn-sm" ng-click="delete(' + data.id + ')">' +
                    '<i class="fa fa-trash-o"></i>' +
                    '</button>';
            })
        ];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                method: 'POST',
                url: BASE_API_URL + 'productComment/getAll?token=' + Session.token,
                error: function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                }
            })
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('initComplete', function (settings) {
                $compile(angular.element('#' + settings.sTableId).contents())($scope);
                $('.dataTables_filter input').addClass('table_search');
                $('.dataTables_length select').addClass('table_length');
            })
            .withOption('fnServerParams', function (aoData) {
                aoData.agent_id = ($scope.agentId != 0) ? $scope.agentId : null;
            })
            .withOption('createdRow', function (row, data, dataIndex) {
                console.log(row);
                console.log(data);
                console.log(dataIndex);

                $compile(angular.element(row).contents())($scope);
            })
            .withOption('aaSorting', [0, 'desc'])
            .withDataProp('dto')
            .withPaginationType('full_numbers');

        $scope.reloadData = function () {
            $scope.dtInstance._renderer.rerender();
        };

        $scope.refreshTable = function () {
            $scope.agentId = $scope.data.agent_id;
            $scope.dtInstance._renderer.rerender();
        };

        $scope.initializeFilterAgent = function () {
            $('#filterSelectAgent').select2(filterAgentOption);
        };

        if (Urls.getActionUrl($location.absUrl(), 'edit')) {
            ProductComment.getDetail($routeParams.id)
                .success(function (response) {
                    $scope.data = response.dto;
                })
                .error(function (xhr, error, thrown) {
                    console.log(xhr);
                    console.log(error);
                    console.log(thrown);

                    if (xhr.status == 401) {
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                    }
                });
        };

        if ($rootScope.messages) {
            Notification.getMessage($rootScope.messages);

            delete $rootScope.messages;
        };
    }
]);
