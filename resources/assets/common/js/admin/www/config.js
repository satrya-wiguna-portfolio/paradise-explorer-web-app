app.config(function ($locationProvider, $routeProvider, $httpProvider, ngIntlTelInputProvider, BASE_URL, USER_ROLES) {
    ngIntlTelInputProvider.set({
        initialCountry: 'id'
    });

    $httpProvider.interceptors.push([
        '$injector',
        function ($injector) {
            return $injector.get('InterceptorAuthentication');
        }
    ]);

    $routeProvider.when('/', {
        redirectTo: '/dashboard',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.otherwise({
        redirectTo: '/404',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/dashboard', {
        templateUrl: 'views/admin/dashboard/index.html',
        controller: 'DashboardController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/login', {
        templateUrl: 'views/admin/auth/login.html',
        controller: 'LoginController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/logoff', {
        templateUrl: 'views/admin/auth/logoff.html',
        controller: 'LogoffController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/401', {
        templateUrl: 'views/admin/errors/401.html',
        controller: 'ErrorController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/403', {
        templateUrl: 'views/admin/errors/403.html',
        controller: 'ErrorController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/404', {
        templateUrl: 'views/admin/errors/404.html',
        controller: 'ErrorController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/440', {
        templateUrl: 'views/admin/errors/440.html',
        controller: 'ErrorController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/500', {
        templateUrl: 'views/admin/errors/500.html',
        controller: 'ErrorController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when('/503', {
        templateUrl: 'views/admin/errors/503.html',
        controller: 'ErrorController',
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    // COUNTRY ROUTE
    $routeProvider.when('/country', {
        templateUrl: 'views/admin/country/index.html',
        controller: 'CountryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/country/add', {
        templateUrl: 'views/admin/country/create.html',
        controller: 'CountryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/country/edit/:id', {
        templateUrl: 'views/admin/country/edit.html',
        controller: 'CountryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // STATE ROUTE
    $routeProvider.when('/state', {
        templateUrl: 'views/admin/state/index.html',
        controller: 'StateController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/state/add', {
        templateUrl: 'views/admin/state/create.html',
        controller: 'StateController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/state/edit/:id', {
        templateUrl: 'views/admin/state/edit.html',
        controller: 'StateController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PROVINCE ROUTE
    $routeProvider.when('/province', {
        templateUrl: 'views/admin/province/index.html',
        controller: 'ProvinceController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/province/add', {
        templateUrl: 'views/admin/province/create.html',
        controller: 'ProvinceController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/province/edit/:id', {
        templateUrl: 'views/admin/province/edit.html',
        controller: 'ProvinceController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // REGENCY ROUTE
    $routeProvider.when('/regency', {
        templateUrl: 'views/admin/regency/index.html',
        controller: 'RegencyController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/regency/add', {
        templateUrl: 'views/admin/regency/create.html',
        controller: 'RegencyController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/regency/edit/:id', {
        templateUrl: 'views/admin/regency/edit.html',
        controller: 'RegencyController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // DISTRICT ROUTE
    $routeProvider.when('/district', {
        templateUrl: 'views/admin/district/index.html',
        controller: 'DistrictController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/district/add', {
        templateUrl: 'views/admin/district/create.html',
        controller: 'DistrictController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/district/edit/:id', {
        templateUrl: 'views/admin/district/edit.html',
        controller: 'DistrictController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // AGENT TYPE ROUTE
    $routeProvider.when('/agentType', {
        templateUrl: 'views/admin/agent_type/index.html',
        controller: 'AgentTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/agentType/add', {
        templateUrl: 'views/admin/agent_type/create.html',
        controller: 'AgentTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/agentType/edit/:id', {
        templateUrl: 'views/admin/agent_type/edit.html',
        controller: 'AgentTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // ROLE ROUTE
    $routeProvider.when('/role', {
        templateUrl: 'views/admin/role/index.html',
        controller: 'RoleController',
        data: {
            authorizedRoles: [USER_ROLES.super]
        }
    });

    $routeProvider.when('/role/add', {
        templateUrl: 'views/admin/role/create.html',
        controller: 'RoleController',
        data: {
            authorizedRoles: [USER_ROLES.super]
        }
    });

    $routeProvider.when('/role/edit/:id', {
        templateUrl: 'views/admin/role/edit.html',
        controller: 'RoleController',
        data: {
            authorizedRoles: [USER_ROLES.super]
        }
    });

    // PROPERTY TYPE ROUTE
    $routeProvider.when('/propertyType', {
        templateUrl: 'views/admin/property_type/index.html',
        controller: 'PropertyTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super]
        }
    });

    $routeProvider.when('/propertyType/add', {
        templateUrl: 'views/admin/property_type/create.html',
        controller: 'PropertyTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super]
        }
    });

    $routeProvider.when('/propertyType/edit/:id', {
        templateUrl: 'views/admin/property_type/edit.html',
        controller: 'PropertyTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super]
        }
    });

    // FACILITY ROUTE
    $routeProvider.when('/facility', {
        templateUrl: 'views/admin/facility/index.html',
        controller: 'FacilityController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/facility/add', {
        templateUrl: 'views/admin/facility/create.html',
        controller: 'FacilityController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/facility/edit/:id', {
        templateUrl: 'views/admin/facility/edit.html',
        controller: 'FacilityController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PAYMENT ROUTE
    $routeProvider.when('/payment', {
        templateUrl: 'views/admin/payment/index.html',
        controller: 'PaymentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/payment/add', {
        templateUrl: 'views/admin/payment/create.html',
        controller: 'PaymentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/payment/edit/:id', {
        templateUrl: 'views/admin/payment/edit.html',
        controller: 'PaymentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // MEMBER ROUTE
    $routeProvider.when('/member', {
        templateUrl: 'views/admin/member/index.html',
        controller: 'MemberController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/member/add', {
        templateUrl: 'views/admin/member/create.html',
        controller: 'MemberController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/member/edit/:id', {
        templateUrl: 'views/admin/member/edit.html',
        controller: 'MemberController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // AGENT ROUTE
    $routeProvider.when('/agent', {
        templateUrl: 'views/admin/agent/index.html',
        controller: 'AgentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/agent/add', {
        templateUrl: 'views/admin/agent/create.html',
        controller: 'AgentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/agent/edit/:id', {
        templateUrl: 'views/admin/agent/edit.html',
        controller: 'AgentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // STAFF ROUTE
    $routeProvider.when('/staff', {
        templateUrl: 'views/admin/staff/index.html',
        controller: 'StaffController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/staff/add', {
        templateUrl: 'views/admin/staff/create.html',
        controller: 'StaffController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/staff/edit/:id', {
        templateUrl: 'views/admin/staff/edit.html',
        controller: 'StaffController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // USER ROUTE
    $routeProvider.when('/user/edit/:id', {
        templateUrl: 'views/admin/user/edit.html',
        controller: 'UserController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/user/profile', {
        templateUrl: 'views/admin/user/profile.html',
        controller: 'UserController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // BLOG CATEGORY ROUTE
    $routeProvider.when('/blogCategory', {
        templateUrl: 'views/admin/blog_category/index.html',
        controller: 'BlogCategoryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blogCategory/add', {
        templateUrl: 'views/admin/blog_category/create.html',
        controller: 'BlogCategoryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blogCategory/edit/:id', {
        templateUrl: 'views/admin/blog_category/edit.html',
        controller: 'BlogCategoryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // BLOG TAG ROUTE
    $routeProvider.when('/blogTag', {
        templateUrl: 'views/admin/blog_tag/index.html',
        controller: 'BlogTagController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blogTag/add', {
        templateUrl: 'views/admin/blog_tag/create.html',
        controller: 'BlogTagController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blogTag/edit/:id', {
        templateUrl: 'views/admin/blog_tag/edit.html',
        controller: 'BlogTagController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // BLOG COMMENT ROUTE
    $routeProvider.when('/blogComment', {
        templateUrl: 'views/admin/blog_comment/index.html',
        controller: 'BlogCommentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blogComment/edit/:id', {
        templateUrl: 'views/admin/blog_comment/edit.html',
        controller: 'BlogCommentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PRODUCT COMMENT ROUTE
    $routeProvider.when('/productComment', {
        templateUrl: 'views/admin/product_comment/index.html',
        controller: 'ProductCommentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/productComment/edit/:id', {
        templateUrl: 'views/admin/product_comment/edit.html',
        controller: 'ProductCommentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PRODUCT TAG ROUTE
    $routeProvider.when('/productTag', {
        templateUrl: 'views/admin/product_tag/index.html',
        controller: 'ProductTagController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productTag/add', {
        templateUrl: 'views/admin/product_tag/create.html',
        controller: 'ProductTagController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productTag/edit/:id', {
        templateUrl: 'views/admin/product_tag/edit.html',
        controller: 'ProductTagController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // BLOG ROUTE
    $routeProvider.when('/blog', {
        templateUrl: 'views/admin/blog/index.html',
        controller: 'BlogController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blog/add', {
        templateUrl: 'views/admin/blog/create.html',
        controller: 'BlogController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/blog/edit/:id', {
        templateUrl: 'views/admin/blog/edit.html',
        controller: 'BlogController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PRODUCT TYPE ROUTE
    $routeProvider.when('/productType', {
        templateUrl: 'views/admin/product_type/index.html',
        controller: 'ProductTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/productType/add', {
        templateUrl: 'views/admin/product_type/create.html',
        controller: 'ProductTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/productType/edit/:id', {
        templateUrl: 'views/admin/product_type/edit.html',
        controller: 'ProductTypeController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PRODUCT CATEGORY ROUTE
    $routeProvider.when('/productCategory', {
        templateUrl: 'views/admin/product_category/index.html',
        controller: 'ProductCategoryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/productCategory/add', {
        templateUrl: 'views/admin/product_category/create.html',
        controller: 'ProductCategoryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/productCategory/edit/:id', {
        templateUrl: 'views/admin/product_category/edit.html',
        controller: 'ProductCategoryController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // PRODUCT ROUTE
    $routeProvider.when('/product', {
        templateUrl: 'views/admin/product/index.html',
        controller: 'ProductController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/product/add', {
        templateUrl: 'views/admin/product/create.html',
        controller: 'ProductController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/product/edit/:id', {
        templateUrl: 'views/admin/product/edit.html',
        controller: 'ProductController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // PRODUCT DISCOUNT ROUTE
    $routeProvider.when('/productDiscount', {
        templateUrl: 'views/admin/product_discount/index.html',
        controller: 'ProductDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productDiscount/add', {
        templateUrl: 'views/admin/product_discount/create.html',
        controller: 'ProductDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productDiscount/edit/:id', {
        templateUrl: 'views/admin/product_discount/edit.html',
        controller: 'ProductDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productDiscount/calendar/:id', {
        templateUrl: 'views/admin/product_discount/calendar.html',
        controller: 'ProductDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // ROOM DISCOUNT ROUTE
    $routeProvider.when('/roomDiscount', {
        templateUrl: 'views/admin/room_discount/index.html',
        controller: 'RoomDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/roomDiscount/add', {
        templateUrl: 'views/admin/room_discount/create.html',
        controller: 'RoomDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/roomDiscount/edit/:id', {
        templateUrl: 'views/admin/room_discount/edit.html',
        controller: 'RoomDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/roomDiscount/calendar/:id', {
        templateUrl: 'views/admin/room_discount/calendar.html',
        controller: 'RoomDiscountController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // ROOM ALLOTMENT ROUTE
    $routeProvider.when('/roomAllotment', {
        templateUrl: 'views/admin/room_allotment/index.html',
        controller: 'RoomAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/roomAllotment/add', {
        templateUrl: 'views/admin/room_allotment/create.html',
        controller: 'RoomAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/roomAllotment/edit/:id', {
        templateUrl: 'views/admin/room_allotment/edit.html',
        controller: 'RoomAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/roomAllotment/calendar/:id', {
        templateUrl: 'views/admin/room_allotment/calendar.html',
        controller: 'RoomAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // PRODUCT ALLOTMENT ROUTE
    $routeProvider.when('/productAllotment', {
        templateUrl: 'views/admin/product_allotment/index.html',
        controller: 'ProductAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productAllotment/add', {
        templateUrl: 'views/admin/product_allotment/create.html',
        controller: 'ProductAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productAllotment/edit/:id', {
        templateUrl: 'views/admin/product_allotment/edit.html',
        controller: 'ProductAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/productAllotment/calendar/:id', {
        templateUrl: 'views/admin/product_allotment/calendar.html',
        controller: 'ProductAllotmentController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // ROOM ROUTE
    $routeProvider.when('/room', {
        templateUrl: 'views/admin/room/index.html',
        controller: 'RoomController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/room/add', {
        templateUrl: 'views/admin/room/create.html',
        controller: 'RoomController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/room/edit/:id', {
        templateUrl: 'views/admin/room/edit.html',
        controller: 'RoomController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // BED ROUTE
    $routeProvider.when('/bed', {
        templateUrl: 'views/admin/bed/index.html',
        controller: 'BedController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/bed/add', {
        templateUrl: 'views/admin/bed/create.html',
        controller: 'BedController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    $routeProvider.when('/bed/edit/:id', {
        templateUrl: 'views/admin/bed/edit.html',
        controller: 'BedController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin, USER_ROLES.agent]
        }
    });

    // DESTINATION ROUTE
    $routeProvider.when('/destination', {
        templateUrl: 'views/admin/destination/index.html',
        controller: 'DestinationController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/destination/add', {
        templateUrl: 'views/admin/destination/create.html',
        controller: 'DestinationController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/destination/edit/:id', {
        templateUrl: 'views/admin/destination/edit.html',
        controller: 'DestinationController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    // CURRENCY ROUTE
    $routeProvider.when('/currency', {
        templateUrl: 'views/admin/currency/index.html',
        controller: 'CurrencyController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/currency/add', {
        templateUrl: 'views/admin/currency/create.html',
        controller: 'CurrencyController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });

    $routeProvider.when('/currency/edit/:id', {
        templateUrl: 'views/admin/currency/edit.html',
        controller: 'CurrencyController',
        data: {
            authorizedRoles: [USER_ROLES.super, USER_ROLES.admin]
        }
    });
});