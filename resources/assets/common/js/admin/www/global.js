app.run(function ($rootScope, $location, $window, $timeout, Authentication, Session, Urls, AUTH_EVENTS) {
    $rootScope.authentication = Authentication;
    $rootScope.userInfo = Session;

    $rootScope.$on('$routeChangeStart', function (event, next, previous) {
        var authorizedRoles = (!angular.isUndefined(next.data)) ? next.data.authorizedRoles : null;

        if (!Authentication.isAuthorized(authorizedRoles)) {
            if (Authentication.isAuthenticated()) {

                if (Session.role == 'member' || Session.role == 'guest') {
                    Authentication.forceOut();
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                }

                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
            }

            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        }

        Urls.getAllUrl(next, previous);
    });

    $rootScope.logout = function () {
        swal({
            title: "Are you sure?",
            text: "You will not be able to access the system",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, logout!",
            closeOnConfirm: true
        }, function () {
            $timeout(function () {
                Authentication.logOut();
                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            }, 1000);
        });
    };

    if ($window.sessionStorage['userInfo']) {
        Session.create(JSON.parse($window.sessionStorage['userInfo']));
        $location.path('/dashboard');
    }
});