@extends('layouts.default')

@section('content')
    <section id="hero_2">
        <div class="intro_title animated fadeInDown">
            <h1>Place your order</h1>
            <br /><br />
            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum"><strong>Cart</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('cart/') }}" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum"><strong>Checkout</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('checkout/contact') }}" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum"><strong>Payment</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('checkout/payment') }}" class="bs-wizard-dot"></a>
                </div>

            </div>  <!-- End bs-wizard -->
        </div>   <!-- End intro-title -->
    </section><!-- End Section hero_2 -->

    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- End Position -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-12">
                @if(Cart::instance('tour')->content()->count() != 0)
                    <h4 style="margin-left: 5px; margin-bottom: 10px;"><strong>Tour Packages Order</strong></h4>
                    <table class="table table-striped cart-list add_bottom_30">
                        <thead>
                        <tr>
                            <th style="width: 750px;">
                                Item
                            </th>
                            <th style="width: 100px;">
                                Pax
                            </th>
                            <th style="width: 100px;">
                                Discount
                            </th>
                            <th style="width: 200px;">
                                Price
                            </th>
                            <th style="width: 200px;">
                                Sub Total
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Cart::instance('tour')->content() as $tourCartItem)
                        <tr>
                            <td>
                                <div class="thumb_cart">
                                    <img src="{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($tourCartItem->options->featured_image_url, '100x100') }}" alt="Image">
                                </div>
                                <span class="item_cart" style="font-size: 14px;"><a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $tourCartItem->id]) }}"><strong>{{ $tourCartItem->name }}</strong></a></span><br />
                                <i><strong>Departure:</strong> {{ date('M, d Y', strtotime($tourCartItem->options->departure)) }}</i><br />
                                <i><strong>Adult:</strong> {{ $tourCartItem->options->adult }}, <strong>Child:</strong> {{ $tourCartItem->options->child }}</i>
                            </td>
                            <td>
                                {{ $tourCartItem->qty }}
                            </td>
                            <td>
                                {!! html_entity_decode(\App\Helper\PostHelper::discountCartFormat($tourCartItem->options->discount, 0, $tourCartItem->options->product_discount)) !!}
                            </td>
                            <td>
                                @if($tourCartItem->options->discount != 0 || $tourCartItem->options->product_discount != 0)
                                    <span style="color: red;"><strike><small>{{ currency($tourCartItem->options->price, currency()->config('default'), Session::get('currency')) }}</small></strike></span><br />
                                @endif
                                <strong>{{ currency($tourCartItem->price, currency()->config('default'), Session::get('currency'))}}</strong>
                            </td>
                            <td>
                                <strong>{{ currency(($tourCartItem->price * $tourCartItem->qty), currency()->config('default'), Session::get('currency'))}}</strong>
                            </td>
                            <td class="options">
                                <a href="{{ url('cart/delete', ['productTypeId' => 1, 'rowId' => $tourCartItem->rowId]) }}" class="action cartItem"><i class=" icon-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

                @if(Cart::instance('hotel')->content()->count() != 0)
                    <h4 style="margin-left: 5px; margin-bottom: 10px;"><strong>Hotel Packages Order</strong></h4>
                    <table class="table table-striped cart-list add_bottom_30">
                        <thead>
                        <tr>
                            <th style="width: 750px;">
                                Item
                            </th>
                            <th style="width: 100px;">
                                Night
                            </th>
                            <th style="width: 100px;">
                                Discount
                            </th>
                            <th style="width: 200px;">
                                Price
                            </th>
                            <th style="width: 200px;">
                                Sub Total
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Cart::instance('hotel')->content() as $hotelCartItem)
                            <tr>
                                <td>
                                    <div class="thumb_cart">
                                        <img src="{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($hotelCartItem->options->featured_image_url, '100x100') }}" alt="Image">
                                    </div>
                                    <span class="item_cart" style="font-size: 14px;"><a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $hotelCartItem->id]) }}"><strong>{{ $hotelCartItem->name }}</strong></a></span><br />
                                    <i><strong>Check In:</strong> {{ date('M, d Y', strtotime($hotelCartItem->options->check_in)) }} - <strong>Check Out:</strong> {{ date('M, d Y', strtotime($hotelCartItem->options->check_out)) }}</i><br />
                                    <i><strong>Adult:</strong> {{ $hotelCartItem->options->adult }},&nbsp;<strong>Child:</strong> {{ $hotelCartItem->options->child }}</i>
                                </td>
                                <td>
                                    {{ $hotelCartItem->qty }}
                                </td>
                                <td>
                                    {!! html_entity_decode(\App\Helper\PostHelper::discountCartFormat($hotelCartItem->options->discount, 0, 0)) !!}
                                </td>
                                <td>
                                    <strong>{{ currency($hotelCartItem->price, currency()->config('default'), Session::get('currency'))}}</strong>
                                </td>
                                <td>
                                    <strong>{{ currency(($hotelCartItem->price * $hotelCartItem->qty), currency()->config('default'), Session::get('currency'))}}</strong>
                                </td>
                                <td class="options">
                                    <a href="{{ url('cart/delete', ['productTypeId' => 2, 'rowId' => $hotelCartItem->rowId]) }}" class="action cartItem"><i class=" icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <strong>Room Item</strong>
                                    {!! html_entity_decode(\App\Helper\PostHelper::cartRoomPage($hotelCartItem->options->discount, $hotelCartItem->options->rooms)) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

                @if(Cart::instance('tour')->content()->count() != 0 ||
                Cart::instance('hotel')->content()->count() != 0)
                        <h3 style="float: right;">Total:&nbsp;<span>{{ currency(Cart::instance('tour')->total() + Cart::instance('hotel')->total(), currency()->config('default'), Session::get('currency')) }}</span></h3>
                @endif

                @if(Cart::instance('tour')->content()->count() == 0 &&
                    Cart::instance('hotel')->count() == 0)
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <i class="fa fa-shopping-cart" style="font-size: 50px"></i><br />
                            <h1>Your cart is empty</h1>
                        </div>
                    </div>
                @endif
                
            </div><!-- End col-md-12 -->

            <br style="clear: both;" />

            <div style="margin-top: 50px;">
                @if(Cart::instance('tour')->content()->count() != 0 ||
                    Cart::instance('hotel')->content()->count() != 0)
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/') }}" class="btn btn-default btn-lg btn-block">Booking</a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('checkout/contact') }}" class="btn btn-danger btn-lg btn-block">Check Out</a>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ url('/') }}" class="btn btn-default btn-lg btn-block">Booking</a>
                        </div>
                    </div>
                @endif
            </div>

        </div><!--End row -->
    </div><!--End container -->
@endsection

@section('addons')
    <link href="{{ asset('assets/common/css/jquery.switch.css') }}" rel="stylesheet">
@endsection