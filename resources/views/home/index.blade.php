@extends('layouts.default')

@section('content')
    <section class="header-video">
        <div id="hero_video">
            <div id="search" class="hidden-xs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tours" data-toggle="tab">Tours</a></li>
                    <li><a href="#hotels" data-toggle="tab">Hotels</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tours">
                        <form id="form-product_search" name="form-product_search" method="GET"
                              action="{{ url('search/tour')  }}">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <input type="text" class="form-control destination" name="destination"
                                               placeholder="Destination">
                                        <form>
                                            <input name="lat" type="hidden" value=""/>
                                            <input name="lng" type="hidden" value=""/>
                                            <input name="type" type="hidden" value="1"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control select2" name="category" style="width: 100%;">
                                            @foreach($model->productCategoryOfTourResults as $productCategoryOfTourResult)
                                                <option value="{{ $productCategoryOfTourResult->id }}">{{ $productCategoryOfTourResult->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="age" style="width: 100%;">
                                            <option value="1">< 20 Years</option>
                                            <option value="2">20 - 30 Years</option>
                                            <option value="3">30 - 40 Years</option>
                                            <option value="4">40 - 50 Years</option>
                                            <option value="5">50 - 60 Years</option>
                                            <option value="6">> 60 Years</option>
                                            <option value="7" selected>All</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn_1 green" style="width: 100%;"><i class="icon-search"></i>Find Tour</button>
                                </div>
                            </div><!-- End row -->
                        </form>
                    </div>
                    <div class="tab-pane" id="hotels">
                        <form id="form-product_search" name="form-product_search" method="GET"
                              action="{{ url('search/hotel')  }}">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control destination" name="destination"
                                               placeholder="Destination">
                                        <form>
                                            <input name="lat" type="hidden" value=""/>
                                            <input name="lng" type="hidden" value=""/>
                                            <input name="type" type="hidden" value="2"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control select2" name="category" style="width: 100%;">
                                            @foreach($model->productCategoryOfHotelResults as $productCategoryOfHotelResult)
                                                <option value="{{ $productCategoryOfHotelResult->id }}">{{ $productCategoryOfHotelResult->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn_1 green" style="width: 100%;"><i class="icon-search"></i>Find Hotel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <img alt="Image"
             class="header-video-media"
             data-video-src=""
             data-teaser-source="{{ asset('assets/common/videos/paradiseexplorers') }}"
             data-provider=""
             data-video-width="854"
             data-video-height="480">
    </section>

    <div class="white_bg">
        <div class="container margin_60">
            <div id="search-mobile" class="visible-xs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tours-mobile" data-toggle="tab">Tours</a></li>
                    <li><a href="#hotels-mobile" data-toggle="tab">Hotels</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tours-mobile">
                        <form id="form-product_search" name="form-product_search" method="GET"
                              action="{{ url('search/tour')  }}">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <input type="text" class="form-control destination" name="destination"
                                               placeholder="Destination">
                                        <form>
                                            <input name="lat" type="hidden" value=""/>
                                            <input name="lng" type="hidden" value=""/>
                                            <input name="type" type="hidden" value="1"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control select2" name="category" style="width: 100%;">
                                            @foreach($model->productCategoryOfTourResults as $productCategoryOfTourResult)
                                                <option value="{{ $productCategoryOfTourResult->id }}">{{ $productCategoryOfTourResult->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="age" style="width: 100%;">
                                            <option value="1">< 20 Years</option>
                                            <option value="2">20 - 30 Years</option>
                                            <option value="3">30 - 40 Years</option>
                                            <option value="4">40 - 50 Years</option>
                                            <option value="5">50 - 60 Years</option>
                                            <option value="6">> 60 Years</option>
                                            <option value="7" selected>All</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn_1 green" style="width: 100%;"><i class="icon-search"></i>Find Tour</button>
                                </div>
                            </div><!-- End row -->
                        </form>
                    </div>
                    <div class="tab-pane" id="hotels-mobile">
                        <form id="form-product_search" name="form-product_search" method="GET"
                              action="{{ url('search/hotel')  }}">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control destination" name="destination"
                                               placeholder="Destination">
                                        <form>
                                            <input name="lat" type="hidden" value=""/>
                                            <input name="lng" type="hidden" value=""/>
                                            <input name="type" type="hidden" value="2"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control select2" name="category" style="width: 100%;">
                                            @foreach($model->productCategoryOfHotelResults as $productCategoryOfHotelResult)
                                                <option value="{{ $productCategoryOfHotelResult->id }}">{{ $productCategoryOfHotelResult->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn_1 green" style="width: 100%;"><i class="icon-search"></i>Find Hotel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="main_title">
                <h2>Why <span>Choose</span> Our Service ?</h2>
                <p>We have the best service</p>
            </div>
            <div class="row">
                <div class="col-md-3 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>Diverse</span> Destinations</h3>
                        <p>
                            Habitasse. Nunc mollis erat dapibus in commodo eu nisi. Odio purus magnis lorem.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>Best</span> Price</h3>
                        <p>
                            Nulla imperdiet dictum laoreet netus. Habitant ullamcorper. Auctor auctor cras.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>Beautiful</span> Place</h3>
                        <p>
                            Eu molestie Purus ac. Facilisis hac in metus nunc parturient ornare consequat enim.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 wow zoomIn" data-wow-delay="0.8s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>Fast</span> Booking</h3>
                        <p>
                            Torquent egestas. Per integer placerat ipsum quam adipiscing vehicula nisi.
                        </p>
                    </div>
                </div>
            </div><!-- End row -->
        </div>
    </div>

    @include('partials.most_popular_tour')

    @include('partials.most_popular_hotel')

    <section class="promo_full">
        <div class="promo_full_wp magnific">
            <div>
                <h3>BELONG ANYWHERE</h3>
                <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor
                    imperdiet deterruisset, doctus volumus explicari qui ex.
                </p>
                <a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" class="video"><i class="icon-play-circled2-1"></i></a>
            </div>
        </div>
    </section><!-- End section -->

    <!-- Destination -->
    @include('partials.destination')

@endsection

@section('addons')
    <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD3zQ7PjQrnblkxp4j05pit8OxFNLgT0QA'></script>

    <script type='text/javascript' src="{{ asset('assets/common/js/modernizr.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/common/js/video_header.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/common/js/geocomplete/jquery.geocomplete.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/select2/dist/js/select2.js') }}"></script>

    <script type='text/javascript'>
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'GET',
            url: localStorage.baseUrl + '/getMostPopularTour',
            crossDomain: true,
            success: function (response) {
                response.forEach(function (item) {
                    var mostPopularTour = '<div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">' +
                            '<div class="tour_container">';

//                    If there any discount
                    if (item.discount != 0) {
                        mostPopularTour += '<div class="ribbon_3 popular"><span>Disc ' + item.discount + '%</span></div>';
                    }

                    mostPopularTour += '<div class="img_container">' +
                            '<a href="search/detail/' + item.id + '">' +
                            '<img src="' + item.featured_image_url + '" class="img-responsive" alt="Image">';

//                    If there any product discount
                    if (item.product_discount != 0) {
                        mostPopularTour += '<div class="badge_save">Off<strong>' + item.product_discount + '%</strong></div>';
                    }

                    mostPopularTour += '<div class="short_info tour">' +
                            '<div style="position: absolute; bottom: 0;">' + item.product_category.category + '</div>' +
                            '<span class="price">';

                    if (item.price_before_discount) {
                        mostPopularTour += '<span style="font-size: 14px; color: red;"><strike>' + item.price_before_discount + '</strike></span>&nbsp;';
                    }

                    if (item.discount_total != 0) {
                        mostPopularTour += '<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save ' + item.discount_total + '%</span><br />';
                    }

                    mostPopularTour += item.price_after_discount + '</span>' +
                            '</div>' +
                            '</a>' +
                            '</div>' +
                            '<div class="tour_title">' +
                            '<h3 style="margin-bottom: 10px;">' +
                            '<strong>' + item.title + '</strong>' +
                            '</h3>' +
                            '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star-empty" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star-empty" aria-hidden="true"></span>' +
                            '</button>' +
                            '<div class="agent" style="padding-top: 35px;">' +
                            '<a href="#"><strong>' + item.agent.company + '</strong></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                    $('.most_popular_tour').append(mostPopularTour);
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'GET',
            url: localStorage.baseUrl + '/getMostPopularHotel',
            crossDomain: true,
            success: function (response) {
                var i = 0;

                response.forEach(function (item) {
                    var mostPopularHotel = '';

                    i++;

                    if (i == 1) {
                        mostPopularHotel += '<div class="item active"><div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">' +
                                '<div class="tour_container">';
                    } else {
                        mostPopularHotel += '<div class="item"><div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">' +
                                '<div class="tour_container">';
                    }

//                    If there any discount
                    if (item.discount != 0) {
                        mostPopularHotel += '<div class="ribbon_3 popular"><span>Disc ' + item.discount + '%</span></div>';
                    }

                    mostPopularHotel += '<div class="img_container">' +
                            '<a href="search/detail/' + item.id + '">' +
                            '<img src="' + item.featured_image_url + '" class="img-responsive" alt="Image">';

//                    If there any product discount
                    if (item.room_discount != 0) {
                        mostPopularHotel += '<div class="badge_save">Off<strong>' + item.product_discount + '%</strong></div>';
                    }

                    mostPopularHotel += '<div class="short_info tour">' +
                            '<div style="position: absolute; bottom: 0;">' + item.product_category.category + '</div>' +
                            '<span class="price">';

                    if (item.price_before_discount) {
                        mostPopularHotel += '<span style="font-size: 14px; color: red;"><strike>' + item.price_before_discount + '</strike></span>&nbsp;';
                    }

                    if (item.discount_total != 0) {
                        mostPopularHotel += '<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save ' + item.discount_total + '%</span><br />';
                    }

                    mostPopularHotel += item.price_after_discount + '</span>' +
                            '</div>' +
                            '</a>' +
                            '</div>' +
                            '<div class="tour_title">' +
                            '<h3 style="margin-bottom: 10px;">' +
                            '<strong>' + item.title + '</strong>' +
                            '</h3>' +
                            '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star-empty" aria-hidden="true"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">' +
                            '<span class="icon-star-empty" aria-hidden="true"></span>' +
                            '</button>' +
                            '<div class="agent" style="padding-top: 35px;">' +
                            '<a href="#"><strong>' + item.agent.company + '</strong></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                    $('.most_popular_hotel').append(mostPopularHotel);
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $(document).ready(function () {
            HeaderVideo.init({
                container: $('.header-video'),
                header: $('.header-video-media'),
                videoTrigger: $('#video-trigger'),
                autoPlayVideo: false
            });

            $('#most-popular-hotel').carousel({
                interval: 10000
            });

            $('.fdi-Carousel .item').each(function () {
                var next = $(this).next();

                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().appendTo($(this));
                } else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
            });

            $(".destination").geocomplete({details: "form"});

            $(".select2").select2();

        });
    </script>
@endsection
