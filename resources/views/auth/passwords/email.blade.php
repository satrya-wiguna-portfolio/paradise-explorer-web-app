@extends('layouts.default')

@section('content')
    <section id="hero" class="forgot_password">
        <div class="container">
            <div id="forgot_password" class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3 class="title">20+ Years of Experience</h3>
                            <p>We are the old cast, professional and experience. But we have a new things for our customer</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3>Hand-picked Tours</h3>
                            <p>Get convenience by hand-picked with us to visit every tour destination</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3>Licensed Tour Guides</h3>
                            <p>All of our tour guides have licensed and trusted. Get more detail information about tour destination</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3>Best Deals</h3>
                            <p>Find out more the best deal for each our packages</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @if (session()->has('status'))
                                <div class="alert alert-success">
                                    {{ session()->get('status') }}
                                </div>
                            @endif
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <h4>Send Reset Password Link</h4>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" data-validation="email">
                                    e.g.: email@example.com
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('addons')
    <script type="text/javascript">
        $.validate({
            modules : 'location, date, security, file'
        });
    </script>
@endsection