@extends('layouts.default')

@section('content')
    <section id="hero" class="register">
        <div class="container">
            <div id="register" class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3 class="title">20+ Years of Experience</h3>
                            <p>We are the old cast, professional and experience. But we have a new things for our customer</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3>Hand-picked Tours</h3>
                            <p>Get convenience by hand-picked with us to visit every tour destination</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3>Licensed Tour Guides</h3>
                            <p>All of our tour guides have licensed and trusted. Get more detail information about tour destination</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 visible-md visible-lg">
                            <img src="{{ asset('assets/common/images/barong.png') }}" style="margin-top: 15px; width: 90px;" />
                        </div>
                        <div class="col-md-8">
                            <h3>Best Deals</h3>
                            <p>Find out more the best deal for each our packages</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <h4>Join Us as a Paradise Explorer Member!</h4>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<div class="btn-group input-group btn-group-justified" data-toggle="buttons">--}}
                                        {{--<label class="btn btn-success active">--}}
                                            {{--<input type="radio" name="role_id" id="member" value="4" checked/>Member</label>--}}
                                        {{--<label class="btn btn-success">--}}
                                            {{--<input type="radio" name="role_id" id="agent" value="3" />Agent</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <input type="hidden" name="role.id" value="4" />
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" data-validation="email">
                                    e.g.: email@example.com
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Password</label>
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password-confirm" class="control-label">Confirm Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" data-validation="confirmation" data-validation-confirm="password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    By registering, I agree with <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a> of Paradise Explorers.
                                    <br /><br />
                                    <button type="submit" class="btn btn-block btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Register
                                    </button>
                                    <input type="hidden" name="role_id" value="4" />
                                    @if(app('request')->input('checkout'))
                                        <input type="hidden" id="checkout" name="checkout" value="{{ app('request')->input('checkout') }}" />
                                    @endif
                                </div>
                                <div class="form-group" style="text-align: center;">
                                    Already registered? <a href="{{ url('login') }}">Login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('addons')
    <script type="text/javascript">
        $.validate({
            modules : 'location, date, security, file'
        });
    </script>
@endsection
