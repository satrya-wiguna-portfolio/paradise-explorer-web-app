@inject('post', 'App\Helper\PostHelper')
@extends('layouts.default')

@section('content')
    <section id="hero" class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-3 col-sm-5 col-sm-offset-2">
                    <div id="login">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form role="form" method="POST" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                    @if (session()->has('type') && session()->has('message'))
                                        @if(session()->get('type') == 'success')
                                            <div class="alert alert-success">
                                                {{ session()->get('message') }}
                                            </div>
                                        @else
                                            <div class="alert alert-danger">
                                                {{ session()->get('message') }}
                                            </div>
                                        @endif
                                    @endif
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 login_social">
                                            <a href="{{ url('auth/facebook') }}" class="btn btn-primary btn-block"><i class="icon-facebook"></i>Facebook</a>
                                        </div>
                                        <div class="col-md-6 col-sm-6 login_social">
                                            <a href="{{ url('auth/twitter') }}" class="btn btn-info btn-block "><i class="icon-twitter"></i>Twitter</a>
                                        </div>
                                    </div> <!-- end row -->
                                    <div class="login-or"><hr class="hr-or"><span class="span-or">or</span></div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="control-label">Username</label>
                                        <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" data-validation="email">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="control-label">Password</label>
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" data-validation="length" data-validation-length="min5">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <p class="small">
                                        <a href="{{ url('/password/reset') }}">Forgot Password?</a><br />
                                        <a href="{{ url('/user/request') }}">Resend Activate</a>
                                    </p>
                                    <button type="submit" class="btn btn_full btn-success">
                                        <i class="fa fa-btn fa-sign-in"></i> Sign in
                                    </button>
                                    <a href="@if(app('request')->input('checkout')) {{ url('/register?checkout=' . urlencode(app('request')->input('checkout'))) }} @else {{ url('/register') }} @endif" class="btn btn_full_outline">Register</a>
                                    @if(app('request')->input('checkout'))
                                        <input type="hidden" id="checkout" name="checkout" value="{{ app('request')->input('checkout') }}" />
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('addons')
@endsection
