@inject('post', 'App\Helper\PostHelper')

@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_blog.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->title }}</h1>
                <p>{{ $model->subTitle }}</p>
            </div>
        </div>
    </section><!-- End section -->

    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render('blogDetail', $model->blogResult) !!}
        </div>
    </div><!-- End position -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-9">
                <div class="box_style_1">
                    <div class="post nopadding">
                        @if(!empty($model->blogResult->featured_image_url))
                            <div class="blog__item__thumbnail">
                                <img src="{{ Config::get('app.url') . $model->blogResult->featured_image_url }}" class="img-responsive" alt="{{ $model->blogResult->slug }}" style="width: 100%;"/>
                            </div>
                        @elseif(!empty($model->blogResult->featured_video_url))
                            <div class="blog__item__thumbnail">
                                @if(strpos($model->blogResult->featured_video_url, 'youtube') || strpos($model->blogResult->featured_video_url, 'vimeo'))
                                    <iframe style="width: 100%; height: 530px;" frameborder="0" src="{{ $model->blogResult->featured_video_url }}" allowfullscreen></iframe>
                                @else
                                    <video src="{{ $model->blogResult->featured_video_url }}" style="width: 100%; height: 530px;" controls="controls"></video>
                                @endif
                            </div>
                        @else
                            &nbsp;<!-- Do Nothing -->
                        @endif
                        <div class="post_info clearfix">
                            <div class="post-left">
                                <ul>
                                    <li><i class="icon-calendar-empty"></i>On <span>{{ date('Y, F d', strtotime($model->blogResult->publish))  }}</span></li>
                                    <li>
                                        <i class="icon-inbox-alt"></i>In
                                        @foreach($model->blogResult->blog_category as $blog_category)
                                            <span><a href="{{ url('blog/category', ['categoryId' => $blog_category->id]) }}">{{ $blog_category->name }}</a></span> @if($model->blogResult->blog_category->last() != $blog_category) , @endif
                                        @endforeach
                                    </li>
                                    <li>
                                        <i class="icon-tags"></i>Tags
                                        @foreach($model->blogResult->blog_tag as $blog_tag)
                                            <a href="{{ url('blog/tag', ['tagId' => $blog_tag->id]) }}" rel="tag">{{ $blog_tag->name }}</a> @if($model->blogResult->blog_tag->last() != $blog_tag) , @endif
                                        @endforeach
                                    </li>
                                    <!--<li>{{ $model->blogResult->user->role[0]->role }}</li>-->
                                </ul>
                            </div>
                            <div class="post-right"><i class="icon-comment"></i><a href="#">{{ ($model->blogCommentResults->count() > 0) ? $model->blogCommentResults->count() . 'Comments' : $model->blogCommentResults->count() . 'Comment' }} </a></div>
                        </div>
                        <h2>{{ $model->blogResult->title }}</h2>
                        {!! html_entity_decode($model->blogResult->contents) !!}
                    </div><!-- end post -->
                </div><!-- end box_style_1 -->

                <h4>{{ ($model->blogCommentResults->count() > 0) ? $model->blogCommentResults->count() . 'Comments' : $model->blogCommentResults->count() . 'Comment' }}</h4>

                <div id="comments">
                    <ol>
                        @foreach($model->blogCommentResults as $blogCommentResult)
                            <li>
                                <div class="avatar"><a href="#"><img src="{{ $post->avatarURLRender($blogCommentResult->user) }}" alt="Image"></a></div>
                                <div class="comment_right clearfix">
                                    <div class="comment_info">
                                        Posted by <a href="#">{{ $post->authorRender($blogCommentResult->user) }}</a><span>|</span> {{ date('Y, F d', strtotime($blogCommentResult->created_at)) }}
                                    </div>
                                    {{ strip_tags($blogCommentResult->comment, '<b><p>') }}
                                </div>
                            </li>
                        @endforeach
                    </ol>
                </div><!-- End Comments -->

                <h4>Leave a comment</h4>
                <form role="form" name="formComment" method="POST" action="{{ url('blog/leaveComment') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        @if ($errors->has('comment'))
                            <span class="help-block">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                        <textarea name="comment" class="form-control style_2" placeholder="Comment"></textarea>
                    </div>
                    <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
                        @if ($errors->has('rating'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rating') }}</strong>
                            </span>
                        @endif
                        <select id="rate" name="rate">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="blog_id" value="{{ $model->blogId }}" />
                        <input type="hidden" name="user_id" value="{{ $model->userId }}" />
                        <input type="reset" class="btn_1" value="Clear Form"/>
                        <input type="submit" class="btn_1" value="Post Comment"/>
                    </div>
                </form>
            </div><!-- End col-md-9-->

            <aside class="col-md-3 add_bottom_30">

                <div class="widget">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
						<button class="btn btn-default" type="button" style="margin-left:0;"><i class="icon-search"></i></button>
						</span>
                    </div><!-- /input-group -->
                </div><!-- End Search -->

                <hr />

                @include('partials.blog_category')

                <hr />

                @include('partials.blog_tag')

                <hr />

                @include('partials.blog_recent')

            </aside><!-- End aside -->
        </div>
    </div>
@endsection

@section('addons')
    <link href="{{ asset('assets/common/css/blog.css') }}" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}" rel="stylesheet">

    <script type='text/javascript' src="{{ asset('assets/vendors/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>

    <script type='text/javascript'>
        $(document).ready(function () {
            $('#rate').barrating({
                theme: 'fontawesome-stars'
            });
        });
    </script>
@endsection