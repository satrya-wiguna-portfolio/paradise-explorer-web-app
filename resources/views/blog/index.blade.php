@inject('post', 'App\Helper\PostHelper')

@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_blog.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->title }}</h1>
                <p>{{ $model->subTitle }}</p>
            </div>
        </div>
    </section><!-- End section -->

    <div id="position">
        <div class="container">
            @if($model->blogCategoryResult)
                {!! Breadcrumbs::render('blogCategory', $model->blogCategoryResult) !!}
            @elseif($model->blogTagResult)
                {!! Breadcrumbs::render('blogTag', $model->blogTagResult) !!}
            @else
                {!! Breadcrumbs::render() !!}
            @endif
        </div>
    </div><!-- End position -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-9">
                <div class="box_style_1">
                    @foreach($model->blogResults as $blogResult)
                        <div class="post">
                            @if(!empty($blogResult->featured_image_url))
                                <a href="{{ url('blog/detail', ['id' => $blogResult->id]) }}">
                                    <img src="{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($blogResult->featured_image_url, '950x375') }}"  alt="{{ $blogResult->slug  }}" class="img-responsive"/>
                                </a>
                            @elseif(!empty($blogResult->featured_video_url))
                                <a href="{{ url('blog/detail', ['id' => $blogResult->id]) }}">
                                    @if(strpos($blogResult->featured_video_url, 'youtube') || strpos($blogResult->featured_video_url, 'vimeo'))
                                        <iframe style="width: 100%; height: 530px;" frameborder="0" src="{{ $blogResult->featured_video_url }}" allowfullscreen></iframe>
                                    @else
                                        <video src="{{ $blogResult->featured_video_url }}" style="width: 100%; height: 530px;" controls="controls"></video>
                                    @endif
                                </a>
                            @else
                                &nbsp;<!-- Do Nothing -->
                            @endif
                            <div class="post_info clearfix">
                                <div class="post-left">
                                    <ul>
                                        <li><i class="icon-calendar-empty"></i> On <span>{{ date('Y, F d', strtotime($blogResult->publish))  }}</span></li>
                                        <li>
                                            <i class="icon-inbox-alt"></i> In
                                            @foreach($blogResult->blog_category as $blog_category)
                                                <a href="{{ url('blog/category', ['categoryId' => $blog_category->id]) }}">{{ $blog_category->name }}</a></span> @if($blogResult->blog_category->last() != $blog_category) , @endif
                                            @endforeach
                                        </li>
                                        <li>
                                            <i class="icon-tags"></i> Tags
                                            @foreach($blogResult->blog_tag as $blog_tag)
                                                <a href="{{ url('blog/tag', ['tagId' => $blog_tag->id]) }}">{{ $blog_tag->name }}</a></span> @if($blogResult->blog_tag->last() != $blog_tag) , @endif
                                            @endforeach
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-right"><i class="icon-comment"></i><a href="#">25 </a></div>
                            </div>
                            <h2>{{ $blogResult->title  }}</h2>
                                @if($post->readMoreRender($blogResult->contents) !== false)
                                    {!! html_entity_decode($post->readMoreRender($blogResult->contents)) !!}<br /><br />
                                    <a href="{{ url('blog/detail', ['id' => $blogResult->id]) }}" class="btn_1" >Read more</a>
                                @else
                                    {!! html_entity_decode($blogResult->contents) !!}
                                @endif
                        </div><!-- end post -->
                    @endforeach
                    <hr>
                </div>
                <hr>

                @include('pagination.custom', ['paginator' => $model->blogResults])
            </div><!-- End col-md-8-->

            <aside class="col-md-3">

                <div class="widget">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
						<button class="btn btn-default" type="button" style="margin-left:0;"><i class="icon-search"></i></button>
						</span>
                    </div><!-- /input-group -->
                </div><!-- End Search -->

                <hr />

                @include('partials.blog_category')

                <hr />

                @include('partials.blog_tag')

                <hr />

                @include('partials.blog_recent')

            </aside><!-- End aside -->

        </div>
    </div>
@endsection

@section('addons')
    <link href="{{ asset('assets/common/css/blog.css') }}" rel="stylesheet">
@endsection