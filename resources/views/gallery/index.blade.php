@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_gallery.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->title }}</h1>
                <p>{{ $model->subTitle }}</p>
            </div>
        </div>
    </section><!-- End Section -->
    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- End Position -->

    <div class="white_bg">
        <div class="container margin_60">
            <div id="gallery" class="final-tiles-gallery effect-dezoom effect-fade-out caption-top social-icons-right">
                <div class="ftg-items">
                    @foreach($model->instagramResults as $instagramResult)
                        <div class="tile">
                            <a class="tile-inner" data-title="{{ ($instagramResult->caption) ? $instagramResult->caption->text : null }}" data-lightbox="{{ ($instagramResult->caption) ? $instagramResult->caption->text : null }}" href="{{ $instagramResult->images->standard_resolution->url }}">
                                <img class="item" src="{{ $instagramResult->images->standard_resolution->url }}" data-src="{{ $instagramResult->images->standard_resolution->url }}" alt="{{ ($instagramResult->caption) ? $instagramResult->caption->text : null }}">
                                <span class='subtitle'>{{ ($instagramResult->caption) ? $instagramResult->caption->text : null }}</span>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('addons')
    <script src="{{ asset('assets/common/js/jquery.finaltilesgallery.js') }}"></script>
    <script src="{{ asset('assets/common/js/lightbox2.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $(".final-tiles-gallery").finalTilesGallery();

        });
    </script>
@endsection