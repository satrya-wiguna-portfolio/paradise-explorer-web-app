<header>
    <div id="top_line">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6"><i class="icon-phone"></i><strong>+62 811 388 1373</strong></div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    @if(Route::currentRouteName() != 'showLoginForm')
                    <ul id="top_links">
                        <li>
                            <div class="dropdown dropdown-mini">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="currency_link">{{ $baseModel->currencyList }}</a>
                                <div class="dropdown-menu">
                                    <ul id="lang_menu">
                                        @foreach($baseModel->currencyResults as $currencyResult)
                                            <li><a href="{{ url()->current() . '?' . http_build_query(['currency' => $currencyResult->code]) }}">{{  $currencyResult->code }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-top-links">
                                <a href="https://www.instagram.com/paradiseexplors/"><i class="icon-instagram"></i></a>
                                <a href="https://www.facebook.com/ParadiseExplors/"><i class="icon-facebook"></i></a>
                                <a href="https://twitter.com/ParadiseExplors"><i class="icon-twitter"></i></a>
                            </div>
                        </li>
                        <li>
                        @if (Auth::guest())
                            <div class="dropdown dropdown-access">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">&nbsp;Sign in</a>
                                <div class="dropdown-menu">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <a href="{{ url('auth/facebook') }}" class="btn btn-primary" style="color: #ffffff;">
                                                <i class="icon-facebook"></i>Facebook
                                            </a>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <a href="{{ url('auth/twitter') }}" class="btn btn-info" style="color: #ffffff;">
                                                <i class="icon-twitter"></i>Twitter
                                            </a>
                                        </div>
                                    </div>
                                    <div class="login-or">
                                        <hr class="hr-or"><span class="span-or">or</span>
                                    </div>
                                    <form role="form"  method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <a id="forgot_pw" href="{{ url('/password/reset') }}">Forgot password?</a>
                                        <a id="forgot_pw" href="{{ url('/user/request') }}">Resend Activate</a>
                                        <input type="submit" value="Sign in" class="button_drop">
                                        <input type="button" value="Register" class="button_drop outline button_signup" onclick="javascript:location.href='{{ url('register') }}';">
                                    </form>
                                </div>
                            </div>
                        @else
                            <div class="dropdown dropdown-mini">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ (Auth::user()->member) ? substr(Auth::user()->member->first_name . ' ' . Auth::user()->member->last_name, 0, 25) : Auth::user()->email }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" style="width: 200px;">
                                    <ul id="lang_menu">
                                        <li><a href="{{ url('/member/profile') }}"><i class="fa fa-btn fa-user"></i> Profile</a></li>
                                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        </li>
                    </ul>
                    @endif
                </div>
            </div><!-- End row -->
        </div><!-- End container-->
    </div><!-- End top line-->

    @include('shared.top_menu')
</header><!-- End Header -->