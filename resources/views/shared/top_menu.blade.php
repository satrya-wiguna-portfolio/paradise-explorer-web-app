<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div id="logo_home">
                <h1><a href="{{ url('/') }}" title="Paradise Explorers">Paradise Explorers</a></h1>
            </div>
        </div>
        <nav class="col-md-9 col-sm-9 col-xs-9">
            <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
            <div class="main-menu">
                <div id="header_menu">
                    <img src="../../assets/common/images/logo_sticky.png" width="175" alt="Paradise Explorers" data-retina="true">
                </div>
                <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                <ul>
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ url('/aboutUs') }}">About Us</a>
                    </li>
                    <li>
                        <a href="{{ url('search/tour') }}">Tour</a>
                    </li>
                    <li>
                        <a href="{{ url('search/hotel') }}">Hotel</a>
                    </li>
                    <li class="submenu">
                        <a href="javascript:void(0);" class="show-submenu">Destination <i class="icon-down-open-mini"></i></a>
                        @if($baseModel->randomLimitDestinationResults)
                            <ul>
                                <li><a href="{{ url('search') }}">All</a></li>
                                @foreach($baseModel->randomLimitDestinationResults as $randomLimitDestinationResult)
                                    <li><a href="{{ url('search/destination', ['id' => $randomLimitDestinationResult->id]) }}">{{ $randomLimitDestinationResult->name }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                    <li>
                        <a href="{{ url('/faq') }}">Faq</a>
                    </li>
                    <li>
                        <a href="{{ url('/blog') }}">Blog</a>
                    </li>
                    <li>
                        <a href="{{ url('/gallery') }}">Gallery</a>
                    </li>
                    <li>
                        <a href="{{ url('/contact') }}">Contact</a>
                    </li>
                </ul>
            </div><!-- End main-menu -->
            <ul id="top_tools">
                <li>
                    <div class="dropdown dropdown-search">
                        <a href="#search_blog"><i class="icon-search"></i></a>
                    </div>
                </li>

                <li>
                    @include('partials.cart')
                </li>
            </ul>
        </nav>
    </div>
</div><!-- container -->