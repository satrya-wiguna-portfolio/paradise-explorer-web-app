<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Paradise Explorers">
    <meta name="author" content="Satrya Wiguna">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Paradise Explorers</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset('assets/common/images/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('assets/common/images/apple-touch-icon-57x57-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('assets/common/images/apple-touch-icon-72x72-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ asset('assets/common/images/apple-touch-icon-114x114-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ asset('assets/common/images/apple-touch-icon-144x144-precomposed.png') }}">

    <!-- CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/common/css/base.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/common/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/dist/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/common/css/finaltilesgallery.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/common/css/lightbox2.css') }}"  rel="stylesheet">


    <!-- Google web fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="{{ asset('assets/common/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('assets/common/js/respond.min.js') }}"></script>
    <![endif]-->

    {{-- Share This --}}
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=594db62e22c0b700127e3c78&product=inline-share-buttons' async='async'></script>

    <script type="text/javascript">
        if (!localStorage.baseUrl) {
            localStorage.baseUrl = '{{ url('/') }}';
        }

        if (!localStorage.clientId) {
            localStorage.clientId = new Fingerprint().get();
        }
    </script>
</head>