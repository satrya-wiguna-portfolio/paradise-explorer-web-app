<div class="row">
    <div class="col-md-4 col-sm-12">
        <p><img src="../../assets/common/images/logo_footer.png" alt="Paradise Explorers" data-retina="true" id="logo"></p>
        <p>Paradise Explorers, was first established in 2000. We strives to achieve the most comprehensive knowledge of Bali</p>
    </div>
    <div class="col-md-2 col-sm-3">
        <h3>About</h3>
        <ul>
            <li><a href="{{ url('aboutUs') }}">About Us</a></li>
            <li><a href="{{ url('faq') }}">Faq</a></li>
            <li><a href="{{ url('login') }}">Login</a></li>
            <li><a href="{{ url('register') }}">Register</a></li>
            <li><a href="#">Terms and Condition</a></li>
        </ul>
    </div>
    <div class="col-md-2 col-sm-3">
        <h3>Discover</h3>
        <ul>
            <li><a href="{{ url('blog') }}">Blog</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Wishlist</a></li>
            <li><a href="{{ url('gallery') }}">Gallery</a></li>
        </ul>
    </div>
    <div class="col-md-3 col-sm-5">
        @include('partials.newsletter')
    </div>
</div><!-- End row -->