<footer id="pattern_2">
    <div class="container">
        @include('shared.bottom_menu')

        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <li><a href="https://www.facebook.com/ParadiseExplors/"><i class="icon-facebook"></i></a></li>
                        <li><a href="https://twitter.com/ParadiseExplors"><i class="icon-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/paradiseexplors/"><i class="icon-instagram"></i></a></li>
                    </ul>
                    <p>© Paradise Explorers 2017</p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
</footer><!-- End footer -->