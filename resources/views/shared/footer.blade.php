<div id="search_blog">
    <button type="button" class="close">×</button>
    <form id="form_search_blog" method="POST"
        action="{{ url('blog/search') }}">
        <input type="search" value="" placeholder="Type keyword(s) here" />
    </form>
</div>

<script type='text/javascript' src="{{ asset('assets/common/js/jquery-1.11.2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('assets/common/js/common_scripts_min.js') }}"></script>
<script type='text/javascript' src='{{ asset('assets/vendors/sweetalert2/dist/sweetalert2.js') }}'></script>
<script type='text/javascript' src='{{ asset('assets/vendors/fingerprint/fingerprint.js') }}'></script>
<script type='text/javascript' src="{{ asset('assets/common/js/functions.js') }}"></script>

<link rel='stylesheet' type='text/css' href='{{ asset('assets/vendors/sweetalert2/dist/sweetalert2.css') }}' />

<script type='text/javascript'>
    @if(Cart::instance('hotel')->count() != 0)
        //SESSION CLIENT ID
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: localStorage.baseUrl + '/refreshRoomAllotmentLog',
            data: {
                client_id: localStorage.clientId
            },
            crossDomain: true,
            success: function (response) {
                console.log(response);
            },
            error: function (e) {
                console.log(e);
            }
        });
    @endif

    @if(Cart::instance('tour')->count() != 0 || Cart::instance('hotel')->count() != 0)
        //SESSION CART LIFETIME

        setInterval(function() {
            swal({
                title: 'Error Message',
                text: 'Session expired, you\'re need refresh browser!',
                type: 'error',
                timer: 1000 * 10
            }).then(
                function () {
                    location.reload();
                },
                function (dismiss) {
                    if (dismiss === 'timer') {
                        location.reload();
                    }
                }
            );
        }, 1000 * 60 * 5);
    @endif

    $(document).ready(function () {
        $('a[href="#search_blog"]').on('click', function(event) {
            event.preventDefault();

            $('#search_blog').addClass('open');
            $('#search_blog > form > input[type="search"]').focus();
        });

        $('#search_blog, #search_blog button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });

        $('.cartItem').each(function () {
            var href = $(this).attr('href') + '/' + localStorage.clientId;

            $(this).attr("href", href);
        });

        $('#client_id').val(localStorage.clientId);

    });
</script>

@yield('addons')