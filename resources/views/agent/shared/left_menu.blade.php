<nav class="left-menu" left-menu ng-class="{'hidden-left-menu': hideLeftMenu}">
    <div class="logo-container">
        <a href="#/dashboard" class="logo">
            <img class="logo-inverse" src="./assets/common/images/admin/logo-inverse.png" alt="Paradise Explorers" />
        </a>
    </div>
    <div class="left-menu-inner scroll-pane">
        <ul class="left-menu-list left-menu-list-root list-unstyled">
            <!-- DASHBOARD -->
            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])">
                <a class="left-menu-link" href="#/dashboard">
                    <i class="left-menu-link-icon icmn-home2"><!-- --></i>
                    <span class="menu-top-hidden">Dashboard</span>
                </a>
            </li>
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-separator"><!-- --></li>

            <!-- MAIN MENU -->
            <li class="left-menu-list-disabled">
                <a class="left-menu-link" href="javascript: void(0);">
                    Main Menu
                </a>
            </li>

            <!-- MASTER DATA -->
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-database"><!-- --></i>
                    Master Data
                </a>
                <ul class="left-menu-list list-unstyled">
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Area Location
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/country">
                                    &nbsp;&nbsp;Country
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/state">
                                    &nbsp;&nbsp;State
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/province">
                                    &nbsp;&nbsp;Province
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/regency">
                                    &nbsp;&nbsp;Regency
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/district">
                                    &nbsp;&nbsp;District
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  ng-show="authentication.isAuthorized(['super'])">
                        <a class="left-menu-link" href="#/role">
                            Roles
                        </a>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Phone, Fax & Handphone
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/phoneCode">
                                    &nbsp;&nbsp;Phone Code
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/faxCode">
                                    &nbsp;&nbsp;Fax Code
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/handphoneCode">
                                    &nbsp;&nbsp;Handphone Code
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                        <a class="left-menu-link" href="#/facility">
                            Facility
                        </a>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                        <a class="left-menu-link" href="#/destination">
                            Destination
                        </a>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                        <a class="left-menu-link" href="#/agentType">
                            Agent Type
                        </a>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Product
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/productType">
                                    &nbsp;&nbsp;Type
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/productCategory">
                                    &nbsp;&nbsp;Category
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-separator"><!-- --></li>

            <!-- USER MANAGEMENT -->
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="menu-top-hidden left-menu-list-submenu">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-users"><!-- --></i>
                    Users
                </a>
                <!-- level 1 -->
                <ul class="left-menu-list list-unstyled">
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Member
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/member">
                                    &nbsp;&nbsp;
                                    Member List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/member/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Agent
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/agent">
                                    &nbsp;&nbsp;
                                    Agent List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/agent/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Staff
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/staff">
                                    &nbsp;&nbsp;
                                    Staff List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])">
                                <a class="left-menu-link" href="#/staff/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-separator"><!-- --></li>

            <!-- BLOG / POST MANAGEMENT -->
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="menu-top-hidden left-menu-list-submenu">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-rss"><!-- --></i>
                    Blogs
                </a>
                <!-- level 1 -->
                <ul class="left-menu-list list-unstyled">
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Posts
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])" >
                                <a class="left-menu-link" href="#/blog">
                                    &nbsp;&nbsp;
                                    Posts List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])" >
                                <a class="left-menu-link" href="#/blog/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Categories
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])" >
                                <a class="left-menu-link" href="#/blogCategory">
                                    &nbsp;&nbsp;
                                    Categories List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])" >
                                <a class="left-menu-link" href="#/blogCategory/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Tags
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])" >
                                <a class="left-menu-link" href="#/blogTag">
                                    &nbsp;&nbsp;
                                    Tags List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin'])" >
                                <a class="left-menu-link" href="#/blogTag/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li ng-show="authentication.isAuthorized(['super', 'admin'])" class="left-menu-list-separator"><!-- --></li>

            <!-- BED MANAGEMENT -->
            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                <a class="left-menu-link" href="#/bed">
                    <i class="left-menu-link-icon icmn-bed2"><!-- --></i>
                    Beds
                </a>
            </li>

            <!-- ROOM MANAGEMENT -->
            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                <a class="left-menu-link" href="#/room">
                    <i class="left-menu-link-icon icmn-enter2"><!-- --></i>
                    Rooms
                </a>
            </li>

            <!-- PRODUCT MANAGEMENT -->
            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" class="menu-top-hidden left-menu-list-submenu">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-database2"><!-- --></i>
                    Products
                </a>

                <!-- level 1 -->
                <ul class="left-menu-list list-unstyled">
                    <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Products
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                                <a class="left-menu-link" href="#/product">
                                    &nbsp;&nbsp;
                                    Product List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                                <a class="left-menu-link" href="#/product/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Tags
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                                <a class="left-menu-link" href="#/productTag">
                                    &nbsp;&nbsp;
                                    Tag List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                                <a class="left-menu-link" href="#/productTag/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" class="left-menu-list-submenu">
                        <a class="left-menu-link" href="javascript: void(0);">
                            Discounts
                        </a>
                        <!-- level 2 -->
                        <ul class="left-menu-list list-unstyled">
                            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                                <a class="left-menu-link" href="#/productDiscount">
                                    &nbsp;&nbsp;
                                    Discount List
                                </a>
                            </li>
                            <li ng-show="authentication.isAuthorized(['super', 'admin', 'agent'])" >
                                <a class="left-menu-link" href="#/productDiscount/add">
                                    &nbsp;&nbsp;
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>