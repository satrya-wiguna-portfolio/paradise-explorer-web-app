@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_checkout.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->title }}</h1>
                <p>{{ $model->subTitle }}</p>
            </div>
        </div>
    </section><!-- End Section -->
    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render('checkoutCancel', $model->reservation) !!}
        </div>
    </div><!-- End Position -->

    <div class="white_bg">
        <div class="container margin_60">
            <h2>Payment Pending #{{ $model->reservation->invoice_number }}</h2>
            Hi {{ $model->reservation->first_name }},<br />
            Thank you! Just one step closer, billing {{ $model->reservation->invoice_number }} is still waiting for payment. Please continue  for payment process to get done your booking<br /><br />
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-5">
                            <table class="table table-user-information table-checkout">
                                <tbody>
                                <tr>
                                    <td class="label-checkout">Name:</td>
                                    <td>{{ $model->reservation->title }}.&nbsp;{{ $model->reservation->first_name }}&nbsp;{{ $model->reservation->last_name }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">Gender:</td>
                                    <td>{{ $model->reservation->gender }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">Payment Method:</td>
                                    <td>{{ $model->reservation->payment->payment }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">Currency:</td>
                                    <td>{{ $model->reservation->currency->name }}&nbsp;({{ $model->reservation->currency->code }})</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-offset-1 col-md-5">
                            <table class="table table-user-information table-checkout">
                                <tbody>
                                <tr>
                                    <td class="label-checkout">Address:</td>
                                    <td>{{ $model->reservation->address }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">Zip:</td>
                                    <td>{{ $model->reservation->zip }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">Country:</td>
                                    <td>{{ $model->reservation->country->name }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">State:</td>
                                    <td>{{ $model->reservation->state->name }}</td>
                                </tr>
                                <tr>
                                    <td class="label-checkout">City:</td>
                                    <td>{{ $model->reservation->city }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Pax</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($model->reservation->reservation_product as $key => $reservation_product)
                            <?php $model->reservation->grandTotal += $reservation_product->total; ?>
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $reservation_product->title }}</td>
                                <td>{{ $reservation_product->product_type->type }}</td>
                                <td><strong>Adult:</strong>&nbsp;{{ $reservation_product->adult }},&nbsp;<strong>Child:</strong>&nbsp;{{ $reservation_product->child }}</td>
                                <td>{{ $model->reservation->currency->symbol }}&nbsp;{{ number_format($reservation_product->total) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"><strong>Total Billing</strong></td>
                            <td>{{ $model->reservation->currency->symbol }}&nbsp;{{ number_format($model->reservation->grandTotal) }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/member/transaction') }}" class="btn btn-default btn-lg btn-block">My Transaction</a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('/') }}" class="btn btn-danger btn-lg btn-block">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

    </div>
@endsection

@section('addons')

@endsection