@extends('layouts.default')

@section('content')
    <section id="hero_2">
        <div class="intro_title animated fadeInDown">
            <h1>Place your order</h1>
            <br /><br />
            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum"><strong>Cart</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('cart/') }}" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum"><strong>Checkout</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('checkout/contact') }}" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum"><strong>Payment</strong></div>
                    <div class="progress"></div>
                    <a href="{{ url('checkout/payment') }}" class="bs-wizard-dot"></a>
                </div>

            </div>  <!-- End bs-wizard -->
        </div>   <!-- End intro-title -->
    </section><!-- End Section hero_2 -->

    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- End Position -->

    <form class="bs-example" method="post" action="{{ url('checkout/payment') }}">
        {{ csrf_field() }}
        <input type="hidden" id="client_id" name="client_id" />
        <div class="container margin_60">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#paymentMethod">Payment Method<i class="indicator icon-minus pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="paymentMethod" class="panel-collapse collapse in" style="padding: 35px 10px 10px 10px;">
                                <p>Please choose one prefer of payment methods. Each of payment methods has step information to do transaction.</p>
                                <div class="panel-group" id="sub-accordion">
                                    @foreach($model->provider->dto as $provider)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <label for='label_{{ \App\Helper\PostHelper::replaceWhiteSpace($provider->payment) }}'>
                                                    <input type='radio' id='label_{{ \App\Helper\PostHelper::replaceWhiteSpace($provider->payment) }}' name='payment' value='{{ $provider->id }}' required @if(session()->has('payment') && session()->get('payment')->payment_id == $provider->id) checked @else {{ ($provider->id == 1) ? 'checked' : null }} @endif />&nbsp;&nbsp;{{ $provider->payment }}
                                                    <a data-toggle="collapse" data-parent="#sub-accordion" href="#{{ \App\Helper\PostHelper::replaceWhiteSpace($provider->payment) }}"></a>
                                                </label>
                                                <img src="{{ Config::get('app.url') . $provider->image_url }}" height="25px" class="pull-right" />
                                            </h4>
                                        </div>
                                        <div id="{{ \App\Helper\PostHelper::replaceWhiteSpace($provider->payment) }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                {!! html_entity_decode($provider->description) !!}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End col-md-6 -->
                <div class="col-md-6">
                    @if(Cart::instance('tour')->content()->count() != 0)
                        <table class="table table-striped cart-list add_bottom_30">
                            <thead>
                            <tr>
                                <th colspan="5">
                                    Tour Packages Order
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 750px;">
                                    Item
                                </th>
                                <th style="width: 100px;">
                                    Pax
                                </th>
                                <th style="width: 100px;">
                                    Discount
                                </th>
                                <th style="width: 200px;">
                                    Price
                                </th>
                                <th style="width: 200px;">
                                    Sub Total
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Cart::instance('tour')->content() as $tourCartItem)
                                <tr>
                                    <td>
                                        <span class="item_cart" style="font-size: 14px;"><a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $tourCartItem->id]) }}"><strong>{{ $tourCartItem->name }}</strong></a></span><br />
                                        <i><strong>Departure:</strong> {{ date('M, d Y', strtotime($tourCartItem->options->departure)) }}</i><br />
                                        <i><strong>Adult:</strong> {{ $tourCartItem->options->adult }}, <strong>Child:</strong> {{ $tourCartItem->options->child }}</i>
                                    </td>
                                    <td>
                                        {{ $tourCartItem->qty }}
                                    </td>
                                    <td>
                                        {!! html_entity_decode(\App\Helper\PostHelper::discountCartFormat($tourCartItem->options->discount, 0, $tourCartItem->options->product_discount)) !!}
                                    </td>
                                    <td>
                                        <strong>{{ currency($tourCartItem->price, currency()->config('default'), Session::get('currency'))}}</strong>
                                    </td>
                                    <td>
                                        <strong>{{ currency(($tourCartItem->price * $tourCartItem->qty), currency()->config('default'), Session::get('currency'))}}</strong>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                    @if(Cart::instance('hotel')->content()->count() != 0)
                        <table class="table table-striped cart-list add_bottom_30">
                            <thead>
                            <tr>
                                <th colspan="5">
                                    Hotel Packages Order
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 750px;">
                                    Item
                                </th>
                                <th style="width: 100px;">
                                    Night
                                </th>
                                <th style="width: 100px;">
                                    Discount
                                </th>
                                <th style="width: 200px;">
                                    Price
                                </th>
                                <th style="width: 200px;">
                                    Sub Total
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Cart::instance('hotel')->content() as $hotelCartItem)
                                <tr>
                                    <td>
                                        <span class="item_cart" style="font-size: 14px;"><a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $hotelCartItem->id]) }}"><strong>{{ $hotelCartItem->name }}</strong></a></span><br />
                                        <i><strong>Check In:</strong> {{ date('M, d Y', strtotime($hotelCartItem->options->check_in)) }} - <strong>Check Out:</strong> {{ date('M, d Y', strtotime($hotelCartItem->options->check_out)) }}</i><br />
                                        <i><strong>Adult:</strong> {{ $hotelCartItem->options->adult }},&nbsp;<strong>Child:</strong> {{ $hotelCartItem->options->child }}</i>
                                    </td>
                                    <td>
                                        {{ $hotelCartItem->qty }}
                                    </td>
                                    <td>
                                        {!! html_entity_decode(\App\Helper\PostHelper::discountCartFormat($hotelCartItem->options->discount, 0, 0)) !!}
                                    </td>
                                    <td>
                                        <strong>{{ currency($hotelCartItem->price, currency()->config('default'), Session::get('currency'))}}</strong>
                                    </td>
                                    <td>
                                        <strong>{{ currency(($hotelCartItem->price * $hotelCartItem->qty), currency()->config('default'), Session::get('currency'))}}</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <strong>Room Item</strong>
                                        {!! html_entity_decode(\App\Helper\PostHelper::cartRoomPage($hotelCartItem->options->discount, $hotelCartItem->options->rooms)) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                    <div style="text-align: right;"><h3><strong>Total:</strong>&nbsp;<span>{{ currency(Cart::instance('tour')->total() + Cart::instance('hotel')->total(), currency()->config('default'), Session::get('currency')) }}</span></h3></div>
                </div><!-- End col-md-6 -->
            </div><!--End row -->

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <a href="{{ url('checkout/contact') }}" class="btn btn-default btn-lg btn-block">Contact</a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-danger btn-lg btn-block">Process</button>
                </div>
            </div>
        </div><!--End container -->
    </form>

@endsection

@section('addons')
    <link rel="stylesheet" href="{{ asset('assets/vendors/jquery-accordion/css/jquery.accordion.css') }}" type="text/css" />

    <script type='text/javascript' src="{{ asset('assets/vendors/jquery-accordion/js/jquery.accordion.js') }}"></script>

    <script>
        $(document).ready(function () {
            @foreach($model->provider->dto as $provider)
            $('#label_{{ \App\Helper\PostHelper::replaceWhiteSpace($provider->payment) }}').on('click', function() {
                $(this).parent().find('a').trigger('click')
            });
            @endforeach
        });

    </script>
@endsection