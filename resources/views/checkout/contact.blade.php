@extends('layouts.default')

@section('content')
    <section id="hero_2">
        <div class="intro_title animated fadeInDown">
            <h1>Place your order</h1>
            <br /><br />
            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum"><strong>Cart</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('cart/') }}" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum"><strong>Checkout</strong></div>
                    <div class="progress"></div>
                    <a href="{{ url('checkout/contact') }}" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum"><strong>Payment</strong></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="{{ url('checkout/payment') }}" class="bs-wizard-dot"></a>
                </div>

            </div>  <!-- End bs-wizard -->
        </div>   <!-- End intro-title -->
    </section><!-- End Section hero_2 -->

    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- End Position -->

    <form id="contactForm" class="bs-example" method="post" action="{{ url('checkout/contact') }}">
        {{ csrf_field() }}
        <input type="hidden" id="client_id" name="client_id"/>
        <div class="container margin_60">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#contactInformation">Contact Information<i class="indicator icon-minus pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="contactInformation" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">Title</label>
                                            @if(session()->has('contact'))
                                                <select id="title"
                                                        name="title"
                                                        class="select2"
                                                        style="width: 100%;">
                                                    <option value="mr" @if(session()->get('contact')->title == 'mr') selected @endif>Mr.</option>
                                                    <option value="mrs" @if(session()->get('contact')->title == 'mrs') selected @endif>Mrs.</option>
                                                </select>
                                            @elseif(isset(Auth::user()->member))
                                                <select id="title"
                                                        name="title"
                                                        class="select2"
                                                        style="width: 100%;">
                                                    <option value="mr" @if(Auth::user()->member->title == 'mr') selected @endif>Mr.</option>
                                                    <option value="mrs" @if(Auth::user()->member->title == 'mrs') selected @endif>Mrs.</option>
                                                </select>
                                            @else
                                                <select id="title"
                                                        name="title"
                                                        class="select2"
                                                        style="width: 100%;"
                                                        data-validation="[NOTEMPTY]">
                                                    <option value="mr" selected>Mr.</option>
                                                    <option value="mrs">Mrs.</option>
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">First Name</label>
                                            <input type="text"
                                                   id="first_name"
                                                   name="first_name"
                                                   class="form-control"
                                                   placeholder="First Name"
                                                   data-validation="[NOTEMPTY]"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->first_name }}@else{{ (isset(Auth::user()->member)) ? Auth::user()->member->first_name : old('first_name') }}@endif">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">Last Name</label>
                                            <input type="text"
                                                   id="last_name"
                                                   name="last_name"
                                                   class="form-control"
                                                   placeholder="Last Name"
                                                   data-validation="[NOTEMPTY]"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->last_name }}@else{{ (isset(Auth::user()->member)) ? Auth::user()->member->last_name : old('last_name') }}@endif">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">Gender</label><br />
                                            <div class="btn-group" data-toggle="buttons">
                                                @if(session()->has('contact'))
                                                    <label class="btn btn-default @if(session()->get('contact')->gender == 'male') active @endif">
                                                        <input name="gender" value="male" type="radio" @if(session()->get('contact')->gender == 'male') checked @endif>Male
                                                    </label>
                                                    <label class="btn btn-default @if(session()->get('contact')->gender == 'female') active @endif">
                                                        <input name="gender" value="female" type="radio" @if(session()->get('contact')->gender == 'female') checked @endif>Female
                                                    </label>
                                                @elseif(isset(Auth::user()->member))
                                                    <label class="btn btn-default @if(Auth::user()->member->gender == 'male') active @endif">
                                                        <input name="gender" value="male" type="radio" @if(Auth::user()->member->gender == 'male') checked @endif>Male
                                                    </label>
                                                    <label class="btn btn-default @if(Auth::user()->member->gender == 'female') active @endif">
                                                        <input name="gender" value="female" type="radio" @if(Auth::user()->member->gender == 'female') checked @endif>Female
                                                    </label>
                                                @else
                                                    <label class="btn btn-default active">
                                                        <input name="gender" value="male" type="radio" checked>Male
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input name="gender" value="female" type="radio">Female
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">Address</label>
                                                    <textarea id="address"
                                                              name="address"
                                                              class="form-control"
                                                              rows="3"
                                                              data-validation="[NOTEMPTY]"
                                                              placeholder="Address">@if(session()->has('contact')){{ session()->get('contact')->address }}@else{{ (isset(Auth::user()->member)) ? Auth::user()->member->address : old('address') }}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="form-control-label text-uppercase">Country</label><br />
                                            <select id="country"
                                                    name="country"
                                                    class="select2"
                                                    style="width: 100%;"
                                                    data-validation="[NOTEMPTY]">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-control-label text-uppercase">State</label><br />
                                            <select id="state"
                                                    name="state"
                                                    class="select2"
                                                    style="width: 100%;"
                                                    data-validation="[NOTEMPTY]">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="form-control-label text-uppercase">City</label><br />
                                            <input type="text"
                                                   id="city"
                                                   name="city"
                                                   class="form-control"
                                                   placeholder="City"
                                                   data-validation="[NOTEMPTY]"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->city }}@else{{ (isset(Auth::user()->member)) ? Auth::user()->member->city : old('city') }}@endif">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-control-label text-uppercase">Zip</label><br />
                                            <input type="text"
                                                   id="zip"
                                                   name="zip"
                                                   class="form-control"
                                                   placeholder="Zip"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->zip }}@else{{ (isset(Auth::user()->member)) ? Auth::user()->member->zip : old('zip') }}@endif">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">Email</label>
                                            <input type="text"
                                                   id="email"
                                                   name="email"
                                                   class="form-control"
                                                   placeholder="Email"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->email }}@else{{ (isset(Auth::user()->email)) ? Auth::user()->email : old('email') }}@endif"
                                                   disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="form-control-label text-uppercase">Phone</label>
                                            <input type="tel"
                                                   id="phone"
                                                   name="phone"
                                                   class="form-control"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->phone }}@else{{ (isset(Auth::user()->member)) ? Auth::user()->member->phone : old('phone') }}@endif">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-control-label text-uppercase">Handphone</label>
                                            <input type="tel"
                                                   id="handphone"
                                                   name="handphone"
                                                   class="form-control"
                                                   data-validation="[NOTEMPTY]"
                                                   value="@if(session()->has('contact')){{ session()->get('contact')->handphone }}@else{{ (isset(Auth::user()->handphone)) ? Auth::user()->handphone : old('handphone') }}@endif">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End col-md-6 -->
                <div class="col-md-6">
                    @if(Cart::instance('tour')->content()->count() != 0)
                        <table class="table table-striped cart-list add_bottom_30">
                            <thead>
                            <tr>
                                <th colspan="5">
                                    Tour Packages Order
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 750px;">
                                    Item
                                </th>
                                <th style="width: 100px;">
                                    Pax
                                </th>
                                <th style="width: 100px;">
                                    Discount
                                </th>
                                <th style="width: 200px;">
                                    Price
                                </th>
                                <th style="width: 200px;">
                                    Sub Total
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Cart::instance('tour')->content() as $tourCartItem)
                                <tr>
                                    <td>
                                        <span class="item_cart" style="font-size: 14px;"><a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $tourCartItem->id]) }}"><strong>{{ $tourCartItem->name }}</strong></a></span><br />
                                        <i><strong>Departure:</strong> {{ date('M, d Y', strtotime($tourCartItem->options->departure)) }}</i><br />
                                        <i><strong>Adult:</strong> {{ $tourCartItem->options->adult }}, <strong>Child:</strong> {{ $tourCartItem->options->child }}</i>
                                    </td>
                                    <td>
                                        {{ $tourCartItem->qty }}
                                    </td>
                                    <td>
                                        {!! html_entity_decode(\App\Helper\PostHelper::discountCartFormat($tourCartItem->options->discount, 0, $tourCartItem->options->product_discount)) !!}
                                    </td>
                                    <td>
                                        <strong>{{ currency($tourCartItem->price, currency()->config('default'), Session::get('currency'))}}</strong>
                                    </td>
                                    <td>
                                        <strong>{{ currency(($tourCartItem->price * $tourCartItem->qty), currency()->config('default'), Session::get('currency'))}}</strong>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                        @if(Cart::instance('hotel')->content()->count() != 0)
                            <table class="table table-striped cart-list add_bottom_30">
                                <thead>
                                <tr>
                                    <th colspan="5">
                                        Hotel Packages Order
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 750px;">
                                        Item
                                    </th>
                                    <th style="width: 100px;">
                                        Night
                                    </th>
                                    <th style="width: 100px;">
                                        Discount
                                    </th>
                                    <th style="width: 200px;">
                                        Price
                                    </th>
                                    <th style="width: 200px;">
                                        Sub Total
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Cart::instance('hotel')->content() as $hotelCartItem)
                                    <tr>
                                        <td>
                                            <span class="item_cart" style="font-size: 14px;"><a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $hotelCartItem->id]) }}"><strong>{{ $hotelCartItem->name }}</strong></a></span><br />
                                            <i><strong>Check In:</strong> {{ date('M, d Y', strtotime($hotelCartItem->options->check_in)) }} - <strong>Check Out:</strong> {{ date('M, d Y', strtotime($hotelCartItem->options->check_out)) }}</i><br />
                                            <i><strong>Adult:</strong> {{ $hotelCartItem->options->adult }},&nbsp;<strong>Child:</strong> {{ $hotelCartItem->options->child }}</i>
                                        </td>
                                        <td>
                                            {{ $hotelCartItem->qty }}
                                        </td>
                                        <td>
                                            {!! html_entity_decode(\App\Helper\PostHelper::discountCartFormat($hotelCartItem->options->discount, 0, 0)) !!}
                                        </td>
                                        <td>
                                            <strong>{{ currency($hotelCartItem->price, currency()->config('default'), Session::get('currency'))}}</strong>
                                        </td>
                                        <td>
                                            <strong>{{ currency(($hotelCartItem->price * $hotelCartItem->qty), currency()->config('default'), Session::get('currency'))}}</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <strong>Room Item</strong>
                                            {!! html_entity_decode(\App\Helper\PostHelper::cartRoomPage($hotelCartItem->options->discount, $hotelCartItem->options->rooms)) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                        <div style="text-align: right;"><h3><strong>Total:</strong>&nbsp;<span>{{ currency(Cart::instance('tour')->total() + Cart::instance('hotel')->total(), currency()->config('default'), Session::get('currency')) }}</span></h3></div>
                </div><!-- End col-md-6 -->
            </div><!--End row -->

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <a href="{{ url('/cart') }}" class="btn btn-default btn-lg btn-block">Cart</a>
                </div>
                <div class="col-md-6">
                    <button type="submit" id="paymentButton" class="btn btn-danger btn-lg btn-block">Payment</button>
                </div>
            </div>
        </div><!--End container -->
    </form>

@endsection

@section('addons')
    <link rel="stylesheet" href="{{ asset('assets/vendors/jquery-accordion/css/jquery.accordion.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" type="text/css" />

    <script type='text/javascript' src="{{ asset('assets/vendors/jquery-accordion/js/jquery.accordion.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/select2/dist/js/select2.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/intl-tel-input/build/js/intlTelInput.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/html5-form-validation/dist/jquery.validation.js') }}"></script>

    <script type='text/javascript'>
        var baseUrl = '{{ str_replace('8888', '8890', str_replace('http', 'https', Config::get('app.url'))) }}';

        @if(session()->has('contact'))
            var countryId = parseInt({{ session()->get('contact')->country_id }});
        @else
            var countryId = parseInt({{ (isset(Auth::user()->member)) ? Auth::user()->member->country_id : null }});
        @endif

        @if(session()->has('contact'))
            var stateId = parseInt({{ session()->get('contact')->state_id }});
        @else
            var stateId = parseInt({{ (isset(Auth::user()->member)) ? Auth::user()->member->state_id : null }});
        @endif

        $(document).ready(function () {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                url: baseUrl + 'checkout/getCountryList/',
                success: function (response) {
                    $.each(response.dto, function (index, data) {
                        if (data.id == countryId) {
                            $("#country").append("<option value=\"" + data.id + "\" selected>" + data.name + "</option>");

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: 'GET',
                                url: baseUrl + 'checkout/getStateListByCountry/' + countryId,
                                success: function (response) {
                                    $("#state option").remove();

                                    $.each(response.dto, function (index, data) {
                                        if (data.id == stateId) {
                                            $("#state").append("<option value=\"" + data.id + "\" selected>" + data.name + "</option>");
                                        } else {
                                            $("#state").append("<option value=\"" + data.id + "\">" + data.name + "</option>");
                                        }
                                    });
                                }
                            });
                        } else {
                            $("#country").append("<option value=\"" + data.id + "\">" + data.name + "</option>");
                        }
                    });
                }
            });

            $("#country").on("select2:selecting", function(e) {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'GET',
                    url: baseUrl + 'checkout/getStateListByCountry/' + e.params.args.data.id,
                    success: function (response) {
                        $("#state option").remove();

                        $.each(response.dto, function (index, data) {
                            $("#state").append("<option value=\"" + data.id + "\">" + data.name + "</option>");
                        });
                    }
                });
            });

            $("#title").select2({
                placeholder: 'Select Title',
                minimumResultsForSearch: -1
            });

            $("#country").select2({
                placeholder: 'Select Country'
            });

            $("#state").select2({
                placeholder: 'Select State'

            });

            $("#phone").intlTelInput({
                nationalMode: false,
                initialCountry: "auto",
                placeholderNumberType: "Phone",
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "../../assets/vendors/intl-tel-input/build/js/utils.js"
            });

            $("#handphone").intlTelInput({
                nationalMode: false,
                initialCountry: "auto",
                placeholderNumberType: "Handphone",
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "../../assets/vendors/intl-tel-input/build/js/utils.js"
            });

            $("#contactForm").validate();

            $("#paymentButton").click(function () {
                $("#phone").val($("#phone").intlTelInput("getNumber"));
                $("#handphone").val($("#handphone").intlTelInput("getNumber"));
            });

        });
    </script>
@endsection