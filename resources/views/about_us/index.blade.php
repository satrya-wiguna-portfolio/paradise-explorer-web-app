@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_about.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->title }}</h1>
                <p>{{ $model->subTitle }}</p>
            </div>
        </div>
    </section><!-- End Section -->
    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- End Position -->

    <div class="white_bg">
        <div class="container margin_60">
            <div class="main_title">
                <h2>We Are <span>Number</span> One</h2>
                <p>The Bali Experts</p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="margin_60">
                        <p>Paradise Explorers is a travel company with over 20 years of experience of tours in Bali, and Indonesia. Our love for nature, Indonesian culture, and exploration started us off in this journey. We started with an aim at providing adventure, fun, and creating unforgettable experiences to last a lifetime. Create moments, and memories that defers from other commonly found tours.</p>
                        <p>Our tours are selected based on varying needs, from relaxing with the family, to a romantic weekend, or adrenaline pumping action. All our tours are carefully picked after scouring the island for it’s most memorable and awe inspiring locations. So expect the finest that Bali, and Indonesia has to offer.</p>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/asita.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/hnt.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/interpark.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/jautour.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/krt.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/lotte-tour.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/onlinetour.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/yblour.jpg') }}" width="100%" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('assets/common/images/pbb.jpg') }}" width="100%" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <ul class="cbp_tmtimeline">
            <li>
                <time class="cbp_tmtime" datetime="1800"><span>1800 paxs</span><span>Apr 2016</span></time>
                <div class="cbp_tmicon timeline_icon_point"></div>
                <div class="cbp_tmlabel">
                    <h2><strong>Prodential</strong></h2>
                    <p>Prudential Insurance Korea at The Westin Hotel</p>
                </div>
            </li>
            <li>
                <time class="cbp_tmtime" datetime="650"><span>650 paxs</span><span>Feb 2016</span></time>
                <div class="cbp_tmicon timeline_icon_point"></div>
                <div class="cbp_tmlabel">
                    <h2><strong>IDS</strong></h2>
                    <p>IDS at The Mulia Hotel</p>
                </div>
            </li>
            <li>
                <time class="cbp_tmtime" datetime="300"><span>300 paxs</span><span>Apr 2015</span></time>
                <div class="cbp_tmicon timeline_icon_point"></div>
                <div class="cbp_tmlabel">
                    <h2><strong>Miev</strong></h2>
                    <p>Miev Korea at Grand Hyatt Hotelá</p>
                </div>
            </li>
            <li>
                <time class="cbp_tmtime" datetime="650"><span>650 paxs</span><span>Feb 2016</span></time>
                <div class="cbp_tmicon timeline_icon_point"></div>
                <div class="cbp_tmlabel">
                    <h2><strong>Hana Card</strong></h2>
                    <p>Hana Card Korea at Intercontinental Hotel</p>
                </div>
            </li>
        </ul>
    </div>
@endsection

@section('addons')
    <link href="{{ asset('assets/common/css/timeline.css') }}" rel="stylesheet">
@endsection