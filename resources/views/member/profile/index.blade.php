@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_member.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <img src="{{ $model->avatar }}" class="img-circle" width="150" />
                @if($model->title)
                    <h1>{{ $model->title }}</h1>
                @else
                    <br /><br />
                @endif
                <p>{{ $model->subTitle }}</p>
            </div>
        </div>
    </section><!-- End Section -->
    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- End Position -->

    <div class="white_bg">
        <div class="container margin_60">
            <div class="row">
                <div class="col-md-12">
                    @include('member.partials.navigation')

                    <div class="col-md-8">
                        <h2>Profile</h2><br />
                        <form id="profileForm" class="bs-example" method="post" action="{{ url('member/profile/update') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Title</label>
                                        @if($model->memberResult)
                                            <select id="title"
                                                    name="title"
                                                    class="select2"
                                                    style="width: 100%;">
                                                <option value="mr" @if($model->memberResult->title == 'mr') selected @endif>Mr.</option>
                                                <option value="mrs" @if($model->memberResult->title == 'mrs') selected @endif>Mrs.</option>
                                            </select>
                                        @else
                                            <select id="title"
                                                    name="title"
                                                    class="select2"
                                                    style="width: 100%;"
                                                    data-validation="[NOTEMPTY]">
                                                <option value="mr" selected>Mr.</option>
                                                <option value="mrs">Mrs.</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">First Name</label>
                                        <input type="text"
                                               id="first_name"
                                               name="first_name"
                                               class="form-control"
                                               placeholder="First Name"
                                               data-validation="[NOTEMPTY]"
                                               value="@if($model->memberResult){{ $model->memberResult->first_name }}@else{{ old('first_name') }}@endif">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Last Name</label>
                                        <input type="text"
                                               id="last_name"
                                               name="last_name"
                                               class="form-control"
                                               placeholder="Last Name"
                                               data-validation="[NOTEMPTY]"
                                               value="@if($model->memberResult){{ $model->memberResult->last_name }}@else{{ old('last_name') }}@endif">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="form-control-label text-uppercase">Gender</label><br />
                                            <div class="btn-group" data-toggle="buttons">
                                                @if($model->memberResult)
                                                    <label class="btn btn-default @if($model->memberResult->gender == 'male') active @endif">
                                                        <input name="gender" value="male" type="radio" @if($model->memberResult->gender == 'male') checked @endif>Male
                                                    </label>
                                                    <label class="btn btn-default @if($model->memberResult->gender == 'female') active @endif">
                                                        <input name="gender" value="female" type="radio" @if($model->memberResult->gender == 'female') checked @endif>Female
                                                    </label>
                                                @else
                                                    <label class="btn btn-default active">
                                                        <input name="gender" value="male" type="radio" checked>Male
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input name="gender" value="female" type="radio">Female
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="form-control-label text-uppercase">Address</label>
                                                        <textarea id="address"
                                                                  name="address"
                                                                  class="form-control"
                                                                  rows="3"
                                                                  data-validation="[NOTEMPTY]"
                                                                  placeholder="Address">@if($model->memberResult){{ $model->memberResult->address }}@else{{ old('address') }}@endif</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="form-control-label text-uppercase">Country</label><br />
                                    <select id="country"
                                            name="country"
                                            class="select2"
                                            style="width: 100%;"
                                            data-validation="[NOTEMPTY]">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-control-label text-uppercase">State</label><br />
                                    <select id="state"
                                            name="state"
                                            class="select2"
                                            style="width: 100%;"
                                            data-validation="[NOTEMPTY]">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="form-control-label text-uppercase">City</label><br />
                                    <input type="text"
                                           id="city"
                                           name="city"
                                           class="form-control"
                                           placeholder="City"
                                           data-validation="[NOTEMPTY]"
                                           value="@if($model->memberResult){{ $model->memberResult->city }}@else{{ old('city') }}@endif">
                                </div>
                                <div class="col-md-6">
                                    <label class="form-control-label text-uppercase">Zip</label><br />
                                    <input type="text"
                                           id="zip"
                                           name="zip"
                                           class="form-control"
                                           placeholder="Zip"
                                           value="@if($model->memberResult){{ $model->memberResult->zip }}@else{{ old('zip') }}@endif">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="form-control-label text-uppercase">Email</label>
                                    <input type="text"
                                           id="email"
                                           name="email"
                                           class="form-control"
                                           placeholder="Email"
                                           data-validation="[NOTEMPTY]"
                                           value="@if($model->memberResult){{ $model->memberResult->email }}@else{{ old('email') }}@endif"
                                           @if($model->memberResult) disabled @endif>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="form-control-label text-uppercase">Phone</label>
                                    <input type="tel"
                                           id="phone"
                                           name="phone"
                                           class="form-control"
                                           value="@if($model->memberResult){{ $model->memberResult->phone }}@else{{ old('phone') }}@endif">
                                </div>
                                <div class="col-md-6">
                                    <label class="form-control-label text-uppercase">Handphone</label>
                                    <input type="tel"
                                           id="handphone"
                                           name="handphone"
                                           class="form-control"
                                           data-validation="[NOTEMPTY]"
                                           value="@if($model->memberResult){{ $model->memberResult->handphone }}@else{{ old('handphone') }}@endif">
                                </div>
                            </div>

                            <br />

                            <hr />

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-lg btn-danger">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('addons')
    <link rel="stylesheet" href="{{ asset('assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" type="text/css" />

    <script type='text/javascript' src="{{ asset('assets/vendors/intl-tel-input/build/js/intlTelInput.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/select2/dist/js/select2.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/html5-form-validation/dist/jquery.validation.js') }}"></script>

    <script type='text/javascript'>
        var baseUrl = '{{ str_replace('8888', '8890', str_replace('http', 'https', Config::get('app.url'))) }}';

        @if($model->memberResult)
        var countryId = parseInt({{ $model->memberResult->country_id }});
        @else
        var countryId = null;
        @endif

        @if($model->memberResult)
        var stateId = parseInt({{ $model->memberResult->state_id }});
        @else
        var stateId = null;
        @endif

        $(document).ready(function () {
            $("#title").select2({
                placeholder: 'Select Title',
                minimumResultsForSearch: -1
            });

            $("#country").select2({
                placeholder: 'Select Country'
            });

            $("#state").select2({
                placeholder: 'Select State'

            });

            $("#profileForm").validate();

            $("#phone").intlTelInput({
                nationalMode: false,
                initialCountry: "auto",
                placeholderNumberType: "Phone",
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "../../assets/vendors/intl-tel-input/build/js/utils.js"
            });

            $("#handphone").intlTelInput({
                nationalMode: false,
                initialCountry: "auto",
                placeholderNumberType: "Handphone",
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "../../assets/vendors/intl-tel-input/build/js/utils.js"
            });

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                url: baseUrl + 'checkout/getCountryList/',
                success: function (response) {
                    $.each(response.dto, function (index, data) {
                        if (data.id == countryId) {
                            $("#country").append("<option value=\"" + data.id + "\" selected>" + data.name + "</option>");

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: 'GET',
                                url: baseUrl + 'checkout/getStateListByCountry/' + countryId,
                                success: function (response) {
                                    $("#state option").remove();

                                    $.each(response.dto, function (index, data) {
                                        if (data.id == stateId) {
                                            $("#state").append("<option value=\"" + data.id + "\" selected>" + data.name + "</option>");
                                        } else {
                                            $("#state").append("<option value=\"" + data.id + "\">" + data.name + "</option>");
                                        }
                                    });
                                }
                            });
                        } else {
                            $("#country").append("<option value=\"" + data.id + "\">" + data.name + "</option>");
                        }
                    });
                }
            });

            $("#country").on("select2:selecting", function(e) {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'GET',
                    url: baseUrl + 'checkout/getStateListByCountry/' + e.params.args.data.id,
                    success: function (response) {
                        $("#state option").remove();

                        $.each(response.dto, function (index, data) {
                            $("#state").append("<option value=\"" + data.id + "\">" + data.name + "</option>");
                        });
                    }
                });
            });
        });

    </script>
@endsection