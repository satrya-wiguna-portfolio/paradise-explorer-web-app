<aside class="col-lg-3 col-md-4 col-sm-4">
    <div class="box_style_cat">
        <ul id="cat_nav">
            <li><a href="{{ url('member') }}"><i class="icon-home-outline"></i>Dashboard</a></li>
            <li><a href="{{ url('member/profile') }}"><i class="icon-user-7"></i>Profile</a></li>
            <li><a href="{{ url('member/mybookings') }}"><i class="icon-bookmark-2"></i>My Bookings</a></li>
            <li><a href="{{ url('member/setting') }}"><i class="icon-cog-5"></i>Setting</a></li>
            <li><a href="#"><i class="icon-lock-7"></i>Logout</a></li>
        </ul>
    </div>
</aside>