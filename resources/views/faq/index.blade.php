@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_faq.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->title  }}</h1>
                <p>{{ $model->subTitle  }}</p>
            </div>
        </div>
    </section><!-- End section -->
    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render() !!}
        </div>
    </div><!-- Position -->

    <div class="container margin_60">
        <div class="row">
            <aside class="col-lg-3 col-md-3"  id="sidebar">
                <div class="theiaStickySidebar">
                    <div class="box_style_cat"  id="faq_box">
                        <ul id="cat_nav">
                            <li><a href="#payment" class="active"><i class="icon_set_1_icon-95"></i>Payment Policy</a></li>
                            <li><a href="#cancel"><i class="icon_set_1_icon-95"></i>Cancellation Policy</a></li>
                            <li><a href="#trans"><i class="icon_set_1_icon-95"></i>Transportation</a></li>
                            <li><a href="#advice"><i class="icon_set_1_icon-95"></i>Travel Tips and Advice</a></li>
                        </ul>
                    </div>
                </div><!--End sticky -->
            </aside><!--End aside -->
            <div class="col-lg-9 col-md-9" id="faq">
                <h3 class="nomargin_top">Payment Policy</h3>

                <div class="panel-group" id="payment">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#payment" href="#collapseTwo_payment">What methods of payment do you accept?<i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseTwo_payment" class="panel-collapse collapse">
                            <div class="panel-body">
                                We accept credit card (Visa, Mastercard, JCB) payments, as well as bank transfers to OCBC or BNI, as well as PayPal.
                            </div>
                        </div>
                    </div>
                </div><!-- End panel-group -->

                <h3>Cancellation Policy</h3>

                <div class="panel-group" id="cancel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#cancel" href="#collapseOne_cancel">What’s your cancellation policy? <i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseOne_cancel" class="panel-collapse collapse">
                            <div class="panel-body">
                                Our cancellation policy is dependent on when you cancel. Each tour / activity has stated its own cancellation policy.
                            </div>
                        </div>
                    </div>
                </div><!-- End panel-group -->

                <h3>Transportation</h3>

                <div class="panel-group" id="trans">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#trans" href="#collapseOne_trans">What type of car do you use?<i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseOne_trans" class="panel-collapse collapse">
                            <div class="panel-body">
                                Our cars range from 6 seaters all the way up to busses. We can accommodate both small or large groups. They are all air conditioned, with licensed, and knowledgeable drivers.
                            </div>
                        </div>
                    </div>
                </div><!-- End panel-group -->

                <h3>Travel Tips and Advice</h3>

                <div class="panel-group" id="advice">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseOne_advice">What is type of luggage should I bring?<i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseOne_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                We highly recommended backpacks as they allow your hands to be free to help navigate potential rough terrain. The recommended contents of your luggage will be listed on the tour pages, and tailored specifically to each individual tour.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseTwo_advice">Is tipping included?<i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseTwo_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tipping is not included in our fees. It would be very appreciated by our tour guides if you can tip, even just a little bit.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseThree_advice">How much should I tip?<i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseThree_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                We recommend tipping more than Rp. 100k, however tip at your own discretion.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseFour_advice">Are there any extra fees that I have to pay?<i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseFour_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                All of our tours are inclusive of fees for the tour itinerary. Some tours may not include things such as lunch, so be sure to keep an eye on the itinerary.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseFive_advice">What if the weather doesn’t allow hiking? <i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseFive_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                In case of extreme weather there will be cancelation on certain activities, such as hiking due to it’s potential danger.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseSix_advice">What should I bring with me? <i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseSix_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                We will send you an email on our recommendations depending on the specific location. However, we generally recommend bug spray and sunscreen at all times.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseSeven_advice">What are the seasons/weather like in Bali? <i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseSeven_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                Though global warming has made the weather much more unpredictable, generally speaking Bali goes through 3 cycles. Monsoon season runs from late November all the way March, if you are looking for hiking type activities this is the worse time to go. June til September is our winter with though it’s not cold, but the weather is at its best during this time. September to November and March through June tends to be the hottest times of the year.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#advice" href="#collapseEight_advice">I want to go out to eat. Do they accept cards at restaurants? <i class="indicator icon-plus pull-right"></i></a>
                            </h4>
                        </div>
                        <div id="collapseEight_advice" class="panel-collapse collapse">
                            <div class="panel-body">
                                Cash is your best bet. Though a lot of restaurants accept cards, there’s always one or two that surprise you and only accepts cash. Try to keep a little cash on you at all times.                             </div>
                        </div>
                    </div>
                </div><!-- End panel-group -->
            </div>
        </div>
    </div>
@endsection

@section('addons')
    <script type='text/javascript' src="{{ asset('assets/common/js/theia-sticky-sidebar.js') }}"></script>

    <script type='text/javascript'>
        $(document).ready(function () {
            'use strict';

            $('#faq_box a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 120
                        }, 500);
                        return false;
                    }
                }
            });

            $('#sidebar').theiaStickySidebar({
                additionalMarginTop: 80
            });
        });
    </script>
@endsection