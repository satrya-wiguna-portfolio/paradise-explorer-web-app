<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin - Paradise Explorers</title>

    <link href="{{ asset('assets/common/images/admin/favicon.144x144.png') }}" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="{{ asset('assets/common/images/admin/favicon.114x114.png') }}" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="{{ asset('assets/common/images/admin/favicon.72x72.png') }}" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="{{ asset('assets/common/images/admin/favicon.57x57.png') }}" rel="apple-touch-icon" type="image/png">
    <link href="{{ asset('assets/common/images/admin/favicon.png') }}" rel="icon" type="image/png">
    <link href="favicon.ico" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}">

    <!-- JQuery -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/jscrollpane/style/jquery.jscrollpane.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/ladda/dist/ladda-themeless.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/cleanhtmlaudioplayer/src/player.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/cleanhtmlvideoplayer/src/player.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/ionrangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/media/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.2/css/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/c3/c3.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/chartist/dist/chartist.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fancybox-plus/css/jquery.fancybox-plus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.css') }}">

    <!-- Angular JS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/angular-datatables/dist/css/angular-datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/angular-bootstrap/ui-bootstrap-csp.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/angular-ui-select/dist/select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/angular-rateit/src/style/ng-rateit.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/ngprogress/ngProgress.css') }}">

    <!-- Other -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/common/css/admin/slim/slim.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/common/css/admin/main.css') }}">


    <!-- Vendors Scripts -->
    <!-- JQuery -->
    <script src="{{ asset('assets/vendors/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/tether/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/spin.js/spin.js') }}"></script>
    <script src="{{ asset('assets/vendors/ladda/dist/ladda.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/html5-form-validation/dist/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js') }}"></script>
    <script src="{{ asset('assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/nestable/jquery.nestable.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <!--<script src="{{ asset('assets/vendors/jquery-datatables-row-grouping/media/js/jquery.dataTables.rowGrouping.min.js') }}"></script>-->
    <!--<script src="{{ asset('assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>-->
    <!--<script src="{{ asset('assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js') }}"></script>-->
    <!--<script src="{{ asset('assets/vendors/datatables-responsive/js/dataTables.responsive.js') }}"></script>-->
    <script src="{{ asset('assets/vendors/editable-table/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('assets/vendors/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/c3/c3.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/gsap/src/minified/TweenMax.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/hackertyper/hackertyper.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-countTo/jquery.countTo.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox-plus/dist/jquery.fancybox-plus.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery.observe_field/jquery.observe_field.js') }}"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/lib/tinymce/tinymce.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Angular JS -->
    {{--<script src="https://maps.google.com/maps/api/js?key=AIzaSyCoXZrB5-BcNNgNIPxlq0w2NVzUMngkpXc"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoXZrB5-BcNNgNIPxlq0w2NVzUMngkpXc&libraries=places"></script>
    <script src="{{ asset('assets/vendors/angular/angular.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-route/angular-route.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/ngprogress/build/ngprogress.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-ladda/dist/angular-ladda.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-datatables/dist/angular-datatables.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-sanitize/angular-sanitize.min.js') }}"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <script src="{{ asset('assets/vendors/angular-datatables/dist/plugins/fixedcolumns/angular-datatables.fixedcolumns.min.js') }}"></script>
    <!--<script src="{{ asset('assets/vendors/angular-bootstrap/ui-bootstrap.min.js') }}"></script>-->
    <script src="{{ asset('assets/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-ui-tinymce/dist/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-file-upload/ng-file-upload-shim.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-file-upload/ng-file-upload.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-file-upload/ng-file-upload-shim.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-file-upload/ng-file-upload.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/checklist-model/checklist-model.js') }}"></script>
    <script src="{{ asset('assets/vendors/ngmap/build/scripts/ng-map.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-ui-select/dist/select.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-thumbnails/dist/angular-thumbnails.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-daterangepicker/js/angular-daterangepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-number-picker/dist/angular-number-picker.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-elastic-input/dist/angular-elastic-input.js') }}"></script>
    <script src="{{ asset('assets/vendors/angular-rateit/src/ng-rateit.js') }}"></script>
    <script src="{{ asset('assets/vendors/intl-tel-input/build/js/intlTelInput.js') }}"></script>
    <script src="{{ asset('assets/vendors/intl-tel-input/build/js/utils.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-intl-tel-input/ng-intl-tel-input.module.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-intl-tel-input/ng-intl-tel-input.provider.js') }}"></script>
    <script src="{{ asset('assets/vendors/ng-intl-tel-input/ng-intl-tel-input.directive.js') }}"></script>

    <!-- Other -->
    <script src="{{ asset('assets/common/js/admin/www/lib/slim/slim.kickstart.min.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/lib/slim/slim.angular.js') }}"></script>


    <!-- Angular Version Common -->
    <script src="{{ asset('assets/common/js/admin/www/app.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/config.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/constant.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/directive.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/filter.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/global.js') }}"></script>

    <!-- Angular Version Services -->
    <script src="{{ asset('assets/common/js/admin/www/services/InterceptorAuthentication.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/services/Authentication.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/services/Session.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/services/Promise.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/services/Urls.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/services/Notification.js') }}"></script>

    <!-- Angular Version Factories -->
    <script src="{{ asset('assets/common/js/admin/www/factories/FileManager.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Role.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Currency.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Country.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/State.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Province.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Regency.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/District.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/AgentType.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Facility.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Payment.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Member.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Agent.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Staff.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/User.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/BlogCategory.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/BlogTag.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/BlogComment.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/ProductTag.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Blog.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/ProductType.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/ProductCategory.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Product.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/ProductDiscount.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/ProductComment.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Room.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/RoomDiscount.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/RoomAllotment.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Bed.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/Destination.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/factories/PropertyType.js') }}"></script>

    <!-- Angular Version Directives -->
    <script src="{{ asset('assets/common/js/admin/www/directives/ngAutoComplete.js') }}"></script>

    <!-- Angular Version Controllers -->
    <script src="{{ asset('assets/common/js/admin/www/controllers/ErrorController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/DashboardController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/LoginController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/LogoffController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/RoleController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/CurrencyController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/CountryController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/StateController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProvinceController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/RegencyController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/DistrictController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/AgentTypeController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/FacilityController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/PaymentController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/MemberController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/AgentController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/StaffController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/UserController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/BlogCategoryController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/BlogTagController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/BlogCommentController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProductTagController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/BlogController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProductTypeController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProductCategoryController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProductController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProductDiscountController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/ProductCommentController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/RoomController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/RoomDiscountController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/RoomAllotmentController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/BedController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/DestinationController.js') }}"></script>
    <script src="{{ asset('assets/common/js/admin/www/controllers/PropertyTypeController.js') }}"></script>

    <!-- Clean UI Scripts -->
    <script src="{{ asset('/assets/common/js/admin/common.js') }}"></script>
</head>
<style>
    .select2-container--open {
        z-index: 9999999;
    }

    .ui-select-multiple.ui-select-bootstrap {
        padding: 6px 10px;
    }
</style>
