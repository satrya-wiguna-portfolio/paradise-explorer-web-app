<div class="widget">
    <h4>Blog Recents</h4>
    <ul class="recent_post">
        @foreach($model->blogRecentResults as $blogRecentResult)
            <li>
                <i class="icon-calendar-empty"></i> {{ date('Y, F d', strtotime($blogRecentResult->publish)) }}
                <div><a href="{{ url('blog/detail', ['id' => $blogRecentResult->id]) }}">{{ $blogRecentResult->title }}</a></div>
            </li>
        @endforeach
    </ul>
</div><!-- End widget -->