<div class="dropdown dropdown-cart">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-basket-1"></i>Cart ({{ count(Cart::instance('tour')->content()) + count(Cart::instance('hotel')->content()) }})</a>
    <ul id="cart_items" class="dropdown-menu">
        @if(Cart::instance('tour')->content()->count() != 0)
            <li><b>Tour Package :</b></li>
            @foreach(Cart::instance('tour')->content() as $row)
                <li>
                    <div class="image"><img src="{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($row->options->featured_image_url, '100x100') }}" alt="{{ $row->name }}"></div>
                    <strong>
                        <a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $row->id]) }}"><b>{{ $row->name }}</b></a>
                        @if($row->options->discount != 0 || $row->options->product_discount != 0)
                            <span style="color: red;"><strike><small>{{ currency($row->options->price, currency()->config('default'), Session::get('currency')) }}</small></strike></span>
                            @if($row->options->discount != 0)
                                &nbsp;<span style="color: #79a527;">-{{ $row->options->discount }}%</span>
                            @endif
                            @if($row->options->product_discount != 0)
                                &nbsp;<span style="color: #ffa100;">-{{ $row->options->product_discount }}%</span>
                            @endif
                        @endif
                        <span style="font-size: 14px;">{{ $row->qty }}&nbsp;<small>pax</small>&nbsp;x&nbsp;&nbsp;{{ currency($row->price, currency()->config('default'), Session::get('currency')) }}</span>
                    </strong>
                    <a href="{{ url('cart/delete', ['productTypeId' => 1, 'rowId' => $row->rowId]) }}" class="action cartItem" style="font-size: 16px; margin-top: 10px;"><i class="fa fa-trash"></i></a>
                </li>
            @endforeach
        @endif

        @if(Cart::instance('hotel')->content()->count() != 0)
            <li><b>Hotel Package :</b></li>
            @foreach(Cart::instance('hotel')->content() as $row)
                <li>
                    <div class="image"><img src="{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($row->options->featured_image_url, '100x100') }}" alt="{{ $row->name }}"></div>
                    <strong>
                        <a style="font-size: 14px;" href="{{ url('search/detail', ['id' => $row->id]) }}"><b>{{ $row->name }}</b></a>
                        <span style="font-size: 14px;">{{ $row->qty }}&nbsp;<small>{{ ($row->qty > 1) ? 'nights' : 'night' }}</small>&nbsp;x&nbsp;&nbsp;{{ currency($row->price, currency()->config('default'), Session::get('currency')) }}</span>
                    </strong>
                    <a href="{{ url('cart/delete', ['productTypeId' => 2, 'rowId' => $row->rowId]) }}" class="action cartItem" style="font-size: 16px; margin-top: 10px;"><i class="fa fa-trash"></i></a>
                    {!! html_entity_decode(\App\Helper\PostHelper::cartRoomPop($row->options->discount, $row->options->rooms)) !!}
                </li>
            @endforeach
        @endif

        @if(Cart::instance('tour')->content()->count() != 0 ||
            Cart::instance('hotel')->content()->count() != 0)
            <li>
                <div>Total:&nbsp;<span>{{ currency(Cart::instance('tour')->total() + Cart::instance('hotel')->total(), currency()->config('default'), Session::get('currency')) }}</span></div>
                <a href="{{ url('cart/') }}" class="button_drop">Go to cart</a>
                <a href="{{ url('checkout/contact') }}" class="button_drop outline">Check out</a>
            </li>
        @endif

        @if(Cart::instance('tour')->content()->count() == 0 &&
            Cart::instance('hotel')->content()->count() == 0)
            <li>
                <div style="text-align: center;">Your cart is still empty</div>
            </li>
        @endif
    </ul>
</div>

