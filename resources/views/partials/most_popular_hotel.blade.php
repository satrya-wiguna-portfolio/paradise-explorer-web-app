<div class="red_bg">
    <div class="container margin_60">
        <div class="main_title">
            <h2>Most <span class="white_title">Popular</span> Hotels</h2>
            <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
        </div>

        <div class="row">
            <div id="most-popular-hotel" class="col-md-12 col-sm-12 carousel fdi-Carousel slide">
                <!-- Carousel items -->
                <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                    <div class="carousel-inner onebyone-carosel most_popular_hotel"></div>
                    <a class="left carousel-control" href="#eventCarousel" data-slide="prev"><i class="icon-left-open"></i></a>
                    <a class="right carousel-control" href="#eventCarousel" data-slide="next"><i class="icon-right-open"></i></a>
                </div>
                <!--/carousel-inner-->
            </div><!--/myCarousel-->
        </div><!-- End row -->
    </div>
</div>