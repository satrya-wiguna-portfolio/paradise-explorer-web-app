<div class="widget tags">
    <h4>Blog Tags</h4>
    @foreach($model->blogTagResults as $blogTagResult)
        <a href="{{ url('blog/tag', ['tagId' => $blogTagResult->id]) }}" title="{{ $blogTagResult->slug }}">{{ $blogTagResult->name  }}</a>
    @endforeach
</div><!-- End widget -->