<div class="modal fade" id="booking" role="dialog">
    <div class="modal-dialog modal-lg  modal-booking">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"></h2>
                <i class="fa fa-map-marker"></i>&nbsp;&nbsp;<span class="modal-address"></span>
            </div>
            <div class="modal-body">
                <form role="form" name="formCheckAvailable" method="POST">
                    <div class="form-group row check-room-available">
                        <div class="col-lg-5">
                            <label class="form-control-label text-uppercase">Check In / Check Out</label>
                            <div class="input-group">
                                <input type="text" id="checkInCheckOut" class="form-control check-in-check-out" />
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <label class="form-control-label text-uppercase">Adult</label>
                            <div class="input-group input-group-md spinner" data-trigger="spinner">
                                <input type="text" id="adult" class="form-control text-center" value="1" data-min="1" data-rule="quantity">
                                  <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-caret-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-caret-down"></i></a>
                                  </span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <label class="form-control-label text-uppercase">Child</label>
                            <div class="input-group input-group-md spinner" data-trigger="spinner">
                                <input type="text" id="child" class="form-control text-center" value="0" data-min="0" data-rule="quantity">
                                  <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-caret-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-caret-down"></i></a>
                                  </span>
                            </div>
                        </div>
                        <div class="col-lg-2" style="padding-top: 25px;">
                            <button type="button" class="btn btn-danger check-available"><i class="fa fa-search"></i>&nbsp;&nbsp;Check Available</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 modal-item"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>