@foreach($model->randomLimitProductByProductTypeIdResults as $randomLimitProductByProductTypeIdResult)
    <div class="row">
        <div class="col-md-12 wow zoomIn" data-wow-delay="0.1s">
            <div class="tour_container">
                @if($randomLimitProductByProductTypeIdResult->discount != 0)
                    <div class="ribbon_3 popular"><span>Disc {{ $randomLimitProductByProductTypeIdResult->discount }}%</span></div>
                @endif
                <div class="img_container">
                    <a href="{{ url('search/detail', ['id' => $randomLimitProductByProductTypeIdResult->product_type_id]) }}">
                        <img src="{{ ($randomLimitProductByProductTypeIdResult->featured_image_url != '') ? Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($randomLimitProductByProductTypeIdResult->featured_image_url, '800x533') : 'https://dummyimage.com/800x533/b8b8b8/696969.jpg&text=No+Image' }}" class="img-responsive" alt="{{ $randomLimitProductByProductTypeIdResult->title }}">

                        {{--Discount off label--}}
                        @if(count($randomLimitProductByProductTypeIdResult->room) != 0)
                            @if(count(\App\Helper\PostHelper::roomDiscount($randomLimitProductByProductTypeIdResult->room)) != 0)
                                <div class="badge_save">Off<strong>{{ \App\Helper\PostHelper::minRoomDiscount($randomLimitProductByProductTypeIdResult->room) }}%</strong></div>
                            @endif
                        @endif
                        {{--End discount off label--}}

                        <div class="short_info">
                            <div style="position: absolute; bottom: 0;">{{ $randomLimitProductByProductTypeIdResult->product_category->category }}</div>
                            @if($randomLimitProductByProductTypeIdResult->discount != 0)
                                @if(count($randomLimitProductByProductTypeIdResult->room) != 0)
                                    @if(count(\App\Helper\PostHelper::roomDiscount($randomLimitProductByProductTypeIdResult->room)) != 0)
                                        <span class="price">
                                            <span style="font-size: 14px; color: red;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room), currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $randomLimitProductByProductTypeIdResult->discount + \App\Helper\PostHelper::minRoomDiscount($randomLimitProductByProductTypeIdResult->room) }}%</span><br />
                                            {{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room) - ((($randomLimitProductByProductTypeIdResult->discount + \App\Helper\PostHelper::minRoomDiscount($randomLimitProductByProductTypeIdResult->room)) / 100) * \App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room)), currency()->config('default'), Session::get('currency')) }}
                                        </span>
                                    @else
                                        <span class="price">
                                            <span style="font-size: 14px; color: red;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room), currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $randomLimitProductByProductTypeIdResult->discount }}%</span><br />
                                            {{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room) - (($randomLimitProductByProductTypeIdResult->discount / 100) * \App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room)), currency()->config('default'), Session::get('currency')) }}
                                        </span>
                                    @endif
                                @endif
                            @else
                                @if(count($randomLimitProductByProductTypeIdResult->room) != 0)
                                    @if(count(\App\Helper\PostHelper::roomDiscount($randomLimitProductByProductTypeIdResult->room)) != 0)
                                        <span class="price">
                                            <span style="font-size: 14px; color: red;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room), currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minRoomDiscount($randomLimitProductByProductTypeIdResult->room) }}%</span><br />
                                            {{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room) - ((\App\Helper\PostHelper::minRoomDiscount($randomLimitProductByProductTypeIdResult->room) / 100) * \App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room)), currency()->config('default'), Session::get('currency')) }}
                                        </span>
                                    @else
                                        <span class="price">
                                            {{ currency(\App\Helper\PostHelper::maxRoomPrice($randomLimitProductByProductTypeIdResult->room), currency()->config('default'), Session::get('currency')) }}
                                        </span>
                                    @endif
                                @endif
                            @endif
                        </div>
                    </a>
                </div>
                <div class="tour_title">
                    <h3 style="margin-bottom: 10px;">
                        <strong>{{ str_limit($randomLimitProductByProductTypeIdResult->title, 30) }}</strong>
                    </h3>
                    <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                        <span class="icon-star" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                        <span class="icon-star" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                        <span class="icon-star" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-grey btn-xs"
                            aria-label="Left Align">
                        <span class="icon-star-empty" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-grey btn-xs"
                            aria-label="Left Align">
                        <span class="icon-star-empty" aria-hidden="true"></span>
                    </button>
                </div>
            </div><!-- End box tour -->
        </div><!-- End col-md-4 -->
    </div>
@endforeach