@if($model->productResults->product_way_point->count() > 2)
    <ol class="timeline">
        @foreach($model->productResults->product_way_point as $key => $product_way_point)
            @if($model->productResults->product_way_point->first() == $product_way_point)
                <li class="timeline__step done">
                    <input class="timeline__step-radio" id="trigger1" name="trigger" type="radio">

                    <label class="timeline__step-label" for="trigger1">
                        <span class="timeline__step-content">{{ $product_way_point->title }}</span>
                    </label>

                    <span class="timeline__step-title">Start</span>

                    <i class="timeline__step-marker">
                        <span class="fa fa-map-marker" style="font-size: 30px; margin-top: 5px;"></span>
                    </i>
                </li>
            @endif
        @endforeach

        {!! $post->wayPointFormat($model->productResults->product_way_point) !!}

        @foreach($model->productResults->product_way_point as $key => $product_way_point)
            @if($model->productResults->product_way_point->last() == $product_way_point)
                <li class="timeline__step done">
                    <input class="timeline__step-radio" id="trigger3" name="trigger" type="radio">

                    <label class="timeline__step-label" for="trigger3">
                        <span class="timeline__step-content">{{ $product_way_point->title }}</span>
                    </label>

                    <span class="timeline__step-title">End</span>

                    <i class="timeline__step-marker">
                        <span class="fa fa-flag" style="font-size: 30px; margin-top: 5px;"></span>
                    </i>
                </li>
            @endif
        @endforeach
    </ol>
@elseif($model->productResults->product_way_point->count() == 2)
    <ol class="timeline">
        @foreach($model->productResults->product_way_point as $key => $product_way_point)
            @if($model->productResults->product_way_point->first() == $product_way_point)
                <li class="timeline__step done">
                    <input class="timeline__step-radio" id="trigger1" name="trigger" type="radio">

                    <label class="timeline__step-label" for="trigger1">
                        <span class="timeline__step-content">{{ $product_way_point->title }}</span>
                    </label>

                    <span class="timeline__step-title">Start</span>

                    <i class="timeline__step-marker">
                        <span class="fa fa-map-marker" style="font-size: 30px; margin-top: 5px;"></span>
                    </i>
                </li>
            @endif
        @endforeach

        @foreach($model->productResults->product_way_point as $key => $product_way_point)
            @if($model->productResults->product_way_point->last() == $product_way_point)
                <li class="timeline__step done" style="margin-left: 250px;">
                    <input class="timeline__step-radio" id="trigger2" name="trigger" type="radio">

                    <label class="timeline__step-label" for="trigger2">
                        <span class="timeline__step-content">{{ $product_way_point->title }}</span>
                    </label>

                    <span class="timeline__step-title">End</span>

                    <i class="timeline__step-marker">
                        <span class="fa fa-flag" style="font-size: 30px; margin-top: 5px;"></span>
                    </i>
                </li>
            @endif
        @endforeach
    </ol>
@elseif(count($model->productResults->product_way_point) < 2)
    <!-- Do Nothing -->
@endif