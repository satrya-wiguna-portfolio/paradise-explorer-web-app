<div class="widget" id="cat_blog">
    <h4>Blog Categories</h4>
    <ul>
        @foreach($model->blogCategoryMostlyResults as $blogCategoryMostlyResult)
            @if(count($blogCategoryMostlyResult->blog) != 0)
                <li>
                    <a href="{{ url('blog/category', ['categoryId' => $blogCategoryMostlyResult->id]) }}">{{ $blogCategoryMostlyResult->name }}&nbsp;({{ count($blogCategoryMostlyResult->blog) }})</a>
                </li>
            @endif
        @endforeach
    </ul>
</div><!-- End widget -->
