<div class="white_bg">
    <div class="container margin_60">
        <div class="main_title">
            <h2>Choose Your <span>Destination</span></h2>
            <p>We have the best service</p>
        </div>
        <div class="row">
            @foreach($model->randomDestinationResults as $randomDestinationResult)
                <div class="col-md-3 col-sm-6 text-center">
                    <p>
                        <a href="#"><img src="{{ $randomDestinationResult->image_url }}" alt="Pic" class="img-responsive"></a>
                    </p>
                    <h4>{{ $randomDestinationResult->name }}</h4>
                </div>
            @endforeach
        </div><!-- End row -->
    </div>
</div>