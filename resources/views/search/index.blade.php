@inject('post', 'App\Helper\PostHelper')
@extends('layouts.fluid')

@section('content')
    <div class="container-fluid full-height">
        <div class="row row-height">
            <div class="col-md-7 content-left">
                <div class="row" style="margin-top: 33px; padding-top: 10px; background-color: #ddd;">
                    <div class="col-md-12">
                    @if($model->productCategoryOfTourResults)
                        <form method="GET"
                              action="{{ url('search/tour')  }}">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <input type="text" class="form-control destination" name="destination"
                                               placeholder="Destination" value="{{ app('request')->input('destination') }}">
                                        <form>
                                            <input name="lat" type="hidden" value="{{ app('request')->input('lat') }}"/>
                                            <input name="lng" type="hidden" value="{{ app('request')->input('lng') }}"/>
                                            <input name="type" type="hidden" value="1"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control select2" name="category" style="width: 100%;">
                                            @foreach($model->productCategoryOfTourResults as $productCategoryOfTourResult)
                                                <option @if(app('request')->input('category') == $productCategoryOfTourResult->id) selected @endif value="{{ $productCategoryOfTourResult->id }}">{{ $productCategoryOfTourResult->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="age" style="width: 100%;">
                                            <option @if(app('request')->input('age') == 1) selected @endif value="1">< 20 Years</option>
                                            <option @if(app('request')->input('age') == 2) selected @endif value="2">20 - 30 Years</option>
                                            <option @if(app('request')->input('age') == 3) selected @endif value="3">30 - 40 Years</option>
                                            <option @if(app('request')->input('age') == 4) selected @endif value="4">40 - 50 Years</option>
                                            <option @if(app('request')->input('age') == 5) selected @endif value="5">50 - 60 Years</option>
                                            <option @if(app('request')->input('age') == 6) selected @endif value="6">> 60 Years</option>
                                            <option @if(app('request')->input('age') == 7) selected @endif @if(!app('request')->input('age')) selected @endif value="7">All</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn_1 green" style="width: 100%;"><i class="icon-search"></i>Find Tour</button>
                                </div>
                            </div><!-- End row -->
                        </form>
                    @endif

                    @if($model->productCategoryOfHotelResults)
                        <form method="GET"
                              action="{{ url('search/hotel')  }}">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control destination" name="destination"
                                               placeholder="Destination" value="{{ app('request')->input('destination') }}">
                                        <form>
                                            <input name="lat" type="hidden" value="{{ app('request')->input('lat') }}"/>
                                            <input name="lng" type="hidden" value="{{ app('request')->input('lng') }}"/>
                                            <input name="type" type="hidden" value="2"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control select2" name="category" style="width: 100%;">
                                            @foreach($model->productCategoryOfHotelResults as $productCategoryOfHotelResult)
                                                <option @if(app('request')->input('category') == $productCategoryOfTourResult->id) selected @endif value="{{ $productCategoryOfHotelResult->id }}">{{ $productCategoryOfHotelResult->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn_1 green" style="width: 100%;"><i class="icon-search"></i>Find Hotel</button>
                                </div>
                            </div>
                        </form>
                    @endif
                    </div>
                </div>
                <br />
                <div class="row">
                    @if(count($model->productResults) > 0)
                        <?php $i = 0; ?>
                        @foreach($model->productResults as $key => $productResult)
                            <div class="col-md-6 col-sm-6">
                                <div class="tour_container">
                                    @if($productResult->discount != 0)
                                        <div class="ribbon_3 popular"><span>Disc {{ $productResult->discount }}%</span></div>
                                    @endif
                                    <div class="img_container">
                                        <a href="{{ url('search/detail', ['id' => $productResult->id]) }}">
                                            <img src="@if($productResult->featured_image_url) {{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($productResult->featured_image_url, '800x533') }} @else https://dummyimage.com/800x533/b8b8b8/696969.jpg&text=No+Image @endif"
                                                 class="img-responsive" alt="{{ $productResult->title }}">

                                            {{--Discount off label--}}
                                            @if($model->type == 1)
                                                @if(count($productResult->product_discount) != 0)
                                                    <div class="badge_save">Off<strong>{{ \App\Helper\PostHelper::minProductDiscount($productResult->product_discount) }}%</strong></div>
                                                @endif
                                            @endif

                                            @if($model->type == 2)
                                                @if(count($productResult->room) != 0)
                                                    @if(count(\App\Helper\PostHelper::roomDiscount($productResult->room)) != 0)
                                                        <div class="badge_save">Off<strong>{{ \App\Helper\PostHelper::minRoomDiscount($productResult->room) }}%</strong></div>
                                                    @endif
                                                @endif
                                            @endif
                                            {{--End discount off label--}}

                                            <div class="short_info tour">
                                                <div style="position: absolute; bottom: 0;">{{ $productResult->product_category->category }}</div>
                                                @if($model->type == 1)
                                                    @if($productResult->discount != 0)
                                                        @if(count($productResult->product_discount) != 0)
                                                            <span class="price">
                                                                <span style="font-size: 14px; color: red;"><strike>{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $productResult->discount + \App\Helper\PostHelper::minProductDiscount($productResult->product_discount) }}%</span><br />
                                                                {{ currency($productResult->price - ((($productResult->discount + \App\Helper\PostHelper::minProductDiscount($productResult->product_discount)) / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}
                                                            </span>
                                                        @else
                                                            <span class="price">
                                                                <span style="font-size: 14px; color: red;"><strike>{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $productResult->discount }}%</span><br />
                                                                {{ currency($productResult->price - (($productResult->discount / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}
                                                            </span>
                                                        @endif
                                                    @else
                                                        @if(count($productResult->product_discount) != 0)
                                                            <span class="price">
                                                                <span style="font-size: 14px; color: red;"><strike>{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minProductDiscount($productResult->product_discount) }}%</span><br />
                                                                {{ currency($productResult->price - ((\App\Helper\PostHelper::minProductDiscount($productResult->product_discount) / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}
                                                            </span>
                                                        @else
                                                            <span class="price">
                                                                {{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}
                                                            </span>
                                                        @endif
                                                    @endif
                                                @endif

                                                @if($model->type == 2)
                                                    @if($productResult->discount != 0)
                                                        @if(count($productResult->room) != 0)
                                                            @if(count(\App\Helper\PostHelper::roomDiscount($productResult->room)) != 0)
                                                                <span class="price">
                                                                    <span style="font-size: 14px; color: red;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room), currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $productResult->discount + \App\Helper\PostHelper::minRoomDiscount($productResult->room) }}%</span><br />
                                                                    {{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room) - ((($productResult->discount + \App\Helper\PostHelper::minRoomDiscount($productResult->room)) / 100) * \App\Helper\PostHelper::maxRoomPrice($productResult->room)), currency()->config('default'), Session::get('currency')) }}
                                                                </span>
                                                            @else
                                                                <span class="price">
                                                                    <span style="font-size: 14px; color: red;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room), currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $productResult->discount }}%</span><br />
                                                                    {{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room) - (($productResult->discount / 100) * \App\Helper\PostHelper::maxRoomPrice($productResult->room)), currency()->config('default'), Session::get('currency')) }}
                                                                </span>
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if(count($productResult->room) != 0)
                                                            @if(count(\App\Helper\PostHelper::roomDiscount($productResult->room)) != 0)
                                                                <span class="price">
                                                                    <span style="font-size: 14px; color: red;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room), currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minRoomDiscount($productResult->room) }}%</span><br />
                                                                    {{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room) - ((\App\Helper\PostHelper::minRoomDiscount($productResult->room) / 100) * \App\Helper\PostHelper::maxRoomPrice($productResult->room)), currency()->config('default'), Session::get('currency')) }}
                                                                </span>
                                                            @else
                                                                <span class="price">
                                                                    {{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room), currency()->config('default'), Session::get('currency')) }}
                                                                </span>
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                    <div class="tour_title">
                                        <h3 style="margin-bottom: 10px;">
                                            <strong>{{ str_limit($productResult->title, 30) }}</strong>
                                        </h3>

                                        {{--Show product rating--}}
                                        {!! html_entity_decode(\App\Helper\PostHelper::productRating($productResult->product_comment)) !!}

                                        <div class="agent">
                                            <a href="#"><strong>{{ $productResult->agent->company }}</strong></a>
                                            <button type="button" class="btn btn-danger pull-right booking"
                                                    data-product="{{ \App\Helper\PostHelper::dataProduct($model->type, $productResult) }}">
                                                <i class="fa fa-calendar "></i>&nbsp;&nbsp;Book Now
                                            </button>
                                        </div>
                                        <div onclick="onHtmlClick('Walking', parseFloat({{ $i }}))" class="view_on_map">
                                            View on map
                                        </div>
                                    </div>
                                </div><!-- End box tour -->
                            </div><!-- End col-md-6 -->
                            <?php $i++; ?>
                        @endforeach
                    @endif
                </div>

                <hr/>

                @include('pagination.custom', ['paginator' => $model->productResults])
            </div>
            <div class="col-md-5 map-right hidden-sm hidden-xs">
                <div class="map" id="map"></div>
            </div>
        </div>
    </div>
    {{--Popup booking--}}
    @if($model->type == 1)
        @include('partials.product_booking')
    @endif

    @if($model->type == 2)
        @include('partials.room_booking')
    @endif
    {{--End popup booking--}}
@endsection

@section('addons')
    <script type='text/javascript'
            src='https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD3zQ7PjQrnblkxp4j05pit8OxFNLgT0QA'></script>
    <script type='text/javascript' src="{{ asset('assets/common/js/infobox.js') }}"></script>
    <script type='text/javascript' src='{{ asset('assets/common/js/bootstrap-number-input.js') }}'></script>
    <script type='text/javascript' src="{{ asset('assets/common/js/geocomplete/jquery.geocomplete.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/moment/moment.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/jquery.spinner/dist/js/jquery.spinner.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/jquery-accordion/js/jquery.accordion.js') }}"></script>
    <script type='text/javascript' src='{{ asset('assets/vendors/unitegallery/dist/js/unitegallery.js') }}'></script>
    <script type='text/javascript' src='{{ asset('assets/vendors/gasparesganga-jquery-loading-overlay/src/loadingoverlay.min.js') }}'></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/select2/dist/js/select2.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/jquery.spinner/dist/css/bootstrap-spinner.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/jquery-accordion/css/jquery.accordion.css') }}" />
    <link rel='stylesheet' type='text/css' href='{{ asset('assets/vendors/unitegallery/dist/css/unite-gallery.css') }}' />

    <!-- Unitegallery Themes -->
    <script type='text/javascript' src='{{ asset('assets/vendors/unitegallery/dist/themes/default/ug-theme-default.js') }}'></script>
    <link rel='stylesheet' href='{{ asset('assets/vendors/unitegallery/dist/themes/default/ug-theme-default.css') }}' type='text/css' />

    <style type="text/css">
        html, body {
            height: 100%;
        }
    </style>

    <script type="text/javascript">
        var product;
        var departure = moment(new Date()).format('YYYY-MM-DD');
        var checkIn = moment(new Date()).format('YYYY-MM-DD');
        var checkOut = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');

        var checkAvailability = function () {
            $('.modal-item').empty();

//          If product type is tour
            @if($model->type == 1)
                product.departure = departure;
                product.adult = $('#adult').val();
                product.child = $('#child').val();

                var totalDiscount = 0;
                var productDiscount = '';
                var productPrice = '';

                if (product.discount != 0) {
                    if (product.product_discount != 0) {
                        totalDiscount = parseFloat(product.discount) + parseFloat(product.product_discount);
                        productDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                        productPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + product.price_convert + '</strike>' + productDiscount + '</h4>' +
                                '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                    } else {
                        totalDiscount = parseFloat(product.discount);
                        productDiscount = '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                        productPrice = '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + product.price_convert + '</strike>' + productDiscount + '</h4>' +
                                '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                    }
                } else {
                    if (product.product_discount != 0) {
                        totalDiscount = parseFloat(product.product_discount);
                        productDiscount = '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                        productPrice = '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + product.price_convert + '</strike>' + productDiscount + '</h4>' +
                                '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                    } else {
                        productPrice = '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                    }
                }

                var productItem = '<div class="item">' +
                        '<div class="row">' +
                        '<div class="col-md-9">' +
                        '<h4>Overview</h4>' +
                        '<p class="item-description">' + product.overview + '</p>' +
                        '<strong>Min Age:</strong>&nbsp;' + product.min_age + '<small>&nbsp;<i>years old</i></small>' +
                        '&nbsp;&nbsp;<strong>Max Age:</strong>&nbsp;' + product.max_age + '<small>&nbsp;<i>years old</i></small>' +
                        '<br /><strong>Duration:</strong>&nbsp;' + durationConvert(product.duration) +
                        '</div>' +
                        '<div class="col-md-2"><br />' +
                        productPrice +
                        '</div>' +
                        '</div>' +
                        '</div><br />';

                $('.modal-item').append(productItem);
                $('.modal-item').append('<button type="submit" class="btn btn-success btn-block"><i class="fa fa-cart"></i>&nbsp;&nbsp;Add to Cart</button>');
                $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');

                $('.modal-content').LoadingOverlay("hide", true);
            @endif


//          If product ype is hotel
            @if($model->type == 2)
                product.client_id = localStorage.clientId;
                product.check_in = checkIn;
                product.check_out = checkOut;
                product.adult = $('#adult').val();
                product.child = $('#child').val();

                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: localStorage.baseUrl + '/search/checkRoomAvailability',
                    data: product,
                    crossDomain: true,
                    success: function (results) {
                        $.each(results.dto, function(index, value) {
                            var facilities = value.facility;
                            var beds = value.bed;
                            var roomImages = value.room_image;

                            var facility = '';

                            $.each(facilities, function(index, value) {
                                if (index != (facilities.length - 1)) {
                                    facility += value.facility + ', ';
                                } else {
                                    facility += value.facility;
                                }
                            });

                            var bed = '';

                            $.each(beds, function(index, value) {
                                if (index != (beds.length - 1)) {
                                    bed += value.name + ', ';
                                } else {
                                    bed += value.name;
                                }
                            });

                            var roomImage = '';

                            roomImage += '<div id="gallery_' + index + '">';

                            $.each(roomImages, function(index, value) {
                                roomImage += '<img alt="' + value.alt + '" src="' + localStorage.baseUrl + '/' + value.url + '" data-image="' + localStorage.baseUrl + '/' + value.url + '" data-description="' + value.caption + '">';
                            });

                            roomImage += '</div>';

                            var totalDiscount = 0;
                            var roomDiscount = '';
                            var roomPrice = '';

                            if (product.discount != 0) {
                                if (value.room_discount != 0) {
                                    totalDiscount = parseFloat(product.discount) + parseFloat(value.room_discount);
                                    roomDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                                    roomPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + value.price_convert + '</strike>' + roomDiscount + '</h4>' +
                                            '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                            '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                            '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                            '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                } else {
                                    totalDiscount = parseFloat(product.discount);
                                    roomDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                                    roomPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + value.price_convert + '</strike>' + roomDiscount + '</h4>' +
                                            '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                            '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                            '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                            '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                }
                            } else {
                                if (value.room_discount != 0) {
                                    totalDiscount = parseFloat(value.room_discount);
                                    roomDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                                    roomPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + value.price_convert + '</strike>' + roomDiscount + '</h4>' +
                                            '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                            '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                            '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                            '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                } else {
                                    roomPrice += '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                            '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                            '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                            '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                }
                            }

                            var roomQuantity = '';

                            if (value.room_available_by_quantity > 0 && (value.room_available_by_date >= value.long_stay)) {
                                roomQuantity = '<br /><input id="qty_' + index + '" type="number" class="form-control text-center quantity" value="0" min="0" max="' + value.room_available_by_quantity + '">';
                            } else {
                                roomQuantity = '<br /><h5><strong>Not Available</strong></h5>';
                            }

                            var roomItem = '<div class="item">' +
                                    '<div class="row">' +
                                    '<div class="col-md-9">' +
                                    '<h4>' + value.title + '</h4>' +
                                    '<p class="item-description">' + value.description + '</p>' +
                                    '<strong>Adult:</strong>&nbsp;' + value.adult +
                                    '&nbsp;&nbsp;<strong>Child:</strong>&nbsp;' + value.child +
                                    '&nbsp;&nbsp;<strong>Max:</strong>&nbsp;' + value.max +
                                    '<br /><strong>Facilities:</strong>&nbsp;' + facility +
                                    '<br /><strong>Beds:</strong>&nbsp;' + bed +
                                    '</div>' +
                                    '<div class="col-md-2">' +
                                    roomPrice +
                                    roomQuantity +
                                    '<input type="hidden" id="id_' + index + '" value="' + value.id + '">' +
                                    '<input type="hidden" id="title_' + index + '" value="' + value.title + '">' +
                                    '</div>' +
                                    '</div>' +
                                    '</div><br />' +
                                    '<div class="row">' +
                                    '<div data-accordion-group class="col-md-12" style="margin-bottom: 20px;">' +
                                    '<div class="accordion" data-accordion>' +
                                    '<div data-control>Gallery</div>' +
                                    '<div data-content>' +
                                    '<div>' +
                                    roomImage +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="accordion" data-accordion>' +
                                    '<div data-control>Option</div>' +
                                    '<div data-content>' +
                                    '<div>' +
                                    value.option +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="accordion" data-accordion>' +
                                    '<div data-control>Policy</div>' +
                                    '<div data-content>' +
                                    '<div>' +
                                    value.policy +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                            $('.modal-item').append(roomItem);

                            $('#gallery_' + index).unitegallery();
                            $('#qty_' + index).bootstrapNumber({
                                upClass: 'default',
                                downClass: 'default'
                            });
                        });

                        if (results.dto.length > 0) {
                            $('.modal-item').append('<button type="submit" class="btn btn-success btn-block"><i class="fa fa-cart"></i>&nbsp;&nbsp;Add to Cart</button>');
                        }

                        $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');

                        $('.accordion').accordion();

                        $('.modal-content').LoadingOverlay("hide", true);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            @endif
        }

        var durationConvert = function (hours) {
            var duration_convert = '';
            var units = {
                "year": 24 * 365,
                "month": 24 * 30,
                "week": 24 * 7,
                "day": 24,
                "hour": 1
            }

            for (var key in units) {
                if (units.hasOwnProperty(key)) {
                    switch (key) {
                        case "year":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Years</i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Year</i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        case "month":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Months</i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Month</i>&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        case "week":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Weeks</i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Week</i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        case "day":
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Days</i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.floor(hours / units[key]) + '&nbsp;<i>Day</i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;

                        default:
                            if (Math.floor(hours / units[key]) > 1) {
                                duration_convert += Math.round(hours / units[key]) + '&nbsp;<i>Hours</i>&nbsp;&nbsp;';
                            } else if (Math.floor(hours / units[key]) == 1) {
                                duration_convert += Math.round(hours / units[key]) + '&nbsp;<i>Hour</i>&nbsp;&nbsp;';
                            } else {
                                //Do Nothing
                            }

                            hours = (hours % units[key]);
                            break;
                    }
                }
            }

            return duration_convert;
        };

        $('.check-in-check-out').daterangepicker({
            "startDate": moment(new Date()).add(1, 'days'),
            "endDate": moment(new Date()).add(2, 'days'),
            'minDate': moment(new Date()).add(1, 'days'),
        }, function(start, end) {
            checkIn = start.format('YYYY-MM-DD');
            checkOut = end.format('YYYY-MM-DD');

            $('.modal-item').empty();
            $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');
        });

        $('.departure').daterangepicker({
            "singleDatePicker": true,
            'minDate': moment(new Date()).add(1, 'days'),
        }, function(start) {
            departure = start.format('YYYY-MM-DD');
        });

        $('.booking').click(function() {
            $('.modal-content').LoadingOverlay("show");

            product = $(this).data('product');

            $('#booking').modal();

            $('.modal-title').empty();
            $('.modal-address').empty();

            $('.modal-title').append(product.title);
            $('.modal-address').append(product.address);

            checkAvailability();
        });

        $('.check-available').click(function() {
            $('.modal-content').LoadingOverlay("show");

            checkAvailability()
        });

        $('form[name="formCheckAvailable"]').submit(function(event) {
//          If product type is tour
            if (product.product_type_id == 1) {
                product.departure = departure;
                product.adult = $('#adult').val();
                product.child = $('#child').val();

                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: localStorage.baseUrl + '/cart/add',
                    data: product,
                    crossDomain: true,
                    success: function () {
                        location.reload();
                    },
                    error: function () {
                    }
                });

                event.preventDefault();
            }


//          If product type is hotel
            if (product.product_type_id == 2) {
                product.client_id = localStorage.clientId;
                product.check_in = checkIn;
                product.check_out = checkOut;
                product.adult = $('#adult').val();
                product.child = $('#child').val();

                var rooms = [];
                var quantities = 0;

                $('.item').each(function(index) {
                    if ($('#qty_' + index).length) {
                        quantities = quantities + parseInt($('#qty_' + index).val());
                    }
                });

                if (quantities == 0) {
                    swal('Error Message', 'You haven\'t selected the room quantity', 'error');
                }

                if (quantities != 0) {
                    $('.item').each(function(index) {
                        var room = {};

                        if ($('#qty_' + index).length && parseInt($('#qty_' + index).val()) != 0) {
                            room.id = parseInt($('#id_' + index).val());
                            room.title = $('#title_' + index).val();
                            room.qty = parseInt($('#qty_' + index).val());
                            room.room_discount = parseFloat($('#room_discount_' + index).val());
                            room.price = parseFloat($('#price_' + index).val());
                            room.price_discount = parseFloat($('#price_discount_' + index).val());

                            rooms.push(room);
                        }
                    });

                    product.rooms = rooms;

                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: localStorage.baseUrl + '/cart/add',
                        data: product,
                        crossDomain: true,
                        success: function () {
                            location.reload();
                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });
                }

                event.preventDefault();
            }
        });

        $(document).ready(function() {
            $(".destination").geocomplete({details: "form"});

            $(".select2").select2();
        });

//        Mapping process
        (function (A) {
            if (!Array.prototype.forEach)
                A.forEach = A.forEach || function (action, that) {
                            for (var i = 0, l = this.length; i < l; i++)
                                if (i in this)
                                    action.call(that, this[i], i, this);
                        };
        })(Array.prototype);

        var points = [];
        var mapObject;
        var markers = [];
        var markersData = {
            'Walking': points
        };
        var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(parseFloat({{ $model->lat }}), parseFloat({{ $model->lng }})),
            mapTypeId: google.maps.MapTypeId.ROADMAP,

            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            panControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            scrollwheel: false,
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            styles: [{
                "featureType": "landscape",
                "stylers": [{
                    "hue": "#FFBB00"
                }, {
                    "saturation": 43.400000000000006
                }, {
                    "lightness": 37.599999999999994
                }, {
                    "gamma": 1
                }]
            }, {
                "featureType": "road.highway",
                "stylers": [{
                    "hue": "#FFC200"
                }, {
                    "saturation": -61.8
                }, {
                    "lightness": 45.599999999999994
                }, {
                    "gamma": 1
                }]
            }, {
                "featureType": "road.arterial",
                "stylers": [{
                    "hue": "#FF0300"
                }, {
                    "saturation": -100
                }, {
                    "lightness": 51.19999999999999
                }, {
                    "gamma": 1
                }]
            }, {
                "featureType": "road.local",
                "stylers": [{
                    "hue": "#FF0300"
                }, {
                    "saturation": -100
                }, {
                    "lightness": 52
                }, {
                    "gamma": 1
                }]
            }, {
                "featureType": "water",
                "stylers": [{
                    "hue": "#0078FF"
                }, {
                    "saturation": -13.200000000000003
                }, {
                    "lightness": 2.4000000000000057
                }, {
                    "gamma": 1
                }]
            }, {
                "featureType": "poi",
                "stylers": [{
                    "hue": "#00FF6A"
                }, {
                    "saturation": -1.0989010989011234
                }, {
                    "lightness": 11.200000000000017
                }, {
                    "gamma": 1
                }]
            }]
        };
        var marker;

        @foreach($model->productResults as $productResult)
            points.push({
            name: '{{ $productResult->title }}',
            location_latitude: parseFloat({{ $productResult->lat }}),
            location_longitude: parseFloat({{ $productResult->lng }}),
            map_image_url: @if($productResult->featured_image_url) '{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($productResult->featured_image_url, '300x200') }}' @else 'https://dummyimage.com/300x200/b8b8b8/696969.jpg&text=No+Image' @endif,
            name_point: '{{ $productResult->title }}',
            description_point: '{!! html_entity_decode($post->readMoreRender($productResult->overview)) !!}',
            get_directions_start_address: '{{ $productResult->address }}',
            price_point: @if($model->type != 2)
                            @if($productResult->discount != 0)
                                @if(count($productResult->product_discount) != 0)
                                    '{{ currency($productResult->price - (((\App\Helper\PostHelper::minProductDiscount($productResult->product_discount) + $productResult->discount) / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}',
                                @else
                                    '{{ currency($productResult->price - (($productResult->discount / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}',
                                @endif
                            @else
                                '{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}',
                            @endif
                         @else
                            @if(count($productResult->room) != 0)
                                @if($productResult->discount != 0)
                                    '{{ currency((\App\Helper\PostHelper::maxRoomPrice($productResult->room) - (($productResult->discount / 100) * \App\Helper\PostHelper::maxRoomPrice($productResult->room))), currency()->config('default'), Session::get('currency')) }}',
                                @else
                                    '{{ currency(\App\Helper\PostHelper::maxRoomPrice($productResult->room), currency()->config('default'), Session::get('currency')) }}',
                                @endif
                            @else
                                '0',
                            @endif
                         @endif
            duration_point: durationConvert({{ $productResult->duration }}),
            url_point: '{{ url('search/detail', ['id' => $productResult->id]) }}'
        });
        @endforeach

        mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);

        for (var key in markersData) {
            markersData[key].forEach(function (item) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                    map: mapObject,
                    icon: '../../assets/common/images/pins/' + key + '.png',
                });

                if ('undefined' === typeof markers[key])
                    markers[key] = [];

                markers[key].push(marker);

                google.maps.event.addListener(marker, 'click', (function () {
                    closeInfoBox();
                    getInfoBox(item).open(mapObject, this);
                    mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
                }));


            });
        }

        function hideAllMarkers() {
            for (var key in markers)
                markers[key].forEach(function (marker) {
                    marker.setMap(null);
                });
        }

        function toggleMarkers(category) {
            hideAllMarkers();
            closeInfoBox();

            if ('undefined' === typeof markers[category])
                return false;

            markers[category].forEach(function (marker) {
                marker.setMap(mapObject);
                marker.setAnimation(google.maps.Animation.DROP);

            });
        }

        function closeInfoBox() {
            $('div.infoBox').remove();
        }

        function getInfoBox(item) {
            return new InfoBox({
                content: '<div class="marker_info_2">' +
                '<img src="' + item.map_image_url + '" alt="' + item.name_point + '"/>' +
                '<h3 style="padding: 0px 5px 0px 5px;">' + item.name_point + '</h3>' +
                '<span>' + item.description_point + '</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="' + item.get_directions_start_address + '" type="hidden"><input type="hidden" name="daddr" value="' + item.location_latitude + ',' + item.location_longitude + '"><button type="submit" value="Get directions" class="btn_infobox_get_directions">' + item.get_directions_start_address + '</button></form><br />' +
                '<a href="#" class="btn_infobox_price">' + item.price_point + '</a>' +
                '<a href="#" class="btn_infobox_duration">' + item.duration_point + '</a>' +
                '</div>' +
                '<a href="' + item.url_point + '" class="btn_infobox">Details</a>' +
                '<br style="clear: both;" /><br style="clear: both;" />' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        }

        function onHtmlClick(location_type, key) {
            google.maps.event.trigger(markers[location_type][key], "click");
        }
    </script>
@endsection