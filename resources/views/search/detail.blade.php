@inject('post', 'App\Helper\PostHelper')
@extends('layouts.default')

@section('content')

    @if($model->productResults->featured_video_url)
    <section class="parallax-window header-video" data-parallax="scroll">
        <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h1 class="title-detail">{{ $model->productResults->title }}</h1>
                        <span>{{ $model->productResults->product_category->category }}</span><br />
                        {{--Show product rating--}}
                        {!! html_entity_decode(\App\Helper\PostHelper::productRating($model->productResults->product_comment)) !!}
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div id="price_single_main" class="row">
                            <div class="col-md-12">
                                {{--If product type id tour--}}
                                @if($model->productResults->product_type_id == 1)
                                    @if($model->productResults->discount != 0)
                                        @if(count($model->productResults->product_discount) != 0)
                                            <span style="font-size: 16px; color: #ff0000; text-shadow: 1px 1px 2px #000000;"><strike>{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</strike></span>
                                            <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) + $model->productResults->discount }}%</span><br />
                                            <span class="price" style="line-height: 30px;">{{ currency($model->productResults->price - (((\App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) + $model->productResults->discount) / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @else
                                            <span style="font-size: 16px; color: #ff0000; text-shadow: 1px 1px 2px #000000"><strike>{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</strike></span><br />
                                            <span class="price" style="line-height: 30px;">{{ currency($model->productResults->price - (($model->productResults->discount / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @endif
                                    @else
                                        @if(count($model->productResults->product_discount) != 0)
                                            <span style="font-size: 16px; color: #ff0000; text-shadow: 1px 1px 2px #000000"><strike>{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</strike></span>
                                            <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) }}%</span><br />
                                            <span class="price" style="line-height: 30px;">{{ currency($model->productResults->price - ((\App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @else
                                            <span class="price">{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @endif
                                    @endif
                                @endif

                                {{--If product type is hotel--}}
                                @if($model->productResults->product_type_id == 2)
                                    @if($model->productResults->discount != 0)
                                        @if(count($model->productResults->room) != 0)
                                            @if(count(\App\Helper\PostHelper::roomDiscount($model->productResults->room)) != 0)
                                                <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</strike></span>
                                                <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $model->productResults->discount + \App\Helper\PostHelper::minRoomDiscount($model->productResults->room) }}%</span><br />
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room) - ((($model->productResults->discount + \App\Helper\PostHelper::minRoomDiscount($model->productResults->room)) / 100) * \App\Helper\PostHelper::maxRoomPrice($model->productResults->room)), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @else
                                                <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</strike></span>
                                                <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $model->productResults->discount }}%</span><br />
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room) - (($model->productResults->discount / 100) * \App\Helper\PostHelper::maxRoomPrice($model->productResults->room)), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @endif
                                        @endif
                                    @else
                                        @if(count($model->productResults->room) != 0)
                                            @if(count(\App\Helper\PostHelper::roomDiscount($model->productResults->room)) != 0)
                                                <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</strike></span>
                                                <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minRoomDiscount($model->productResults->room) }}%</span><br />
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room) - ((\App\Helper\PostHelper::minRoomDiscount($model->productResults->room) / 100) * \App\Helper\PostHelper::maxRoomPrice($model->productResults->room)), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @else
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @endif
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row visible-md visible-lg">
                            <div class="col-md-12">
                                <a class="btn btn-success pull-right booking" data-product="{{ \App\Helper\PostHelper::dataProduct($model->productResults->product_type_id, $model->productResults) }}"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Booking</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img alt="Image"
             class="header-video-media"
             data-video-src="{{ $post->videoCodeFormat($model->productResults->featured_video_url) }}"
             data-teaser-source=""
             data-provider="youtube"
             data-video-width="1400"
             data-video-height="470">
    </section><!-- End section -->
    @else
    <section class="parallax-window" data-parallax="scroll"
             data-image-src="{{ ($model->productResults->featured_image_url) ? Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($model->productResults->featured_image_url, '1400x470') : 'https://dummyimage.com/1400x470/b8b8b8/696969.jpg&text=No+Image' }}"
             data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h1 class="title-detail">{{ $model->productResults->title }}</h1>
                        <span>{{ $model->productResults->product_category->category }}</span><br />
                        {{--Show product rating--}}
                        {!! html_entity_decode(\App\Helper\PostHelper::productRating($model->productResults->product_comment)) !!}
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div id="price_single_main" class="row">
                            <div class="col-md-12">
                                {{--If product type id tour--}}
                                @if($model->productResults->product_type_id == 1)
                                    @if($model->productResults->discount != 0)
                                        @if(count($model->productResults->product_discount) != 0)
                                            <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</strike></span>
                                            <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) }}%</span><br />
                                            <span class="price" style="line-height: 30px;">{{ currency($model->productResults->price - (((\App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) + $model->productResults->discount) / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @else
                                            <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</strike></span><br />
                                            <span class="price" style="line-height: 30px;">{{ currency($model->productResults->price - (($model->productResults->discount / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @endif
                                    @else
                                        @if(count($model->productResults->product_discount) != 0)
                                            <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</strike></span>
                                            <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) }}%</span><br />
                                            <span class="price" style="line-height: 30px;">{{ currency($model->productResults->price - ((\App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @else
                                            <span class="price">{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}</span><br />
                                            <span style="font-size: 14px; color: #ffffff;">per/ Pax</span><br />
                                        @endif
                                    @endif
                                @endif

                                {{--If product type is hotel--}}
                                @if($model->productResults->product_type_id == 2)
                                    @if($model->productResults->discount != 0)
                                        @if(count($model->productResults->room) != 0)
                                            @if(count(\App\Helper\PostHelper::roomDiscount($model->productResults->room)) != 0)
                                                <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</strike></span>
                                                <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $model->productResults->discount + \App\Helper\PostHelper::minRoomDiscount($model->productResults->room) }}%</span><br />
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room) - ((($model->productResults->discount + \App\Helper\PostHelper::minRoomDiscount($model->productResults->room)) / 100) * \App\Helper\PostHelper::maxRoomPrice($model->productResults->room)), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @else
                                                <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</strike></span>
                                                <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $model->productResults->discount }}%</span><br />
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room) - (($model->productResults->discount / 100) * \App\Helper\PostHelper::maxRoomPrice($model->productResults->room)), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @endif
                                        @endif
                                    @else
                                        @if(count($model->productResults->room) != 0)
                                            @if(count(\App\Helper\PostHelper::roomDiscount($model->productResults->room)) != 0)
                                                <span style="font-size: 16px; color: #ff0000;"><strike>{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</strike></span>
                                                <span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minRoomDiscount($model->productResults->room) }}%</span><br />
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room) - ((\App\Helper\PostHelper::minRoomDiscount($model->productResults->room) / 100) * \App\Helper\PostHelper::maxRoomPrice($model->productResults->room)), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @else
                                                <span class="price" style="line-height: 30px;">{{ currency(\App\Helper\PostHelper::maxRoomPrice($model->productResults->room), currency()->config('default'), Session::get('currency')) }}</span><br />
                                                <span style="font-size: 14px; color: #ffffff;">per/ Night</span><br />
                                            @endif
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row visible-md visible-lg">
                            <div class="col-md-12">
                                <a class="btn btn-success pull-right booking" data-product="{{ \App\Helper\PostHelper::dataProduct($model->productResults->product_type_id, $model->productResults) }}"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Booking</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End section -->
    @endif

    <div id="position">
        <div class="container">
            {!! Breadcrumbs::render('searchDetail', $model->productResults) !!}
        </div>
    </div><!-- End Position -->

    <div class="collapse" id="collapseMap">
        <div id="location" class="location"></div>
    </div><!-- End Map -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-8" id="single_tour_desc">

                <p class="visible-sm visible-xs">
                    <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View onmap</a>
                </p><!-- Map button for tablets/mobiles -->

                <div class="btn-pref btn-group btn-group-justified btn-group-lg detail" role="group" aria-label="..." style="border-bottom: solid 2px #e04f67;">
                    <div class="btn-group" role="group">
                        <button type="button" id="detail" class="btn btn-danger no-rounded" href="#tabDetail"
                                data-toggle="tab">Detail
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="description" class="btn btn-default no-rounded" href="#tabDescription"
                                data-toggle="tab">Description
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="picture" class="btn btn-default no-rounded" href="#tabPicture"
                                data-toggle="tab">Picture
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="video" class="btn btn-default no-rounded" href="#tabVideo"
                                data-toggle="tab">Video
                        </button>
                    </div>
                </div>
                <div class="product-detail tab-content">
                    <div class="tab-pane fade in active" id="tabDetail">
                        <div style="border: 20px solid #f5f5f5; background-color: #ffffff; padding: 10px; margin-top: 25px;">
                            @if($model->productResults->overview)
                            <div class="row">
                                <div class="col-md-12">
                                    {!! html_entity_decode($model->productResults->overview) !!}
                                    <hr />
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <h4><strong>Address :</strong></h4>
                                    @if($model->productResults->address)
                                        {{ $model->productResults->address }}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <h4><strong>Discount :</strong></h4>
                                    @if($model->productResults->discount != 0)
                                        {{ $model->productResults->discount }}%
                                    @else
                                        N/A
                                    @endif
                                </div>
                            </div>

                            {{--If product type is tour--}}
                            @if($model->productResults->product_type->id == 1)
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Duration :</strong></h4>
                                        {!! html_entity_decode(\App\Helper\PostHelper::durationConvert($model->productResults->duration)) !!}
                                    </div>
                                    <div class="col-md-6">
                                        <h4><strong>Age Range :</strong></h4>
                                        {{ $post->ageRangeFormat($model->productResults->min_age, $model->productResults->max_age) }}
                                    </div>
                                </div>
                            @endif

                            {{--If product type is hotel--}}
                            @if($model->productResults->product_type->id == 2)
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Grade :</strong></h4>
                                        <select id="grade" name="grade">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <h4><strong>Property Type :</strong></h4>
                                        {{ $model->productResults->property_type->name }}
                                    </div>
                                </div>
                            @endif
                        </div>

                        @if(count($model->productResults->facility) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><strong>Facilities :</strong></h4>
                                    @foreach($model->productResults->facility as $key => $facility)
                                        <button type="button" class="btn btn-primary">{{ $facility->facility }} <span class="badge"><img src="{{ Config::get('app.url') . $facility->image_url }}" width="15"/></span></button>&nbsp;
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane fade in" id="tabDescription">
                        <div style="border: 20px solid #f5f5f5; background-color: #ffffff; padding: 10px; margin-top: 25px;">
                            @if($model->productResults->description)
                                {!! html_entity_decode($model->productResults->description) !!}
                            @else
                                N/A
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="tabPicture">
                        @if(count($model->productResults->product_image) > 0)
                            <div id="gallery" style="display:none;">
                                @foreach($model->productResults->product_image as $product_image)
                                    <img alt="{{ $product_image->alt }}" src="{{ Config::get('app.url') . $product_image->url }}"
                                         data-image="{{ Config::get('app.url') . $product_image->url }}"
                                         data-description="{{ $product_image->description }}">
                                @endforeach
                            </div>
                        @else
                            <img alt="No Image" src="https://dummyimage.com/1000x550/b8b8b8/696969.jpg&text=No+Image"
                                 width="100%"/>
                        @endif

                        {{--If product type is tour--}}
                        @if($model->productResults->product_type_id == 1 && count($model->productResults->product_way_point) > 0)
                            @include('partials.waypoint')
                        @endif

                        <br /><br />
                    </div>
                    <div class="tab-pane fade in" id="tabVideo">
                        @if(count($model->productResults->product_video) > 0)
                            <div id="villery" style="display:none;">
                                @foreach($model->productResults->product_video as $product_video)
                                    @if(strpos($product_video->url, "youtube"))
                                        <img alt="{{ $product_video->alt }}"
                                             data-type="youtube"
                                             data-videoid="{{ $post->videoCodeFormat($product_video->url) }}"
                                             data-description="{{ $product_video->description }}">
                                    @elseif(strpos($product_video->url, "vimeo"))
                                        <img alt="{{ $product_video->alt }}"
                                             data-type="vimeo"
                                             data-videoid="{{ $post->videoCodeFormat($product_video->url) }}"
                                             data-description="{{ $product_video->description }}">
                                    @else
                                        <img alt="{{ $product_video->alt }}"
                                             data-type="html5video"
                                             data-videomp4="{{ Config::get('app.url') . $product_video->url }}"
                                             data-description="{{ $product_video->description }}">
                                    @endif
                                @endforeach
                            </div>
                        @else
                            <img alt="No Video" src="https://dummyimage.com/1000x550/b8b8b8/696969.jpg&text=No+Video"
                                 width="100%"/>
                        @endif

                        {{--If product type is tour--}}
                        @if($model->productResults->product_type_id == 1 && count($model->productResults->product_way_point) > 0)
                            @include('partials.waypoint')
                        @endif

                        <br /><br />
                    </div>
                </div>
                <div class="panel-group" id="accordion">
                    @if($model->productResults->product_type->id == 1 && count($model->productResults->product_way_point) > 0)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Way Point<i class="indicator icon-minus pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div id="waypoint"></div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($model->productResults->product_type->id == 1 && count($model->productResults->product_itinerary) > 0)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Itinerary<i class="indicator icon-plus pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="detail_tmtimeline">
                                        @foreach($model->productResults->product_itinerary as $product_itinerary)
                                            <li>
                                                <div class="detail_tmicon">{{ $product_itinerary->order }}</div>
                                                <div class="detail_tmlabel">
                                                    <h2><strong><span>{{ $product_itinerary->title }}</span></strong><span style="font-size: 12px; margin-top: 10px;">{{ $product_itinerary->sub_title }}</span></h2>
                                                    {!! html_entity_decode($product_itinerary->description) !!}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($model->productResults->product_type->id == 1 && $model->productResults->itinerary_brochure_url)
                        <div class="banner colored" style="margin: 5px 0px 20px 0px;">
                            <h4>{{ $model->productResults->title }}</h4>
                            <p><strong>Itinerary brochure</strong></p>
                            <a href="{{ Config::get('app.url') . $model->productResults->itinerary_brochure_url }}" class="btn_1 white" target="_blank">Download</a>
                        </div>
                    @endif

                    @if($model->productResults->product_type->id == 1 && count($model->productResults->product_include) > 0)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Include<i class="indicator icon-plus pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="list_ok">
                                        @foreach($model->productResults->product_include as $product_include)
                                            <li>
                                                <h3><strong>{{ $product_include->title }}</strong><br /><span style="font-size: 12px; margin-top: 10px;">{{ $product_include->sub_title }}</span></h3>
                                                {!! html_entity_decode($product_include->description) !!}</p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($model->productResults->include_brochure_url)
                        <div class="banner" style="margin: 5px 0px 20px 0px;">
                            <h4>{{ $model->productResults->title }}</h4>
                            <a href="{{ Config::get('app.url') . $model->productResults->include_brochure_url }}" class="btn_1 white" target="_blank">Download</a>
                            <p><strong>Include brochure</strong></p>
                        </div>
                    @endif

                    @if(count($model->productResults->product_tag) > 0)
                        <br />
                        @foreach($model->productResults->product_tag as $product_tag)
                            <span class="badge default"><i class="fa fa-tag"></i>&nbsp;{{ $product_tag->name }}</span>
                        @endforeach
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-12">
                        @include('partials.social_share', ['description' => $model->productResults->title])
                    </div>
                </div>

                <hr />

                <h4>{{ ($model->productCommentResults->count() > 0) ? $model->productCommentResults->count() . ' Comments' : $model->productCommentResults->count() . ' Comment' }}</h4>

                <div id="comments">
                    <ul>
                        @foreach($model->productCommentResults as $productCommentResult)
                            <li>
                                <img src="{{ $post->avatarURLRender($productCommentResult->user) }}" />
                                <div class="comment_right clearfix">
                                    <div class="comment_info">
                                        Posted by <a href="#">{{ $post->authorRender($productCommentResult->user) }}</a><span>&nbsp;-&nbsp;</span> {{ date('F, d Y', strtotime($productCommentResult->created_at)) }}
                                    </div>
                                    {{ strip_tags($productCommentResult->comment, '<b><p>') }}
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div><!-- End Comments -->

                <h4>Leave a comment</h4>
                <form role="form" name="formComment" method="POST" action="{{ url('search/leaveComment') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        @if ($errors->has('comment'))
                            <span class="help-block">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                        <textarea name="comment" class="form-control style_2" placeholder="Comment"></textarea>
                    </div>
                    <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
                        @if ($errors->has('rating'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rating') }}</strong>
                            </span>
                        @endif
                        <select id="rate" name="rate">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="form-group" style="position: relative;">
                        {!! Recaptcha::render() !!}
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="product_id" value="{{ $model->productId }}" />
                        <input type="hidden" name="user_id" value="{{ $model->userId }}" />
                        <input type="reset" class="btn_1" value="Clear Form"/>
                        <input type="submit" class="btn_1" value="Post Comment"/>
                    </div>
                </form>
            </div><!--End  single_tour_desc-->

            <aside class="col-md-4" id="sidebar" style="z-index:999">
                <div class="hidden-sm hidden-xs">
                    <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false"
                       aria-controls="collapseMap" data-text-swap=" Hide map" data-text-original=" View on map">
                        <i class="fa fa-map-marker"></i>&nbsp;&nbsp;View on map
                    </a>
                </div>
                <div class="theiaStickySidebar">
                    {{--If product type is tour--}}
                    @if($model->productResults->product_type_id == 1)
                        @include('partials.random_hotel')
                    @endif

                    {{--If product type is hotel--}}
                    @if($model->productResults->product_type_id == 2)
                        @include('partials.random_tour')
                    @endif
                </div><!--/end sticky -->
            </aside>
        </div><!--End row -->
    </div><!--End container -->

    <div id="toTop"></div><!-- Back to top button -->
    <div id="overlay"></div><!-- Mask on input focus -->

    @if($model->productResults->product_type_id == 1)
        @include('partials.product_booking')
    @endif

    @if($model->productResults->product_type_id == 2)
        @include('partials.room_booking')
    @endif
@endsection

@section('addons')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD3zQ7PjQrnblkxp4j05pit8OxFNLgT0QA"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/jquery.sliderPro.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/infobox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/theia-sticky-sidebar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/leaflet/leaflet.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/movingmarker/movingmarker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/common/js/bootstrap-number-input.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/common/js/modernizr.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/common/js/video_header.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/jquery.spinner/dist/js/jquery.spinner.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery-accordion/js/jquery.accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/unitegallery/dist/js/unitegallery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/moment/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/sweetalert2/dist/sweetalert2.js') }}"></script>
    <script type='text/javascript' src='{{ asset('assets/vendors/unitegallery/dist/themes/default/ug-theme-default.js') }}'></script>

    <link rel="stylesheet" href="{{ asset('assets/common/css/slider-pro.min.css') }}" type='text/css'/>
    <link rel="stylesheet" href="{{ asset('assets/common/css/date_time_picker.css') }}" type='text/css'/>
    <link rel="stylesheet" href="{{ asset('assets/common/css/timeline.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/common/css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/common/css/jquery.switch.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/common/js/leaflet/leaflet.css') }}" type='text/css'/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/jquery.spinner/dist/css/bootstrap-spinner.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/jquery-accordion/css/jquery.accordion.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/sweetalert2/dist/sweetalert2.css') }}" type='text/css' />
    <link rel="stylesheet" href="{{ asset('assets/vendors/unitegallery/dist/css/unite-gallery.css') }}" type='text/css'/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}">


    <!-- Unitegallery Themes -->

    <link rel='stylesheet' href='{{ asset('assets/vendors/unitegallery/dist/themes/default/ug-theme-default.css') }}' type='text/css' />

    <script type="text/javascript">
        $(document).ready(function ($) {
            HeaderVideo.init({
                container: $('.header-video'),
                header: $('.header-video-media'),
                videoTrigger: $('#video-trigger'),
                autoPlayVideo: false
            });

            var product;
            var departure = moment(new Date()).format('YYYY-MM-DD');
            var checkIn = moment(new Date()).format('YYYY-MM-DD');
            var checkOut = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');

            var checkAvailability = function () {
                $('.modal-item').empty();

//                If product type is tour
                @if($model->productResults->product_type_id == 1)
                    product.departure = departure;
                    product.adult = $('#adult').val();
                    product.child = $('#child').val();

                    var totalDiscount = 0;
                    var productDiscount = '';
                    var productPrice = '';

                    if (product.discount != 0) {
                        if (product.product_discount != 0) {
                            totalDiscount = parseFloat(product.discount) + parseFloat(product.product_discount);
                            productDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                            productPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + product.price_convert + '</strike>' + productDiscount + '</h4>' +
                                    '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                        } else {
                            totalDiscount = parseFloat(product.discount);
                            productDiscount = '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                            productPrice = '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + product.price_convert + '</strike>' + productDiscount + '</h4>' +
                                    '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                        }
                    } else {
                        if (product.product_discount != 0) {
                            totalDiscount = parseFloat(product.product_discount);
                            productDiscount = '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                            productPrice = '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + product.price_convert + '</strike>' + productDiscount + '</h4>' +
                                    '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                        } else {
                            productPrice = '<h2 style="margin: 0; padding: 0;"><strong>' + product.price_convert + '</strong></h2><small>/&nbsp;Pax</small>';
                        }
                    }

                    var productItem = '<div class="item">' +
                            '<div class="row">' +
                            '<div class="col-md-9">' +
                            '<h4>Overview</h4>' +
                            '<p class="item-description">' + product.overview + '</p>' +
                            '<strong>Min Age:</strong>&nbsp;' + product.min_age + '<small>&nbsp;<i>years old</i></small>' +
                            '&nbsp;&nbsp;<strong>Max Age:</strong>&nbsp;' + product.max_age + '<small>&nbsp;<i>years old</i></small>' +
                            '<br /><strong>Duration:</strong>&nbsp;' + product.duration + '<small><i>days</i></small>' +
                            '</div>' +
                            '<div class="col-md-2"><br />' +
                            productPrice +
                            '</div>' +
                            '</div>' +
                            '</div><br />';

                    $('.modal-item').append(productItem);

                    $('.modal-item').append('<button type="submit" class="btn btn-success btn-block"><i class="fa fa-cart"></i>&nbsp;&nbsp;Add to Cart</button>');
                    $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');
                @endif

//                If product type is hotel
                @if($model->productResults->product_type_id == 2)
                    product.client_id = localStorage.clientId;
                    product.check_in = checkIn;
                    product.check_out = checkOut;
                    product.adult = $('#adult').val();
                    product.child = $('#child').val();

                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: localStorage.baseUrl + '/search/checkRoomAvailability',
                        data: product,
                        success: function (results) {
                            $.each(results.dto, function(index, value) {
                                var facilities = value.facility;
                                var beds = value.bed;
                                var roomImages = value.room_image;

                                var facility = '';

                                $.each(facilities, function(index, value) {
                                    if (index != (facilities.length - 1)) {
                                        facility += value.facility + ', ';
                                    } else {
                                        facility += value.facility;
                                    }
                                });

                                var bed = '';

                                $.each(beds, function(index, value) {
                                    if (index != (beds.length - 1)) {
                                        bed += value.name + ', ';
                                    } else {
                                        bed += value.name;
                                    }
                                });

                                var roomImage = '';

                                roomImage += '<div id="gallery_' + index + '">';

                                $.each(roomImages, function(index, value) {
                                    roomImage += '<img alt="' + value.alt + '" src="' + localStorage.baseUrl + '/' + value.url + '" data-image="' + localStorage.baseUrl + '/' + value.url + '" data-description="' + value.caption + '">';
                                });

                                roomImage += '</div>';

                                var totalDiscount = 0;
                                var roomDiscount = '';
                                var roomPrice = '';

                                if (product.discount != 0) {
                                    if (value.room_discount != 0) {
                                        totalDiscount = parseFloat(product.discount) + parseFloat(value.room_discount);
                                        roomDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                                        roomPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + value.price_convert + '</strike>' + roomDiscount + '</h4>' +
                                                '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                                '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                                '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                                '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                    } else {
                                        totalDiscount = parseFloat(product.discount);
                                        roomDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                                        roomPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + value.price_convert + '</strike>' + roomDiscount + '</h4>' +
                                                '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                                '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                                '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                                '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                    }
                                } else {
                                    if (value.room_discount != 0) {
                                        totalDiscount = parseFloat(value.room_discount);
                                        roomDiscount += '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + totalDiscount + '%</span>';
                                        roomPrice += '<h4 style="color: #ff0000; margin: 0; padding: 0;"><strike>' + value.price_convert + '</strike>' + roomDiscount + '</h4>' +
                                                '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_discount_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                                '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                                '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                                '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                    } else {
                                        roomPrice += '<h2 style="margin: 0; padding: 0;"><strong>' + value.price_convert + '</strong></h2><small>/&nbsp;Pax</small>' +
                                                '<input type="hidden" id="room_discount_' + index + '" value="' + value.room_discount + '">' +
                                                '<input type="hidden" id="price_' + index + '" value="' + value.price + '">' +
                                                '<input type="hidden" id="price_discount_' + index + '" value="' + value.price_discount + '">';
                                    }
                                }

                                var roomQuantity = '';

                                if (value.room_available_by_quantity > 0 && (value.room_available_by_date >= value.long_stay)) {
                                    roomQuantity = '<br /><input id="qty_' + index + '" type="number" class="form-control text-center quantity" value="0" min="0" max="' + value.room_available_by_quantity + '">';
                                } else {
                                    roomQuantity = '<br /><h5><strong>Not Available</strong></h5>';
                                }

                                var roomItem = '<div class="item">' +
                                        '<div class="row">' +
                                        '<div class="col-md-9">' +
                                        '<h4>' + value.title + '</h4>' +
                                        '<p class="item-description">' + value.description + '</p>' +
                                        '<strong>Adult:</strong>&nbsp;' + value.adult +
                                        '&nbsp;&nbsp;<strong>Child:</strong>&nbsp;' + value.child +
                                        '&nbsp;&nbsp;<strong>Max:</strong>&nbsp;' + value.max +
                                        '<br /><strong>Facilities:</strong>&nbsp;' + facility +
                                        '<br /><strong>Beds:</strong>&nbsp;' + bed +
                                        '</div>' +
                                        '<div class="col-md-2">' +
                                        roomPrice +
                                        roomQuantity +
                                        '<input type="hidden" id="id_' + index + '" value="' + value.id + '">' +
                                        '<input type="hidden" id="title_' + index + '" value="' + value.title + '">' +
                                        '</div>' +
                                        '</div>' +
                                        '</div><br />' +
                                        '<div class="row">' +
                                        '<div data-accordion-group class="col-md-12" style="margin-bottom: 20px;">' +
                                        '<div class="accordion" data-accordion>' +
                                        '<div data-control>Gallery</div>' +
                                        '<div data-content>' +
                                        '<div>' +
                                        roomImage +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="accordion" data-accordion>' +
                                        '<div data-control>Option</div>' +
                                        '<div data-content>' +
                                        '<div>' +
                                        value.option +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="accordion" data-accordion>' +
                                        '<div data-control>Policy</div>' +
                                        '<div data-content>' +
                                        '<div>' +
                                        value.policy +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                                $('.modal-item').append(roomItem);

                                $('#gallery_' + index).unitegallery();
                                $('#qty_' + index).bootstrapNumber({
                                    upClass: 'default',
                                    downClass: 'default'
                                });
                            });

                            $('.modal-item').append('<button type="submit" class="btn btn-success btn-block"><i class="fa fa-cart"></i>&nbsp;&nbsp;Add to Cart</button>');
                            $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');

                            $('.accordion').accordion();
                        }
                    });
                @endif
            };

//            Image gallery and video gallery
            @if(count($model->productResults->product_image) > 0)
                $('#gallery').unitegallery();
            @endif

            @if(count($model->productResults->product_video) > 0)
                $('#villery').unitegallery();
            @endif

            $('#rate').barrating({
                theme: 'fontawesome-stars'
            });

            $('#grade').barrating({
                theme: 'fontawesome-stars',
                initialRating: parseInt('{{ $model->productResults->grade }}')
            });

            $('.btn-pref .btn').click(function () {
                $(".btn-pref .btn").removeClass("btn-danger").addClass("btn-default");
                $(this).removeClass("btn-default").addClass("btn-danger");
            });

            $('.check-in-check-out').daterangepicker({
                "startDate": moment(new Date()),
                "endDate": moment(new Date()).add(1, 'days'),
                'minDate': moment(new Date()),
            }, function(start, end) {
                checkIn = start.format('YYYY-MM-DD');
                checkOut = end.format('YYYY-MM-DD');

                $('.modal-item').empty();
                $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');
            });

            $('.departure').daterangepicker({
                "singleDatePicker": true,
                'minDate': moment(new Date()),
            }, function(start) {
                departure = start.format('YYYY-MM-DD');
            });

            $('.booking').click(function() {
                product = $(this).data('product');

                $('#booking').modal();

                $('.modal-title').empty();
                $('.modal-address').empty();

                $('.modal-title').append(product.title);
                $('.modal-address').append(product.address);

                checkAvailability();
            });

            $('.check-available').click(function() {
                checkAvailability()
            });

            $('form[name="formCheckAvailable"]').submit(function(event) {
                if (product.product_type_id != 2) {
                    product.departure = departure;
                    product.adult = $('#adult').val();
                    product.child = $('#child').val();

                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: localStorage.baseUrl + '/cart/add',
                        data: product,
                        success: function () {
                            location.reload();
                        }
                    });

                    event.preventDefault();
                } else {
                    product.check_in = checkIn;
                    product.check_out = checkOut;
                    product.adult = $('#adult').val();
                    product.child = $('#child').val();

                    var rooms = [];
                    var quantities = 0;

                    $('.item').each(function(index) {
                        if ($('#qty_' + index).length) {
                            quantities = quantities + parseInt($('#qty_' + index).val());
                        }
                    });

                    if (quantities == 0) {
                        swal('Error Message', 'You haven\'t selected the room quantity', 'error');
                    }

                    if (quantities != 0) {
                        $('.item').each(function(index) {
                            var room = {};

                            if ($('#qty_' + index).length && parseInt($('#qty_' + index).val()) != 0) {
                                room.id = parseInt($('#id_' + index).val());
                                room.title = $('#title_' + index).val();
                                room.qty = parseInt($('#qty_' + index).val());
                                room.room_discount = parseFloat($('#room_discount_' + index).val());
                                room.price = parseFloat($('#price_' + index).val());

                                rooms.push(room);
                            }
                        });

                        product.rooms = rooms;

                        $.ajax({
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            url: localStorage.baseUrl + '/cart/add',
                            data: product,
                            success: function () {
                                location.reload();
                            }
                        });
                    }

                    event.preventDefault();
                }
            });

            $('#sidebar').theiaStickySidebar({
                additionalMarginTop: 80
            });

            $('#collapseMap').on('shown.bs.collapse', function (e) {
                (function (A) {
                    if (!Array.prototype.forEach)
                        A.forEach = A.forEach || function (action, that) {
                                    for (var i = 0, l = this.length; i < l; i++)
                                        if (i in this)
                                            action.call(that, this[i], i, this);
                                };

                })(Array.prototype);

                var points = [];

                points.push({
                    name: '{{ $model->productResults->title }}',
                    location_latitude: parseFloat({{ $model->productResults->lat }}),
                    location_longitude: parseFloat({{ $model->productResults->lng }}),
                    map_image_url: @if($model->productResults->featured_image_url) '{{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($model->productResults->featured_image_url, '300x200') }}', @else 'https://dummyimage.com/300x200/b8b8b8/696969.jpg&text=No+Image', @endif
                    name_point: '{{ $model->productResults->title }}',
                    description_point: '{!! html_entity_decode($post->readMoreRender($model->productResults->description)) !!}',
                    price_point: @if($model->productResults->discount != 0)
                                    @if(count($model->productResults->product_discount) != 0)
                                        '{{ currency($model->productResults->price - (((\App\Helper\PostHelper::minProductDiscount($model->productResults->product_discount) + $model->productResults->discount) / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}',
                                    @else
                                        '{{ currency($model->productResults->price - (($model->productResults->discount / 100) * $model->productResults->price), currency()->config('default'), Session::get('currency')) }}',
                                    @endif
                                 @else
                                    '{{ currency($model->productResults->price, currency()->config('default'), Session::get('currency')) }}',
                                 @endif
                    duration_point: '{{ $model->productResults->duration }}',
                    url_point: '{{ url('search/detail', ['id' => $model->productResults->id]) }}'
                });

                var mapLocationObject,
                    markersLocation = [],
                    markerLocation,
                    markersLocationData = {
                        'Walking': points
                    };

                var mapLocationOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(parseFloat({{ $model->productResults->lat }}), parseFloat({{ $model->productResults->lng }})),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,

                    mapTypeControl: false,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    panControl: false,
                    panControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.TOP_LEFT
                    },
                    scrollwheel: false,
                    scaleControl: false,
                    scaleControlOptions: {
                        position: google.maps.ControlPosition.TOP_LEFT
                    },
                    streetViewControl: true,
                    streetViewControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    styles: [{
                        "featureType": "landscape",
                        "stylers": [{
                            "hue": "#FFBB00"
                        }, {
                            "saturation": 43.400000000000006
                        }, {
                            "lightness": 37.599999999999994
                        }, {
                            "gamma": 1
                        }]
                    }, {
                        "featureType": "road.highway",
                        "stylers": [{
                            "hue": "#FFC200"
                        }, {
                            "saturation": -61.8
                        }, {
                            "lightness": 45.599999999999994
                        }, {
                            "gamma": 1
                        }]
                    }, {
                        "featureType": "road.arterial",
                        "stylers": [{
                            "hue": "#FF0300"
                        }, {
                            "saturation": -100
                        }, {
                            "lightness": 51.19999999999999
                        }, {
                            "gamma": 1
                        }]
                    }, {
                        "featureType": "road.local",
                        "stylers": [{
                            "hue": "#FF0300"
                        }, {
                            "saturation": -100
                        }, {
                            "lightness": 52
                        }, {
                            "gamma": 1
                        }]
                    }, {
                        "featureType": "water",
                        "stylers": [{
                            "hue": "#0078FF"
                        }, {
                            "saturation": -13.200000000000003
                        }, {
                            "lightness": 2.4000000000000057
                        }, {
                            "gamma": 1
                        }]
                    }, {
                        "featureType": "poi",
                        "stylers": [{
                            "hue": "#00FF6A"
                        }, {
                            "saturation": -1.0989010989011234
                        }, {
                            "lightness": 11.200000000000017
                        }, {
                            "gamma": 1
                        }]
                    }]
                };

                mapLocationObject = new google.maps.Map(document.getElementById('location'), mapLocationOptions);

                for (var key in markersLocationData)
                    markersLocationData[key].forEach(function (item) {
                        markerLocation = new google.maps.Marker({
                            position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                            map: mapLocationObject,
                            icon: '../../assets/common/images/pins/' + key + '.png',
                        });

                        if ('undefined' === typeof markersLocation[key])
                            markersLocation[key] = [];

                        markersLocation[key].push(markerLocation);

                        google.maps.event.addListener(markerLocation, 'click', (function () {
                            closeInfoBox();
                            getInfoBox(item).open(mapLocationObject, this);
                            mapLocationObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
                        }));

                    });

                function closeInfoBox() {
                    $('div.infoBox').remove();
                }

                function getInfoBox(item) {
                    return new InfoBox({
                        content: '<div class="marker_info_2">' +
                        '<img src="' + item.map_image_url + '" alt="' + item.name_point + '"/>' +
                        '<h3 style="padding: 0px 5px 0px 5px;">'+ item.name_point +'</h3>' +
                        '<span>'+ item.description_point +'</span>' +
                        '<div class="marker_tools">' +
                        '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden"><input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">' + item.name_point + '</button></form><br />' +
                        '<a href="#" class="btn_infobox_price">' + item.price_point + '</a>' +
                        '<a href="#" class="btn_infobox_duration">' + item.duration_point + '</a>' +
                        '</div>' +
                        '<br style="clear: both;" /><br style="clear: both;" />' +
                        '</div>',
                        disableAutoPan: false,
                        maxWidth: 0,
                        pixelOffset: new google.maps.Size(10, 125),
                        closeBoxMargin: '5px -20px 2px 2px',
                        closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                        isHidden: false,
                        alignBottom: true,
                        pane: 'floatPane',
                        enableEventPropagation: true
                    });
                }

            });

            @if($model->productResults->product_type->id == 1 && count($model->productResults->product_way_point) > 0)
                //Waypoint
                var mapWaypointObject = new L.Map('waypoint', {
                    zoom: 6,
                    minZoom: 3
                });
                var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', layerWaypoint = new L.TileLayer(tileUrl, {
                    attribution: 'Maps © <a href=\"https://paradise-explorers.id/copyright\">Paradise Explorer</a> contributors',
                    maxZoom: 18
                });

                mapWaypointObject.addLayer(layerWaypoint);

                var markersWaypoint = [];
                var durationWaypoint = [];

                @foreach($model->productResults->product_way_point as $way_point)
                    markersWaypoint.push([parseFloat({{ $way_point->lat }}), parseFloat({{ $way_point->lng }})]);
                    durationWaypoint.push(4000);
                @endforeach

                mapWaypointObject.fitBounds(markersWaypoint);

                var movingMarkerWaypoint = L.Marker.movingMarker(markersWaypoint, durationWaypoint, {
                    autostart: true,
                    loop: true
                }).addTo(mapWaypointObject);

                @foreach($model->productResults->product_way_point as $key => $way_point)
                    movingMarkerWaypoint.addStation(parseInt({{ $key + 1 }}), 1000);
                @endforeach

                L.polyline(markersWaypoint, {color: '#e24d65'}).addTo(mapWaypointObject);
            @endif
        });
    </script>
@endsection