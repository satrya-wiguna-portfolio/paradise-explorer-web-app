@inject('post', 'App\Helper\PostHelper')
@extends('layouts.default')

@section('content')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/common/images/header_bg_destination.jpg') }}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{ $model->destination  }}</h1>
            </div>
        </div>
    </section><!-- End section -->

    <div id="position">
        <div class="container">
            @if(!empty($model->destinationResults))
                {!! Breadcrumbs::render('searchDestination', $model->destinationResults) !!}
            @else
                {!! Breadcrumbs::render() !!}
            @endif
        </div>
    </div><!-- Position -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @foreach(array_chunk($model->productResults->items(), 3) as $chunk)
                <div class="row">
                    @foreach($chunk as $productResult)
                    <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.1s">
                        <div class="tour_container">
                            @if($productResult->discount != 0)
                                <div class="ribbon_3 popular"><span>Disc {{ $productResult->discount }}%</span></div>
                            @endif
                            <div class="img_container">
                                <a href="{{ url('search/detail', ['id' => $productResult->id]) }}">
                                    <img src="@if($productResult->featured_image_url) {{ Config::get('app.url') . \App\Helper\PostHelper::pathResizeFile($productResult->featured_image_url, '800x533') }} @else https://dummyimage.com/800x533/b8b8b8/696969.jpg&text=No+Image @endif"
                                         class="img-responsive"
                                         alt="{{ $productResult->title }}">

                                    {{--Discount off label--}}
                                    @if(count($productResult->product_discount) != 0)
                                        <div class="badge_save">Off<strong>{{ \App\Helper\PostHelper::minProductDiscount($productResult->product_discount) }}%</strong></div>
                                    @endif
                                    {{--End discount off label--}}

                                    <div class="short_info">
                                        <div style="position: absolute; bottom: 0;">{{ $productResult->product_category->category }}</div>
                                        @if($productResult->discount != 0)
                                            @if(count($productResult->product_discount) != 0)
                                                <span class="price">
                                                    <span style="font-size: 14px; color: red;"><strike>{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $productResult->discount + \App\Helper\PostHelper::minProductDiscount($productResult->product_discount) }}%</span><br />
                                                    {{ currency($productResult->price - ((($productResult->discount + \App\Helper\PostHelper::minProductDiscount($productResult->product_discount)) / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}
                                                </span>
                                            @else
                                                <span class="price">
                                                    <span style="font-size: 14px; color: red;"><strike>{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ $productResult->discount }}%</span><br />
                                                    {{ currency($productResult->price - (($productResult->discount / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}
                                                </span>
                                            @endif
                                        @else
                                            @if(count($productResult->product_discount) != 0)
                                                <span class="price">
                                                    <span style="font-size: 14px; color: red;"><strike>{{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}</strike></span>&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;{{ \App\Helper\PostHelper::minProductDiscount($productResult->product_discount) }}%</span><br />
                                                    {{ currency($productResult->price - ((\App\Helper\PostHelper::minProductDiscount($productResult->product_discount) / 100) * $productResult->price), currency()->config('default'), Session::get('currency')) }}
                                                </span>
                                            @else
                                                <span class="price">
                                                    {{ currency($productResult->price, currency()->config('default'), Session::get('currency')) }}
                                                </span>
                                            @endif
                                        @endif
                                    </div>
                                </a>
                            </div>
                            <div class="tour_title">
                                <h3 style="margin-bottom: 10px;">
                                    <strong>{{ str_limit($productResult->title, 30) }}</strong></h3>
                                <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                                    <span class="icon-star" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                                    <span class="icon-star" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                                    <span class="icon-star" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                    <span class="icon-star-empty" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                    <span class="icon-star-empty" aria-hidden="true"></span>
                                </button>
                                <div class="agent" style="margin-top: 35px;">
                                    <a href="#"><strong>{{ $productResult->agent->name }}</strong></a>
                                </div>
                                <div style="margin-top: 10px;">
                                    <button type="button" class="btn btn-danger btn-block booking"
                                            data-product="{{ \App\Helper\PostHelper::dataProduct($model->type, $productResult) }}">
                                        <i class="fa fa-calendar "></i>&nbsp;&nbsp;Book Now
                                    </button>
                                </div>
                            </div>
                        </div><!-- End box tour -->
                    </div><!-- End col-md-6 -->
                    @endforeach
                </div><!-- End row -->
                @endforeach

                <hr/>

                @include('pagination.custom', ['paginator' => $model->productResults])
            </div>
        </div>
    </div>
    @include('partials.product_booking')
@endsection

@section('addons')
    <script type='text/javascript' src="{{ asset('assets/vendors/moment/moment.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/vendors/jquery.spinner/dist/js/jquery.spinner.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/jquery.spinner/dist/css/bootstrap-spinner.css') }}" />

    <script type="text/javascript">
        var product;
        var departure = moment(new Date()).format('YYYY-MM-DD');
        var baseUrl = '{{ Config::get('app.url') }}';

        $('.departure').daterangepicker({
            "singleDatePicker": true,
            'minDate': moment(new Date()),
        }, function(start) {
            departure = start.format('YYYY-MM-DD');
        });

        $('.booking').click(function() {
            product = $(this).data('product');

            $('#booking').modal();

            $('.modal-title').empty();
            $('.modal-address').empty();

            $('.modal-title').append(product.title);
            $('.modal-address').append(product.address);

            checkAvailability();
        });

        function checkAvailability() {
            $('.modal-item').empty();

            product.departure = departure;
            product.adult = $('#adult').val();
            product.child = $('#child').val();

            var productPrice = '';

            if (product.discount != 0 || product.product_discount != 0) {
                var productDiscount = '';

                if (product.product_discount != 0) {
                    productDiscount = '&nbsp;<span style="color: #ccc; font-size: 12px; font-weight: bold;">Save&nbsp;' + product.product_discount + '%</span>'
                }

                productPrice = '<strike><h4 style="color: #ff0000; margin: 0; padding: 0;">' + product.price_convert + '</strike>' +
                        productDiscount +
                        '</h4>' +
                        '<strong><h2 style="margin: 0; padding: 0;">' + product.price_discount_convert + '</h2></strong><small>/&nbsp;Pax</small>'
            } else {
                productPrice = '<strong><h2 style="margin: 0; padding: 0;">' + product.price_convert + '</h2></strong><small>/&nbsp;Pax</small>'
            }

            var productItem = '<div class="item">' +
                    '<div class="row">' +
                    '<div class="col-md-9">' +
                    '<p class="item-description">' + product.overview + '</p>' +
                    '<strong><i class="fa fa-user"></i>&nbsp;Min Age:</strong>&nbsp;' + product.min_age +
                    '&nbsp;&nbsp;<strong><i class="fa fa-child"></i>&nbsp;Max Age:</strong>&nbsp;' + product.max_age +
                    '<br /><strong>Duration:</strong>&nbsp;' + product.duration +
                    '</div>' +
                    '<div class="col-md-2"><br />' +
                    productPrice +
                    '</div>' +
                    '</div>' +
                    '</div><br />';

            $('.modal-item').append(productItem);

            $('.modal-item').append('<button type="submit" class="btn btn-success btn-block"><i class="fa fa-cart"></i>&nbsp;&nbsp;Add to Cart</button>');
            $('.modal-item').append('<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>');
        }

        $('form[name="formCheckAvailable"]').submit(function(event) {
//          If product type is tour
            if (product.product_type_id == 1) {
                product.departure = departure;
                product.adult = $('#adult').val();
                product.child = $('#child').val();

                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: localStorage.baseUrl + '/cart/add',
                    data: product,
                    crossDomain: true,
                    success: function () {
                        location.reload();
                    },
                    error: function () {
                    }
                });

                event.preventDefault();
            }


//          If product type is hotel
            if (product.product_type_id == 2) {
                product.client_id = localStorage.clientId;
                product.check_in = checkIn;
                product.check_out = checkOut;
                product.adult = $('#adult').val();
                product.child = $('#child').val();

                var rooms = [];
                var quantities = 0;

                $('.item').each(function(index) {
                    if ($('#qty_' + index).length) {
                        quantities = quantities + parseInt($('#qty_' + index).val());
                    }
                });

                if (quantities == 0) {
                    swal('Error Message', 'You haven\'t selected the room quantity', 'error');
                }

                if (quantities != 0) {
                    $('.item').each(function(index) {
                        var room = {};

                        if ($('#qty_' + index).length && parseInt($('#qty_' + index).val()) != 0) {
                            room.id = parseInt($('#id_' + index).val());
                            room.title = $('#title_' + index).val();
                            room.qty = parseInt($('#qty_' + index).val());
                            room.room_discount = parseFloat($('#room_discount_' + index).val());
                            room.price = parseFloat($('#price_' + index).val());
                            room.price_discount = parseFloat($('#price_discount_' + index).val());

                            rooms.push(room);
                        }
                    });

                    product.rooms = rooms;

                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: localStorage.baseUrl + '/cart/add',
                        data: product,
                        crossDomain: true,
                        success: function () {
                            location.reload();
                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });
                }

                event.preventDefault();
            }
        });
    </script>
@endsection