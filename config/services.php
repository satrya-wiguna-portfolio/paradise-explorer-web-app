<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
//        'client_id' => '443714306009241',
        'client_id' => '344952699305625',
//        'client_secret' => 'ae1c5d7bff28f3703937108fb3293b6f',
        'client_secret' => '6076e0618ed449956c7bdf27269ca3cc',
//        'redirect' => 'http://paradise-explorers.local.id:8888/auth/facebook/callback',
        'redirect' => 'http://tailored-explorers.com/auth/facebook/callback',
    ],

    'twitter' => [
        'client_id'     => 'xMGZ3enPKX5fwfnUEdLHeC5rY',
        'client_secret' => 'SefnzVtIbzvrtETavAJxTzcbS1OMVCNqrbB8v0oGOvgnYfu1vY',
//        'redirect'      => 'http://paradise-explorers.local.id:8888/auth/twitter/callback',
        'redirect'      => 'http://tailored-explorers.com/auth/twitter/callback',
    ],

    'paypal' => [
//        'client_id' => 'AVgFrGZSBzc1fTyDqOKft1d3qG9ed_-DvlG9xVT5h4JqzazNs9shdltOiIz83sIdpNINDlE1Z8MSSlBX',
//        'secret' => 'EJneynr9GnXIIaSJORT60xjUBaTXGRGEgslzvz3xQqzfoP6i4JTqvDXUiGSdhSlG3TvQWltVUtddYZfu',
        'client_id' => 'ATCR77kVmkisgwrMtgATjbBnjn0Hglo_Z97-Zvy3cMhoHSpKcH9ezQcif5jl5Kf28Q6B-q1LS5YNwLZ7',
        'secret' => 'EO8R4on-3Xkk5poZl8U4XZV-C1HR9kumDERHJWKGWvePfFpgT9iaOwnXF_MrZ2LRKw1CML4Iarf8qh8q',
        'mode' => 'sandbox',
        'end_point' => 'https://api.sandbox.paypal.com/',
        'time_out' => 60,
        'log' => true,
        'file' => storage_path('logs/paypal.log'),
        'level' => 'FINE'
    ],

];
