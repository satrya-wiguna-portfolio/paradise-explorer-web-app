<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;

    protected $table = 'currencies';

    protected $fillable = [
        'name',
        'code',
        'symbol',
        'format',
        'exchange_rate',
        'active'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Has many
    public function reservation()
    {
        return $this->hasMany('App\Models\Reservation', 'currency_id');
    }
}
