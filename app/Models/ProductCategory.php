<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes;

    protected $table = 'product_categories';

    protected $fillable = [
        'product_type_id',
        'category',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product_type()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    //Has many
    public function product()
    {
        return $this->hasMany('App\Models\Product', 'product_category_id');
    }
}
