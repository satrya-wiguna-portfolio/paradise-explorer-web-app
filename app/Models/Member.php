<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;

    protected $table = 'members';

    protected $fillable = [
        'user_id',
        'title',
        'first_name',
        'last_name',
        'gender',
        'address',
        'country_id',
        'state_id',
        'city',
        'zip',
        'image_url',
        'phone'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }
}
