<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentType extends Model
{
    use SoftDeletes;

    protected $table = 'agent_types';

    protected $fillable = [
        'name',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Has many
    public function agent()
    {
        return $this->hasMany('App\Models\Agent', 'agent_type_id');
    }
}
