<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SoftDeletes;

    protected $table = 'agents';

    protected $fillable = [
        'user_id',
        'agent_type_id',
        'title',
        'first_name',
        'last_name',
        'gender',
        'contact',
        'address',
        'country_id',
        'state_id',
        'city',
        'zip',
        'image_url',
        'phone',
        'fax'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function angent_type()
    {
        return $this->belongsTo('App\Models\AgentType', 'agent_type_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }

    //Has many
    public function product()
    {
        return $this->hasMany('App\Models\Product', 'agent_id');
    }

    public function product_tag()
    {
        return $this->hasMany('App\Models\Product', 'agent_id');
    }

    public function bed()
    {
        return $this->hasMany('App\Models\Bed', 'agent_id');
    }

    public function reservation_product()
    {
        return $this->hasMany('App\Models\ReservationProduct', 'agent_id');
    }

}
