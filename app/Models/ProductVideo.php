<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVideo extends Model
{
    use SoftDeletes;

    protected $table = 'product_videos';

    protected $fillable = [
        'product_id',
        'url',
        'title',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
