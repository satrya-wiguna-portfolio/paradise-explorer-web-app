<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomAllotment extends Model
{
    use SoftDeletes;

    protected $table = 'room_allotments';

    protected $fillable = [
        'room_id',
        'date',
        'allotment',
        'lock',
        'sold',
        'balance',
        'price',
        'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id');
    }

    //Has many
    public function room_allotment_log()
    {
        return $this->hasMany('App\Models\RoomAllotmentLog', 'room_allotment_id');
    }
}
