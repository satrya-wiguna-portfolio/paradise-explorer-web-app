<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;

    protected $table = 'staffs';

    protected $fillable = [
        'user_id',
        'title',
        'first_name',
        'last_name',
        'gender',
        'address',
        'province_id',
        'regency_id',
        'district_id',
        'village',
        'zip',
        'image_url',
        'phone'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }

    public function regency()
    {
        return $this->belongsTo('App\Models\Regency', 'regency_id');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
}
