<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Destination extends Model
{
    use SoftDeletes;

    protected $table = 'destinations';

    protected $fillable = [
        'parent_id',
        'name',
        'image_url',
        'lat',
        'lng',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function parent()
    {
        return $this->belongsTo('App\Models\Destination', 'parent_id');
    }

    //Has many
    public function children()
    {
        return $this->hasMany('App\Models\Destination', 'parent_id');
    }
}
