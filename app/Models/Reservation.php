<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;

    protected $table = 'reservations';

    protected $fillable = [
        'invoice_number',
        'invoice_release_date',
        'invoice_paid_date',
        'user_id',
        'title',
        'first_name',
        'last_name',
        'gender',
        'address',
        'country_id',
        'state_id',
        'city',
        'zip',
        'phone',
        'handphone',
        'payment_id',
        'currency_id',
        'exchange_rate',
        'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Payment', 'payment_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }

    //Has many
    public function reservation_product()
    {
        return $this->hasMany('App\Models\ReservationProduct', 'reservation_id');
    }

    public function reservation_paypal_response()
    {
        return $this->hasMany('App\Models\ReservationPaypalResponse', 'reservation_id');
    }

    public function reservation_doku_response()
    {
        return $this->hasMany('App\Models\ReservationDokuResponse', 'reservation_id');
    }
}