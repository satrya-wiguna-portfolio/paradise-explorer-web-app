<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;

    protected $table = 'states';

    protected $fillable = [
        'country_id',
        'name'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    //Has many
    public function agent()
    {
        return $this->hasMany('App\Models\Agent', 'state_id');
    }

    public function member()
    {
        return $this->hasMany('App\Models\Member', 'state_id');
    }

    public function reservation()
    {
        return $this->hasMany('App\Models\Reservation', 'country_id');
    }
}
