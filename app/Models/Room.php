<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;

    protected $table = 'rooms';

    protected $fillable = [
        'product_id',
        'title',
        'description',
        'adult',
        'children',
        'max',
        'footage',
        'price',
        'option',
        'policy'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($product) {
            $product->room_allotment()->delete();
            $product->room_discount()->delete();
            $product->room_image()->delete();
        });
    }

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function price_detail()
    {
        return $this->belongsTo('App\Models\PriceDetail', 'room_id');
    }

    //Belongs to many
    public function facility()
    {
        return $this->belongsToMany('App\Models\Facility', 'room_facilities', 'room_id', 'facility_id');
    }

    public function bed()
    {
        return $this->belongsToMany('App\Models\Bed', 'bed_rooms', 'room_id', 'bed_id');
    }

    //Has many
    public function room_allotment()
    {
        return $this->hasMany('App\Models\RoomAllotment', 'room_id');
    }

    public function room_discount()
    {
        return $this->hasMany('App\Models\RoomDiscount', 'room_id');
    }

    public function room_image()
    {
        return $this->hasMany('App\Models\RoomImage', 'room_id');
    }

    public function reservation_product_room()
    {
        return $this->hasMany('App\Models\ReservationProductRoom', 'room_id');
    }

    /**
     * @param array $room_images
     */
    public function sync_room_image(array $room_images)
    {
        $children = $this->room_image;

        $room_images = collect($room_images);

        $deleted_ids = $children->filter(function ($child) use ($room_images) {
            return empty($room_images->where('id', $child->id)->first());
        })->map(function ($child) {
            $id = $child->id;
            $child->delete();

            return $id;
        });

        $attachments = $room_images->filter(function ($room_image) {
            return empty($room_image['id']);
        })->map(function ($room_image) use ($deleted_ids) {
            $room_image['id'] = $deleted_ids->pop();

            return new RoomImage($room_image);
        });

        $this->room_image()->saveMany($attachments);
    }
}
