<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivate extends Model
{
    protected $table = 'user_activates';

    protected $primaryKey = 'email';

    protected $fillable = [
        'email',
        'token'
    ];

    public $timestamps = false;
}
