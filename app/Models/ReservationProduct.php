<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationProduct extends Model
{
    use SoftDeletes;

    protected $table = 'reservation_products';

    protected $fillable = [
        'reservation_id',
        'product_id',
        'title',
        'agent_id',
        'product_type_id',
        'price',
        'discount',
        'product_discount',
        'quantity',
        'total',
        'check_in',
        'check_out',
        'depature',
        'adult',
        'child'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function product_type()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    //Has many
    public function reservation_product_room()
    {
        return $this->hasMany('App\Models\ReservationProductRoom', 'reservation_product_id');
    }
}
