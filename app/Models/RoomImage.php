<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomImage extends Model
{
    use SoftDeletes;

    protected $table = 'room_images';

    protected $fillable = [
        'room_id',
        'url',
        'title',
        'caption',
        'alt',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id');
    }
}
