<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDiscount extends Model
{
    use SoftDeletes;

    protected $table = 'product_discounts';

    protected $fillable = [
        'product_id',
        'open_date',
        'close_date',
        'discount',
        'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
