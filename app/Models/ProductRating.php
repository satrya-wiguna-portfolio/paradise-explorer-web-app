<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductRating extends Model
{
    use SoftDeletes;

    protected $table = 'product_ratings';

    protected $fillable = [
        'product_id',
        'user_id',
        'rate',
        'review'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
