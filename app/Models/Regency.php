<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regency extends Model
{
    use SoftDeletes;

    protected $table = 'regencies';

    protected $fillable = [
        'province_id',
        'name'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }

    //Has many
    public function district()
    {
        return $this->hasMany('App\Models\District', 'regency_id');
    }

    public function staff()
    {
        return $this->hasMany('App\Models\Staff', 'regency_id');
    }
}
