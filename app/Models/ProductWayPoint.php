<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductWayPoint extends Model
{
    use SoftDeletes;

    protected $table = 'product_way_points';

    protected $fillable = [
        'product_id',
        'title',
        'address',
        'lat',
        'lng',
        'order'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
