<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{
    use SoftDeletes;

    protected $table = 'facilities';

    protected $fillable = [
        'facility',
        'image_url'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to many
    public function product()
    {
        return $this->belongsToMany('App\Models\Product', 'product_facilities', 'facility_id', 'product_id');
    }

    public function room()
    {
        return $this->belongsToMany('App\Models\Room', 'product_rooms', 'facility_id', 'room_id');
    }
}
