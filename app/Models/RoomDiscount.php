<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomDiscount extends Model
{
    use SoftDeletes;

    protected $table = 'room_discounts';

    protected $fillable = [
        'room_id',
        'open_date',
        'close_date',
        'discount',
        'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id');
    }
}