<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $table = 'countries';

    protected $fillable = [
        'code',
        'name'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Has many
    public function state()
    {
        return $this->hasMany('App\Models\State', 'country_id');
    }

    public function agent()
    {
        return $this->hasMany('App\Models\Agent', 'country_id');
    }

    public function member()
    {
        return $this->hasMany('App\Models\Member', 'country_id');
    }

    public function reservation()
    {
        return $this->hasMany('App\Models\Reservation', 'country_id');
    }
}
