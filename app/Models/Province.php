<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;

    protected $table = 'provinces';

    protected $fillable = [
        'name'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Has many
    public function regency()
    {
        return $this->hasMany('App\Models\Regency', 'province_id');
    }

    public function staff()
    {
        return $this->hasMany('App\Models\Staff', 'province_id');
    }
}
