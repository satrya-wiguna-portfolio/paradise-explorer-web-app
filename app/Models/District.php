<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;

    protected $table = 'districts';

    protected $fillable = [
        'regency_id',
        'name'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function regency()
    {
        return $this->belongsTo('App\Models\Regency', 'regency_id');
    }

    //Has many
    public function staff()
    {
        return $this->hasMany('App\Models\Staff', 'district_id');
    }
}
