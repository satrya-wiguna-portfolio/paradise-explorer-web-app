<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'email',
        'password',
        'provider_id',
        'provider',
        'handphone',
        'current_login',
        'current_ip_address',
        'status'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($user) {
            $user->member()->delete();
            $user->agent()->delete();
            $user->staff()->delete();
            $user->user_log()->delete();
        });
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }

        return false;

    }

    public function hasRole($role)
    {
        if ($this->role()->where('role', $role)->first()) {
            return true;
        }

        return false;
    }

    //Belongs to many
    public function role()
    {
        return $this->belongsToMany('App\Models\Role', 'user_roles', 'user_id', 'role_id');
    }

    //Has one
    public function agent()
    {
        return $this->hasOne('App\Models\Agent', 'user_id');
    }

    public function member()
    {
        return $this->hasOne('App\Models\Member', 'user_id');
    }

    public function staff()
    {
        return $this->hasOne('App\Models\Staff', 'user_id');
    }

    //Has many
    public function product_rating()
    {
        return $this->hasMany('App\Models\ProductRating', 'user_id');
    }

    public function blog()
    {
        return $this->hasMany('App\Models\Blog', 'user_id');
    }

    public function blog_comment()
    {
        return $this->hasMany('App\Models\BlogComment', 'user_id');
    }

    public function user_log()
    {
        return $this->hasMany('App\Models\UserLog', 'user_id');
    }

    public function reservation()
    {
        return $this->hasMany('App\Models\Reservation', 'user_id');
    }
}
