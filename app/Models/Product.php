<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $table = 'products';

    protected $fillable = [
        'product_type_id',
        'product_category_id',
        'property_type_id',
        'agent_id',
        'publish',
        'title',
        'slug',
        'overview',
        'description',
        'featured_image_url',
        'featured_video_url',
        'include_brochure_url',
        'itinerary_brochure_url',
        'lat',
        'lng',
        'address',
        'duration',
        'min_age',
        'max_age',
        'price',
        'grade',
        'discount',
        'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($product) {
            $product->product_itinerary()->delete();
            $product->product_rating()->delete();
            $product->product_image()->delete();
            $product->product_video()->delete();
            $product->product_discount()->delete();
            $product->product_include()->delete();
            $product->product_way_point()->delete();
            $product->product_comment()->delete();
        });
    }

    //Belongs to
    public function product_type()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    public function product_category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'product_category_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function property_type()
    {
        return $this->belongsTo('App\Models\PropertyType', 'property_type_id');
    }

    public function price_detail()
    {
        return $this->belongsTo('App\Models\PriceDetail', 'product_id');
    }

    //Belongs to many
    public function facility()
    {
        return $this->belongsToMany('App\Models\Facility', 'product_facilities', 'product_id', 'facility_id');
    }

    public function product_tag()
    {
        return $this->belongsToMany('App\Models\ProductTag', 'product_product_tags', 'product_id', 'product_tag_id');
    }

    //Has many
    public function product_itinerary()
    {
        return $this->hasMany('App\Models\ProductItinerary', 'product_id');
    }

    public function product_rating()
    {
        return $this->hasMany('App\Models\ProductRating', 'product_id');
    }

    public function product_image()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

    public function product_video()
    {
        return $this->hasMany('App\Models\ProductVideo', 'product_id');
    }

    public function product_discount()
    {
        return $this->hasMany('App\Models\ProductDiscount', 'product_id');
    }

    public function product_include()
    {
        return $this->hasMany('App\Models\ProductInclude', 'product_id');
    }

    public function product_way_point()
    {
        return $this->hasMany('App\Models\ProductWayPoint', 'product_id');
    }

    public function product_comment()
    {
        return $this->hasMany('App\Models\ProductComment', 'product_id');
    }

    public function room()
    {
        return $this->hasMany('App\Models\Room', 'product_id');
    }

    public function reservation_product()
    {
        return $this->hasMany('App\Models\ReservationProduct', 'product_id');
    }

    /**
     * @param array $product_images
     */
    public function sync_product_image(array $product_images)
    {
        $children = $this->product_image;

        $product_images = collect($product_images);

        $deleted_ids = $children->filter(function ($child) use ($product_images) {
                return empty($product_images->where('id', $child->id)->first());
            })->map(function ($child) {
            $id = $child->id;
            $child->delete();

            return $id;
        });

        $attachments = $product_images->filter(function ($product_image) {
                return empty($product_image['id']);
            })->map(function ($product_image) use ($deleted_ids) {
            $product_image['id'] = $deleted_ids->pop();

            return new ProductImage($product_image);
        });

        $this->product_image()->saveMany($attachments);
    }

    /**
     * @param array $product_videos
     */
    public function sync_product_video(array $product_videos)
    {
        $children = $this->product_video;

        $product_videos = collect($product_videos);

        $deleted_ids = $children->filter(function ($child) use ($product_videos) {
            return empty($product_videos->where('id', $child->id)->first());
        })->map(function ($child) {
            $id = $child->id;
            $child->delete();

            return $id;
        });

        $attachments = $product_videos->filter(function ($product_video) {
            return empty($product_video['id']);
        })->map(function ($product_video) use ($deleted_ids) {
            $product_video['id'] = $deleted_ids->pop();

            return new ProductVideo($product_video);
        });

        $this->product_video()->saveMany($attachments);
    }

    /**
     * @param array $product_way_points
     */
    public function sync_product_way_point(array $product_way_points)
    {
        $children = $this->product_way_point;

        $product_way_points = collect($product_way_points);

        $deleted_ids = $children->filter(function ($child) use ($product_way_points) {
            return empty($product_way_points->where('id', $child->id)->first());
        })->map(function ($child) {
            $id = $child->id;
            $child->delete();

            return $id;
        });

        $attachments = $product_way_points->filter(function ($product_way_point) {
            return empty($product_way_point['id']);
        })->map(function ($product_way_point) use ($deleted_ids) {
            $product_way_point['id'] = $deleted_ids->pop();

            return new ProductWayPoint($product_way_point);
        });

        $this->product_way_point()->saveMany($attachments);
    }

    /**
     * @param array $product_includes
     */
    public function sync_product_include(array $product_includes)
    {
        $children = $this->product_include;

        $product_includes = collect($product_includes);

        $deleted_ids = $children->filter(function ($child) use ($product_includes) {
            return empty($product_includes->where('id', $child->id)->first());
        })->map(function ($child) {
            $id = $child->id;
            $child->delete();

            return $id;
        });

        $attachments = $product_includes->filter(function ($product_include) {
            return empty($product_include['id']);
        })->map(function ($product_include) use ($deleted_ids) {
            $product_include['id'] = $deleted_ids->pop();

            return new ProductInclude($product_include);
        });

        $this->product_include()->saveMany($attachments);
    }

    /**
     * @param array $product_itineraries
     */
    public function sync_product_itinerary(array $product_itineraries)
    {
        $children = $this->product_itinerary;

        $product_itineraries = collect($product_itineraries);

        $deleted_ids = $children->filter(function ($child) use ($product_itineraries) {
            return empty($product_itineraries->where('id', $child->id)->first());
        })->map(function ($child) {
            $id = $child->id;
            $child->delete();

            return $id;
        });

        $attachments = $product_itineraries->filter(function ($product_itinerary) {
            return empty($product_itinerary['id']);
        })->map(function ($product_itinerary) use ($deleted_ids) {
            $product_itinerary['id'] = $deleted_ids->pop();

            return new ProductItinerary($product_itinerary);
        });

        $this->product_itinerary()->saveMany($attachments);
    }

    //Sluggable
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['title']
            ]
        ];
    }

    //Scope
    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model, $attribute, $config, $slug)
    {
        return $query->where('agent_id', $config['agent_id']);
    }
}
