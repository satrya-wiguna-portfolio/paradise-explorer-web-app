<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomAllotmentLog extends Model
{
    use SoftDeletes;

    protected $table = 'room_allotment_logs';

    protected $fillable = [
        'client_id',
        'cart_item_id',
        'room_allotment_id',
        'lock'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function room()
    {
        return $this->belongsTo('App\Models\RoomAllotment', 'room_allotment_id');
    }
}
