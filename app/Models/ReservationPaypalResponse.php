<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationPaypalResponse extends Model
{
    use SoftDeletes;

    protected $table = 'reservation_paypal_responses';

    protected $fillable = [
        'reservation_id',
        'payment_id',
        'payer_id',
        'token',
        'intent',
        'state',
        'cart'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }
}
