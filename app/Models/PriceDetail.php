<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceDetail extends Model
{
    use SoftDeletes;

    protected $table = 'price_details';

    protected $fillable = [
        'product_id',
        'room_id',
        'price_adult',
        'price_child',
        'price_infant'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id');
    }
}
