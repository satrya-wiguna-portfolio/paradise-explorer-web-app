<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTag extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $table = 'product_tags';

    protected $fillable = [
        'agent_id',
        'name',
        'slug'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    //Belongs to many
    public function product()
    {
        return $this->belongsToMany('App\Models\Product', 'product_product_tags', 'product_tag_id', 'product_id');
    }

    //Sluggable
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['name']
            ]
        ];
    }

    //Scope
    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model, $attribute, $config, $slug)
    {
        return $query->where('agent_id', $config['agent_id']);
    }
}
