<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductItinerary extends Model
{
    use SoftDeletes;

    protected $table = 'product_itineraries';

    protected $fillable = [
        'product_id',
        'title',
        'sub_title',
        'description',
        'order'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
