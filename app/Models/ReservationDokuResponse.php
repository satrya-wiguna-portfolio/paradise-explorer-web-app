<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDokuResponse extends Model
{
    use SoftDeletes;

    protected $table = 'reservation_doku_responses';

    protected $fillable = [
        'reservation_id'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }
}
