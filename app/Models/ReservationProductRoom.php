<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationProductRoom extends Model
{
    use SoftDeletes;

    protected $table = 'reservation_product_rooms';

    protected $fillable = [
        'reservation_product_id',
        'room_id',
        'title',
        'price',
        'room_discount',
        'quantity',
        'total'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function reservation_product()
    {
        return $this->belongsTo('App\Models\ReservationProduct', 'reservation_product_id');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id');
    }
}
