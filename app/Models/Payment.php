<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'payments';

    protected $fillable = [
        'payment',
        'description',
        'image_url',
        'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Has many
    public function reservation()
    {
        return $this->hasMany('App\Models\Reservation', 'payment_id');
    }
}
