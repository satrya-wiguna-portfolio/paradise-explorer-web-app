<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bed extends Model
{
    use SoftDeletes;

    protected $table = 'beds';

    protected $fillable = [
        'agent_id',
        'name',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Belongs to
    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    //Belongs to many
    public function room()
    {
        return $this->belongsToMany('App\Models\Room', 'bed_rooms', 'bed_id', 'room_id');
    }
}
