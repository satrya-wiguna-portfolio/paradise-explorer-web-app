<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductType extends Model
{
    use SoftDeletes;

    protected $table = 'product_types';

    protected $fillable = [
        'type',
        'description'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //Has many
    public function product_category()
    {
        return $this->hasMany('App\Models\ProductCategory', 'product_type_id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'product_type_id');
    }

    public function reservation_product()
    {
        return $this->hasMany('App\Models\ReservationProduct', 'product_type_id');
    }
}