<?php
namespace App\Providers;

use App\Services\Contract\IAgentService;
use App\Services\Contract\IAgentTypeService;
use App\Services\Contract\IAuthService;
use App\Services\Contract\IBedService;
use App\Services\Contract\IBlogCategoryService;
use App\Services\Contract\IBlogCommentService;
use App\Services\Contract\IBlogService;
use App\Services\Contract\IBlogTagService;
use App\Services\Contract\ICartService;
use App\Services\Contract\ICountryService;
use App\Services\Contract\ICurrencyService;
use App\Services\Contract\IDestinationService;
use App\Services\Contract\IDistrictService;
use App\Services\Contract\IFacilityService;
use App\Services\Contract\IFileManagerService;
use App\Services\Contract\IHomeService;
use App\Services\Contract\IMemberService;
use App\Services\Contract\IPaymentService;
use App\Services\Contract\IProductCategoryService;
use App\Services\Contract\IProductCommentService;
use App\Services\Contract\IProductDiscountService;
use App\Services\Contract\IProductService;
use App\Services\Contract\IProductTagService;
use App\Services\Contract\IProductTypeService;
use App\Services\Contract\IPropertyTypeService;
use App\Services\Contract\IProvinceService;
use App\Services\Contract\IRegencyService;
use App\Services\Contract\IReservationDokuResponseService;
use App\Services\Contract\IReservationPaypalResponseService;
use App\Services\Contract\IReservationService;
use App\Services\Contract\IRoleService;
use App\Services\Contract\IRoomAllotmentLogService;
use App\Services\Contract\IRoomAllotmentService;
use App\Services\Contract\IRoomDiscountService;
use App\Services\Contract\IRoomService;
use App\Services\Contract\IStaffService;
use App\Services\Contract\IStateService;
use App\Services\Contract\IUserService;
use App\Services\Implement\AgentService;
use App\Services\Implement\AgentTypeService;
use App\Services\Implement\AuthService;
use App\Services\Implement\BedService;
use App\Services\Implement\BlogCategoryService;
use App\Services\Implement\BlogCommentService;
use App\Services\Implement\BlogService;
use App\Services\Implement\BlogTagService;
use App\Services\Implement\CartService;
use App\Services\Implement\CountryService;
use App\Services\Implement\CurrencyService;
use App\Services\Implement\DestinationService;
use App\Services\Implement\DistrictService;
use App\Services\Implement\FacilityService;
use App\Services\Implement\FileManagerService;
use App\Services\Implement\HomeService;
use App\Services\Implement\MemberService;
use App\Services\Implement\PaymentService;
use App\Services\Implement\ProductCategoryService;
use App\Services\Implement\ProductCommentService;
use App\Services\Implement\ProductDiscountService;
use App\Services\Implement\ProductService;
use App\Services\Implement\ProductTagService;
use App\Services\Implement\ProductTypeService;
use App\Services\Implement\PropertyTypeService;
use App\Services\Implement\ProvinceService;
use App\Services\Implement\RegencyService;
use App\Services\Implement\ReservationDokuResponseService;
use App\Services\Implement\ReservationPaypalResponseService;
use App\Services\Implement\ReservationService;
use App\Services\Implement\RoleService;
use App\Services\Implement\RoomAllotmentLogService;
use App\Services\Implement\RoomAllotmentService;
use App\Services\Implement\RoomDiscountService;
use App\Services\Implement\RoomService;
use App\Services\Implement\StaffService;
use App\Services\Implement\StateService;
use App\Services\Implement\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IAuthService::class, AuthService::class);
        $this->app->bind(IUserService::class, UserService::class);
        $this->app->bind(IFileManagerService::class, FileManagerService::class);

        $this->app->bind(ICountryService::class, CountryService::class);
        $this->app->bind(IStateService::class, StateService::class);
        $this->app->bind(IProvinceService::class, ProvinceService::class);
        $this->app->bind(IRegencyService::class, RegencyService::class);
        $this->app->bind(IDistrictService::class, DistrictService::class);
        $this->app->bind(IAgentTypeService::class, AgentTypeService::class);

        $this->app->bind(IRoleService::class, RoleService::class);
        $this->app->bind(ICurrencyService::class, CurrencyService::class);
        $this->app->bind(IFacilityService::class, FacilityService::class);
        $this->app->bind(IPaymentService::class, PaymentService::class);
        $this->app->bind(IMemberService::class, MemberService::class);
        $this->app->bind(IDestinationService::class, DestinationService::class);
        $this->app->bind(IAgentService::class, AgentService::class);
        $this->app->bind(IStaffService::class, StaffService::class);
        $this->app->bind(IProductTypeService::class, ProductTypeService::class);
        $this->app->bind(IProductCategoryService::class, ProductCategoryService::class);
        $this->app->bind(IProductService::class, ProductService::class);
        $this->app->bind(IProductDiscountService::class, ProductDiscountService::class);
        $this->app->bind(IProductTagService::class, ProductTagService::class);
        $this->app->bind(IProductCommentService::class, ProductCommentService::class);
        $this->app->bind(IPropertyTypeService::class, PropertyTypeService::class);

        $this->app->bind(IRoomService::class, RoomService::class);
        $this->app->bind(IRoomDiscountService::class, RoomDiscountService::class);
        $this->app->bind(IRoomAllotmentService::class, RoomAllotmentService::class);
        $this->app->bind(IRoomAllotmentLogService::class, RoomAllotmentLogService::class);
        $this->app->bind(IBedService::class, BedService::class);

        $this->app->bind(IBlogCategoryService::class, BlogCategoryService::class);
        $this->app->bind(IBlogTagService::class, BlogTagService::class);
        $this->app->bind(IBlogService::class, BlogService::class);
        $this->app->bind(IBlogCommentService::class, BlogCommentService::class);

        $this->app->bind(ICartService::class, CartService::class);
        $this->app->bind(IReservationService::class, ReservationService::class);
        $this->app->bind(IReservationPaypalResponseService::class, ReservationPaypalResponseService::class);
        $this->app->bind(IReservationDokuResponseService::class, ReservationDokuResponseService::class);

        $this->app->bind(IHomeService::class, HomeService::class);
    }
}
