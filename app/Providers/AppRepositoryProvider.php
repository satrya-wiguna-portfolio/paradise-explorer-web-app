<?php
namespace App\Providers;


use App\Repositories\Contract\IAgentRepository;
use App\Repositories\Contract\IAgentTypeRepository;
use App\Repositories\Contract\IAuthRepository;
use App\Repositories\Contract\IBedRepository;
use App\Repositories\Contract\IBlogCategoryRepository;
use App\Repositories\Contract\IBlogCommentRepository;
use App\Repositories\Contract\IBlogRepository;
use App\Repositories\Contract\IBlogTagRepository;
use App\Repositories\Contract\ICountryRepository;
use App\Repositories\Contract\ICurrencyRepository;
use App\Repositories\Contract\IDestinationRepository;
use App\Repositories\Contract\IDistrictRepository;
use App\Repositories\Contract\IFacilityRepository;
use App\Repositories\Contract\IPaymentRepository;
use App\Repositories\Contract\IProductCommentRepository;
use App\Repositories\Contract\IProductItineraryRepository;
use App\Repositories\Contract\IMemberRepository;
use App\Repositories\Contract\IProductCategoryRepository;
use App\Repositories\Contract\IProductDiscountRepository;
use App\Repositories\Contract\IProductImageRepository;
use App\Repositories\Contract\IProductIncludeRepository;
use App\Repositories\Contract\IProductRatingRepository;
use App\Repositories\Contract\IProductRepository;
use App\Repositories\Contract\IProductTagRepository;
use App\Repositories\Contract\IProductTypeRepository;
use App\Repositories\Contract\IProductVideoRepository;
use App\Repositories\Contract\IPropertyTypeRepository;
use App\Repositories\Contract\IProvinceRepository;
use App\Repositories\Contract\IRegencyRepository;
use App\Repositories\Contract\IReservationDokuResponseRepository;
use App\Repositories\Contract\IReservationPaypalResponseRepository;
use App\Repositories\Contract\IReservationProductRepository;
use App\Repositories\Contract\IReservationProductRoomRepository;
use App\Repositories\Contract\IReservationRepository;
use App\Repositories\Contract\IRoleRepository;
use App\Repositories\Contract\IRoomAllotmentLogRepository;
use App\Repositories\Contract\IRoomAllotmentRepository;
use App\Repositories\Contract\IRoomDiscountRepository;
use App\Repositories\Contract\IRoomRepository;
use App\Repositories\Contract\IStaffRepository;
use App\Repositories\Contract\IStateRepository;
use App\Repositories\Contract\IUserActivateRepository;
use App\Repositories\Contract\IUserLogRepository;
use App\Repositories\Contract\IUserRepository;
use App\Repositories\Contract\IProductWayPointRepository;
use App\Repositories\Implement\AgentRepository;
use App\Repositories\Implement\AgentTypeRepository;
use App\Repositories\Implement\AuthRepository;
use App\Repositories\Implement\BedRepository;
use App\Repositories\Implement\BlogCategoryRepository;
use App\Repositories\Implement\BlogCommentRepository;
use App\Repositories\Implement\BlogRepository;
use App\Repositories\Implement\BlogTagRepository;
use App\Repositories\Implement\CountryRepository;
use App\Repositories\Implement\CurrencyRepository;
use App\Repositories\Implement\DestinationRepository;
use App\Repositories\Implement\DistrictRepository;
use App\Repositories\Implement\FacilityRepository;
use App\Repositories\Implement\PaymentRepository;
use App\Repositories\Implement\ProductCommentRepository;
use App\Repositories\Implement\ProductItineraryRepository;
use App\Repositories\Implement\MemberRepository;
use App\Repositories\Implement\ProductCategoryRepository;
use App\Repositories\Implement\ProductDiscountRepository;
use App\Repositories\Implement\ProductImageRepository;
use App\Repositories\Implement\ProductIncludeRepository;
use App\Repositories\Implement\ProductRatingRepository;
use App\Repositories\Implement\ProductRepository;
use App\Repositories\Implement\ProductTagRepository;
use App\Repositories\Implement\ProductTypeRepository;
use App\Repositories\Implement\ProductVideoRepository;
use App\Repositories\Implement\PropertyTypeRepository;
use App\Repositories\Implement\ProvinceRepository;
use App\Repositories\Implement\RegencyRepository;
use App\Repositories\Implement\ReservationDokuResponseRepository;
use App\Repositories\Implement\ReservationPaypalResponseRepository;
use App\Repositories\Implement\ReservationProductRepository;
use App\Repositories\Implement\ReservationProductRoomRepository;
use App\Repositories\Implement\ReservationRepository;
use App\Repositories\Implement\RoleRepository;
use App\Repositories\Implement\RoomAllotmentLogRepository;
use App\Repositories\Implement\RoomAllotmentRepository;
use App\Repositories\Implement\RoomDiscountRepository;
use App\Repositories\Implement\RoomRepository;
use App\Repositories\Implement\StaffRepository;
use App\Repositories\Implement\StateRepository;
use App\Repositories\Implement\UserActivateRepository;
use App\Repositories\Implement\UserLogRepository;
use App\Repositories\Implement\UserRepository;
use App\Repositories\Implement\ProductWayPointRepository;
use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IAuthRepository::class, AuthRepository::class);
        $this->app->bind(IUserRepository::class, UserRepository::class);

        $this->app->bind(ICountryRepository::class, CountryRepository::class);
        $this->app->bind(IStateRepository::class, StateRepository::class);
        $this->app->bind(IProvinceRepository::class, ProvinceRepository::class);
        $this->app->bind(IRegencyRepository::class, RegencyRepository::class);
        $this->app->bind(IDistrictRepository::class, DistrictRepository::class);
        $this->app->bind(IAgentTypeRepository::class, AgentTypeRepository::class);

        $this->app->bind(IRoleRepository::class, RoleRepository::class);
        $this->app->bind(ICurrencyRepository::class, CurrencyRepository::class);
        $this->app->bind(IFacilityRepository::class, FacilityRepository::class);
        $this->app->bind(IPaymentRepository::class, PaymentRepository::class);
        $this->app->bind(IReservationRepository::class, ReservationRepository::class);
        $this->app->bind(IReservationProductRepository::class, ReservationProductRepository::class);
        $this->app->bind(IReservationProductRoomRepository::class, ReservationProductRoomRepository::class);
        $this->app->bind(IReservationPaypalResponseRepository::class, ReservationPaypalResponseRepository::class);
        $this->app->bind(IReservationDokuResponseRepository::class, ReservationDokuResponseRepository::class);
        $this->app->bind(IMemberRepository::class, MemberRepository::class);
        $this->app->bind(IDestinationRepository::class, DestinationRepository::class);
        $this->app->bind(IAgentRepository::class, AgentRepository::class);
        $this->app->bind(IStaffRepository::class, StaffRepository::class);
        $this->app->bind(IProductTypeRepository::class, ProductTypeRepository::class);
        $this->app->bind(IProductCategoryRepository::class, ProductCategoryRepository::class);
        $this->app->bind(IProductRepository::class, ProductRepository::class);
        $this->app->bind(IProductDiscountRepository::class, ProductDiscountRepository::class);
        $this->app->bind(IProductTagRepository::class, ProductTagRepository::class);
        $this->app->bind(IProductCommentRepository::class, ProductCommentRepository::class);
        $this->app->bind(IPropertyTypeRepository::class, PropertyTypeRepository::class);

        $this->app->bind(IRoomRepository::class, RoomRepository::class);
        $this->app->bind(IRoomDiscountRepository::class, RoomDiscountRepository::class);
        $this->app->bind(IRoomAllotmentRepository::class, RoomAllotmentRepository::class);
        $this->app->bind(IRoomAllotmentLogRepository::class, RoomAllotmentLogRepository::class);
        $this->app->bind(IBedRepository::class, BedRepository::class);

        $this->app->bind(IBlogCategoryRepository::class, BlogCategoryRepository::class);
        $this->app->bind(IBlogTagRepository::class, BlogTagRepository::class);
        $this->app->bind(IBlogRepository::class, BlogRepository::class);
        $this->app->bind(IBlogCommentRepository::class, BlogCommentRepository::class);

        $this->app->bind(IProductItineraryRepository::class, ProductItineraryRepository::class);
        $this->app->bind(IProductImageRepository::class, ProductImageRepository::class);
        $this->app->bind(IProductIncludeRepository::class, ProductIncludeRepository::class);
        $this->app->bind(IProductRatingRepository::class, ProductRatingRepository::class);
        $this->app->bind(IProductVideoRepository::class, ProductVideoRepository::class);
        $this->app->bind(IProductWayPointRepository::class, ProductWayPointRepository::class);
        $this->app->bind(IUserLogRepository::class, UserLogRepository::class);
        $this->app->bind(IUserActivateRepository::class, UserActivateRepository::class);

    }
}