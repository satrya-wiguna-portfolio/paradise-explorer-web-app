<?php

namespace App\Listeners;

use App\Events\SendEmailEvent;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailHandler
{
    /**
     * SendEmailHandler constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SendEmailEvent $event
     * @throws Exception
     */
    public function handle(SendEmailEvent $event)
    {
        $template = $event->_template;
        $data = $event->_data;
        $model = $data['model'];

        $result = Mail::send($template, $model, function ($mail) use ($data) {
            $mail->to($data['to']);
            $mail->from($data['from_address'], $data['from_name']);
            $mail->subject($data['subject']);
        });

        $fail = Mail::failures();

        if (!empty($fail)) {
            throw new Exception('Could not send message to '. $data['to']);
        }

        if (empty($result)) {
            throw new Exception('Email could not be sent');
        }
    }
}
