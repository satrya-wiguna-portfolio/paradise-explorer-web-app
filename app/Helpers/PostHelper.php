<?php
namespace App\Helper;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class PostHelper
{
    /**
     * @param $content
     * @return mixed
     */
    public function readMoreRender($content)
    {
        if (!strpos($content, '<!--more-->'))
            return false;

        $excerpt = explode('<!--more-->', $content);

        return $excerpt[0];
    }

    /**
     * @param $url
     * @return mixed
     */
    public function videoCodeFormat($url)
    {
        $urls = explode('/', $url);
        $code = $urls[count($urls) - 1];

        return $code;
    }

    /**
     * @param $min
     * @param $max
     * @return string
     */
    public function ageRangeFormat($min, $max)
    {
        if ($min <= 17 && $max >= 40) {
            return 'All Age';
        }

        return $min . ' to ' . $max;
    }

    /**
     * @param $wayPoints
     * @return string
     */
    public function wayPointFormat($wayPoints)
    {
        $station = null;
        $i = 0;

        foreach($wayPoints as $key => $wayPoint) {
            if ($wayPoints->first() != $wayPoint && $wayPoints->last() != $wayPoint) {
                $station .= $wayPoint->title . ', ';
                $i++;
            }
        }

        $station = substr($station, 0, -2);
        $destination = ($i > 1) ? '+1 Destinations' : '1 Destination';

        return '<li class="timeline__step done">
                    <input class="timeline__step-radio" id="trigger2" name="trigger" type="radio">

                    <label class="timeline__step-label" for="trigger2">
                        <span class="timeline__step-content">' . $station . '</span>
                    </label>

                    <span class="timeline__step-title">' . $destination . '</span>

                    <i class="timeline__step-marker">
                        <span class="fa fa-dot-circle-o" style="font-size: 30px; margin-top: 5px;"></span>
                    </i>
                </li>';
    }

    /**
     * @param $user
     * @return string
     */
    public function avatarURLRender($user)
    {
        $avatarUrl = asset('assets/common/images/admin/avatar.jpg');

        if ($user->member) {
            if ($user->member->image_url)
                $avatarUrl = $user->member->image_url;
        }

        if ($user->agent) {
            if ($user->agent->image_url)
                $avatarUrl = $user->agent->image_url;
        }

        if ($user->staff) {
            if ($user->staff->image_url)
                $avatarUrl = $user->staff->image_url;
        }

        return $avatarUrl;
    }

    /**
     * @param $user
     * @return string
     */
    public function authorRender($user)
    {
        $author = 'Who am i?';

        if ($user->member) {
            $author = $user->member->first_name . ' ' . $user->member->last_name;
        }

        if ($user->agent) {
            $author = $user->agent->first_name . ' ' . $user->agent->last_name;
        }

        if ($user->staff) {
            $author = $user->staff->first_name . ' ' . $user->staff->last_name;
        }

        return $author;
    }

    /**
     * @param $path
     * @param $size
     * @return mixed
     */
    public static function pathResizeFile($path, $size)
    {
        $extension = pathinfo(basename($path), PATHINFO_EXTENSION);
        $base = basename($path, '.' . $extension);

        $file = str_replace($base , 'resize/' . $base . '_' . $size, $path);

        return $file;
    }

    /**
     * @param $rooms
     * @return array
     */
    public static function roomDiscount($rooms)
    {
        $roomDiscount = [];

        if (count($rooms) != 0) {
            $max = $rooms->where('price', $rooms->max('price'))
                ->first();

            $roomDiscount = $max->room_discount;
        }

        return $roomDiscount;
    }

    /**
     * @param $rooms
     * @return int
     */
    public static function maxRoomPrice($rooms)
    {
        $price = 0;

        if (count($rooms) != 0) {
            $max = $rooms->where('price', $rooms->max('price'))
                ->first();

            $price = $max->price;
        }

        return $price;
    }

    /**
     * @param $rooms
     * @return int
     */
    public static function minRoomDiscount($rooms)
    {
        $roomDiscount = self::roomDiscount($rooms);
        $discount = 0;

        if (count($roomDiscount) != 0) {
            $min = $roomDiscount->where('discount', $roomDiscount->min('discount'))
                ->first();

            $discount = $min->discount;
        }

        return $discount;
    }

    /**
     * @param $productDiscount
     * @return mixed
     */
    public static function minProductDiscount($productDiscount)
    {
        $discount = 0;

        if (count($productDiscount) != 0) {
            $min = $productDiscount->where('discount', $productDiscount->min('discount'))
                ->first();

            $discount = $min->discount;
        }

        return $discount;
    }

    /**
     * @param $text
     * @return mixed
     */
    public static function replaceWhiteSpace($text)
    {
        return str_replace(' ', '-', strtolower($text));
    }

    /**
     * @param $discount
     * @param $rooms
     * @return string
     */
    public static function cartRoomPop($discount, $rooms)
    {
        $view = '<div class="row"><div class="col-md-11 cart-room-pop" style="line-height: 15px;">';

        foreach($rooms as $room) {
            $priceDiscountAndRoomDiscount = self::priceDiscountAndRoomDiscount($room->price, $discount, $room->room_discount);

            $view .= '<span style="font-weight: bold;">'
                . $room->title
                . '</span><br />';

            if ($discount > 0 || $room->room_discount > 0) {
                $view .= '<span style="color: red;"><strike><small>' . currency($room->price, currency()->config('default'), Session::get('currency')) . '</small></strike></span>';

                if ($discount != 0) {
                    $view .= '&nbsp;' . self::discountCartFormat($discount, 0, 0) . '&nbsp;';
                }

                if ($room->room_discount != 0) {
                    $view .= '&nbsp;' . self::discountCartFormat($room->room_discount, 0, 0);
                }

                $view .= '<br />';
            }

            $view .= currency(($priceDiscountAndRoomDiscount * $room->qty), currency()->config('default'), Session::get('currency')) . '&nbsp;';

            $view .= '<i>(';
            $view .= ($room->qty > 1) ? $room->qty . '&nbsp;<small>rooms</small>&nbsp;x&nbsp;' : $room->qty . '&nbsp;<small>room</small>&nbsp;x&nbsp;';
            $view .= currency($priceDiscountAndRoomDiscount, currency()->config('default'), Session::get('currency'));
            $view .= ')</i>';
        }

        $view .= '</div></div><br />';

        return $view;
    }

    /**
     * @param $discount
     * @param $rooms
     * @return string
     */
    public static function cartRoomPage($discount, $rooms)
    {
        $view = '<ul>';

        foreach($rooms as $room) {
            $priceDiscountAndRoomDiscount = self::priceDiscountAndRoomDiscount($room->price, $discount, $room->room_discount);

            $view .= '<li>';
            $view .= '<span style="font-weight: bold;">'
                . $room->title
                . '</span><br />';

            if ($room->room_discount > 0) {
                $view .= '<span style="color: red;"><strike><small>' . currency($room->price, currency()->config('default'), Session::get('currency')) . '</small></strike></span>';

                if ($room->room_discount != 0) {
                    $view .= '&nbsp;' . self::discountCartFormat(0, $room->room_discount, 0);
                }

                $view .= '<br />';
            }

            $view .= currency(($priceDiscountAndRoomDiscount * $room->qty), currency()->config('default'), Session::get('currency')) . '&nbsp;';

            $view .= '<i>(';
            $view .= ($room->qty > 1) ? $room->qty . '&nbsp;<small>rooms</small>&nbsp;x&nbsp;' : $room->qty . '&nbsp;<small>room</small>&nbsp;x&nbsp;';
            $view .= currency($priceDiscountAndRoomDiscount, currency()->config('default'), Session::get('currency'));
            $view .= ')</i>';
            $view .= '</li>';
        }

        $view .= '</ul>';

        return $view;
    }

    /**
     * @param $type
     * @param $productResult
     * @return array
     */
    public static function dataProduct($type, $productResult)
    {
        if ($type != 2) {
            if ($productResult->discount != 0) {
                if (count($productResult->product_discount) != 0) {
                    $price_discount = $productResult->price - ((($productResult->discount + self::minProductDiscount($productResult->product_discount)) / 100) * $productResult->price);
                } else {
                    $price_discount = $productResult->price - (($productResult->discount / 100) * $productResult->price);
                }
            } else {
                if (count($productResult->product_discount) != 0) {
                    $price_discount = $productResult->price - ((self::minProductDiscount($productResult->product_discount) / 100) * $productResult->price);
                } else {
                    $price_discount = $productResult->price;
                }
            }

            $data = [
                "id" => $productResult->id,
                "product_type_id" => $productResult->product_type_id,
                "product_category_id" => $productResult->product_category_id,
                "agent_id" => $productResult->agent_id,
                "title" => $productResult->title,
                "overview" => $productResult->overview,
                "description" => $productResult->description,
                "address" => $productResult->address,
                "featured_image_url" => $productResult->featured_image_url,
                "duration" => $productResult->duration,
                "min_age" => $productResult->min_age,
                "max_age" => $productResult->max_age,
                "price" => (float)$productResult->price,
                "price_convert" => currency($productResult->price, currency()->config('default'), Session::get('currency')),
                "price_discount" => (float)$price_discount,
                "price_discount_convert" => currency($price_discount, currency()->config('default'), Session::get('currency')),
                "discount" => (float)$productResult->discount,
                "product_discount" => (float)self::minProductDiscount($productResult->product_discount)
            ];
        } else {
            $data = [
                "id" => $productResult->id,
                "product_type_id" => $productResult->product_type_id,
                "product_category_id" => $productResult->product_category_id,
                "agent_id" => $productResult->agent_id,
                "title" => $productResult->title,
                "overview" => $productResult->overview,
                "description" => $productResult->description,
                "address" => $productResult->address,
                "featured_image_url" => $productResult->featured_image_url,
                "property_type_id" => $productResult->property_type_id,
                "grade" => $productResult->grade,
                "discount" => (float)$productResult->discount
            ];
        }

        $result = json_encode($data);

        return $result;
    }

    /**
     * @param null $discount
     * @param null $room_discount
     * @param null $product_discount
     * @return string
     */
    public static function discountCartFormat($discount = null, $room_discount = null, $product_discount = null)
    {
        $result = '';

        if (!is_null($discount) && $discount != 0) {
            $result .= '<span style="color: #79a527;">-' . $discount . '%</span>';
        }

        if (!is_null($room_discount) && $room_discount != 0) {
            $result .= '<span style="color: #ffa100;">-' . $room_discount . '%</span>';
        }

        if (!is_null($product_discount) && $product_discount != 0) {
            $result .= '<span style="color: #ffa100;">-' . $product_discount . '%</span>';
        }

        if (empty($result)) {
            $result = 'N/A';
        }

        return $result;
    }

    public static function durationConvert($hours)
    {
        $duration_convert = '';
        $units = [
                "year" => 24 * 365,
                "month" => 24 * 30,
                "week" => 24 * 7,
                "day" => 24,
                "hour" => 1
            ];


        foreach ($units as $key => $value) {
            switch ($key) {
                case "year":
                    if (floor($hours / $units[$key]) > 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Years</strong></i>&nbsp;&nbsp;';
                    } else if (floor($hours / $units[$key]) == 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Year</strong></i>&nbsp;&nbsp;';
                    } else {
                        //Do Nothing
                    }

                    $hours = ($hours % $units[$key]);
                    break;

                case "month":
                    if (floor($hours / $units[$key]) > 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Months</strong></i>&nbsp;&nbsp;';
                    } else if (floor($hours / $units[$key]) == 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Month</strong></i>&nbsp;';
                    } else {
                        //Do Nothing
                    }

                    $hours = ($hours % $units[$key]);
                    break;

                case "week":
                    if (floor($hours / $units[$key]) > 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Weeks</strong></i>&nbsp;&nbsp;';
                    } else if (floor($hours / $units[$key]) == 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Week</strong></i>&nbsp;&nbsp;';
                    } else {
                        //Do Nothing
                    }

                    $hours = ($hours % $units[$key]);
                    break;

                case "day":
                    if (floor($hours / $units[$key]) > 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Days</strong></i>&nbsp;&nbsp;';
                    } else if (floor($hours / $units[$key]) == 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Day</strong></i>&nbsp;&nbsp;';
                    } else {
                        //Do Nothing
                    }

                    $hours = ($hours % $units[$key]);
                    break;

                default:
                    if (floor($hours / $units[$key]) > 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Hours</strong></i>&nbsp;&nbsp;';
                    } else if (floor($hours / $units[$key]) == 1) {
                        $duration_convert .= floor($hours / $units[$key]) . '&nbsp;<i><strong>Hour</strong></i>&nbsp;&nbsp;';
                    } else {
                        //Do Nothing
                    }

                    $hours = ($hours % $units[$key]);
                    break;
            }
        }

        return $duration_convert;
    }

    public static function productRating($productComments)
    {
        $totalComments = count($productComments);
        $point = 0;
        $rating = 0;

        foreach ($productComments as $productComment) {
            $point += $productComment->rate;
        }

        if ($totalComments > 0) {
            $rating = floor($point / $totalComments);
        }

        $stars = '';

        for ($i = 0; $i < 5; $i++) {
            if ($rating > 0 && $i < $rating) {
                $stars .= '<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align"><span class="icon-star" aria-hidden="true"></span></button>';
            } else {
                $stars .= '<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align"><span class="icon-star-empty" aria-hidden="true"></span></button>';
            }
        }

        return $stars;
    }

    /**
     * @param $price
     * @param $discount
     * @param $roomDiscount
     * @return float
     */
    private static function priceDiscountAndRoomDiscount($price, $discount, $roomDiscount)
    {
        return (float)$price - ((((float)$discount + $roomDiscount) / 100) *
                        (float)$price);
    }
}