<?php
namespace App\Helper;


use BrowscapPHP\Browscap;

class AccessHelper
{
    public static function getIPAddress()
    {
        $ip_address = '';

        if (getenv('HTTP_CLIENT_IP'))
            $ip_address = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ip_address = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ip_address = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ip_address = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ip_address = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ip_address = getenv('REMOTE_ADDR');
        else
            $ip_address = 'UNKNOWN';

        return $ip_address;
    }

    public static function getBrowserInformation()
    {
        $browscap = new Browscap();

        $info = $browscap->getBrowser();

        return $info->browser . ' - v' . $info->version;
    }
}