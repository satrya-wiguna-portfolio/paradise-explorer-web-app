<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('home'));
});

// About Us
Breadcrumbs::register('aboutUs', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About Us', route('aboutUs'));
});

// FAQ
Breadcrumbs::register('faq', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('FAQ', route('faq'));
});

// Search
Breadcrumbs::register('search', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Search', route('search'));
});
Breadcrumbs::register('searchDestination', function($breadcrumbs, $destinationResult)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push($destinationResult->name, route('searchDestination', $destinationResult->id));
});
Breadcrumbs::register('searchTour', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Tour', route('searchTour'));
});
Breadcrumbs::register('searchHotel', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Hotel', route('searchHotel'));
});
Breadcrumbs::register('searchDetail', function($breadcrumbs, $productResults)
{
    if($productResults->product_type->id == 1)
        $breadcrumbs->parent('searchTour');
    else
        $breadcrumbs->parent('searchHotel');

    $breadcrumbs->push($productResults->title, route('searchDetail', $productResults->id));
});

// Blog
Breadcrumbs::register('blog', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});
Breadcrumbs::register('blogDetail', function($breadcrumbs, $blogResults)
{
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($blogResults->title, route('blogDetail', $blogResults->id));
});
Breadcrumbs::register('blogCategory', function($breadcrumbs, $blogCategoryResult)
{
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($blogCategoryResult->name, route('blogCategory', $blogCategoryResult->id));
});
Breadcrumbs::register('blogTag', function($breadcrumbs, $blogTagResult)
{
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($blogTagResult->name, route('blogTag', $blogTagResult->id));
});

// Gallery
Breadcrumbs::register('gallery', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Gallery', route('gallery'));
});

// Contact
Breadcrumbs::register('contact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Contact', route('contact'));
});

// Cart
Breadcrumbs::register('cart', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cart', route('cart'));
});

// Checkout
Breadcrumbs::register('checkoutContact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Checkout Contact', route('checkoutContact'));
});
Breadcrumbs::register('checkoutPayment', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Checkout Payment', route('checkoutPayment'));
});
Breadcrumbs::register('checkoutSuccess', function($breadcrumbs, $checkoutResult)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Checkout Success', route('getPaypalSuccess', $checkoutResult->id));
});
Breadcrumbs::register('checkoutCancel', function($breadcrumbs, $checkoutResult)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Checkout Cancel', route('getPaypalCancel', $checkoutResult->id));
});

// Member Dashboard
Breadcrumbs::register('memberDashboard', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Dashboard', route('memberDashboard'));
});
// Member Profile
Breadcrumbs::register('memberProfile', function($breadcrumbs)
{
    $breadcrumbs->parent('memberDashboard');
    $breadcrumbs->push('Profile', route('memberProfile'));
});
// Member Booking
Breadcrumbs::register('memberBooking', function($breadcrumbs)
{
    $breadcrumbs->parent('memberDashboard');
    $breadcrumbs->push('Booking', route('memberBooking'));
});
// Member Setting
Breadcrumbs::register('memberSetting', function($breadcrumbs)
{
    $breadcrumbs->parent('memberDashboard');
    $breadcrumbs->push('setting', route('memberSetting'));
});