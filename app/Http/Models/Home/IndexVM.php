<?php
namespace App\Http\Models\Home;


class IndexVM
{
    public $randomDestinationResults;

    public $productCategoryOfTourResults;

    public $productCategoryOfHotelResults;
}