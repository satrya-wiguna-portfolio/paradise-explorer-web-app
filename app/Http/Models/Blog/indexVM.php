<?php
namespace App\Http\Models\Blog;

class indexVM
{
    public $categoryId;

    public $tagId;

    public $title;

    public $subTitle;

    public $blogResults;

    public $blogCategoryMostlyResults;

    public $blogTagResults;

    public $blogRecentResults;

    public $blogCategoryResult;

    public $blogTagResult;
}