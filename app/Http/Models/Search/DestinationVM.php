<?php
namespace App\Http\Models\Search;


class DestinationVM
{
    public $id;

    public $type;

    public $destination;

    public $lat;

    public $lng;

    public $products;

    public $productResults;

    public $destinationResult;

    public $destinationResults;
}