<?php
namespace App\Http\Models\Search;


class IndexVM
{
    public $type;

    public $destination;

    public $lat;

    public $lng;

    public $products;

    public $productResults;

    public $productTypeResults;

    public $productCategoryResults;

    public $productCategoryOfTourResults;

    public $productCategoryOfHotelResults;
}