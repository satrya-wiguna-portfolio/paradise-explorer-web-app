<?php
namespace App\Http\Models\Search;


class DetailVM
{
    public $productId;

    public $userId;

    public $productResults;

    public $randomLimitProductByProductTypeIdResults;

    public $productCommentResults;

    public $openGraph;
}