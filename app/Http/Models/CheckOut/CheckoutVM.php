<?php
namespace App\Http\Models\CheckOut;


class CheckoutVM
{
    public $provider;

    public $title;

    public $subTitle;

    public $grandTotal;

    public $reservation;

    public $paypal_response;

    public $doku_response;

    public $bank_transfer_response;

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSubtitle()
    {
        return $this->subTitle;
    }

    /**
     * @param mixed $subTitle
     */
    public function setSubtitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * @return mixed
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * @param mixed $grandTotal
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * @return mixed
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * @param mixed $reservation
     */
    public function setReservation($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * @return mixed
     */
    public function getPaypalResponse()
    {
        return $this->paypal_response;
    }

    /**
     * @param mixed $paypal_response
     */
    public function setPaypalResponse($paypal_response)
    {
        $this->paypal_response = $paypal_response;
    }

    /**
     * @return mixed
     */
    public function getDokuResponse()
    {
        return $this->doku_response;
    }

    /**
     * @param mixed $doku_response
     */
    public function setDokuResponse($doku_response)
    {
        $this->doku_response = $doku_response;
    }

    /**
     * @return mixed
     */
    public function getBankTransferResponse()
    {
        return $this->bank_transfer_response;
    }

    /**
     * @param mixed $bank_transfer_response
     */
    public function setBankTransferResponse($bank_transfer_response)
    {
        $this->bank_transfer_response = $bank_transfer_response;
    }

}