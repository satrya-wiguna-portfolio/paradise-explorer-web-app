<?php
namespace App\Http\Requests\ReservationDokuResponse;


class CreateReservationDokuResponseRequest
{
    public $reservation_id;

    /**
     * @return mixed
     */
    public function getReservationId()
    {
        return $this->reservation_id;
    }

    /**
     * @param mixed $reservation_id
     */
    public function setReservationId($reservation_id)
    {
        $this->reservation_id = $reservation_id;
    }
}