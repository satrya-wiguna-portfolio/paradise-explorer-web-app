<?php
namespace App\Http\Requests\Staff;


class StaffRequest
{
    public $province_id;

    public $regency_id;

    public $district_id;

    /**
     * @return mixed
     */
    public function getProvinceId()
    {
        return $this->province_id;
    }

    /**
     * @param mixed $province_id
     */
    public function setProvinceId($province_id)
    {
        $this->province_id = $province_id;
    }

    /**
     * @return mixed
     */
    public function getRegencyId()
    {
        return $this->regency_id;
    }

    /**
     * @param mixed $regency_id
     */
    public function setRegencyId($regency_id)
    {
        $this->regency_id = $regency_id;
    }

    /**
     * @return mixed
     */
    public function getDistrictId()
    {
        return $this->district_id;
    }

    /**
     * @param mixed $district_id
     */
    public function setDistrictId($district_id)
    {
        $this->district_id = $district_id;
    }
}