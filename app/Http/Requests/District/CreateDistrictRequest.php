<?php

namespace App\Http\Requests\District;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateDistrictRequest extends Request
{
    // Default Property

    public $regency_id;

    public $name;

    /**
     * @return mixed
     */
    public function getRegencyId()
    {
        return $this->regency_id;
    }

    /**
     * @param mixed $regency_id
     */
    public function setRegencyId($regency_id)
    {
        $this->regency_id = $regency_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param null $guard
     * @return bool
     */
    public function authorize($guard = null)
    {
        if (Auth::guard($guard)->guest() && !Auth::user()->hasAnyRole('admin')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'regency_id' => 'required',
            'name' => 'required'
        ];
    }
}
