<?php
namespace App\Http\Requests\Room;


use Illuminate\Support\Facades\Auth;

class CreatePriceDetailRequest
{
    // Default Property

    public $product_id;

    public $room_id;

    public $price_adult;

    public $price_child;

    public $price_infant;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    /**
     * @return mixed
     */
    public function getPriceAdult()
    {
        return $this->price_adult;
    }

    /**
     * @param mixed $price_adult
     */
    public function setPriceAdult($price_adult)
    {
        $this->price_adult = $price_adult;
    }

    /**
     * @return mixed
     */
    public function getPriceChild()
    {
        return $this->price_child;
    }

    /**
     * @param mixed $price_child
     */
    public function setPriceChild($price_child)
    {
        $this->price_child = $price_child;
    }

    /**
     * @return mixed
     */
    public function getPriceInfant()
    {
        return $this->price_infant;
    }

    /**
     * @param mixed $price_infant
     */
    public function setPriceInfant($price_infant)
    {
        $this->price_infant = $price_infant;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param null $guard
     * @return bool
     */
    public function authorize($guard = null)
    {
        if (Auth::guard($guard)->guest() && !Auth::user()->hasAnyRole('admin')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => '',
            'room_id' => '',
            'price_adult' => '',
            'price_child' => '',
            'price_infant' => ''
        ];
    }

}