<?php
namespace App\Http\Requests\Product;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateProductRequest extends Request
{
    // Default Property

    public $product_type_id;

    public $product_category_id;

    public $property_type_id;

    public $agent_id;

    public $publish;

    public $title;

    public $slug;

    public $overview;

    public $description;

    public $featured_image_url;

    public $featured_video_url;

    public $include_brochure_url;

    public $itinerary_brochure_url;

    public $lat;

    public $lng;

    public $address;

    public $duration;

    public $min_age;

    public $max_age;

    public $price;

    public $grade;

    public $discount;

    public $status;

    // Custom Property

    public $product_tag;

    public $product_facility;

    public $product_room;

    public $product_image;

    public $product_video;

    public $product_way_point;

    public $product_include;

    public $product_itinerary;

    /**
     * @return mixed
     */
    public function getProductTypeId()
    {
        return $this->product_type_id;
    }

    /**
     * @param mixed $product_type_id
     */
    public function setProductTypeId($product_type_id)
    {
        $this->product_type_id = $product_type_id;
    }

    /**
     * @return mixed
     */
    public function getProductCategoryId()
    {
        return $this->product_category_id;
    }

    /**
     * @param mixed $product_category_id
     */
    public function setProductCategoryId($product_category_id)
    {
        $this->product_category_id = $product_category_id;
    }

    /**
     * @return mixed
     */
    public function getPropertyTypeId()
    {
        return $this->property_type_id;
    }

    /**
     * @param mixed $property_type_id
     */
    public function setPropertyTypeId($property_type_id)
    {
        $this->property_type_id = $property_type_id;
    }

    /**
     * @return mixed
     */
    public function getAgentId()
    {
        return $this->agent_id;
    }

    /**
     * @param mixed $agent_id
     */
    public function setAgentId($agent_id)
    {
        $this->agent_id = $agent_id;
    }

    /**
     * @return mixed
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * @param mixed $publish
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * @param mixed $overview
     */
    public function setOverview($overview)
    {
        $this->overview = $overview;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFeaturedImageUrl()
    {
        return $this->featured_image_url;
    }

    /**
     * @param mixed $featured_image_url
     */
    public function setFeaturedImageUrl($featured_image_url)
    {
        $this->featured_image_url = $featured_image_url;
    }

    /**
     * @return mixed
     */
    public function getFeaturedVideoUrl()
    {
        return $this->featured_video_url;
    }

    /**
     * @param mixed $featured_video_url
     */
    public function setFeaturedVideoUrl($featured_video_url)
    {
        $this->featured_video_url = $featured_video_url;
    }

    /**
     * @return mixed
     */
    public function getIncludeBrochureUrl()
    {
        return $this->include_brochure_url;
    }

    /**
     * @param mixed $include_brochure_url
     */
    public function setIncludeBrochureUrl($include_brochure_url)
    {
        $this->include_brochure_url = $include_brochure_url;
    }

    /**
     * @return mixed
     */
    public function getItineraryBrochureUrl()
    {
        return $this->itinerary_brochure_url;
    }

    /**
     * @param mixed $itinerary_brochure_url
     */
    public function setItineraryBrochureUrl($itinerary_brochure_url)
    {
        $this->itinerary_brochure_url = $itinerary_brochure_url;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getMinAge()
    {
        return $this->min_age;
    }

    /**
     * @param mixed $min_age
     */
    public function setMinAge($min_age)
    {
        $this->min_age = $min_age;
    }

    /**
     * @return mixed
     */
    public function getMaxAge()
    {
        return $this->max_age;
    }

    /**
     * @param mixed $max_age
     */
    public function setMaxAge($max_age)
    {
        $this->max_age = $max_age;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getProductTag()
    {
        return $this->product_tag;
    }

    /**
     * @param mixed $product_tag
     */
    public function setProductTag($product_tag)
    {
        $this->product_tag = $product_tag;
    }

    /**
     * @return mixed
     */
    public function getProductFacility()
    {
        return $this->product_facility;
    }

    /**
     * @param mixed $product_facility
     */
    public function setProductFacility($product_facility)
    {
        $this->product_facility = $product_facility;
    }

    /**
     * @return mixed
     */
    public function getProductRoom()
    {
        return $this->product_room;
    }

    /**
     * @param mixed $product_room
     */
    public function setProductRoom($product_room)
    {
        $this->product_room = $product_room;
    }

    /**
     * @return mixed
     */
    public function getProductImage()
    {
        return $this->product_image;
    }

    /**
     * @param mixed $product_image
     */
    public function setProductImage($product_image)
    {
        $this->product_image = $product_image;
    }

    /**
     * @return mixed
     */
    public function getProductVideo()
    {
        return $this->product_video;
    }

    /**
     * @param mixed $product_video
     */
    public function setProductVideo($product_video)
    {
        $this->product_video = $product_video;
    }

    /**
     * @return mixed
     */
    public function getProductWayPoint()
    {
        return $this->product_way_point;
    }

    /**
     * @param mixed $product_way_point
     */
    public function setProductWayPoint($product_way_point)
    {
        $this->product_way_point = $product_way_point;
    }

    /**
     * @return mixed
     */
    public function getProductInclude()
    {
        return $this->product_include;
    }

    /**
     * @param mixed $product_include
     */
    public function setProductInclude($product_include)
    {
        $this->product_include = $product_include;
    }

    /**
     * @return mixed
     */
    public function getProductItinerary()
    {
        return $this->product_itinerary;
    }

    /**
     * @param mixed $product_itinerary
     */
    public function setProductItinerary($product_itinerary)
    {
        $this->product_itinerary = $product_itinerary;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param null $guard
     * @return bool
     */
    public function authorize($guard = null)
    {
        if (Auth::guard($guard)->guest() && !Auth::user()->hasAnyRole('admin')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type_id' => 'required',
            'product_category_id' => '',
            'agent_id' => 'required',
            'publish' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'overview' => '',
            'description' => '',
            'featured_image_url' => '',
            'featured_video_url' => '',
            'include_brochure_url' => '',
            'itinerary_brochure_url' => '',
            'lat' => '',
            'lng' => '',
            'address' => '',
            'duration' => '',
            'min_age' => '',
            'max_age' => '',
            'price' => '',
            'grade' => '',
            'discount' => '',
            'status' => ''
        ];
    }
}
