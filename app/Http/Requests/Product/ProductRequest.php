<?php
namespace App\Http\Requests\Product;


class ProductRequest
{
    // Default Property

    public $product_type_id;

    public $product_category_id;

    public $product_tag_id;

    public $agent_id;

    /**
     * @return mixed
     */
    public function getProductTypeId()
    {
        return $this->product_type_id;
    }

    /**
     * @param mixed $product_type_id
     */
    public function setProductTypeId($product_type_id)
    {
        $this->product_type_id = $product_type_id;
    }

    /**
     * @return mixed
     */
    public function getProductCategoryId()
    {
        return $this->product_category_id;
    }

    /**
     * @param mixed $product_category_id
     */
    public function setProductCategoryId($product_category_id)
    {
        $this->product_category_id = $product_category_id;
    }

    /**
     * @return mixed
     */
    public function getProductTagId()
    {
        return $this->product_tag_id;
    }

    /**
     * @param mixed $product_tag_id
     */
    public function setProductTagId($product_tag_id)
    {
        $this->product_tag_id = $product_tag_id;
    }

    /**
     * @return mixed
     */
    public function getAgentId()
    {
        return $this->agent_id;
    }

    /**
     * @param mixed $agent_id
     */
    public function setAgentId($agent_id)
    {
        $this->agent_id = $agent_id;
    }

}