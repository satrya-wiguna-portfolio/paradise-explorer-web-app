<?php
namespace App\Http\Requests\Bed;


use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateBedRequest extends Request
{
    // Default Property
    public $agent_id;

    public $name;

    public $description;

    /**
     * @return mixed
     */
    public function getAgentId()
    {
        return $this->agent_id;
    }

    /**
     * @param mixed $agent_id
     */
    public function setAgentId($agent_id)
    {
        $this->agent_id = $agent_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param null $guard
     * @return bool
     */
    public function authorize($guard = null)
    {
        if (Auth::guard($guard)->guest() && !Auth::user()->hasAnyRole('admin')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required',
            'name' => 'required',
            'description' => ''
        ];
    }
}
