<?php
namespace App\Http\Requests\RoomDiscount;


class RoomDiscountRequest
{
    public $agent_id;

    public $product_id;

    public $room_id;

    public $open_date;

    public $close_date;

    /**
     * @return mixed
     */
    public function getAgentId()
    {
        return $this->agent_id;
    }

    /**
     * @param mixed $agent_id
     */
    public function setAgentId($agent_id)
    {
        $this->agent_id = $agent_id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    /**
     * @return mixed
     */
    public function getOpenDate()
    {
        return $this->open_date;
    }

    /**
     * @param mixed $open_date
     */
    public function setOpenDate($open_date)
    {
        $this->open_date = $open_date;
    }

    /**
     * @return mixed
     */
    public function getCloseDate()
    {
        return $this->close_date;
    }

    /**
     * @param mixed $close_date
     */
    public function setCloseDate($close_date)
    {
        $this->close_date = $close_date;
    }
}