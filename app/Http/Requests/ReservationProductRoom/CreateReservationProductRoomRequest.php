<?php
namespace App\Http\Requests\ReservationProductRoom;


class CreateReservationProductRoomRequest
{
    public $reservation_product_id;

    public $room_id;

    public $title;

    public $price;

    public $room_discount;

    public $quantity;

    public $total;

    /**
     * @return mixed
     */
    public function getReservationProductId()
    {
        return $this->reservation_product_id;
    }

    /**
     * @param mixed $reservation_product_id
     */
    public function setReservationProductId($reservation_product_id)
    {
        $this->reservation_product_id = $reservation_product_id;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getRoomDiscount()
    {
        return $this->room_discount;
    }

    /**
     * @param mixed $room_discount
     */
    public function setRoomDiscount($room_discount)
    {
        $this->room_discount = $room_discount;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

}