<?php
namespace App\Http\Requests\ProductComment;


class ProductCommentRequest
{
    // Default Property

    public $agent_id;

    /**
     * @return mixed
     */
    public function getAgentId()
    {
        return $this->agent_id;
    }

    /**
     * @param mixed $agent_id
     */
    public function setAgentId($agent_id)
    {
        $this->agent_id = $agent_id;
    }

}