<?php
namespace App\Http\Requests\Room;


class CheckRoomAvailableRequest
{
    public $id;

    public $discount;

    public $check_in;

    public $check_out;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getCheckIn()
    {
        return $this->check_in;
    }

    /**
     * @param mixed $check_in
     */
    public function setCheckIn($check_in)
    {
        $this->check_in = $check_in;
    }

    /**
     * @return mixed
     */
    public function getCheckOut()
    {
        return $this->check_out;
    }

    /**
     * @param mixed $check_out
     */
    public function setCheckOut($check_out)
    {
        $this->check_out = $check_out;
    }


}