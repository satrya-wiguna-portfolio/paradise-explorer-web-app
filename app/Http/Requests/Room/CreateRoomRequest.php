<?php
namespace App\Http\Requests\Room;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateRoomRequest extends Request
{
    // Default Property

    public $product_id;

    public $title;

    public $description;

    public $adult;

    public $children;

    public $max;

    public $footage;

    public $price;

    public $option;

    public $policy;

    // Custom Property

    public $bed;

    public $room_facility;

    public $room_image;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getAdult()
    {
        return $this->adult;
    }

    /**
     * @param mixed $adult
     */
    public function setAdult($adult)
    {
        $this->adult = $adult;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param mixed $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return mixed
     */
    public function getFootage()
    {
        return $this->footage;
    }

    /**
     * @param mixed $footage
     */
    public function setFootage($footage)
    {
        $this->footage = $footage;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param mixed $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return mixed
     */
    public function getPolicy()
    {
        return $this->policy;
    }

    /**
     * @param mixed $policy
     */
    public function setPolicy($policy)
    {
        $this->policy = $policy;
    }

    /**
     * @return mixed
     */
    public function getBed()
    {
        return $this->bed;
    }

    /**
     * @param mixed $bed
     */
    public function setBed($bed)
    {
        $this->bed = $bed;
    }

    /**
     * @return mixed
     */
    public function getRoomFacility()
    {
        return $this->room_facility;
    }

    /**
     * @param mixed $room_facility
     */
    public function setRoomFacility($room_facility)
    {
        $this->room_facility = $room_facility;
    }

    /**
     * @return mixed
     */
    public function getRoomImage()
    {
        return $this->room_image;
    }

    /**
     * @param mixed $room_image
     */
    public function setRoomImage($room_image)
    {
        $this->room_image = $room_image;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param null $guard
     * @return bool
     */
    public function authorize($guard = null)
    {
        if (Auth::guard($guard)->guest() && !Auth::user()->hasAnyRole('admin')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'title' => 'required',
            'description' => '',
            'adult' => '',
            'children' => '',
            'max' => '',
            'footage' => '',
            'price' => '',
            'option' => '',
            'policy' => ''
        ];
    }
}
