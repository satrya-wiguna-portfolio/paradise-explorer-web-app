<?php
namespace App\Http\Requests\ProductCategory;


class ProductCategoryRequest
{
    // Default Property

    public $product_type_id;

    /**
     * @return mixed
     */
    public function getProductTypeId()
    {
        return $this->product_type_id;
    }

    /**
     * @param mixed $product_type_id
     */
    public function setProductTypeId($product_type_id)
    {
        $this->product_type_id = $product_type_id;
    }
}