<?php
namespace App\Http\Requests\ProductCategory;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class UpdateProductCategoryRequest extends Request
{
    // Default Property

    public $id;

    public $product_type_id;

    public $category;

    public $description;

    /**
     * @return mixed
     */
    public function getProductTypeId()
    {
        return $this->product_type_id;
    }

    /**
     * @param mixed $product_type_id
     */
    public function setProductTypeId($product_type_id)
    {
        $this->product_type_id = $product_type_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param null $guard
     * @return bool
     */
    public function authorize($guard = null)
    {
        if (Auth::guard($guard)->guest() && !Auth::user()->hasAnyRole('admin')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'product_type_id' => 'required',
            'category' => 'required',
            'description' => ''
        ];
    }
}
