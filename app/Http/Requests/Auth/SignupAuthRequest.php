<?php
namespace App\Http\Requests\Auth;


use App\Http\Requests\Request;

class SignupAuthRequest extends Request
{
    // Default Property

    public $email;

    public $password;

    // Custom Property

    public $role;

    public $user_activate;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getUserActivate()
    {
        return $this->user_activate;
    }

    /**
     * @param mixed $user_activate
     */
    public function setUserActivate($user_activate)
    {
        $this->user_activate = $user_activate;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:5'
        ];
    }
}
