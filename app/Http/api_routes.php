<?php
	
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	$api->post('auth/login', 'App\Api\V1\Controllers\AuthController@login');
	$api->post('auth/register', 'App\Api\V1\Controllers\AuthController@register');
	$api->post('auth/recovery', 'App\Api\V1\Controllers\AuthController@recovery');
//	$api->post('auth/reset', 'App\Api\V1\Controllers\AuthController@reset');

	$api->post('/fileManager/getAccessKey', ['middleware' => 'api.auth', 'as' => 'fileManagerGetAccessKey', 'uses' => 'App\Api\V1\Controllers\FileManagerController@getAccessKey']);
	$api->post('/fileManager/folderPath', ['middleware' => 'api.auth', 'as' => 'fileManagerFolderPath', 'uses' => 'App\Api\V1\Controllers\FileManagerController@folderPath']);
	$api->post('/fileManager/subFolderPath', ['middleware' => 'api.auth', 'as' => 'fileManagerSubFolderPath', 'uses' => 'App\Api\V1\Controllers\FileManagerController@subFolderPath']);

	$api->post('/country/getAll', ['middleware' => 'api.auth', 'as' => 'countryGetAll', 'uses' => 'App\Api\V1\Controllers\CountryController@getAll']);
	$api->get('/country/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'countryGetDetail', 'uses' => 'App\Api\V1\Controllers\CountryController@getDetail']);
	$api->post('/country/create', ['middleware' => 'api.auth', 'as' => 'countryCreate', 'uses' => 'App\Api\V1\Controllers\CountryController@create']);
	$api->post('/country/update', ['middleware' => 'api.auth', 'as' => 'countryUpdate', 'uses' => 'App\Api\V1\Controllers\CountryController@update']);
	$api->get('/country/delete/{id}', ['middleware' => 'api.auth', 'as' => 'countryDelete', 'uses' => 'App\Api\V1\Controllers\CountryController@delete']);
	$api->get('/country/getCountryList', ['middleware' => 'api.auth', 'as' => 'countryList', 'uses' => 'App\Api\V1\Controllers\CountryController@getCountryList']);

	$api->post('/state/getAll', ['middleware' => 'api.auth', 'as' => 'stateGetAll', 'uses' => 'App\Api\V1\Controllers\StateController@getAll']);
	$api->get('/state/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'stateGetDetail', 'uses' => 'App\Api\V1\Controllers\StateController@getDetail']);
	$api->post('/state/create', ['middleware' => 'api.auth', 'as' => 'stateCreate', 'uses' => 'App\Api\V1\Controllers\StateController@create']);
	$api->post('/state/update', ['middleware' => 'api.auth', 'as' => 'stateUpdate', 'uses' => 'App\Api\V1\Controllers\StateController@update']);
	$api->get('/state/delete/{id}', ['middleware' => 'api.auth', 'as' => 'stateDelete', 'uses' => 'App\Api\V1\Controllers\StateController@delete']);
	$api->get('/state/getStateList', ['middleware' => 'api.auth', 'as' => 'stateList', 'uses' => 'App\Api\V1\Controllers\StateController@getStateList']);
	$api->get('/state/getStateListByCountry/{id}', ['middleware' => 'api.auth', 'as' => 'stateListByCountry', 'uses' => 'App\Api\V1\Controllers\StateController@getStateListByCountry']);

	$api->post('/province/getAll', ['middleware' => 'api.auth', 'as' => 'provinceGetAll', 'uses' => 'App\Api\V1\Controllers\ProvinceController@getAll']);
	$api->get('/province/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'provinceGetDetail', 'uses' => 'App\Api\V1\Controllers\ProvinceController@getDetail']);
	$api->post('/province/create', ['middleware' => 'api.auth', 'as' => 'provinceCreate', 'uses' => 'App\Api\V1\Controllers\ProvinceController@create']);
	$api->post('/province/update', ['middleware' => 'api.auth', 'as' => 'provinceUpdate', 'uses' => 'App\Api\V1\Controllers\ProvinceController@update']);
	$api->get('/province/delete/{id}', ['middleware' => 'api.auth', 'as' => 'provinceDelete', 'uses' => 'App\Api\V1\Controllers\ProvinceController@delete']);
	$api->get('/province/getProvinceList', ['middleware' => 'api.auth', 'as' => 'provinceList', 'uses' => 'App\Api\V1\Controllers\ProvinceController@getProvinceList']);

	$api->post('/regency/getAll', ['middleware' => 'api.auth', 'as' => 'regencyGetAll', 'uses' => 'App\Api\V1\Controllers\RegencyController@getAll']);
	$api->get('/regency/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'regencyGetDetail', 'uses' => 'App\Api\V1\Controllers\RegencyController@getDetail']);
	$api->post('/regency/create', ['middleware' => 'api.auth', 'as' => 'regencyCreate', 'uses' => 'App\Api\V1\Controllers\RegencyController@create']);
	$api->post('/regency/update', ['middleware' => 'api.auth', 'as' => 'regencyUpdate', 'uses' => 'App\Api\V1\Controllers\RegencyController@update']);
	$api->get('/regency/delete/{id}', ['middleware' => 'api.auth', 'as' => 'regencyDelete', 'uses' => 'App\Api\V1\Controllers\RegencyController@delete']);
	$api->get('/regency/getRegencyList', ['middleware' => 'api.auth', 'as' => 'regencyList', 'uses' => 'App\Api\V1\Controllers\RegencyController@getRegencyList']);
	$api->get('/regency/getRegencyListByProvince/{id}', ['middleware' => 'api.auth', 'as' => 'regencyListByProvince', 'uses' => 'App\Api\V1\Controllers\RegencyController@getRegencyListByProvince']);

	$api->post('/district/getAll', ['middleware' => 'api.auth', 'as' => 'districtGetAll', 'uses' => 'App\Api\V1\Controllers\DistrictController@getAll']);
	$api->get('/district/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'districtGetDetail', 'uses' => 'App\Api\V1\Controllers\DistrictController@getDetail']);
	$api->post('/district/create', ['middleware' => 'api.auth', 'as' => 'districtCreate', 'uses' => 'App\Api\V1\Controllers\DistrictController@create']);
	$api->post('/district/update', ['middleware' => 'api.auth', 'as' => 'districtUpdate', 'uses' => 'App\Api\V1\Controllers\DistrictController@update']);
	$api->get('/district/delete/{id}', ['middleware' => 'api.auth', 'as' => 'districtDelete', 'uses' => 'App\Api\V1\Controllers\DistrictController@delete']);
	$api->get('/district/getDistrictList', ['middleware' => 'api.auth', 'as' => 'districtList', 'uses' => 'App\Api\V1\Controllers\DistrictController@getDistrictList']);
	$api->get('/district/getDistrictListByRegency/{id}', ['middleware' => 'api.auth', 'as' => 'districtListByRegency', 'uses' => 'App\Api\V1\Controllers\DistrictController@getDistrictListByRegency']);

	$api->post('/agentType/getAll', ['middleware' => 'api.auth', 'as' => 'agentTypeGetAll', 'uses' => 'App\Api\V1\Controllers\AgentTypeController@getAll']);
	$api->get('/agentType/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'agentTypeGetDetail', 'uses' => 'App\Api\V1\Controllers\AgentTypeController@getDetail']);
	$api->post('/agentType/create', ['middleware' => 'api.auth', 'as' => 'agentTypeCreate', 'uses' => 'App\Api\V1\Controllers\AgentTypeController@create']);
	$api->post('/agentType/update', ['middleware' => 'api.auth', 'as' => 'agentTypeUpdate', 'uses' => 'App\Api\V1\Controllers\AgentTypeController@update']);
	$api->get('/agentType/delete/{id}', ['middleware' => 'api.auth', 'as' => 'agentTypeDelete', 'uses' => 'App\Api\V1\Controllers\AgentTypeController@delete']);
	$api->get('/agentType/getAgentTypeList', ['middleware' => 'api.auth', 'as' => 'agentTypeList', 'uses' => 'App\Api\V1\Controllers\AgentTypeController@getAgentTypeList']);

	$api->post('/role/getAll', ['middleware' => 'api.auth', 'as' => 'roleGetAll', 'uses' => 'App\Api\V1\Controllers\RoleController@getAll']);
	$api->get('/role/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'roleGetDetail', 'uses' => 'App\Api\V1\Controllers\RoleController@getDetail']);
	$api->post('/role/create', ['middleware' => 'api.auth', 'as' => 'roleCreate', 'uses' => 'App\Api\V1\Controllers\RoleController@create']);
	$api->post('/role/update', ['middleware' => 'api.auth', 'as' => 'roleUpdate', 'uses' => 'App\Api\V1\Controllers\RoleController@update']);
	$api->get('/role/delete/{id}', ['middleware' => 'api.auth', 'as' => 'roleDelete', 'uses' => 'App\Api\V1\Controllers\RoleController@delete']);
	$api->get('/role/getRoleList', ['middleware' => 'api.auth', 'as' => 'roleList', 'uses' => 'App\Api\V1\Controllers\RoleController@getRoleList']);

	$api->post('/currency/getAll', ['middleware' => 'api.auth', 'as' => 'currencyGetAll', 'uses' => 'App\Api\V1\Controllers\CurrencyController@getAll']);
	$api->get('/currency/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'currencyGetDetail', 'uses' => 'App\Api\V1\Controllers\CurrencyController@getDetail']);
	$api->post('/currency/create', ['middleware' => 'api.auth', 'as' => 'currencyCreate', 'uses' => 'App\Api\V1\Controllers\CurrencyController@create']);
	$api->post('/currency/update', ['middleware' => 'api.auth', 'as' => 'currencyUpdate', 'uses' => 'App\Api\V1\Controllers\CurrencyController@update']);
	$api->get('/currency/delete/{code}', ['middleware' => 'api.auth', 'as' => 'currencyDelete', 'uses' => 'App\Api\V1\Controllers\CurrencyController@delete']);
	$api->post('/currency/sync', ['middleware' => 'api.auth', 'as' => 'currencySync', 'uses' => 'App\Api\V1\Controllers\CurrencyController@sync']);

	$api->post('/facility/getAll', ['middleware' => 'api.auth', 'as' => 'facilityGetAll', 'uses' => 'App\Api\V1\Controllers\FacilityController@getAll']);
	$api->get('/facility/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'facilityGetDetail', 'uses' => 'App\Api\V1\Controllers\FacilityController@getDetail']);
	$api->post('/facility/create', ['middleware' => 'api.auth', 'as' => 'facilityCreate', 'uses' => 'App\Api\V1\Controllers\FacilityController@create']);
	$api->post('/facility/update', ['middleware' => 'api.auth', 'as' => 'facilityUpdate', 'uses' => 'App\Api\V1\Controllers\FacilityController@update']);
	$api->get('/facility/delete/{id}', ['middleware' => 'api.auth', 'as' => 'facilityDelete', 'uses' => 'App\Api\V1\Controllers\FacilityController@delete']);
	$api->get('/facility/getFacilityByName', ['middleware' => 'api.auth', 'as' => 'facilityByName', 'uses' => 'App\Api\V1\Controllers\FacilityController@getFacilityByName']);

	$api->post('/payment/getAll', ['middleware' => 'api.auth', 'as' => 'paymentGetAll', 'uses' => 'App\Api\V1\Controllers\PaymentController@getAll']);
	$api->get('/payment/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'paymentGetDetail', 'uses' => 'App\Api\V1\Controllers\PaymentController@getDetail']);
	$api->post('/payment/create', ['middleware' => 'api.auth', 'as' => 'paymentCreate', 'uses' => 'App\Api\V1\Controllers\PaymentController@create']);
	$api->post('/payment/update', ['middleware' => 'api.auth', 'as' => 'paymentUpdate', 'uses' => 'App\Api\V1\Controllers\PaymentController@update']);
	$api->get('/payment/delete/{id}', ['middleware' => 'api.auth', 'as' => 'paymentDelete', 'uses' => 'App\Api\V1\Controllers\PaymentController@delete']);
	$api->get('/payment/getPaymentByName', ['middleware' => 'api.auth', 'as' => 'paymentByName', 'uses' => 'App\Api\V1\Controllers\PaymentController@getPaymentByName']);

	$api->post('/member/getAll', ['middleware' => 'api.auth', 'as' => 'memberGetAll', 'uses' => 'App\Api\V1\Controllers\MemberController@getAll']);
	$api->get('/member/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'memberGetDetail', 'uses' => 'App\Api\V1\Controllers\MemberController@getDetail']);
	$api->post('/member/create', ['middleware' => 'api.auth', 'as' => 'memberCreate', 'uses' => 'App\Api\V1\Controllers\MemberController@create']);
	$api->post('/member/update', ['middleware' => 'api.auth', 'as' => 'memberUpdate', 'uses' => 'App\Api\V1\Controllers\MemberController@update']);
	$api->get('/member/delete/{id}', ['middleware' => 'api.auth', 'as' => 'memberDelete', 'uses' => 'App\Api\V1\Controllers\MemberController@delete']);
	$api->get('/member/getMemberByUserId/{userId}', ['middleware' => 'api.auth', 'as' => 'memberByUserId', 'uses' => 'App\Api\V1\Controllers\MemberController@getMemberByUserId']);

	$api->post('/agent/getAll', ['middleware' => 'api.auth', 'as' => 'agentGetAll', 'uses' => 'App\Api\V1\Controllers\AgentController@getAll']);
	$api->get('/agent/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'agentGetDetail', 'uses' => 'App\Api\V1\Controllers\AgentController@getDetail']);
	$api->post('/agent/create', ['middleware' => 'api.auth', 'as' => 'agentCreate', 'uses' => 'App\Api\V1\Controllers\AgentController@create']);
	$api->post('/agent/update', ['middleware' => 'api.auth', 'as' => 'agentUpdate', 'uses' => 'App\Api\V1\Controllers\AgentController@update']);
	$api->get('/agent/delete/{id}', ['middleware' => 'api.auth', 'as' => 'agentDelete', 'uses' => 'App\Api\V1\Controllers\AgentController@delete']);
	$api->get('/agent/getAgentByTypeAndCompany', ['middleware' => 'api.auth', 'as' => 'agentByName', 'uses' => 'App\Api\V1\Controllers\AgentController@getAgentByTypeAndCompany']);
	$api->get('/agent/getAgentByUserId/{userId}', ['middleware' => 'api.auth', 'as' => 'agentByUserId', 'uses' => 'App\Api\V1\Controllers\AgentController@getAgentByUserId']);

	$api->post('/staff/getAll', ['middleware' => 'api.auth', 'as' => 'staffGetAll', 'uses' => 'App\Api\V1\Controllers\StaffController@getAll']);
	$api->get('/staff/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'staffGetDetail', 'uses' => 'App\Api\V1\Controllers\StaffController@getDetail']);
	$api->post('/staff/create', ['middleware' => 'api.auth', 'as' => 'staffCreate', 'uses' => 'App\Api\V1\Controllers\StaffController@create']);
	$api->post('/staff/update', ['middleware' => 'api.auth', 'as' => 'staffUpdate', 'uses' => 'App\Api\V1\Controllers\StaffController@update']);
	$api->get('/staff/delete/{id}', ['middleware' => 'api.auth', 'as' => 'staffDelete', 'uses' => 'App\Api\V1\Controllers\StaffController@delete']);
	$api->get('/staff/getStaffByUserId/{userId}', ['middleware' => 'api.auth', 'as' => 'staffByUserId', 'uses' => 'App\Api\V1\Controllers\StaffController@getStaffByUserId']);

	$api->get('/user/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'userGetDetail', 'uses' => 'App\Api\V1\Controllers\UserController@getDetail']);
	$api->post('/user/create', ['middleware' => 'api.auth', 'as' => 'userCreate', 'uses' => 'App\Api\V1\Controllers\UserController@create']);
	$api->post('/user/update', ['middleware' => 'api.auth', 'as' => 'userUpdate', 'uses' => 'App\Api\V1\Controllers\UserController@update']);
	$api->get('/user/getUserByEmail', ['middleware' => 'api.auth', 'as' => 'userByEmail', 'uses' => 'App\Api\V1\Controllers\UserController@getUserByEmail']);
	$api->get('/user/getUserInfo', ['middleware' => 'api.auth', 'as' => 'userInfo', 'uses' => 'App\Api\V1\Controllers\UserController@getUserInfo']);
	$api->get('/user/getUserProfile', ['middleware' => 'api.auth', 'as' => 'userProfile', 'uses' => 'App\Api\V1\Controllers\UserController@getUserProfile']);

	$api->post('/blogCategory/getAll', ['middleware' => 'api.auth', 'as' => 'blogCategoryGetAll', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@getAll']);
	$api->get('/blogCategory/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'blogCategoryGetDetail', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@getDetail']);
	$api->post('/blogCategory/create', ['middleware' => 'api.auth', 'as' => 'blogCategoryCreate', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@create']);
	$api->post('/blogCategory/update', ['middleware' => 'api.auth', 'as' => 'blogCategoryUpdate', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@update']);
	$api->get('/blogCategory/delete/{id}', ['middleware' => 'api.auth', 'as' => 'blogCategoryDelete', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@delete']);
	$api->get('/blogCategory/getBlogCategoryByName', ['middleware' => 'api.auth', 'as' => 'blogCategoryByName', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@getBlogCategoryByName']);
	$api->get('/blogCategory/getBlogCategoryList', ['middleware' => 'api.auth', 'as' => 'blogCategoryList', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@getBlogCategoryList']);
	$api->get('/blogCategory/getBlogCategoryHierarchy', ['middleware' => 'api.auth', 'as' => 'blogCategoryHierarchy', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@getBlogCategoryHierarchy']);
	$api->get('/blogCategory/getSlug', ['middleware' => 'api.auth', 'as' => 'slug', 'uses' => 'App\Api\V1\Controllers\BlogCategoryController@getSlug']);

	$api->post('/blogTag/getAll', ['middleware' => 'api.auth', 'as' => 'blogTagGetAll', 'uses' => 'App\Api\V1\Controllers\BlogTagController@getAll']);
	$api->get('/blogTag/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'blogTagGetDetail', 'uses' => 'App\Api\V1\Controllers\BlogTagController@getDetail']);
	$api->post('/blogTag/create', ['middleware' => 'api.auth', 'as' => 'blogTagCreate', 'uses' => 'App\Api\V1\Controllers\BlogTagController@create']);
	$api->post('/blogTag/update', ['middleware' => 'api.auth', 'as' => 'blogTagUpdate', 'uses' => 'App\Api\V1\Controllers\BlogTagController@update']);
	$api->get('/blogTag/delete/{id}', ['middleware' => 'api.auth', 'as' => 'blogTagDelete', 'uses' => 'App\Api\V1\Controllers\BlogTagController@delete']);
	$api->get('/blogTag/getBlogTagByName', ['middleware' => 'api.auth', 'as' => 'blogTagByName', 'uses' => 'App\Api\V1\Controllers\BlogTagController@getBlogTagByName']);
	$api->get('/blogTag/getSlug', ['middleware' => 'api.auth', 'as' => 'slug', 'uses' => 'App\Api\V1\Controllers\BlogTagController@getSlug']);

	$api->post('/blogComment/getAll', ['middleware' => 'api.auth', 'as' => 'blogCommentGetAll', 'uses' => 'App\Api\V1\Controllers\BlogCommentController@getAll']);
	$api->get('/blogComment/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'blogCommentGetDetail', 'uses' => 'App\Api\V1\Controllers\BlogCommentController@getDetail']);
	$api->post('/blogComment/update', ['middleware' => 'api.auth', 'as' => 'blogTagUpdate', 'uses' => 'App\Api\V1\Controllers\BlogCommentController@update']);

	$api->post('/productComment/getAll', ['middleware' => 'api.auth', 'as' => 'productCommentGetAll', 'uses' => 'App\Api\V1\Controllers\ProductCommentController@getAll']);
	$api->get('/productComment/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'productCommentGetDetail', 'uses' => 'App\Api\V1\Controllers\ProductCommentController@getDetail']);
	$api->post('/productComment/update', ['middleware' => 'api.auth', 'as' => 'productTagUpdate', 'uses' => 'App\Api\V1\Controllers\ProductCommentController@update']);

	$api->post('/productTag/getAll', ['middleware' => 'api.auth', 'as' => 'productTagGetAll', 'uses' => 'App\Api\V1\Controllers\ProductTagController@getAll']);
	$api->get('/productTag/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'productTagGetDetail', 'uses' => 'App\Api\V1\Controllers\ProductTagController@getDetail']);
	$api->post('/productTag/create', ['middleware' => 'api.auth', 'as' => 'productTagCreate', 'uses' => 'App\Api\V1\Controllers\ProductTagController@create']);
	$api->post('/productTag/update', ['middleware' => 'api.auth', 'as' => 'productTagUpdate', 'uses' => 'App\Api\V1\Controllers\ProductTagController@update']);
	$api->get('/productTag/delete/{id}', ['middleware' => 'api.auth', 'as' => 'productTagDelete', 'uses' => 'App\Api\V1\Controllers\ProductTagController@delete']);
	$api->get('/productTag/getProductTagByName', ['middleware' => 'api.auth', 'as' => 'productTagByName', 'uses' => 'App\Api\V1\Controllers\ProductTagController@getProductTagByName']);
	$api->get('/productTag/getSlug', ['middleware' => 'api.auth', 'as' => 'slug', 'uses' => 'App\Api\V1\Controllers\ProductTagController@getSlug']);

	$api->post('/blog/getAll', ['middleware' => 'api.auth', 'as' => 'blogGetAll', 'uses' => 'App\Api\V1\Controllers\BlogController@getAll']);
	$api->get('/blog/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'blogGetDetail', 'uses' => 'App\Api\V1\Controllers\BlogController@getDetail']);
	$api->post('/blog/create', ['middleware' => 'api.auth', 'as' => 'blogCreate', 'uses' => 'App\Api\V1\Controllers\BlogController@create']);
	$api->post('/blog/update', ['middleware' => 'api.auth', 'as' => 'blogUpdate', 'uses' => 'App\Api\V1\Controllers\BlogController@update']);
	$api->get('/blog/delete/{id}', ['middleware' => 'api.auth', 'as' => 'blogDelete', 'uses' => 'App\Api\V1\Controllers\BlogController@delete']);
	$api->get('/blog/getSlug', ['middleware' => 'api.auth', 'as' => 'slug', 'uses' => 'App\Api\V1\Controllers\BlogController@getSlug']);

	$api->post('/productType/getAll', ['middleware' => 'api.auth', 'as' => 'productTypeGetAll', 'uses' => 'App\Api\V1\Controllers\ProductTypeController@getAll']);
	$api->get('/productType/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'productTypeGetDetail', 'uses' => 'App\Api\V1\Controllers\ProductTypeController@getDetail']);
	$api->post('/productType/create', ['middleware' => 'api.auth', 'as' => 'productTypeCreate', 'uses' => 'App\Api\V1\Controllers\ProductTypeController@create']);
	$api->post('/productType/update', ['middleware' => 'api.auth', 'as' => 'productTypeUpdate', 'uses' => 'App\Api\V1\Controllers\ProductTypeController@update']);
	$api->get('/productType/delete/{id}', ['middleware' => 'api.auth', 'as' => 'productTypeDelete', 'uses' => 'App\Api\V1\Controllers\ProductTypeController@delete']);
	$api->get('/productType/getProductTypeList', ['middleware' => 'api.auth', 'as' => 'productTypeList', 'uses' => 'App\Api\V1\Controllers\ProductTypeController@getProductTypeList']);

	$api->post('/productCategory/getAll', ['middleware' => 'api.auth', 'as' => 'productCategoryGetAll', 'uses' => 'App\Api\V1\Controllers\ProductCategoryController@getAll']);
	$api->get('/productCategory/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'productCategoryGetDetail', 'uses' => 'App\Api\V1\Controllers\ProductCategoryController@getDetail']);
	$api->post('/productCategory/create', ['middleware' => 'api.auth', 'as' => 'productCategoryCreate', 'uses' => 'App\Api\V1\Controllers\ProductCategoryController@create']);
	$api->post('/productCategory/update', ['middleware' => 'api.auth', 'as' => 'productCategoryUpdate', 'uses' => 'App\Api\V1\Controllers\ProductCategoryController@update']);
	$api->get('/productCategory/delete/{id}', ['middleware' => 'api.auth', 'as' => 'productCategoryDelete', 'uses' => 'App\Api\V1\Controllers\ProductCategoryController@delete']);
	$api->get('/productCategory/getProductCategoryList', ['middleware' => 'api.auth', 'as' => 'productCategoryList', 'uses' => 'App\Api\V1\Controllers\ProductCategoryController@getProductCategoryList']);

	$api->post('/product/getAll', ['middleware' => 'api.auth', 'as' => 'productGetAll', 'uses' => 'App\Api\V1\Controllers\ProductController@getAll']);
	$api->get('/product/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'productGetDetail', 'uses' => 'App\Api\V1\Controllers\ProductController@getDetail']);
	$api->post('/product/create', ['middleware' => 'api.auth', 'as' => 'productCreate', 'uses' => 'App\Api\V1\Controllers\ProductController@create']);
	$api->post('/product/update', ['middleware' => 'api.auth', 'as' => 'productUpdate', 'uses' => 'App\Api\V1\Controllers\ProductController@update']);
	$api->get('/product/delete/{id}', ['middleware' => 'api.auth', 'as' => 'productDelete', 'uses' => 'App\Api\V1\Controllers\ProductController@delete']);
	$api->get('/product/getProductByTitle', ['middleware' => 'api.auth', 'as' => 'productByTitle', 'uses' => 'App\Api\V1\Controllers\ProductController@getProductByTitle']);
	$api->get('/product/getSlug', ['middleware' => 'api.auth', 'as' => 'slug', 'uses' => 'App\Api\V1\Controllers\ProductController@getSlug']);

	$api->post('/productDiscount/getAll', ['middleware' => 'api.auth', 'as' => 'productDiscountGetAll', 'uses' => 'App\Api\V1\Controllers\ProductDiscountController@getAll']);
	$api->get('/productDiscount/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'productDiscountGetDetail', 'uses' => 'App\Api\V1\Controllers\ProductDiscountController@getDetail']);
	$api->post('/productDiscount/create', ['middleware' => 'api.auth', 'as' => 'productDiscountCreate', 'uses' => 'App\Api\V1\Controllers\ProductDiscountController@create']);
	$api->post('/productDiscount/update', ['middleware' => 'api.auth', 'as' => 'productDiscountUpdate', 'uses' => 'App\Api\V1\Controllers\ProductDiscountController@update']);
	$api->get('/productDiscount/delete/{id}', ['middleware' => 'api.auth', 'as' => 'productDiscountDelete', 'uses' => 'App\Api\V1\Controllers\ProductDiscountController@delete']);
	$api->get('/productDiscount/getByProductId/{id}', ['middleware' => 'api.auth', 'as' => 'productDiscountGetByProductId', 'uses' => 'App\Api\V1\Controllers\ProductDiscountController@getByProductId']);

	$api->post('/room/getAll', ['middleware' => 'api.auth', 'as' => 'roomGetAll', 'uses' => 'App\Api\V1\Controllers\RoomController@getAll']);
	$api->get('/room/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'roomGetDetail', 'uses' => 'App\Api\V1\Controllers\RoomController@getDetail']);
	$api->post('/room/create', ['middleware' => 'api.auth', 'as' => 'roomCreate', 'uses' => 'App\Api\V1\Controllers\RoomController@create']);
	$api->post('/room/update', ['middleware' => 'api.auth', 'as' => 'roomUpdate', 'uses' => 'App\Api\V1\Controllers\RoomController@update']);
	$api->get('/room/delete/{id}', ['middleware' => 'api.auth', 'as' => 'roomDelete', 'uses' => 'App\Api\V1\Controllers\RoomController@delete']);
	$api->get('/room/getRoomListByAgentId/{agentId}', ['middleware' => 'api.auth', 'as' => 'roomListByAgentId', 'uses' => 'App\Api\V1\Controllers\RoomController@getRoomListByAgentId']);
	$api->get('/room/getRoomByTitle', ['middleware' => 'api.auth', 'as' => 'roomByTitle', 'uses' => 'App\Api\V1\Controllers\RoomController@getRoomByTitle']);

	$api->post('/roomDiscount/getAll', ['middleware' => 'api.auth', 'as' => 'roomDiscountGetAll', 'uses' => 'App\Api\V1\Controllers\RoomDiscountController@getAll']);
	$api->get('/roomDiscount/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'roomDiscountGetDetail', 'uses' => 'App\Api\V1\Controllers\RoomDiscountController@getDetail']);
	$api->post('/roomDiscount/create', ['middleware' => 'api.auth', 'as' => 'roomDiscountCreate', 'uses' => 'App\Api\V1\Controllers\RoomDiscountController@create']);
	$api->post('/roomDiscount/update', ['middleware' => 'api.auth', 'as' => 'roomDiscountUpdate', 'uses' => 'App\Api\V1\Controllers\RoomDiscountController@update']);
	$api->get('/roomDiscount/delete/{id}', ['middleware' => 'api.auth', 'as' => 'roomDiscountDelete', 'uses' => 'App\Api\V1\Controllers\RoomDiscountController@delete']);
	$api->get('/roomDiscount/getByRoomId/{id}', ['middleware' => 'api.auth', 'as' => 'roomDiscountGetByRoomId', 'uses' => 'App\Api\V1\Controllers\RoomDiscountController@getByRoomId']);

	$api->post('/roomAllotment/getAll', ['middleware' => 'api.auth', 'as' => 'roomAllotmentGetAll', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@getAll']);
	$api->get('/roomAllotment/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'roomAllotmentGetDetail', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@getDetail']);
	$api->post('/roomAllotment/create', ['middleware' => 'api.auth', 'as' => 'roomAllotmentCreate', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@create']);
	$api->post('/roomAllotment/update', ['middleware' => 'api.auth', 'as' => 'roomAllotmentUpdate', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@update']);
	$api->get('/roomAllotment/delete/{id}', ['middleware' => 'api.auth', 'as' => 'roomAllotmentDelete', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@delete']);
	$api->get('/roomAllotment/getByRoomId/{id}', ['middleware' => 'api.auth', 'as' => 'roomAllotmentGetByRoomId', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@getByRoomId']);
	$api->get('/roomAllotment/getLockByRoomId/{id}', ['middleware' => 'api.auth', 'as' => 'roomAllotmentGetLockByRoomId', 'uses' => 'App\Api\V1\Controllers\RoomAllotmentController@getLockByRoomId']);

	$api->post('/bed/getAll', ['middleware' => 'api.auth', 'as' => 'bedGetAll', 'uses' => 'App\Api\V1\Controllers\BedController@getAll']);
	$api->get('/bed/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'bedGetDetail', 'uses' => 'App\Api\V1\Controllers\BedController@getDetail']);
	$api->post('/bed/create', ['middleware' => 'api.auth', 'as' => 'bedCreate', 'uses' => 'App\Api\V1\Controllers\BedController@create']);
	$api->post('/bed/update', ['middleware' => 'api.auth', 'as' => 'bedUpdate', 'uses' => 'App\Api\V1\Controllers\BedController@update']);
	$api->get('/bed/delete/{id}', ['middleware' => 'api.auth', 'as' => 'bedDelete', 'uses' => 'App\Api\V1\Controllers\BedController@delete']);
	$api->get('/bed/getBedListByAgentId/{agentId}', ['middleware' => 'api.auth', 'as' => 'bedListByAgentId', 'uses' => 'App\Api\V1\Controllers\BedController@getBedListByAgentId']);

	$api->post('/destination/getAll', ['middleware' => 'api.auth', 'as' => 'destinationGetAll', 'uses' => 'App\Api\V1\Controllers\DestinationController@getAll']);
	$api->get('/destination/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'destinationGetDetail', 'uses' => 'App\Api\V1\Controllers\DestinationController@getDetail']);
	$api->post('/destination/create', ['middleware' => 'api.auth', 'as' => 'destinationCreate', 'uses' => 'App\Api\V1\Controllers\DestinationController@create']);
	$api->post('/destination/update', ['middleware' => 'api.auth', 'as' => 'destinationUpdate', 'uses' => 'App\Api\V1\Controllers\DestinationController@update']);
	$api->get('/destination/delete/{id}', ['middleware' => 'api.auth', 'as' => 'destinationDelete', 'uses' => 'App\Api\V1\Controllers\DestinationController@delete']);
	$api->get('/destination/getDestinationByName', ['middleware' => 'api.auth', 'as' => 'destinationByName', 'uses' => 'App\Api\V1\Controllers\DestinationController@getDestinationByName']);
	$api->get('/destination/getDestinationList', ['middleware' => 'api.auth', 'as' => 'destinationList', 'uses' => 'App\Api\V1\Controllers\DestinationController@getDestinationList']);
	$api->get('/destination/getDestinationHierarchy', ['middleware' => 'api.auth', 'as' => 'destinationHierarchy', 'uses' => 'App\Api\V1\Controllers\DestinationController@getDestinationHierarchy']);

	$api->post('/propertyType/getAll', ['middleware' => 'api.auth', 'as' => 'propertyTypeGetAll', 'uses' => 'App\Api\V1\Controllers\PropertyTypeController@getAll']);
	$api->get('/propertyType/getDetail/{id}', ['middleware' => 'api.auth', 'as' => 'propertyTypeGetDetail', 'uses' => 'App\Api\V1\Controllers\PropertyTypeController@getDetail']);
	$api->post('/propertyType/create', ['middleware' => 'api.auth', 'as' => 'propertyTypeCreate', 'uses' => 'App\Api\V1\Controllers\PropertyTypeController@create']);
	$api->post('/propertyType/update', ['middleware' => 'api.auth', 'as' => 'propertyTypeUpdate', 'uses' => 'App\Api\V1\Controllers\PropertyTypeController@update']);
	$api->get('/propertyType/delete/{id}', ['middleware' => 'api.auth', 'as' => 'propertyTypeDelete', 'uses' => 'App\Api\V1\Controllers\PropertyTypeController@delete']);
	$api->get('/propertyType/getPropertyTypeList', ['middleware' => 'api.auth', 'as' => 'propertyTypeList', 'uses' => 'App\Api\V1\Controllers\PropertyTypeController@getPropertyTypeList']);
});
