<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('test/sendEmail', 'HomeController@sendEmail');

Route::group(['middleware' => 'force.ssl'], function () {
    Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

    Route::auth();

    Route::get('user/activate/{token?}', ['as' => 'userActivate', 'uses' => 'Auth\UserController@activate']);
    Route::get('user/request', ['as' => 'userRequest', 'uses' =>'Auth\UserController@request']);
    Route::post('user/email', ['as' => 'userEmail', 'uses' =>'Auth\UserController@email']);

    Route::get('test/sendEmail', 'HomeController@sendEmail');
});

Route::group(['middleware' => 'force.nonssl'], function () {
    Route::post('/search/checkRoomAvailability', ['as' => 'checkRoomAvailability', 'uses' => 'SearchController@checkRoomAvailability']);
    Route::post('/refreshRoomAllotmentLog', ['as' => 'refreshRoomAllotmentLog', 'uses' => 'HomeController@refreshRoomAllotmentLog']);
    Route::get('/getMostPopularTour', ['as' => 'getMostPopularTour', 'uses' => 'HomeController@getMostPopularTour']);
    Route::get('/getMostPopularHotel', ['as' => 'getMostPopularHotel', 'uses' => 'HomeController@getMostPopularHotel']);
});

Route::group(['middleware' => ['web', 'force.nonssl']], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

    Route::get('/aboutUs', ['as' => 'aboutUs', 'uses' => 'AboutUsController@index']);

    Route::get('/search/', ['as' => 'search', 'uses' => 'SearchController@index']);
    Route::get('/search/destination/{id}', ['as' => 'searchDestination', 'uses' => 'SearchController@destination']);
    Route::get('/search/tour', ['as' => 'searchTour', 'uses' => 'SearchController@tour']);
    Route::get('/search/hotel', ['as' => 'searchHotel', 'uses' => 'SearchController@hotel']);
    Route::get('/search/getRoomByProductId/{id}', ['as' => 'roomByProductId', 'uses' => 'SearchController@getRoomByProductId']);
    Route::get('/search/detail/{id?}', ['as' => 'searchDetail', 'uses' => 'SearchController@detail']);
    Route::post('/search/leaveComment', ['as' => 'searchLeaveComment', 'uses' => 'SearchController@leaveComment']);

    Route::get('/faq', ['as' => 'faq', 'uses' => 'FaqController@index']);

    Route::get('/blog', ['as' => 'blog', 'uses' => 'BlogController@index']);
    Route::get('/blog/detail/{id?}', ['as' => 'blogDetail', 'uses' => 'BlogController@detail']);
    Route::get('/blog/category/{categoryId?}', ['as' => 'blogCategory', 'uses' => 'BlogController@category']);
    Route::get('/blog/tag/{tagId?}', ['as' => 'blogTag', 'uses' => 'BlogController@tag']);
    Route::post('/blog/leaveComment', ['as' => 'blogLeaveComment', 'uses' => 'BlogController@leaveComment']);

    Route::get('/gallery', ['as' => 'gallery', 'uses' => 'GalleryController@index']);
    Route::get('/contact', ['as' => 'contact', 'uses' => 'ContactController@index']);

    Route::get('/cart', ['as' => 'cart', 'uses' => 'CartController@index']);
    Route::post('/cart/add', ['as' => 'cartAdd', 'uses' => 'CartController@add']);
    Route::get('/cart/delete/{productTypeId}/{rowId}/{clientId}', ['as' => 'cartDelete', 'uses' => 'CartController@delete']);
});

Route::group(['middleware' => ['web', 'force.ssl']], function () {
    Route::get('/member', ['middleware' => 'auth.member', 'as' => 'memberDashboard', 'uses' => 'Member\DashboardController@index']);
    Route::get('/member/profile', ['middleware' => 'auth.member', 'as' => 'memberProfile', 'uses' => 'Member\ProfileController@index']);
    Route::post('/member/profile/update', ['middleware' => 'auth.member', 'as' => 'memberProfileUpdate', 'uses' => 'Member\ProfileController@update']);
    Route::get('/member/booking', ['middleware' => 'auth.member', 'as' => 'memberBooking', 'uses' => 'Member\BookingController@index']);
    Route::get('/member/setting', ['middleware' => 'auth.member', 'as' => 'memberSetting', 'uses' => 'Member\SettingController@index']);

    Route::get('/agent', ['middleware' => 'auth.agent', 'as' => 'agentDashboard', 'uses' => 'Agent\DashboardController@index']);
    Route::get('/admin', ['middleware' => 'auth.admin', 'as' => 'adminDashboard', 'uses' => 'Admin\DashboardController@index']);

    Route::get('/checkout/getPaypalSuccess/{reservationId?}', ['as'=>'getPaypalSuccess','uses'=>'CheckOutController@getPaypalSuccess']);
    Route::get('/checkout/getPaypalCancel/{reservationId?}', ['as'=>'getPaypalCancel','uses'=>'CheckOutController@getPaypalCancel']);
    Route::get('/checkout/getDokuSuccess/{reservationId?}', ['as'=>'getDokuSuccess','uses'=>'CheckOutController@getDokuSuccess']);
    Route::get('/checkout/getDokuCancel/{reservationId?}', ['as'=>'getDokuCancel','uses'=>'CheckOutController@getDokuCancel']);
    Route::get('/checkout/getBankTransfer/{reservationId?}', ['as'=>'getBankTransfer','uses'=>'CheckOutController@getBankTransfer']);

    Route::get('/checkout/contact', ['middleware' => ['auth.member'], 'as' => 'checkoutContact', 'uses' => 'CheckOutController@getContact']);
    Route::post('/checkout/contact', ['middleware' => ['auth.member'], 'as' => 'checkoutContact', 'uses' => 'CheckOutController@postContact']);
    Route::get('/checkout/payment', ['middleware' => ['auth.member'], 'as' => 'checkoutPayment', 'uses' => 'CheckOutController@getPayment']);
    Route::post('/checkout/payment', ['middleware' => ['auth.member'], 'as' => 'checkoutPayment', 'uses' => 'CheckOutController@postPayment']);

    Route::get('/checkout/getCountryList', ['middleware' => 'auth.member', 'as' => 'getCountryList', 'uses' => 'CheckOutController@getCountryList']);
    Route::get('/checkout/getStateListByCountry/{id}', ['middleware' => 'auth.member', 'as' => 'getStateListByCountry', 'uses' => 'CheckOutController@getStateListByCountry']);
    Route::get('/checkout/getPaymentProvider', ['middleware' => 'auth.member', 'as' => 'getPaymentProvider', 'uses' => 'CheckOutController@getPaymentProvider']);
});

