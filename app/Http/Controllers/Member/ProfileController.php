<?php
namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Models\Member\Profile\IndexVM;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $model = new indexVM();

        if (Auth::user()->member) {
            $model->title = substr(Auth::user()->member->first_name . ' ' . Auth::user()->member->last_name, 0, 25);
            $model->subTitle = Auth::user()->email;
            $model->avatar = (Auth::user()->member->image_url) ? Auth::user()->member->image_url : asset('assets/common/images/admin/avatar.jpg');

            $model->memberResult = Auth::user()->member;
        } else {
            $model->subTitle = Auth::user()->email;
            $model->avatar = asset('assets/common/images/admin/avatar.jpg');
        }

        return view('member.profile.index', ['model' => $model]);
    }

    public function update()
    {
        echo "tai";
    }
}
