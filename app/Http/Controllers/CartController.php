<?php
namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Requests\Cart\AddCartRequest;
use App\Http\Requests\Cart\RoomCartRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Services\Contract\ICartService;
use App\Services\Contract\IRoomAllotmentService;
use DateTime;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    private $_request;

    private $_addCartRequest;

    private $_roomCartRequest;

    private $_roomAllotmentRequest;

    private $_cartService;

    private $_roomAllotmentService;

    /**
     * CartController constructor.
     * @param Request $request
     * @param ICartService $cartService
     * @param IRoomAllotmentService $roomAllotmentService
     */
    public function __construct(Request $request, ICartService $cartService, IRoomAllotmentService $roomAllotmentService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_cartService = $cartService;

        $this->_roomAllotmentService = $roomAllotmentService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cart.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function add()
    {
        $productTypeId = $this->_request->input('product_type_id');

        if ($productTypeId != 2) {
            if (Cart::instance('tour')->content()->count() != 0) {
                $id = (int)$this->_request->input('id');
                $tourCartItems = Cart::instance('tour')->content()->where('id', $id);

                if ($tourCartItems->count() > 0) {
                    $this->_cartService->delete($productTypeId, $tourCartItems->keys()[0]);
                }

                return self::addTourToCart();
            } else {
                return self::addTourToCart();
            }
        } else {
            if (Cart::instance('hotel')->content()->count() != 0) {
                $id = (int)$this->_request->input('id');
                $hotelCartItems = Cart::instance('hotel')->content()->where('id', $id);

                if ($hotelCartItems->count() > 0) {
                    //Room allotment unlock
                    $this->_roomAllotmentRequest = new RoomAllotmentRequest();

                    $this->_roomAllotmentRequest->client_id = $this->_request->input('client_id');
                    $this->_roomAllotmentRequest->cart_item_id = $hotelCartItems->first()->rowId;

                    $this->_roomAllotmentService->roomUnlock($this->_roomAllotmentRequest);

                    //Cart item delete
                    $this->_cartService->delete($productTypeId, $hotelCartItems->keys()[0]);
                }

                return self::addHotelToCart();
            } else {
                return self::addHotelToCart();
            }
        }
    }

    /**
     * @param $productTypeId
     * @param $rowId
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($productTypeId, $rowId, $clientId)
    {
        if ($productTypeId == 2) {
            //Room allotment unlock
            $this->_roomAllotmentRequest = new RoomAllotmentRequest();

            $this->_roomAllotmentRequest->client_id = $clientId;
            $this->_roomAllotmentRequest->cart_item_id = $rowId;

            $this->_roomAllotmentService->roomUnlock($this->_roomAllotmentRequest);

        }

        $this->_cartService->delete($productTypeId, $rowId);

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function addTourToCart()
    {
        $options = [
            'agent_id' => $this->_request->input('agent_id'),
            'product_type_id' => $this->_request->input('product_type_id'),
            'featured_image_url' => $this->_request->input('featured_image_url'),
            'price' => (float)$this->_request->input('price'),
            'discount' => (float)$this->_request->input('discount'),
            'product_discount' => (float)$this->_request->input('product_discount'),
            'departure' => $this->_request->input('departure'),
            'adult' => (int)$this->_request->input('adult'),
            'child' => (int)$this->_request->input('child')
        ];

        $this->_addCartRequest = new AddCartRequest();

        $totalPax = (int)$this->_request->input('adult') + (int)$this->_request->input('child');

        $this->_addCartRequest->id = (int)$this->_request->input('id');
        $this->_addCartRequest->name = $this->_request->input('title');
        $this->_addCartRequest->qty = $totalPax;
        $this->_addCartRequest->price = (float)$this->_request->input('price_discount');
        $this->_addCartRequest->options = $options;

        $result = $this->_cartService->add($this->_request->input('product_type_id'), $this->_addCartRequest);

        return response()->json($result);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function addHotelToCart()
    {
        $rooms = [];
        $priceDiscount = 0;

        foreach ($this->_request->input('rooms') as $room) {
            $this->_roomCartRequest = new RoomCartRequest();

            $this->_roomCartRequest->id = $room['id'];
            $this->_roomCartRequest->title = $room['title'];
            $this->_roomCartRequest->qty = $room['qty'];
            $this->_roomCartRequest->price = $room['price'];
            $this->_roomCartRequest->room_discount = $room['room_discount'];

            array_push($rooms, $this->_roomCartRequest);

            $priceDiscount += ($room['price_discount'] * $room['qty']);
        }

        $this->_addCartRequest = new AddCartRequest();

        $checkIn = new DateTime($this->_request->input('check_in'));
        $checkOut = new DateTime($this->_request->input('check_out'));
        $totalOfNight = (int)$checkOut->diff($checkIn)->days;

        $options = [
            'agent_id' => $this->_request->input('agent_id'),
            'product_type_id' => $this->_request->input('product_type_id'),
            'featured_image_url' => $this->_request->input('featured_image_url'),
            'discount' => (float)$this->_request->input('discount'),
            'check_in' => $this->_request->input('check_in'),
            'check_out' => $this->_request->input('check_out'),
            'adult' => (int)$this->_request->input('adult'),
            'child' => (int)$this->_request->input('child'),
            'rooms' => $rooms
        ];

        $this->_addCartRequest->id = (int)$this->_request->input('id');
        $this->_addCartRequest->name = $this->_request->input('title');
        $this->_addCartRequest->qty = (int)$totalOfNight;
        $this->_addCartRequest->price = (float)$priceDiscount;
        $this->_addCartRequest->options = $options;

        $result = $this->_cartService->add($this->_request->input('product_type_id'), $this->_addCartRequest);

        //Room allotment lock
        foreach ($this->_request->input('rooms') as $room) {
            $this->_roomAllotmentRequest = new RoomAllotmentRequest();

            $this->_roomAllotmentRequest->from = $this->_request->input('check_in');
            $this->_roomAllotmentRequest->to = $this->_request->input('check_out');
            $this->_roomAllotmentRequest->client_id = $this->_request->input('client_id');
            $this->_roomAllotmentRequest->cart_item_id = $result->_result->rowId;
            $this->_roomAllotmentRequest->room_id = $room['id'];
            $this->_roomAllotmentRequest->lock = $room['qty'];

            $this->_roomAllotmentService->roomLock($this->_roomAllotmentRequest);
        }

        return response()->json($result);
    }
}
