<?php
namespace App\Http\Controllers;


use App\Http\Models\baseVM;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    private $_destinationService;

    private $_currencyService;

    private $_limit;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->_destinationService = App::make('App\Services\Contract\IDestinationService');
        $this->_currencyService = App::make('App\Services\Contract\ICurrencyService');

        $baseModel = new baseVM();

        $this->_limit = 5;

        if (!Session::has('currency')) {
            Session::set('currency', currency()->config('default'));
        }

        $baseModel->randomLimitDestinationResults = $this->_destinationService->getRandomLimitDestination($this->_limit);
        $baseModel->currencyResults = $this->_currencyService->getActiveCurrency();
        $baseModel->currencyList = Session::get('currency');

        view()->share(['baseModel' => $baseModel]);
    }
}
