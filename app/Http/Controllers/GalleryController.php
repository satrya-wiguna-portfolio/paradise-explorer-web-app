<?php
namespace App\Http\Controllers;

use App\Http\Models\Gallery\indexVM;
use App\Http\Requests;
use Vinkla\Instagram\Facades\Instagram;

class GalleryController extends Controller
{
    public function index()
    {
        $model = new IndexVM();

        $model->title = 'Gallery';
        $model->subTitle = 'Follow up Our Instagram @ParadiseExplorers';

        $response = Instagram::users()->getMedia('self');
        $model->instagramResults = json_decode($response->get());

        return view('gallery.index', ['model' => $model]);
    }
}
