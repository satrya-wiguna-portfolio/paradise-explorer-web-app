<?php
namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Models\Home\IndexVM;
use App\Services\Contract\IDestinationService;
use App\Services\Contract\IHomeService;
use App\Services\Contract\IProductCategoryService;
use App\Services\Contract\IProductService;
use App\Services\Contract\IProductTypeService;
use App\Services\Contract\IRoomAllotmentLogService;
use App\Services\Contract\IRoomAllotmentService;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $_request;

    private $_productTypeService;

    private $_productCategoryService;

    private $_destinationService;

    private $_roomAllotmentService;

    private $_roomAllotmentLogService;

    private $_homeService;

    private $_productService;

    private $_typeId;

    /**
     * HomeController constructor.
     * @param Request $request
     * @param IProductTypeService $productTypeService
     * @param IProductCategoryService $productCategoryService
     * @param IDestinationService $destinationService
     * @param IRoomAllotmentService $roomAllotmentService
     * @param IRoomAllotmentLogService $roomAllotmentLogService
     * @param IHomeService $homeService
     * @param IProductService $productService
     */
    public function __construct(Request $request, IProductTypeService $productTypeService, IProductCategoryService $productCategoryService, IDestinationService $destinationService,
                                IRoomAllotmentService $roomAllotmentService, IRoomAllotmentLogService $roomAllotmentLogService, IHomeService $homeService, IProductService $productService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_productTypeService = $productTypeService;

        $this->_productCategoryService = $productCategoryService;

        $this->_destinationService = $destinationService;

        $this->_roomAllotmentService = $roomAllotmentService;

        $this->_roomAllotmentLogService = $roomAllotmentLogService;

        $this->_homeService = $homeService;

        $this->_productService = $productService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new IndexVM();
        $model->randomDestinationResults = $this->_destinationService->getRandomDestination();

        $this->_typeId = 1;
        $model->productCategoryOfTourResults = $this->_productCategoryService->getProductCategoryByProductTypeId($this->_typeId);

        $this->_typeId = 2;
        $model->productCategoryOfHotelResults = $this->_productCategoryService->getProductCategoryByProductTypeId($this->_typeId);

        return view('home.index', ['model' => $model]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshRoomAllotmentLog()
    {
        $clientId = $this->_request->input('client_id');
        $cartItemId = "";

        if (Cart::instance('hotel')->content()->count() != 0) {
            $i = 0;

            foreach (Cart::instance('hotel')->content() as $item) {
                if(++$i !== (Cart::instance('hotel')->content()->count())) {
                    $cartItemId .= "'" . $item->rowId . "', ";
                } else {
                    $cartItemId .= "'" . $item->rowId . "'";
                }
            }
        }

        $result = $this->_roomAllotmentLogService->refresh($clientId, $cartItemId);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMostPopularTour()
    {
        $result = $this->_productService->getMostPopularTour();

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMostPopularHotel()
    {
        $result = $this->_productService->getMostPopularHotel();

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmail()
    {
        $result = $this->_homeService->testSendEmail();

        return response()->json($result);
    }
}
