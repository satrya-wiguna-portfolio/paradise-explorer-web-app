<?php

namespace App\Http\Controllers;

use App\Http\Models\CheckOut\CheckoutVM;
use App\Http\Requests\Checkout\CheckoutContactRequest;
use App\Http\Requests\Checkout\CheckoutPaymentRequest;
use App\Http\Requests\Reservation\CreateReservationRequest;
use App\Http\Requests\Reservation\UpdateReservationRequest;
use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;
use App\Http\Requests\ReservationProduct\CreateReservationProductRequest;
use App\Http\Requests\ReservationProductRoom\CreateReservationProductRoomRequest;
use App\Services\Contract\ICountryService;
use App\Services\Contract\IPaymentService;
use App\Services\Contract\IReservationDokuResponseService;
use App\Services\Contract\IReservationPaypalResponseService;
use App\Services\Contract\IReservationService;
use App\Services\Contract\IRoomAllotmentLogService;
use App\Services\Contract\IStateService;
use Dingo\Api\Exception\ValidationHttpException;
use Exception;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CheckOutController extends Controller
{
    private $_request;

    private $_countryService;

    private $_stateService;

    private $_paymentService;

    private $_reservationService;

    private $_reservationPaypalResponseService;

    private $_reservationDokuResponseService;

    private $_roomAllotmentLogService;

    private $_checkoutContactRequest;

    private $_checkoutPaymentRequest;

    private $_reservationRequest;

    private $_reservationProductRequest;

    private $_reservationProductRoomRequest;

    private $_reservationPaypalResponseRequest;

    private $_reservationDokuResponseRequest;

    private $_updateReservationRequest;


    /**
     * DashboardController constructor.
     * @param Request $request
     * @param ICountryService $countryService
     * @param IStateService $stateService
     * @param IPaymentService $paymentService
     * @param IReservationService $reservationService
     * @param IRoomAllotmentLogService $roomAllotmentLogService
     * @param IReservationPaypalResponseService $reservationPaypalResponseService
     * @param IReservationDokuResponseService $reservationDokuResponseService
     */
    public function __construct(Request $request, ICountryService $countryService, IStateService $stateService, IPaymentService $paymentService, IReservationService $reservationService,
                                IRoomAllotmentLogService $roomAllotmentLogService, IReservationPaypalResponseService $reservationPaypalResponseService, IReservationDokuResponseService $reservationDokuResponseService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_countryService = $countryService;

        $this->_stateService = $stateService;

        $this->_paymentService = $paymentService;

        $this->_reservationService = $reservationService;

        $this->_roomAllotmentLogService = $roomAllotmentLogService;

        $this->_reservationPaypalResponseService = $reservationPaypalResponseService;

        $this->_reservationDokuResponseService = $reservationDokuResponseService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getContact()
    {
        if ((Cart::instance('tour')->content()->count() == 0)
            && (Cart::instance('hotel')->content()->count() == 0)) {
            return redirect()->to('cart');
        }

        return view('checkout.contact');
    }

    public function postContact()
    {
        if ((Cart::instance('tour')->content()->count() == 0)
            && (Cart::instance('hotel')->content()->count() == 0)) {
            return redirect()->to('cart');
        }

        $this->_checkoutContactRequest = new CheckoutContactRequest();

        $this->_checkoutContactRequest->date = date('Y-m-d', strtotime('today'));
        $this->_checkoutContactRequest->user_id = Auth::user()->id;
        $this->_checkoutContactRequest->title = $this->_request->input('title');
        $this->_checkoutContactRequest->first_name = $this->_request->input('first_name');
        $this->_checkoutContactRequest->last_name = $this->_request->input('last_name');
        $this->_checkoutContactRequest->gender = $this->_request->input('gender');
        $this->_checkoutContactRequest->address = $this->_request->input('address');
        $this->_checkoutContactRequest->country_id = $this->_request->input('country');
        $this->_checkoutContactRequest->state_id = $this->_request->input('state');
        $this->_checkoutContactRequest->city = $this->_request->input('city');
        $this->_checkoutContactRequest->zip = $this->_request->input('zip');
        $this->_checkoutContactRequest->email = $this->_request->input('email');
        $this->_checkoutContactRequest->phone = $this->_request->input('phone');
        $this->_checkoutContactRequest->handphone = $this->_request->input('handphone');

        if ($this->_request->session()->has('contact')) {
            $this->_request->session()->forget('contact');
            $this->_request->session()->set('contact', $this->_checkoutContactRequest);
        } else {
            $this->_request->session()->set('contact', $this->_checkoutContactRequest);
        }

        return redirect()->to('checkout/payment');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getPayment()
    {
        if ((Cart::instance('tour')->content()->count() == 0)
            && (Cart::instance('hotel')->content()->count() == 0)) {
            return redirect()->to('cart');
        }

        $model = new CheckoutVM();

        $model->provider = $this->_paymentService->getPayment();

        return view('checkout.payment', ['model' => $model]);
    }

    public function postPayment()
    {
        if ((Cart::instance('tour')->content()->count() == 0)
            && (Cart::instance('hotel')->content()->count() == 0)) {
            return redirect()->to('cart');
        }

        $this->_checkoutPaymentRequest = new CheckoutPaymentRequest();

        $this->_checkoutPaymentRequest->payment_id = $this->_request->input('payment');
        $this->_checkoutPaymentRequest->currency_id = ($this->_request->session()->has('currency')) ? currency()->find($this->_request->session()->get('currency'))->id : currency()->find('IDR')->id;
        $this->_checkoutPaymentRequest->exchange_rate = ($this->_request->session()->has('currency')) ? currency()->find($this->_request->session()->get('currency'))->exchange_rate : currency()->find('IDR')->exchange_rate;

        if ($this->_request->session()->has('payment')) {
            $this->_request->session()->forget('payment');
            $this->_request->session()->set('payment', $this->_checkoutPaymentRequest);
        } else {
            $this->_request->session()->set('payment', $this->_checkoutPaymentRequest);
        }

        return self::process();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process()
    {
        if (!$this->_request->session()->has('reservation')) {
            // Reservation process
            $this->_reservationRequest = new CreateReservationRequest();

            $this->_reservationRequest->invoice_number = strtotime('now') . rand(10, 99);
            $this->_reservationRequest->invoice_release_date = $this->_request->session()->get('contact')->getDate();
            $this->_reservationRequest->user_id = $this->_request->session()->get('contact')->getUserId();
            $this->_reservationRequest->title = $this->_request->session()->get('contact')->getTitle();
            $this->_reservationRequest->first_name = $this->_request->session()->get('contact')->getFirstName();
            $this->_reservationRequest->last_name = $this->_request->session()->get('contact')->getLastName();
            $this->_reservationRequest->gender = $this->_request->session()->get('contact')->getGender();
            $this->_reservationRequest->address = $this->_request->session()->get('contact')->getAddress();
            $this->_reservationRequest->country_id = $this->_request->session()->get('contact')->getCountryId();
            $this->_reservationRequest->state_id = $this->_request->session()->get('contact')->getStateId();
            $this->_reservationRequest->city = $this->_request->session()->get('contact')->getCity();
            $this->_reservationRequest->zip = $this->_request->session()->get('contact')->getZip();
            $this->_reservationRequest->phone = $this->_request->session()->get('contact')->getPhone();
            $this->_reservationRequest->handphone = $this->_request->session()->get('contact')->getHandphone();
            $this->_reservationRequest->payment_id = $this->_request->session()->get('payment')->getPaymentId();
            $this->_reservationRequest->currency_id = $this->_request->session()->get('payment')->getCurrencyId();
            $this->_reservationRequest->exchange_rate = $this->_request->session()->get('payment')->getExchangeRate();

            $products = [];

            if (Cart::instance('tour')->content()->count() != 0) {
                foreach(Cart::instance('tour')->content() as $product) {
                    $this->_reservationProductRequest = new CreateReservationProductRequest();

                    $this->_reservationProductRequest->product_id = $product->id;
                    $this->_reservationProductRequest->title = $product->name;
                    $this->_reservationProductRequest->agent_id = $product->options->agent_id;
                    $this->_reservationProductRequest->product_type_id = $product->options->product_type_id;
                    $this->_reservationProductRequest->price = $product->options->price;
                    $this->_reservationProductRequest->discount = $product->options->discount;
                    $this->_reservationProductRequest->product_discount = $product->options->product_discount;
                    $this->_reservationProductRequest->quantity = $product->qty;
                    $this->_reservationProductRequest->total = $product->price * $product->qty;
                    $this->_reservationProductRequest->departure = $product->options->departure;
                    $this->_reservationProductRequest->adult = $product->options->adult;
                    $this->_reservationProductRequest->child = $product->options->child;

                    array_push($products, $this->_reservationProductRequest);
                }
            }


            if (Cart::instance('hotel')->content()->count() != 0) {
                foreach(Cart::instance('hotel')->content() as $product) {
                    $this->_reservationProductRequest = new CreateReservationProductRequest();

                    $this->_reservationProductRequest->product_id = $product->id;
                    $this->_reservationProductRequest->title = $product->name;
                    $this->_reservationProductRequest->agent_id = $product->options->agent_id;
                    $this->_reservationProductRequest->product_type_id = $product->options->product_type_id;
                    $this->_reservationProductRequest->discount = $product->options->discount;
                    $this->_reservationProductRequest->quantity = $product->qty;
                    $this->_reservationProductRequest->total = $product->price * $product->qty;
                    $this->_reservationProductRequest->check_in = $product->options->check_in;
                    $this->_reservationProductRequest->check_out = $product->options->check_out;
                    $this->_reservationProductRequest->adult = $product->options->adult;
                    $this->_reservationProductRequest->child = $product->options->child;

                    $rooms = [];

                    foreach($product->options->rooms as $room) {
                        $this->_reservationProductRoomRequest = new CreateReservationProductRoomRequest();

                        $this->_reservationProductRoomRequest->room_id = $room->id;
                        $this->_reservationProductRoomRequest->title = $room->title;
                        $this->_reservationProductRoomRequest->price = $room->price;
                        $this->_reservationProductRoomRequest->room_discount = $room->room_discount;
                        $this->_reservationProductRoomRequest->quantity = $room->qty;

                        array_push($rooms, $this->_reservationProductRoomRequest);
                    }

                    $this->_reservationProductRequest->rooms = Collection::make($rooms);

                    array_push($products, $this->_reservationProductRequest);
                }
            }

            $this->_reservationRequest->products = Collection::make($products);

            //Save reservation
            $reservationResult = $this->_reservationService->save($this->_reservationRequest);

            //Set reservation session
            $this->_request->session()->set('reservation', $reservationResult);

        } else {
            //Get reservation session
            $reservationResult = $this->_request->session()->get('reservation');

        }

        // Payment process
        switch($this->_request->session()->get('payment')->getPaymentId()) {
            case 1:
                try {
                    //Update status to unpaid
                    $this->_updateReservationRequest = new UpdateReservationRequest();

                    $this->_updateReservationRequest->id = $reservationResult->_result;
                    $this->_updateReservationRequest->status = 1;

                    $this->_reservationService->updateStatus($this->_updateReservationRequest);

                    $paymentResult = $this->_paymentService->payByPaypal($reservationResult->_result);

                    //Clear allotment logs
                    $clientId = $this->_request->input('client_id');
                    $cartItemId = "";

                    if (Cart::instance('hotel')->content()->count() != 0) {
                        $i = 0;

                        foreach (Cart::instance('hotel')->content() as $item) {
                            if(++$i !== (Cart::instance('hotel')->content()->count())) {
                                $cartItemId .= "'" . $item->rowId . "', ";
                            } else {
                                $cartItemId .= "'" . $item->rowId . "'";
                            }
                        }

                        $this->_roomAllotmentLogService->clear($clientId, $cartItemId);
                    }

                    //Clear cart session
                    Cart::instance('hotel')->destroy();
                    Cart::instance('tour')->destroy();
                    $this->_request->session()->remove('reservation');

                    //Redirect to paypal
                    return redirect($paymentResult->_result);

                } catch (Exception $e) {
                    return  "Exception: " . $e->getMessage() . PHP_EOL;

                }

                break;

            case 2:
                try {
                    $paymentResult = $this->_paymentService->payByDoku($reservationResult->_result);

                    return redirect($paymentResult->_result);

                } catch (Exception $e) {
                    return  "Exception: " . $e->getMessage() . PHP_EOL;

                }

                break;

            default:
                try {
                    //Update status to unpaid
                    $this->_updateReservationRequest = new UpdateReservationRequest();

                    $this->_updateReservationRequest->id = $reservationResult->_result;
                    $this->_updateReservationRequest->status = 1;

                    $this->_reservationService->updateStatus($this->_updateReservationRequest);

                    $paymentResult = $this->_paymentService->payByBankTransfer($reservationResult->_result);

                    //Clear allotment logs
                    $clientId = $this->_request->input('client_id');
                    $cartItemId = "";

                    if (Cart::instance('hotel')->content()->count() != 0) {
                        $i = 0;

                        foreach (Cart::instance('hotel')->content() as $item) {
                            if(++$i !== (Cart::instance('hotel')->content()->count())) {
                                $cartItemId .= "'" . $item->rowId . "', ";
                            } else {
                                $cartItemId .= "'" . $item->rowId . "'";
                            }
                        }

                        $this->_roomAllotmentLogService->clear($clientId, $cartItemId);
                    }

                    //Clear cart session
                    Cart::instance('hotel')->destroy();
                    Cart::instance('tour')->destroy();

                    return redirect($paymentResult->_result);

                } catch (Exception $e) {
                    return  "Exception: " . $e->getMessage() . PHP_EOL;

                }

                break;
        }
    }

    /**
     * @param $reservationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getPaypalSuccess($reservationId)
    {
        try {
            $this->_reservationPaypalResponseRequest = new CreateReservationPaypalResponseRequest();

            $this->_reservationPaypalResponseRequest->reservation_id = $reservationId;
            $this->_reservationPaypalResponseRequest->payment_id = $this->_request->get('paymentId');
            $this->_reservationPaypalResponseRequest->payer_id = $this->_request->get('PayerID');
            $this->_reservationPaypalResponseRequest->token = $this->_request->get('token');

            //Execution payment
            $getPaypalSuccessResult = $this->_paymentService->getPaypalSuccess($this->_reservationPaypalResponseRequest);

            $this->_reservationPaypalResponseRequest->intent = $getPaypalSuccessResult->_result->intent;
            $this->_reservationPaypalResponseRequest->state = $getPaypalSuccessResult->_result->state;
            $this->_reservationPaypalResponseRequest->cart = $getPaypalSuccessResult->_result->cart;

            //Insert reservation paypal response
            $this->_reservationPaypalResponseService->save($this->_reservationPaypalResponseRequest);

            //Update status to paid
            $this->_updateReservationRequest = new UpdateReservationRequest();

            $this->_updateReservationRequest->id = $reservationId;
            $this->_updateReservationRequest->invoice_paid_date = date('Y-m-d H:i:s', strtotime('now'));
            $this->_updateReservationRequest->status = 2;

            $this->_reservationService->updateStatus($this->_updateReservationRequest);

            //Get reservation
            $getReservationDetailResult = $this->_reservationService->getReservationDetail($reservationId);

            //Set view model
            $model = new CheckoutVM();

            $model->title = 'Congratulation';
            $model->subTitle = 'Your booking ID#' . $getReservationDetailResult->invoice_number . ' has been paid';
            $model->reservation = $getReservationDetailResult;
            $model->paypal_response = $this->_reservationPaypalResponseRequest;

            return view('checkout.paypal_success', ['model' => $model]);

        } catch(Exception $e) {
            return redirect()->to('/');

        }

    }

    /**
     * @param $reservationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPaypalCancel($reservationId)
    {
        try {
            //Get reservation
            $getReservationDetailResult = $this->_reservationService->getReservationDetail($reservationId);

            //Set view model
            $model = new CheckoutVM();

            $model->title = 'Thank you';
            $model->subTitle = 'Your booking ID#' . $getReservationDetailResult->invoice_number . ' has not been paid';
            $model->reservation = $getReservationDetailResult;

            return view('checkout.paypal_cancel', ['model' => $model]);

        } catch(Exception $e) {
            return redirect()->to('/');

        }
    }

    public function getDokuSuccess($reservationId)
    {

    }

    public function getDokuCancel()
    {

    }

    public function getBankTransfer($reservationId)
    {
        try {
            //Get reservation
            $getReservationDetailResult = $this->_reservationService->getReservationDetail($reservationId);

            //Set view model
            $model = new CheckoutVM();

            $model->title = 'Thank you';
            $model->subTitle = 'Your booking ID#' . $getReservationDetailResult->invoice_number . ' has not been paid';
            $model->reservation = $getReservationDetailResult;

            return view('checkout.bank_transfer', ['model' => $model]);

        } catch(Exception $e) {
            return redirect()->to('/');

        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountryList()
    {
        $result = $this->_countryService->getCountryList();

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStateListByCountry($id)
    {
        $result = $this->_stateService->getStateListByCountry($id);

        return response()->json($result);
    }
}
