<?php
namespace App\Http\Controllers;


use App\Http\Requests;

class ContactController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $title = 'Contact';
        $subTitle = 'We\'d love to hear from you!';

        return view('contact.index', compact('title', 'subTitle'));
    }
}
