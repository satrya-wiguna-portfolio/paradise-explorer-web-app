<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\SignupAuthRequest;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests\UserLog\CreateUserActivateRequest;
use App\Models\User;
use App\Http\Requests;
use App\Services\Contract\IAuthService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    private $_request;

    private $_signupAuthRequest;

    private $_roleRequest;

    private $_userActivateRequest;

    private $_authService;

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * AuthController constructor.
     * @param Request $request
     * @param IAuthService $authService
     */
    public function __construct(Request $request, IAuthService $authService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_authService = $authService;

        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

//    /**
//     * Get a validator for an incoming registration request.
//     *
//     * @param  array  $data
//     * @return \Illuminate\Contracts\Validation\Validator
//     */
//    protected function validator(array $data)
//    {
//        return Validator::make($data, [
//            'email' => 'required|email|max:255|unique:users',
//            'password' => 'required|min:6|confirmed',
//        ]);
//    }
//
//    /**
//     * Create a new user instance after a valid registration.
//     *
//     * @param  array  $data
//     * @return User
//     */
//    protected function create(array $data)
//    {
//        return User::create([
//            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
//        ]);
//    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        $this->_roleRequest = new RoleRequest();

        $this->_roleRequest->id = $this->_request->input('role_id');

        $this->_userActivateRequest = new CreateUserActivateRequest();

        $this->_userActivateRequest->email = $this->_request->input('email');
        $this->_userActivateRequest->token = str_random(60);

        $this->_signupAuthRequest = new SignupAuthRequest();

        $this->_signupAuthRequest->email = $this->_request->input('email');
        $this->_signupAuthRequest->password = $this->_request->input('password');
        $this->_signupAuthRequest->role = $this->_roleRequest;
        $this->_signupAuthRequest->user_activate = $this->_userActivateRequest;

        $signupResult = $this->_authService->signupByWeb($this->_signupAuthRequest);

        if ($signupResult->getValidator()) {
            return redirect()->back()->withInput()->withErrors($signupResult->getValidator());
        } else {
            $messages = $signupResult->_messages;

            if ($this->_request->input('checkout')) {
                return redirect()->to('login?checkout=' . urlencode($this->_request->input('checkout')))->with(['type' => $messages[0]->type, 'message' => $messages[0]->text]);
            } else {
                return redirect()->to('login')->with(['type' => $messages[0]->type, 'message' => $messages[0]->text]);
            }

        }
    }

    //SOCIAL LOGIN
    /**
     * @param $provider
     * @return mixed
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();

            $authUser = $this->findOrCreateUser($user, $provider);

            Auth::login($authUser, true);

            return redirect($this->redirectTo);

        } catch (\Exception $e) {
            return redirect('login');
        }
    }

    /**
     * @param $user
     * @param $provider
     * @return static
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();

        if ($authUser) {
            return $authUser;
        }

        return User::create([
            'email' => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'status' => 1
        ]);
    }
    //END SOCIAL LOGIN
}
