<?php
namespace App\Http\Controllers\Auth;

use App\Http\Requests\UserLog\UpdateUserActivateRequest;
use App\Services\Contract\IUserService;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private $_request;

    private $_userActivateRequest;

    private $_userService;
    /**
     * CartController constructor.
     * @param Request $request
     * @param IUserService $userService
     */
    public function __construct(Request $request, IUserService $userService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_userService = $userService;
    }

    /**
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($token)
    {
        $result = $this->_userService->activate($token, urldecode($this->_request->get('email')));

        $messages = $result->_messages;

        return redirect()->to('login')->with(['type' => $messages[0]->type, 'message' => $messages[0]->text]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function request()
    {
        return view('auth.users.request');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function email()
    {
        $this->_userActivateRequest = new UpdateUserActivateRequest();

        $this->_userActivateRequest->email = $this->_request->input('email');
        $this->_userActivateRequest->token = str_random(60);

        $result = $this->_userService->email($this->_userActivateRequest);

        $messages = $result->_messages;

        return redirect()->back()->with(['type' => $messages[0]->type, 'message' => $messages[0]->text]);
    }
}
