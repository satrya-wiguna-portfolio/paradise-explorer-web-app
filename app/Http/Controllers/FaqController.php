<?php
namespace App\Http\Controllers;


use App\Http\Models\FAQ\indexVM;
use App\Http\Requests;

class FaqController extends Controller
{
    /**
     * FaqController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new indexVM();

        $model->title = 'FAQs';
        $model->subTitle = 'We are happy to answer your questions';

        return view('faq.index', ['model' => $model]);
    }
}
