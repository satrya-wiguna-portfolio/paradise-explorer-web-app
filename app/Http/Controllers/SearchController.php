<?php
namespace App\Http\Controllers;


use App\Helper\AccessHelper;
use App\Http\Models\Search\DestinationVM;
use App\Http\Models\Search\DetailVM;
use App\Http\Models\Search\IndexVM;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Product\CheckProductAvailableRequest;
use App\Http\Requests\Product\SearchProductRequest;
use App\Http\Requests\ProductComment\CreateProductCommentRequest;
use App\Http\Requests\Room\CheckRoomAvailableRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Services\Contract\IDestinationService;
use App\Services\Contract\IProductCategoryService;
use App\Services\Contract\IProductCommentService;
use App\Services\Contract\IProductService;
use App\Services\Contract\IProductTypeService;
use App\Services\Contract\IRoomAllotmentService;
use App\Services\Contract\IRoomService;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    private $_request;

    private $_createProductCommentRequest;

    private $_roomAllotmentRequest;

    private $_productService;

    private $_productTypeService;

    private $_productCategoryService;

    private $_destinationService;

    private $_productCommentService;

    private $_roomService;

    private $_roomAllotmentService;

    private $_typeId;

    /**
     * SearchController constructor.
     * @param Request $request
     * @param IProductService $productService
     * @param IProductCategoryService $productCategoryService
     * @param IProductTypeService $productTypeService
     * @param IDestinationService $destinationService
     * @param IRoomService $roomService
     * @param IProductCommentService $productCommentService
     * @param IRoomAllotmentService $roomAllotmentService
     */
    public function __construct(Request $request, IProductService $productService, IProductCategoryService $productCategoryService, IProductTypeService $productTypeService,
                                IDestinationService $destinationService, IRoomService $roomService, IProductCommentService $productCommentService, IRoomAllotmentService $roomAllotmentService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_productService = $productService;

        $this->_productTypeService = $productTypeService;

        $this->_productCategoryService = $productCategoryService;

        $this->_destinationService = $destinationService;

        $this->_productCommentService = $productCommentService;

        $this->_roomService = $roomService;

        $this->_roomAllotmentService = $roomAllotmentService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new IndexVM();

        $model->type = 1;
        $model->destination = 'All Destinations';

        return $this->getProductByAllDestinations($model);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destination($id)
    {
        $model = new DestinationVM();

        $model->id = $id;
        $model->type = 1;
        $model->destinationResult = $this->_destinationService->getDestinationDetail($model->id);
        $model->destination = $model->destinationResult->name;
        $model->lat = $model->destinationResult->lat;
        $model->lng = $model->destinationResult->lng;

        return $this->getProductByDestination($model);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function tour()
    {
        $model = new IndexVM();

        $this->_typeId = 1;
        $model->productCategoryOfTourResults = $this->_productCategoryService->getProductCategoryByProductTypeId($this->_typeId);

        if (!$this->_request->get('destination')) {
            $remote  = '125.167.100.35'; //$_SERVER['REMOTE_ADDR']
            $data = \Location::get($remote);

            $model->type = 1;
            $model->destination = $data->cityName . ', ' . $data->regionName;
            $model->lat = $data->latitude;
            $model->lng = $data->longitude;
        } else {
            $model->type = $this->_request->get('type');
            $model->destination = $this->_request->get('destination');
            $model->lat = $this->_request->get('lat');
            $model->lng = $this->_request->get('lng');
        }

        return $this->getProductBySearch($model);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function hotel()
    {
        $model = new IndexVM();

        $this->_typeId = 2;
        $model->productCategoryOfHotelResults = $this->_productCategoryService->getProductCategoryByProductTypeId($this->_typeId);

        if (!$this->_request->get('destination')) {
            $remote  = '125.167.100.35'; //$_SERVER['REMOTE_ADDR']
            $data = \Location::get($remote);

            $model->type = 2;
            $model->destination = $data->cityName . ', ' . $data->regionName;
            $model->lat = $data->latitude;
            $model->lng = $data->longitude;
        } else {
            $model->type = $this->_request->get('type');
            $model->destination = $this->_request->get('destination');
            $model->lat = $this->_request->get('lat');
            $model->lng = $this->_request->get('lng');
        }

        return $this->getProductBySearch($model);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $model = new DetailVM();

        $model->productId = $id;

        return $this->getProductDetail($model);
    }

    /**
     * @return mixed
     */
    public function leaveComment()
    {
        $this->_createProductCommentRequest = new CreateProductCommentRequest();

        $this->_createProductCommentRequest->product_id = $this->_request->input('product_id');
        $this->_createProductCommentRequest->user_id = $this->_request->input('user_id');
        $this->_createProductCommentRequest->comment = $this->_request->input('comment');
        $this->_createProductCommentRequest->rate = $this->_request->input('rate');
        $this->_createProductCommentRequest->ip_address = AccessHelper::getIPAddress();
        $this->_createProductCommentRequest->browser = AccessHelper::getBrowserInformation();

        $result = $this->_productCommentService->save($this->_createProductCommentRequest);

        if (Auth::guest()) {
            return redirect()->to('login');
        }

        if ($result->getValidator()) {
            return redirect()->back()->withInput()->withErrors($result->getValidator());
        }

        return redirect()->back();
    }

    /**
     * @return bool
     */
    public function checkProductAvailability()
    {
        $checkProductAvailability = new CheckProductAvailableRequest();

        return true;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkRoomAvailability()
    {
        self::setLockUnlockRoom('unlock');

        $checkRoomAvailableRequest = new CheckRoomAvailableRequest();

        $checkRoomAvailableRequest->id = $this->_request->input('id');
        $checkRoomAvailableRequest->discount = $this->_request->input('discount');
        $checkRoomAvailableRequest->check_in = $this->_request->input('check_in');
        $checkRoomAvailableRequest->check_out = $this->_request->input('check_out');

        $rooms = $this->_roomService->checkRoomAvailability($checkRoomAvailableRequest);

        self::setLockUnlockRoom('lock');

        return response()->json($rooms);
    }

    /**
     * @param IndexVM $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getProductBySearch(IndexVM $model)
    {
        $pageProductRequest = new GenericPageRequest();
        $pageProductRequest->length = 10;

        $order[0]['column'] = 0;
        $order[0]['dir'] = 'asc';
        $pageProductRequest->order = $order[0];

        $columns[0]['name'] = 'distance';
        $pageProductRequest->columns = $columns;

        $pageProductRequest->search = $this->_request->get('search')['value'];

        $customProductRequest = new SearchProductRequest();
        $customProductRequest->destination = $model->destination;
        $customProductRequest->lat = $model->lat;
        $customProductRequest->lng = $model->lng;
        $customProductRequest->product_type_id = $model->type;
        $customProductRequest->product_category_id = $this->_request->get('category');
        $customProductRequest->age = $this->_request->get('age');

        $pageProductRequest->custom = $customProductRequest;

        $model->products = $this->_productService->getProduct($pageProductRequest);

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageItems = $model->products->slice(($currentPage * $pageProductRequest->getLength()) - $pageProductRequest->getLength(), $pageProductRequest->getLength())->all();

        $model->productResults = new LengthAwarePaginator($currentPageItems , count($model->products), $pageProductRequest->getLength());
        $model->productResults->appends($this->_request->all());
        $model->productResults->setPath($this->_request->url());

        $model->productTypeResults = $this->_productTypeService->getProductType();
        $model->productCategoryResults = $this->_productCategoryService->getProductCategoryByProductTypeId($model->type);

        return view('search.index', ['model' => $model]);
    }

    /**
     * @param DestinationVM $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $id
     */
    private function getProductByDestination(DestinationVM $model)
    {
        $pageProductRequest = new GenericPageRequest();
        $pageProductRequest->length = 15;

        $order[0]['column'] = 0;
        $order[0]['dir'] = 'asc';
        $pageProductRequest->order = $order[0];

        $columns[0]['name'] = 'distance';
        $pageProductRequest->columns = $columns;

        $pageProductRequest->search = $this->_request->get('search')['value'];

        $customProductRequest = new SearchProductRequest();
        $customProductRequest->destination = $model->destination;
        $customProductRequest->lat = $model->lat;
        $customProductRequest->lng = $model->lng;
        $customProductRequest->product_type_id = $model->type;

        $pageProductRequest->custom = $customProductRequest;

        $model->products = $this->_productService->getProduct($pageProductRequest);

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageItems = $model->products->slice(($currentPage * $pageProductRequest->getLength()) - $pageProductRequest->getLength(), $pageProductRequest->getLength())->all();

        $model->productResults = new LengthAwarePaginator($currentPageItems , count($model->products), $pageProductRequest->getLength());
        $model->productResults->appends($this->_request->all());
        $model->productResults->setPath($this->_request->url());

        $model->destinationResults = $this->_destinationService->getDestinationDetail($model->id);

        return view('search.destination', ['model' => $model]);
    }

    /**
     * @param IndexVM $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getProductByAllDestinations(IndexVM $model)
    {
        $pageProductRequest = new GenericPageRequest();
        $pageProductRequest->length = 15;

        $order[0]['column'] = 0;
        $order[0]['dir'] = 'desc';
        $pageProductRequest->order = $order[0];

        $columns[0]['name'] = 'publish';
        $pageProductRequest->columns = $columns;

        $pageProductRequest->search = $this->_request->get('search')['value'];

        $customProductRequest = new SearchProductRequest();
        $customProductRequest->destination = $model->destination;
        $customProductRequest->product_type_id = $model->type;

        $pageProductRequest->custom = $customProductRequest;

        $model->products = $this->_productService->getProduct($pageProductRequest);

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageItems = $model->products->slice(($currentPage * $pageProductRequest->getLength()) - $pageProductRequest->getLength(), $pageProductRequest->getLength())->all();

        $model->productResults = new LengthAwarePaginator($currentPageItems , count($model->products), $pageProductRequest->getLength());
        $model->productResults->appends($this->_request->all());
        $model->productResults->setPath($this->_request->url());

        return view('search.destination', ['model' => $model]);
    }

    /**
     * @param DetailVM $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    private function getProductDetail(DetailVM $model)
    {
        $model->userId = (!Auth::guest()) ? Auth::user()->id : null;
        $model->productResults = $this->_productService->getProductDetail($model->productId);
        $model->randomLimitProductByProductTypeIdResults = $this->_productService->getRandomLimitProductByProductTypeId($model->productResults->product_type_id);
        $model->productCommentResults = $this->_productCommentService->getProductCommentByProductId($model->productId);

        $model->openGraph = new OpenGraph();

        $model->openGraph->title($model->productResults->title)
            ->type($model->productResults->product_type->type)
            ->image($model->productResults->featured_image_url)
            ->description($model->productResults->description)
            ->siteName('Paradise Explorers')
            ->url();

        return view('search.detail', ['model' => $model]);
    }

    /**
     * @param null $mode
     */
    private function setLockUnlockRoom($mode = null)
    {
        if (Cart::instance('hotel')->content()->count() != 0) {
            $id = (int)$this->_request->input('id');
            $hotelCartItems = Cart::instance('hotel')->content()->where('id', $id);

            if ($hotelCartItems->count() > 0) {
                if ($mode == 'unlock') {
                    $this->_roomAllotmentRequest = new RoomAllotmentRequest();

                    $this->_roomAllotmentRequest->client_id = $this->_request->input('client_id');
                    $this->_roomAllotmentRequest->cart_item_id = $hotelCartItems->first()->rowId;

                    $this->_roomAllotmentService->roomUnlock($this->_roomAllotmentRequest);
                }

                if ($mode == 'lock') {
                    foreach ($hotelCartItems->first()->options->rooms as $room) {
                        $this->_roomAllotmentRequest = new RoomAllotmentRequest();

                        $this->_roomAllotmentRequest->from = $hotelCartItems->first()->options->check_in;
                        $this->_roomAllotmentRequest->to = $hotelCartItems->first()->options->check_out;
                        $this->_roomAllotmentRequest->client_id = $this->_request->input('client_id');
                        $this->_roomAllotmentRequest->cart_item_id = $hotelCartItems->first()->rowId;
                        $this->_roomAllotmentRequest->room_id = $room->id;
                        $this->_roomAllotmentRequest->lock = $room->qty;

                        $this->_roomAllotmentService->roomLock($this->_roomAllotmentRequest);
                    }
                }

            }
        }

    }
}
