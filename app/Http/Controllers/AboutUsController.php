<?php
namespace App\Http\Controllers;

use App\Http\Models\AboutUs\IndexVM;
use Illuminate\Http\Request;
use App\Http\Requests;

class AboutUsController extends Controller
{

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new indexVM();

        $model->title = 'About Us';
        $model->subTitle = 'True story of our journey';

        return view('about_us.index', ['model' => $model]);
    }
}
