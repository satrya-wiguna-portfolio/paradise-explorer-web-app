<?php
namespace App\Http\Controllers;

use App\Helper\AccessHelper;
use App\Http\Models\Blog\detailVM;
use App\Http\Models\Blog\indexVM;
use App\Http\Requests\Blog\BlogRequest;
use App\Http\Requests\BlogComment\CreateBlogCommentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IBlogCategoryService;
use App\Services\Contract\IBlogCommentService;
use App\Services\Contract\IBlogService;
use App\Services\Contract\IBlogTagService;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    private $_request;

    private $_createBlogCommentRequest;

    private $_blogService;

    private $_blogCategoryService;

    private $_blogTagService;

    private $_blogCommentService;

    /**
     * HomeController constructor.
     * @param Request $request
     * @param IBlogService $blogService
     * @param IBlogCategoryService $blogCategoryService
     * @param IBlogTagService $blogTagService
     * @param IBlogCommentService $blogCommentService
     */
    public function __construct(Request $request, IBlogService $blogService, IBlogCategoryService $blogCategoryService, IBlogTagService $blogTagService, IBlogCommentService $blogCommentService)
    {
        parent::__construct();

        $this->_request = $request;

        $this->_blogService = $blogService;

        $this->_blogCategoryService = $blogCategoryService;

        $this->_blogTagService = $blogTagService;

        $this->_blogCommentService = $blogCommentService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new indexVM();

        return self::getBlog($model);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $model = new detailVM();

        $model->title = 'Blog';
        $model->subTitle = 'We share stories and give advice';

        $model->blogId = $id;
        $model->userId = (!Auth::guest()) ? Auth::user()->id : null;

        $model->blogResult = $this->_blogService->getBlogDetail($id);
        $model->blogRecentResults = $this->_blogService->getBlogRecent();
        $model->blogCategoryMostlyResults = $this->_blogCategoryService->getBlogCategoryMostly();
        $model->blogTagResults = $this->_blogTagService->getBlogTag();
        $model->blogCommentResults = $this->_blogCommentService->getBlogCommentByBlogId($id);

        $category = null;

        foreach($model->blogResult->blog_category as $blog_category) {
            if ($blog_category->name != $model->blogResult->blog_category->last()->name) {
                $category .= $blog_category->name . ', ';
            } else {
                $category .= $blog_category->name;
            }
        }

        $openGraph = new OpenGraph();

        $openGraph->title($model->blogResult->title)
            ->type($category)
            ->image($model->blogResult->featured_image_url)
            ->description($model->blogResult->description)
            ->siteName('Paradise Explorers')
            ->url();

        return view('blog.detail', ['model' => $model]);
    }

    /**
     * @param $categoryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($categoryId)
    {
        $model = new IndexVm();

        $model->categoryId = $categoryId;

        return self::getBlog($model);
    }

    /**
     * @param $tagId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag($tagId)
    {
        $model = new IndexVM();

        $model->tagId = $tagId;

        return self::getBlog($model);
    }

    /**
     * @return mixed
     */
    public function leaveComment()
    {
        $this->_createBlogCommentRequest = new CreateBlogCommentRequest();

        $this->_createBlogCommentRequest->blog_id = $this->_request->input('blog_id');
        $this->_createBlogCommentRequest->user_id = $this->_request->input('user_id');
        $this->_createBlogCommentRequest->comment = $this->_request->input('comment');
        $this->_createBlogCommentRequest->rate = $this->_request->input('rate');
        $this->_createBlogCommentRequest->ip_address = AccessHelper::getIPAddress();
        $this->_createBlogCommentRequest->browser = AccessHelper::getBrowserInformation();

        $result = $this->_blogCommentService->save($this->_createBlogCommentRequest);

        if (Auth::guest()) {
            return redirect()->to('login');
        }

        if ($result->getValidator()) {
            return redirect()->back()->withInput()->withErrors($result->getValidator());
        }

        return redirect()->back();
    }

    /**
     * @param indexVM $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getBlog(IndexVM $model)
    {
        $model->title = 'Blog';
        $model->subTitle = 'We share stories and give advice';

        $pageBlogRequest = new GenericPageRequest();
        $pageBlogRequest->length = 10;

        $order[0]['column'] = 0;
        $order[0]['dir'] = 'desc';
        $pageBlogRequest->order = $order[0];

        $columns[0]['name'] = 'publish';
        $pageBlogRequest->columns = $columns;

        $pageBlogRequest->search = $this->_request->get('search')['value'];

        $customBlogRequest = new BlogRequest();
        $customBlogRequest->blog_category_id = $model->categoryId;
        $customBlogRequest->blog_tag_id = $model->tagId;

        $pageBlogRequest->custom = $customBlogRequest;

        $model->blogResults = $this->_blogService->getBlog($pageBlogRequest);
        $model->blogCategoryMostlyResults = $this->_blogCategoryService->getBlogCategoryMostly();
        $model->blogTagResults = $this->_blogTagService->getBlogTag();
        $model->blogRecentResults = $this->_blogService->getBlogRecent();
        $model->blogCategoryResult = ($model->categoryId) ? $this->_blogCategoryService->getBlogCategoryDetail($model->categoryId) : null;
        $model->blogTagResult = ($model->tagId) ? $this->_blogTagService->getBlogTagDetail($model->tagId) : null;

        return view('blog.index', ['model' => $model]);
    }
}
