<?php

namespace App\Http\Middleware;

use Closure;

class ForceSSL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->secure()) {
            $secureUrl = 'https://' . str_replace('8888', '8890', $request->getHttpHost()) . $request->getRequestUri();

            return redirect($secureUrl);
        }

        return $next($request);
    }
}
