<?php

namespace App\Http\Middleware;

use Closure;

class ForceNonSSL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->secure()) {
            $secureUrl = 'http://' . str_replace('8890', '8888', $request->getHttpHost()) . $request->getRequestUri();

            return redirect($secureUrl);
        }

        return $next($request);
    }
}
