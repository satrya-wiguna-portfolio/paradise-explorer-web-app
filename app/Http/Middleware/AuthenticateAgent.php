<?php

namespace App\Http\Middleware;

use App\Services\Contract\IUserService;
use Closure;

class AuthenticateAgent
{
    private $_userService;

    public function __construct(IUserService $userService)
    {
        $this->_userService = $userService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = $request->get('email');

        $resultUser = $this->_userService->getUserByEmailStatusAndAgent($email);

        if ($email == null || !$resultUser) {
            return redirect()->guest('/');
        }

        return $next($request);
    }
}
