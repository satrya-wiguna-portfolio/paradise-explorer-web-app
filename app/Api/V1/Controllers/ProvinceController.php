<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Province\CreateProvinceRequest;
use App\Http\Requests\Province\UpdateProvinceRequest;
use App\Services\Contract\IProvinceService;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    private $_provinceService;

    private $_request;

    private $_createProvinceRequest;

    private $_updateProvinceRequest;

    /**
     * ProvinceController constructor.
     * @param Request $request
     * @param IProvinceService $provinceService
     */
    public function __construct(Request $request, IProvinceService $provinceService)
    {
        $this->_request = $request;

        $this->_provinceService = $provinceService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_provinceService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_provinceService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createProvinceRequest = new CreateProvinceRequest();

        $this->_createProvinceRequest->name = $this->_request->input('name');

        $result = $this->_provinceService->save($this->_createProvinceRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateProvinceRequest = new UpdateProvinceRequest();

        $this->_updateProvinceRequest->id = $this->_request->input('id');
        $this->_updateProvinceRequest->name = $this->_request->input('name');

        $result = $this->_provinceService->update($this->_updateProvinceRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_provinceService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProvinceList()
    {
        $result = $this->_provinceService->getProvinceList();

        return response()->json($result);
    }
}
