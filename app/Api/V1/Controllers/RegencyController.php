<?php
namespace App\Api\V1\Controllers;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Regency\CreateRegencyRequest;
use App\Http\Requests\Regency\UpdateRegencyRequest;
use App\Services\Contract\IRegencyService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegencyController extends Controller
{
    private $_regencyService;

    private $_request;

    private $_createRegencyRequest;

    private $_updateRegencyRequest;

    /**
     * RegencyController constructor.
     * @param Request $request
     * @param IRegencyService $regencyService
     */
    public function __construct(Request $request, IRegencyService $regencyService)
    {
        $this->_request = $request;

        $this->_regencyService = $regencyService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_regencyService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_regencyService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createRegencyRequest = new CreateRegencyRequest();

        $this->_createRegencyRequest->province_id = $this->_request->input('province_id');
        $this->_createRegencyRequest->name = $this->_request->input('name');

        $result = $this->_regencyService->save($this->_createRegencyRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateRegencyRequest = new UpdateRegencyRequest();

        $this->_updateRegencyRequest->id = $this->_request->input('id');
        $this->_updateRegencyRequest->province_id = $this->_request->input('province_id');
        $this->_updateRegencyRequest->name = $this->_request->input('name');

        $result = $this->_regencyService->update($this->_updateRegencyRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_regencyService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRegencyList()
    {
        $result = $this->_regencyService->getRegencyList();

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRegencyListByProvince($id)
    {
        $result = $this->_regencyService->getRegencyListByProvince($id);

        return response()->json($result);
    }
}
