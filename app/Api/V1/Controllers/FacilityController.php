<?php
namespace App\Api\v1\Controllers;

use App\Helper\DirectoryHelper;
use App\Http\Requests\Facility\CreateFacilityRequest;
use App\Http\Requests\Facility\UpdateFacilityRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IFacilityService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FacilityController extends Controller
{
    private $_facilityService;

    private $_request;

    private $_createFacilityRequest;

    private $_updateFacilityRequest;

    /**
     * CountryController constructor.
     * @param Request $request
     * @param IFacilityService $facilityService
     */
    public function __construct(Request $request, IFacilityService $facilityService)
    {
        $this->_request = $request;

        $this->_facilityService = $facilityService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_facilityService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_facilityService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createFacilityRequest = new CreateFacilityRequest();

        $this->_createFacilityRequest->facility = $this->_request->input('facility');

        if ($image_file = $this->_request->input('image_file')) {
            $image_file = $this->_request->input('image_file');
            $file_name = uniqid() . strtotime('now');
            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_createFacilityRequest->image_url = DirectoryHelper::FACILITY . $image_file['output']['name'];
            $this->_createFacilityRequest->image_file = json_encode($image_file);
        }

        $result = $this->_facilityService->save($this->_createFacilityRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateFacilityRequest = new UpdateFacilityRequest();

        $this->_updateFacilityRequest->id = $this->_request->input('id');
        $this->_updateFacilityRequest->facility = $this->_request->input('facility');

        if ($image_file = $this->_request->input('image_file')) {
            $image_file = $this->_request->input('image_file');

            if ($this->_request->input('image_url')) {
                $file_name = array_slice(explode('.', basename($this->_request->input('image_url'))), 0, 1)[0];
            } else {
                $file_name = uniqid() . strtotime('now');
            }

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_updateFacilityRequest->image_url = DirectoryHelper::FACILITY . $image_file['output']['name'];
            $this->_updateFacilityRequest->image_file = json_encode($image_file);
        }

        $result = $this->_facilityService->update($this->_updateFacilityRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_facilityService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFacilityByName()
    {
        $name = $this->_request->get('name');

        $result = $this->_facilityService->getFacilityByName($name);

        return response()->json($result);
    }
}
