<?php
namespace App\Api\v1\controllers;


use App\Http\Requests\AgentType\CreateAgentTypeRequest;
use App\Http\Requests\AgentType\UpdateAgentTypeRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IAgentTypeService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AgentTypeController extends Controller
{
    private $_agentTypeService;

    private $_request;

    private $_createAgentTypeRequest;

    private $_updateAgentTypeRequest;

    /**
     * AgentTypeController constructor.
     * @param Request $request
     * @param IAgentTypeService $agentTypeService
     */
    public function __construct(Request $request, IAgentTypeService $agentTypeService)
    {
        $this->_request = $request;

        $this->_agentTypeService = $agentTypeService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_agentTypeService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_agentTypeService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createAgentTypeRequest = new CreateAgentTypeRequest();

        $this->_createAgentTypeRequest->name = $this->_request->input('name');
        $this->_createAgentTypeRequest->description = $this->_request->input('description');

        $result = $this->_agentTypeService->save($this->_createAgentTypeRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateAgentTypeRequest = new UpdateAgentTypeRequest();

        $this->_updateAgentTypeRequest->id = $this->_request->input('id');
        $this->_updateAgentTypeRequest->name = $this->_request->input('name');
        $this->_updateAgentTypeRequest->description = $this->_request->input('description');

        $result = $this->_agentTypeService->update($this->_updateAgentTypeRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_agentTypeService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAgentTypeList()
    {
        $result = $this->_agentTypeService->getAgentTypeList();

        return response()->json($result);
    }
}
