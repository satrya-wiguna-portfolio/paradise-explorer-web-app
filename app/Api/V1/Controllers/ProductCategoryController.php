<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductCategory\CreateProductCategoryRequest;
use App\Http\Requests\ProductCategory\ProductCategoryRequest;
use App\Http\Requests\ProductCategory\UpdateProductCategoryRequest;
use App\Services\Contract\IProductCategoryService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    private $_productCategoryService;

    private $_request;

    private $_createProductCategoryRequest;

    private $_updateProductCategoryRequest;

    /**
     * ProductCategoryController constructor.
     * @param Request $request
     * @param IProductCategoryService $productCategoryService
     */
    public function __construct(Request $request, IProductCategoryService $productCategoryService)
    {
        $this->_request = $request;

        $this->_productCategoryService = $productCategoryService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new ProductCategoryRequest();

        $customRequest->product_type_id = $this->_request->get('product_type_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_productCategoryService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_productCategoryService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createProductCategoryRequest = new CreateProductCategoryRequest();

        $this->_createProductCategoryRequest->product_type_id = $this->_request->input('product_type_id');
        $this->_createProductCategoryRequest->category = $this->_request->input('category');
        $this->_createProductCategoryRequest->description = $this->_request->input('description');

        $result = $this->_productCategoryService->save($this->_createProductCategoryRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateProductCategoryRequest = new UpdateProductCategoryRequest();

        $this->_updateProductCategoryRequest->id = $this->_request->input('id');
        $this->_updateProductCategoryRequest->product_type_id = $this->_request->input('product_type_id');
        $this->_updateProductCategoryRequest->category = $this->_request->input('category');
        $this->_updateProductCategoryRequest->description = $this->_request->input('description');

        $result = $this->_productCategoryService->update($this->_updateProductCategoryRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_productCategoryService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductCategoryList()
    {
        $product_type_id = $this->_request->get('product_type_id');

        $result = $this->_productCategoryService->getProductCategoryList($product_type_id);

        return response()->json($result);
    }
}
