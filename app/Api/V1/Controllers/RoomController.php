<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Room\CreateRoomRequest;
use App\Http\Requests\Room\RoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Services\Contract\IRoomService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoomController extends Controller
{
    private $_roomService;

    private $_request;

    private $_createRoomRequest;

    private $_updateRoomRequest;

    /**
     * RoomController constructor.
     * @param Request $request
     * @param IRoomService $roomService
     */
    public function __construct(Request $request, IRoomService $roomService)
    {
        $this->_request = $request;

        $this->_roomService = $roomService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new RoomRequest();

        $customRequest->agent_id = $this->_request->get('agent_id');
        $customRequest->product_id = $this->_request->get('product_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_roomService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_roomService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createRoomRequest = new CreateRoomRequest();

        $this->_createRoomRequest->product_id = $this->_request->input('product_id');
        $this->_createRoomRequest->title = $this->_request->input('title');
        $this->_createRoomRequest->description = $this->_request->input('description');
        $this->_createRoomRequest->adult = $this->_request->input('adult');
        $this->_createRoomRequest->children = $this->_request->input('children');
        $this->_createRoomRequest->max = $this->_request->input('max');
        $this->_createRoomRequest->footage = $this->_request->input('footage');
        $this->_createRoomRequest->price = $this->_request->input('price');
        $this->_createRoomRequest->option = $this->_request->input('option');
        $this->_createRoomRequest->policy = $this->_request->input('policy');
        $this->_createRoomRequest->bed = $this->_request->input('bed');
        $this->_createRoomRequest->room_facility = $this->_request->input('room_facility');
        $this->_createRoomRequest->room_image = $this->_request->input('room_image');

        $result = $this->_roomService->save($this->_createRoomRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateRoomRequest = new UpdateRoomRequest();

        $this->_updateRoomRequest->id = $this->_request->input('id');
        $this->_updateRoomRequest->product_id = $this->_request->input('product_id');
        $this->_updateRoomRequest->title = $this->_request->input('title');
        $this->_updateRoomRequest->description = $this->_request->input('description');
        $this->_updateRoomRequest->adult = $this->_request->input('adult');
        $this->_updateRoomRequest->children = $this->_request->input('children');
        $this->_updateRoomRequest->max = $this->_request->input('max');
        $this->_updateRoomRequest->footage = $this->_request->input('footage');
        $this->_updateRoomRequest->price = $this->_request->input('price');
        $this->_updateRoomRequest->option = $this->_request->input('option');
        $this->_updateRoomRequest->policy = $this->_request->input('policy');
        $this->_updateRoomRequest->bed = $this->_request->input('bed');
        $this->_updateRoomRequest->room_facility = $this->_request->input('room_facility');
        $this->_updateRoomRequest->room_image = $this->_request->input('room_image');

        $result = $this->_roomService->update($this->_updateRoomRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_roomService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $agentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomListByAgentId($agentId)
    {
        $result = $this->_roomService->getRoomListByAgentId($agentId);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomByTitle()
    {
        $agent_id = $this->_request->get('agent_id');
        $product_id =  $this->_request->get('product_id');
        $title = $this->_request->get('title');

        $result = $this->_roomService->getRoomByTitle($title, $agent_id, $product_id);

        return response()->json($result);
    }
}
