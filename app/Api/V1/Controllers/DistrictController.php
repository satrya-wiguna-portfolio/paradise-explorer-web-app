<?php
namespace App\Api\V1\Controllers;

use App\Http\Requests\District\CreateDistrictRequest;
use App\Http\Requests\District\UpdateDistrictRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IDistrictService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    private $_districtService;

    private $_request;

    private $_createDistrictRequest;

    private $_updateDistrictRequest;

    /**
     * DistrictController constructor.
     * @param Request $request
     * @param IDistrictService $districtService
     */
    public function __construct(Request $request, IDistrictService $districtService)
    {
        $this->_request = $request;

        $this->_districtService = $districtService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_districtService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_districtService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createDistrictRequest = new CreateDistrictRequest();

        $this->_createDistrictRequest->regency_id = $this->_request->input('regency_id');
        $this->_createDistrictRequest->name = $this->_request->input('name');

        $result = $this->_districtService->save($this->_createDistrictRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateDistrictRequest = new UpdateDistrictRequest();

        $this->_updateDistrictRequest->id = $this->_request->input('id');
        $this->_updateDistrictRequest->regency_id = $this->_request->input('regency_id');
        $this->_updateDistrictRequest->name = $this->_request->input('name');

        $result = $this->_districtService->update($this->_updateDistrictRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_districtService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistrictList()
    {
        $result = $this->_districtService->getDistrictList();

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistrictListByRegency($id)
    {
        $result = $this->_districtService->getDistrictListByRegency($id);

        return response()->json($result);
    }
}
