<?php
namespace App\Api\V1\Controllers;


use App\Helper\DirectoryHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Member\CreateMemberRequest;
use App\Http\Requests\Member\MemberRequest;
use App\Http\Requests\Member\UpdateMemberRequest;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\UserLog\CreateUserActivateRequest;
use App\Services\Contract\IMemberService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MemberController extends Controller
{
    private $_memberService;

    private $_request;

    private $_roleRequest;

    private $_userActivateRequest;

    private $_createUserRequest;

    private $_createMemberRequest;

    private $_updateMemberRequest;


    /**
     * MemberController constructor.
     * @param Request $request
     * @param IMemberService $memberService
     */
    public function __construct(Request $request, IMemberService $memberService)
    {
        $this->_request = $request;

        $this->_memberService = $memberService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new MemberRequest();

        $customRequest->country_id = $this->_request->get('country_id');
        $customRequest->state_id = $this->_request->get('state_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $columns[4]['name'] = $this->_request->get('columns')[4]['name'];
        $columns[5]['name'] = $this->_request->get('columns')[5]['name'];
        $columns[6]['name'] = $this->_request->get('columns')[6]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_memberService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_memberService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_roleRequest = new RoleRequest();

        $this->_roleRequest->id = $this->_request->input('user.role.id');

        $this->_userActivateRequest = new CreateUserActivateRequest();

        $this->_userActivateRequest->email = $this->_request->input('user.email');
        $this->_userActivateRequest->token = str_random(60);

        $this->_createUserRequest = new CreateUserRequest();

        $this->_createUserRequest->email = $this->_request->input('user.email');
        $this->_createUserRequest->password = $this->_request->input('user.password');
        $this->_createUserRequest->password_confirmation = $this->_request->input('user.password_confirmation');
        $this->_createUserRequest->handphone = $this->_request->input('user.handphone');
        $this->_createUserRequest->role = $this->_roleRequest;
        $this->_createUserRequest->user_activate = $this->_userActivateRequest;

        $this->_createMemberRequest = new CreateMemberRequest();

        $this->_createMemberRequest->user_id = $this->_request->input('user_id');
        $this->_createMemberRequest->title = $this->_request->input('title');
        $this->_createMemberRequest->first_name = $this->_request->input('first_name');
        $this->_createMemberRequest->last_name = $this->_request->input('last_name');
        $this->_createMemberRequest->gender = $this->_request->input('gender');
        $this->_createMemberRequest->address = $this->_request->input('address');
        $this->_createMemberRequest->country_id = $this->_request->input('country_id');
        $this->_createMemberRequest->state_id = $this->_request->input('state_id');
        $this->_createMemberRequest->city = $this->_request->input('city');
        $this->_createMemberRequest->zip = $this->_request->input('zip');

        $image_file = $this->_request->input('image_file');

        if (!empty($image_file)) {
            $file_name = uniqid() . strtotime('now');

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_createMemberRequest->image_url = DirectoryHelper::MEMBER . $image_file['output']['name'];
            $this->_createMemberRequest->image_file = json_encode($image_file);
        }

        $this->_createMemberRequest->phone = $this->_request->input('phone');
        $this->_createMemberRequest->user = $this->_createUserRequest;

        $result = $this->_memberService->save($this->_createMemberRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateMemberRequest = new UpdateMemberRequest();

        $this->_updateMemberRequest->id = $this->_request->input('id');
        $this->_updateMemberRequest->user_id = $this->_request->input('user_id');
        $this->_updateMemberRequest->title = $this->_request->input('title');
        $this->_updateMemberRequest->first_name = $this->_request->input('first_name');
        $this->_updateMemberRequest->last_name = $this->_request->input('last_name');
        $this->_updateMemberRequest->gender = $this->_request->input('gender');
        $this->_updateMemberRequest->address = $this->_request->input('address');
        $this->_updateMemberRequest->country_id = $this->_request->input('country_id');
        $this->_updateMemberRequest->state_id = $this->_request->input('state_id');
        $this->_updateMemberRequest->city = $this->_request->input('city');
        $this->_updateMemberRequest->zip = $this->_request->input('zip');

        $image_file = $this->_request->input('image_file');

        if (!empty($image_file)) {
            $image_url = $this->_request->input('image_url');

            if (!empty($image_url)) {
                $file_name = array_slice(explode('.', basename($this->_request->input('image_url'))), 0, 1)[0];
            } else {
                $file_name = uniqid() . strtotime('now');
            }

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_updateMemberRequest->image_url = DirectoryHelper::MEMBER . $image_file['output']['name'];
            $this->_updateMemberRequest->image_file = json_encode($image_file);
        } else {
            $image_url = $this->_request->input('image_url');

            if (!empty($image_url)) {
                File::delete(public_path($this->_request->input('image_url')));
            }
        }

        $this->_updateMemberRequest->phone = $this->_request->input('phone');

        $result = $this->_memberService->update($this->_updateMemberRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_memberService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMemberByUserId($userId)
    {
        $result = $this->_memberService->getMemberByUserId($userId);

        return response()->json($result);
    }
}
