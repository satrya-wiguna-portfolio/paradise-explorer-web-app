<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\RoomAllotment\CreateRoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\UpdateRoomAllotmentRequest;
use App\Services\Contract\IRoomAllotmentService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoomAllotmentController extends Controller
{
    private $_roomAllotmentService;

    private $_request;

    private $_createRoomAllotmentRequest;

    private $_updateRoomAllotmentRequest;

    /**
     * ProductRoomController constructor.
     * @param Request $request
     * @param IRoomAllotmentService $roomAllotmentService
     */
    public function __construct(Request $request, IRoomAllotmentService $roomAllotmentService)
    {
        $this->_request = $request;

        $this->_roomAllotmentService = $roomAllotmentService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new RoomAllotmentRequest();

        $customRequest->agent_id = $this->_request->get('agent_id');
        $customRequest->product_id = $this->_request->get('product_id');
        $customRequest->room_id = $this->_request->get('room_id');
        $customRequest->from = $this->_request->get('from');
        $customRequest->to = $this->_request->get('to');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $columns[4]['name'] = $this->_request->get('columns')[4]['name'];
        $columns[5]['name'] = $this->_request->get('columns')[5]['name'];
        $columns[6]['name'] = $this->_request->get('columns')[6]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_roomAllotmentService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_roomAllotmentService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createRoomAllotmentRequest = new CreateRoomAllotmentRequest();

        $this->_createRoomAllotmentRequest->room_id = $this->_request->input('room_id');
        $this->_createRoomAllotmentRequest->from = $this->_request->input('from');
        $this->_createRoomAllotmentRequest->to = $this->_request->input('to');
        $this->_createRoomAllotmentRequest->allotment = $this->_request->input('allotment');
        $this->_createRoomAllotmentRequest->lock = $this->_request->input('lock');
        $this->_createRoomAllotmentRequest->sold = $this->_request->input('sold');
        $this->_createRoomAllotmentRequest->balance = $this->_request->input('balance');
        $this->_createRoomAllotmentRequest->price = $this->_request->input('price');

        $result = $this->_roomAllotmentService->save($this->_createRoomAllotmentRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateRoomAllotmentRequest = new UpdateRoomAllotmentRequest();

        $this->_updateRoomAllotmentRequest->id = $this->_request->input('id');
        $this->_updateRoomAllotmentRequest->room_id = $this->_request->input('room_id');
        $this->_updateRoomAllotmentRequest->date = $this->_request->input('date');
        $this->_updateRoomAllotmentRequest->allotment = $this->_request->input('allotment');
        $this->_updateRoomAllotmentRequest->lock = $this->_request->input('lock');
        $this->_updateRoomAllotmentRequest->sold = $this->_request->input('sold');
        $this->_updateRoomAllotmentRequest->balance = $this->_request->input('balance');
        $this->_updateRoomAllotmentRequest->price = $this->_request->input('price');
        $this->_updateRoomAllotmentRequest->status = $this->_request->input('status');

        $result = $this->_roomAllotmentService->update($this->_updateRoomAllotmentRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_roomAllotmentService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByRoomId($id)
    {
        $result = $this->_roomAllotmentService->getByRoomId($id);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLockByRoomId($id)
    {
        $result = $this->_roomAllotmentService->getLockByRoomId($id);

        return response()->json($result);
    }
}
