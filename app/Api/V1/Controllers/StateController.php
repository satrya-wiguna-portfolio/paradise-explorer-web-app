<?php
namespace App\Api\V1\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\State\CreateStateRequest;
use App\Http\Requests\State\UpdateStateRequest;
use App\Services\Contract\IStateService;
use Illuminate\Http\Request;

class StateController extends Controller
{
    private $_stateService;

    private $_request;

    private $_createStateRequest;

    private $_updateStateRequest;

    /**
     * StateController constructor.
     * @param Request $request
     * @param IStateService $stateService
     */
    public function __construct(Request $request, IStateService $stateService)
    {
        $this->_request = $request;

        $this->_stateService = $stateService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_stateService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_stateService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createStateRequest = new CreateStateRequest();

        $this->_createStateRequest->regency_id = $this->_request->input('regency_id');
        $this->_createStateRequest->name = $this->_request->input('name');

        $result = $this->_stateService->save($this->_createStateRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateStateRequest = new UpdateStateRequest();

        $this->_updateStateRequest->id = $this->_request->input('id');
        $this->_updateStateRequest->regency_id = $this->_request->input('regency_id');
        $this->_updateStateRequest->name = $this->_request->input('name');

        $result = $this->_stateService->update($this->_updateStateRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_stateService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStateList()
    {
        $result = $this->_stateService->getStateList();

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStateListByCountry($id)
    {
        $result = $this->_stateService->getStateListByCountry($id);

        return response()->json($result);
    }
}