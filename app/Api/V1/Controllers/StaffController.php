<?php
namespace App\Api\V1\Controllers;


use App\Helper\DirectoryHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests\Staff\CreateStaffRequest;
use App\Http\Requests\Staff\StaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Services\Contract\IStaffService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class StaffController extends Controller
{
    private $_staffService;

    private $_request;

    private $_roleRequest;

    private $_createUserRequest;

    private $_createStaffRequest;

    private $_updateStaffRequest;

    /**
     * MemberController constructor.
     * @param Request $request
     * @param IStaffService $staffService
     */
    public function __construct(Request $request, IStaffService $staffService)
    {
        $this->_request = $request;

        $this->_staffService = $staffService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new StaffRequest();

        $customRequest->province_id = $this->_request->get('province_id');
        $customRequest->regency_id = $this->_request->get('regency_id');
        $customRequest->district_id = $this->_request->get('district_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $columns[4]['name'] = $this->_request->get('columns')[4]['name'];
        $columns[5]['name'] = $this->_request->get('columns')[5]['name'];
        $columns[6]['name'] = $this->_request->get('columns')[6]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_staffService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_staffService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_roleRequest = new RoleRequest();

        $this->_roleRequest->id = $this->_request->input('user.role.id');

        $this->_createUserRequest = new CreateUserRequest();

        $this->_createUserRequest->email = $this->_request->input('user.email');
        $this->_createUserRequest->password = $this->_request->input('user.password');
        $this->_createUserRequest->password_confirmation = $this->_request->input('user.password_confirmation');
        $this->_createUserRequest->handphone = $this->_request->input('user.handphone');
        $this->_createUserRequest->role = $this->_roleRequest;

        $this->_createStaffRequest = new CreateStaffRequest();

        $this->_createStaffRequest->user_id = $this->_request->input('user_id');
        $this->_createStaffRequest->nik = $this->_request->input('nik');
        $this->_createStaffRequest->title = $this->_request->input('title');
        $this->_createStaffRequest->first_name = $this->_request->input('first_name');
        $this->_createStaffRequest->last_name = $this->_request->input('last_name');
        $this->_createStaffRequest->gender = $this->_request->input('gender');
        $this->_createStaffRequest->address = $this->_request->input('address');
        $this->_createStaffRequest->province_id = $this->_request->input('province_id');
        $this->_createStaffRequest->regency_id = $this->_request->input('regency_id');
        $this->_createStaffRequest->district_id = $this->_request->input('district_id');
        $this->_createStaffRequest->village = $this->_request->input('village');
        $this->_createStaffRequest->zip = $this->_request->input('zip');

        $image_file = $this->_request->input('image_file');

        if (!empty($image_file)) {
            $file_name = uniqid() . strtotime('now');

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_createStaffRequest->image_url = DirectoryHelper::STAFF . $image_file['output']['name'];
            $this->_createStaffRequest->image_file = json_encode($image_file);
        }

        $this->_createStaffRequest->phone = $this->_request->input('phone');
        $this->_createStaffRequest->user = $this->_createUserRequest;

        $result = $this->_staffService->save($this->_createStaffRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateStaffRequest = new UpdateStaffRequest();

        $this->_updateStaffRequest->id = $this->_request->input('id');
        $this->_updateStaffRequest->user_id = $this->_request->input('user_id');
        $this->_updateStaffRequest->nik = $this->_request->input('nik');
        $this->_updateStaffRequest->title = $this->_request->input('title');
        $this->_updateStaffRequest->first_name = $this->_request->input('first_name');
        $this->_updateStaffRequest->last_name = $this->_request->input('last_name');
        $this->_updateStaffRequest->gender = $this->_request->input('gender');
        $this->_updateStaffRequest->address = $this->_request->input('address');
        $this->_updateStaffRequest->province_id = $this->_request->input('province_id');
        $this->_updateStaffRequest->regency_id = $this->_request->input('regency_id');
        $this->_updateStaffRequest->district_id = $this->_request->input('district_id');
        $this->_updateStaffRequest->village = $this->_request->input('village');
        $this->_updateStaffRequest->zip = $this->_request->input('zip');

        $image_file = $this->_request->input('image_file');

        if (!empty($image_file)) {
            $image_url = $this->_request->input('image_url');

            if (!empty($image_url)) {
                $file_name = array_slice(explode('.', basename($this->_request->input('image_url'))), 0, 1)[0];
            } else {
                $file_name = uniqid() . strtotime('now');
            }

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_updateStaffRequest->image_url = DirectoryHelper::STAFF . $image_file['output']['name'];
            $this->_updateStaffRequest->image_file = json_encode($image_file);
        } else {
            $image_url = $this->_request->input('image_url');

            if (!empty($image_url)) {
                File::delete(public_path($this->_request->input('image_url')));
            }
        }

        $this->_updateStaffRequest->phone = $this->_request->input('phone');

        $result = $this->_staffService->update($this->_updateStaffRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_staffService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStaffByUserId($userId)
    {
        $result = $this->_staffService->getStaffByUserId($userId);

        return response()->json($result);
    }
}
