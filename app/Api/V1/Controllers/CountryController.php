<?php
namespace App\Api\V1\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\Country\CreateCountryRequest;
use App\Http\Requests\Country\UpdateCountryRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\ICountryService;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    private $_countryService;

    private $_request;

    private $_createCountryRequest;

    private $_updateCountryRequest;

    /**
     * CountryController constructor.
     * @param Request $request
     * @param ICountryService $countryService
     */
    public function __construct(Request $request, ICountryService $countryService)
    {
        $this->_request = $request;

        $this->_countryService = $countryService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_countryService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_countryService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createCountryRequest = new CreateCountryRequest();

        $this->_createCountryRequest->code = $this->_request->input('code');
        $this->_createCountryRequest->name = $this->_request->input('name');

        $result = $this->_countryService->save($this->_createCountryRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateCountryRequest = new UpdateCountryRequest();

        $this->_updateCountryRequest->id = $this->_request->input('id');
        $this->_updateCountryRequest->code = $this->_request->input('code');
        $this->_updateCountryRequest->name = $this->_request->input('name');

        $result = $this->_countryService->update($this->_updateCountryRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_countryService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountryList()
    {
        $result = $this->_countryService->getCountryList();

        return response()->json($result);
    }
}