<?php
namespace App\Api\V1\Controllers;


use App\Http\Requests\Auth\LoginAuthRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RecoveryAuthRequest;
use App\Http\Requests\Auth\SignupAuthRequest;
use App\Http\Requests\Role\RoleRequest;
use App\Services\Contract\IAuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $_request;

    private $_signupAuthRequest;

    private $_roleRequest;

    private $_authService;

    /**
     * AuthController constructor.
     * @param Request $request
     * @param IAuthService $authService
     */
    public function __construct(Request $request, IAuthService $authService)
    {
        $this->_request = $request;

        $this->_authService = $authService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $loginAuthRequest = new LoginAuthRequest();

        $loginAuthRequest->email = $this->_request->input('email');
        $loginAuthRequest->password = $this->_request->input('password');

        $result = $this->_authService->login($loginAuthRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        $this->_roleRequest = new RoleRequest();

        $this->_roleRequest->id = $this->_request->input('role_id');

        $this->_signupAuthRequest = new SignupAuthRequest();

        $this->_signupAuthRequest->email = $this->_request->input('email');
        $this->_signupAuthRequest->password = $this->_request->input('password');
        $this->_signupAuthRequest->role = $this->_roleRequest;

        $result = $this->_authService->signupByApi($this->_signupAuthRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function recovery()
    {
        $recoveryAuthRequest = new RecoveryAuthRequest();

        $recoveryAuthRequest->email = $this->_request->input('email');

        $result = $this->_authService->recovery($recoveryAuthRequest);

        return response()->json($result);
    }

//    /**
//     * @return mixed
//     */
//    public function reset()
//    {
//        $resetAuthRequest = new ResetAuthRequest();
//
//        $resetAuthRequest->email = $this->_request->input('email');
//        $resetAuthRequest->password = $this->_request->input('password');
//        $resetAuthRequest->password_confirmation = $this->_request->input('password_confirmation');
//        $resetAuthRequest->token = $this->_request->input('token');
//
//        $result = $this->_authService->recovery($resetAuthRequest);
//
//        return response()->json($result);
//    }
}