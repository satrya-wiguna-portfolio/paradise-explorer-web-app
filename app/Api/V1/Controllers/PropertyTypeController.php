<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\PropertyType\CreatePropertyTypeRequest;
use App\Http\Requests\PropertyType\UpdatePropertyTypeRequest;
use App\Services\Contract\IPropertyTypeService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PropertyTypeController extends Controller
{
    private $_propertyTypeService;

    private $_request;

    private $_createPropertyTypeRequest;

    private $_updatePropertyTypeRequest;

    /**
     * PropertyTypeController constructor.
     * @param Request $request
     * @param IPropertyTypeService $propertyTypeService
     */
    public function __construct(Request $request, IPropertyTypeService $propertyTypeService)
    {
        $this->_request = $request;

        $this->_propertyTypeService = $propertyTypeService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_propertyTypeService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_propertyTypeService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createPropertyTypeRequest = new CreatePropertyTypeRequest();

        $this->_createPropertyTypeRequest->name = $this->_request->input('name');
        $this->_createPropertyTypeRequest->description = $this->_request->input('description');

        $result = $this->_propertyTypeService->save($this->_createPropertyTypeRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updatePropertyTypeRequest = new UpdatePropertyTypeRequest();

        $this->_updatePropertyTypeRequest->id = $this->_request->input('id');
        $this->_updatePropertyTypeRequest->name = $this->_request->input('name');
        $this->_updatePropertyTypeRequest->description = $this->_request->input('description');

        $result = $this->_propertyTypeService->update($this->_updatePropertyTypeRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_propertyTypeService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPropertyTypeList()
    {
        $result = $this->_propertyTypeService->getPropertyTypeList();

        return response()->json($result);
    }
}
