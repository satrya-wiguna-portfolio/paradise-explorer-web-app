<?php
namespace App\Api\v1\controllers;


use App\Helper\DirectoryHelper;
use App\Http\Requests\Agent\AgentRequest;
use App\Http\Requests\Agent\CreateAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Services\Contract\IAgentService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class AgentController extends Controller
{
    private $_agentService;

    private $_request;

    private $_roleRequest;

    private $_createUserRequest;

    private $_createAgentRequest;

    private $_updateAgentRequest;

    /**
     * MemberController constructor.
     * @param Request $request
     * @param IAgentService $agentService
     */
    public function __construct(Request $request, IAgentService $agentService)
    {
        $this->_request = $request;

        $this->_agentService = $agentService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new AgentRequest();

        $customRequest->country_id = $this->_request->get('country_id');
        $customRequest->state_id = $this->_request->get('state_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $columns[4]['name'] = $this->_request->get('columns')[4]['name'];
        $columns[5]['name'] = $this->_request->get('columns')[5]['name'];
        $columns[6]['name'] = $this->_request->get('columns')[6]['name'];
        $columns[7]['name'] = $this->_request->get('columns')[7]['name'];
        $columns[8]['name'] = $this->_request->get('columns')[8]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_agentService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_agentService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_roleRequest = new RoleRequest();

        $this->_roleRequest->id = $this->_request->input('user.role.id');

        $this->_createUserRequest = new CreateUserRequest();

        $this->_createUserRequest->email = $this->_request->input('user.email');
        $this->_createUserRequest->password = $this->_request->input('user.password');
        $this->_createUserRequest->password_confirmation = $this->_request->input('user.password_confirmation');
        $this->_createUserRequest->handphone = $this->_request->input('user.handphone');
        $this->_createUserRequest->role = $this->_roleRequest;

        $this->_createAgentRequest = new CreateAgentRequest();

        $this->_createAgentRequest->user_id = $this->_request->input('user_id');
        $this->_createAgentRequest->agent_type_id = $this->_request->input('agent_type_id');
        $this->_createAgentRequest->title = $this->_request->input('title');
        $this->_createAgentRequest->first_name = $this->_request->input('first_name');
        $this->_createAgentRequest->last_name = $this->_request->input('last_name');
        $this->_createAgentRequest->gender = $this->_request->input('gender');
        $this->_createAgentRequest->company = $this->_request->input('company');
        $this->_createAgentRequest->address = $this->_request->input('address');
        $this->_createAgentRequest->country_id = $this->_request->input('country_id');
        $this->_createAgentRequest->state_id = $this->_request->input('state_id');
        $this->_createAgentRequest->city = $this->_request->input('city');
        $this->_createAgentRequest->zip = $this->_request->input('zip');

        $image_file = $this->_request->input('image_file');

        if (!empty($image_file)) {
            $file_name = uniqid() . strtotime('now');

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_createAgentRequest->image_url = DirectoryHelper::AGENT . $image_file['output']['name'];
            $this->_createAgentRequest->image_file = json_encode($image_file);
        }

        $this->_createAgentRequest->phone = $this->_request->input('phone');
        $this->_createAgentRequest->fax = $this->_request->input('fax');
        $this->_createAgentRequest->user = $this->_createUserRequest;

        $result = $this->_agentService->save($this->_createAgentRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateAgentRequest = new UpdateAgentRequest();

        $this->_updateAgentRequest->id = $this->_request->input('id');
        $this->_updateAgentRequest->user_id = $this->_request->input('user_id');
        $this->_updateAgentRequest->agent_type_id = $this->_request->input('agent_type_id');
        $this->_updateAgentRequest->title = $this->_request->input('title');
        $this->_updateAgentRequest->first_name = $this->_request->input('first_name');
        $this->_updateAgentRequest->last_name = $this->_request->input('last_name');
        $this->_updateAgentRequest->gender = $this->_request->input('gender');
        $this->_updateAgentRequest->company = $this->_request->input('company');
        $this->_updateAgentRequest->address = $this->_request->input('address');
        $this->_updateAgentRequest->country_id = $this->_request->input('country_id');
        $this->_updateAgentRequest->state_id = $this->_request->input('state_id');
        $this->_updateAgentRequest->city = $this->_request->input('city');
        $this->_updateAgentRequest->zip = $this->_request->input('zip');

        $image_file = $this->_request->input('image_file');

        if (!empty($image_file)) {
            $image_url = $this->_request->input('image_url');

            if (!empty($image_url)) {
                $file_name = array_slice(explode('.', basename($this->_request->input('image_url'))), 0, 1)[0];
            } else {
                $file_name = uniqid() . strtotime('now');
            }

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_updateAgentRequest->image_url = DirectoryHelper::AGENT . $image_file['output']['name'];
            $this->_updateAgentRequest->image_file = json_encode($image_file);
        } else {
            $image_url = $this->_request->input('image_url');

            if (!empty($image_url)) {
                File::delete(public_path($this->_request->input('image_url')));
            }
        }

        $this->_updateAgentRequest->phone = $this->_request->input('phone');
        $this->_updateAgentRequest->fax = $this->_request->input('fax');

        $result = $this->_agentService->update($this->_updateAgentRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_agentService->delete($id);

        return response()->json($result);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAgentByTypeAndCompany()
    {
        $agentTypeId = $this->_request->get('agent_type_id');
        $company = $this->_request->get('company');

        $result = $this->_agentService->getAgentByTypeAndCompany($agentTypeId, $company);

        return response()->json($result);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAgentByUserId($userId)
    {
        $result = $this->_agentService->getAgentByUserId($userId);

        return response()->json($result);
    }
}
