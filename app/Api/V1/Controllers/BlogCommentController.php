<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\BlogComment\UpdateBlogCommentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IBlogCommentService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogCommentController extends Controller
{
    public $_request;

    public $_updateBlogCommentRequest;

    public $_blogCommentService;

    /**
     * BlogCommentController constructor.
     * @param Request $request
     * @param IBlogCommentService $blogCommentService
     */
    public function __construct(Request $request, IBlogCommentService $blogCommentService)
    {
        $this->_request = $request;

        $this->_blogCommentService = $blogCommentService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_blogCommentService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_blogCommentService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateBlogCommentRequest = new UpdateBlogCommentRequest();

        $this->_updateBlogCommentRequest->id = $this->_request->input('id');
        $this->_updateBlogCommentRequest->status = $this->_request->input('status');

        $result = $this->_blogCommentService->update($this->_updateBlogCommentRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_updateBlogCommentRequest->delete($id);

        return response()->json($result);
    }
}
