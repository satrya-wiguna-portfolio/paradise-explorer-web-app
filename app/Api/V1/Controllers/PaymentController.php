<?php

namespace App\Api\V1\Controllers;

use App\Helper\DirectoryHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Payment\CreatePaymentRequest;
use App\Http\Requests\Payment\UpdatePaymentRequest;
use App\Services\Contract\IPaymentService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    private $_paymentService;

    private $_request;

    private $_createPaymentRequest;

    private $_updatePaymentRequest;

    /**
     * CountryController constructor.
     * @param Request $request
     * @param IPaymentService $paymentService
     */
    public function __construct(Request $request, IPaymentService $paymentService)
    {
        $this->_request = $request;

        $this->_paymentService = $paymentService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_paymentService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_paymentService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createPaymentRequest = new CreatePaymentRequest();

        $this->_createPaymentRequest->payment = $this->_request->input('payment');
        $this->_createPaymentRequest->description = $this->_request->input('description');

        if ($image_file = $this->_request->input('image_file')) {
            $image_file = $this->_request->input('image_file');
            $file_name = uniqid() . strtotime('now');
            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_createPaymentRequest->image_url = DirectoryHelper::PAYMENT . $image_file['output']['name'];
            $this->_createPaymentRequest->image_file = json_encode($image_file);
        }

        $result = $this->_paymentService->save($this->_createPaymentRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updatePaymentRequest = new UpdatePaymentRequest();

        $this->_updatePaymentRequest->id = $this->_request->input('id');
        $this->_updatePaymentRequest->payment = $this->_request->input('payment');
        $this->_updatePaymentRequest->description = $this->_request->input('description');
        $this->_updatePaymentRequest->status = $this->_request->input('status');

        if ($image_file = $this->_request->input('image_file')) {
            $image_file = $this->_request->input('image_file');

            if ($this->_request->input('image_url')) {
                $file_name = array_slice(explode('.', basename($this->_request->input('image_url'))), 0, 1)[0];
            } else {
                $file_name = uniqid() . strtotime('now');
            }

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_updatePaymentRequest->image_url = DirectoryHelper::PAYMENT . $image_file['output']['name'];
            $this->_updatePaymentRequest->image_file = json_encode($image_file);
        }

        $result = $this->_paymentService->update($this->_updatePaymentRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_paymentService->delete($id);

        return response()->json($result);
    }
}
