<?php
namespace App\Api\V1\Controllers;

use App\Http\Requests\Currency\CreateCurrencyRequest;
use App\Http\Requests\Currency\SyncCurrencyRequest;
use App\Http\Requests\Currency\UpdateCurrencyRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\ICurrencyService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    private $_request;

    private $_currencyService;

    private $_createCurrencyRequest;

    private $_updateCurrencyRequest;
    
    private $_syncCurrencyRequest;

    /**
     * RoleController constructor.
     * @param Request $request
     * @param ICurrencyService $currencyService
     */
    public function __construct(Request $request, ICurrencyService $currencyService)
    {
        $this->_request = $request;

        $this->_currencyService = $currencyService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $columns[4]['name'] = $this->_request->get('columns')[4]['name'];
        $columns[5]['name'] = $this->_request->get('columns')[5]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $result = $this->_currencyService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_currencyService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createCurrencyRequest = new CreateCurrencyRequest();

        $this->_createCurrencyRequest->code = $this->_request->input('code');

        $result = $this->_currencyService->save($this->_createCurrencyRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateCurrencyRequest = new UpdateCurrencyRequest();

        $this->_updateCurrencyRequest->id = $this->_request->input('id');
        $this->_updateCurrencyRequest->name = $this->_request->input('name');
        $this->_updateCurrencyRequest->symbol = $this->_request->input('symbol');
        $this->_updateCurrencyRequest->pattern = $this->_request->input('format');
        $this->_updateCurrencyRequest->exchange_rate = $this->_request->input('exchange_rate');
        $this->_updateCurrencyRequest->active = $this->_request->input('active');

        $result = $this->_currencyService->update($this->_updateCurrencyRequest);

        return response()->json($result);
    }

    /**
     * @param $code
     * @return mixed
     */
    public function delete($code)
    {
        $result = $this->_currencyService->delete($code);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function sync()
    {
        $this->_syncCurrencyRequest = new SyncCurrencyRequest();

        $this->_syncCurrencyRequest->code = $this->_request->input('code');

        $result = $this->_currencyService->sync($this->_syncCurrencyRequest);

        return response()->json($result);
    }
}
