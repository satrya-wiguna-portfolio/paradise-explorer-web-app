<?php
namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductComment\ProductCommentRequest;
use App\Http\Requests\ProductComment\UpdateProductCommentRequest;
use App\Services\Contract\IProductCommentService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductCommentController extends Controller
{
    public $_request;

    public $_updateProductCommentRequest;

    public $_productCommentService;

    /**
     * ProductCommentController constructor.
     * @param Request $request
     * @param IProductCommentService $productCommentService
     */
    public function __construct(Request $request, IProductCommentService $productCommentService)
    {
        $this->_request = $request;

        $this->_productCommentService = $productCommentService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new ProductCommentRequest();

        $customRequest->agent_id = $this->_request->get('agent_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_productCommentService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_productCommentService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateProductCommentRequest = new UpdateProductCommentRequest();

        $this->_updateProductCommentRequest->id = $this->_request->input('id');
        $this->_updateProductCommentRequest->status = $this->_request->input('status');

        $result = $this->_productCommentService->update($this->_updateProductCommentRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_updateProductCommentRequest->delete($id);

        return response()->json($result);
    }
}
