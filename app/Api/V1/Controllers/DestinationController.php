<?php

namespace App\Api\V1\Controllers;

use App\Helper\DirectoryHelper;
use App\Http\Requests\BlogCategory\CreateDestinationRequest;
use App\Http\Requests\BlogCategory\DestinationRequest;
use App\Http\Requests\BlogCategory\UpdateDestinationRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IDestinationService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DestinationController extends Controller
{
    private $_destinationService;

    private $_request;

    private $_createDestinationRequest;

    private $_updateDestinationRequest;

    /**
     * DestinationController constructor.
     * @param Request $request
     * @param IDestinationService $destinationService
     */
    public function __construct(Request $request, IDestinationService $destinationService)
    {
        $this->_request = $request;

        $this->_destinationService = $destinationService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new DestinationRequest();

        $customRequest->parent_id = $this->_request->get('parent_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_destinationService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_destinationService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createDestinationRequest = new CreateDestinationRequest();

        $this->_createDestinationRequest->parent_id = $this->_request->input('parent_id');
        $this->_createDestinationRequest->name = $this->_request->input('name');

        if ($this->_request->input('image_file')) {
            $image_file = $this->_request->input('image_file');
            $file_name = uniqid() . strtotime('now');
            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_createDestinationRequest->image_url = DirectoryHelper::DESTINATION . $image_file['output']['name'];
            $this->_createDestinationRequest->image_file = json_encode($image_file);
        }

        $this->_createDestinationRequest->lat = $this->_request->input('lat');
        $this->_createDestinationRequest->lng = $this->_request->input('lng');
        $this->_createDestinationRequest->description = $this->_request->input('description');

        $result = $this->_destinationService->save($this->_createDestinationRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateDestinationRequest = new UpdateDestinationRequest();

        $this->_updateDestinationRequest->id = $this->_request->input('id');
        $this->_updateDestinationRequest->parent_id = $this->_request->input('parent_id');
        $this->_updateDestinationRequest->name = $this->_request->input('name');

        if ($this->_request->input('image_file')) {
            $image_file = $this->_request->input('image_file');

            if ($this->_request->input('image_url')) {
                $file_name = array_slice(explode('.', basename($this->_request->input('image_url'))), 0, 1)[0];
            } else {
                $file_name = uniqid() . strtotime('now');
            }

            $image_file['input']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['input']['name']), -1)[0];
            $image_file['output']['name'] = $file_name . '.' . array_slice(explode('.', $image_file['output']['name']), -1)[0];

            $this->_updateDestinationRequest->image_url = DirectoryHelper::DESTINATION . $image_file['output']['name'];
            $this->_updateDestinationRequest->image_file = json_encode($image_file);
        }

        $this->_updateDestinationRequest->lat = $this->_request->input('lat');
        $this->_updateDestinationRequest->lng = $this->_request->input('lng');
        $this->_updateDestinationRequest->description = $this->_request->input('description');

        $result = $this->_destinationService->update($this->_updateDestinationRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_destinationService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDestinationByName()
    {
        $id = $this->_request->get('id');
        $name = $this->_request->get('name');

        $result = $this->_destinationService->getDestinationByName($id, $name);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDestinationList()
    {
        $result = $this->_destinationService->getDestinationList();

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDestinationHierarchy()
    {
        $result = $this->_destinationService->getDestinationHierarchy();

        return response()->json($result);
    }
}
