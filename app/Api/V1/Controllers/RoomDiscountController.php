<?php
namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\RoomDiscount\CreateRoomDiscountRequest;
use App\Http\Requests\RoomDiscount\RoomDiscountRequest;
use App\Http\Requests\RoomDiscount\UpdateRoomDiscountRequest;
use App\Services\Contract\IRoomDiscountService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoomDiscountController extends Controller
{
    private $_roomDiscountService;

    private $_request;

    private $_createRoomDiscountRequest;

    private $_updateRoomDiscountRequest;

    /**
     * ProductRoomController constructor.
     * @param Request $request
     * @param IRoomDiscountService $roomDiscountService
     */
    public function __construct(Request $request, IRoomDiscountService $roomDiscountService)
    {
        $this->_request = $request;

        $this->_roomDiscountService = $roomDiscountService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new RoomDiscountRequest();

        $customRequest->agent_id = $this->_request->get('agent_id');
        $customRequest->product_id = $this->_request->get('product_id');
        $customRequest->room_id = $this->_request->get('room_id');
        $customRequest->open_date = $this->_request->get('open_date');
        $customRequest->close_date = $this->_request->get('close_date');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_roomDiscountService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_roomDiscountService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createRoomDiscountRequest = new CreateRoomDiscountRequest();

        $this->_createRoomDiscountRequest->room_id = $this->_request->input('room_id');
        $this->_createRoomDiscountRequest->open_date = $this->_request->input('open_date');
        $this->_createRoomDiscountRequest->close_date = $this->_request->input('close_date');
        $this->_createRoomDiscountRequest->discount = $this->_request->input('discount');
        $this->_createRoomDiscountRequest->status = $this->_request->input('status');

        $result = $this->_roomDiscountService->save($this->_createRoomDiscountRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateRoomDiscountRequest = new UpdateRoomDiscountRequest();

        $this->_updateRoomDiscountRequest->id = $this->_request->input('id');
        $this->_updateRoomDiscountRequest->room_id = $this->_request->input('room_id');
        $this->_updateRoomDiscountRequest->open_date = $this->_request->input('open_date');
        $this->_updateRoomDiscountRequest->close_date = $this->_request->input('close_date');
        $this->_updateRoomDiscountRequest->discount = $this->_request->input('discount');
        $this->_updateRoomDiscountRequest->status = $this->_request->input('status');

        $result = $this->_roomDiscountService->update($this->_updateRoomDiscountRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_roomDiscountService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByRoomId($id)
    {
        $result = $this->_roomDiscountService->getByRoomId($id);

        return response()->json($result);
    }
}
