<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\Bed\BedRequest;
use App\Http\Requests\Bed\CreateBedRequest;
use App\Http\Requests\Bed\UpdateBedRequest;
use App\Http\Requests\GenericPageRequest;
use App\Services\Contract\IBedService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BedController extends Controller
{
    private $_bedService;

    private $_request;

    private $_createBedRequest;

    private $_updateBedRequest;

    /**
     * BedController constructor.
     * @param Request $request
     * @param IBedService $bedService
     */
    public function __construct(Request $request, IBedService $bedService)
    {
        $this->_request = $request;

        $this->_bedService = $bedService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new BedRequest();

        $customRequest->agent_id = $this->_request->get('agent_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_bedService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_bedService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createBedRequest = new CreateBedRequest();

        $this->_createBedRequest->agent_id = $this->_request->input('agent_id');
        $this->_createBedRequest->name = $this->_request->input('name');
        $this->_createBedRequest->description = $this->_request->input('description');

        $result = $this->_bedService->save($this->_createBedRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateBedRequest = new UpdateBedRequest();

        $this->_updateBedRequest->id = $this->_request->input('id');
        $this->_updateBedRequest->agent_id = $this->_request->input('agent_id');
        $this->_updateBedRequest->name = $this->_request->input('name');
        $this->_updateBedRequest->description = $this->_request->input('description');

        $result = $this->_bedService->update($this->_updateBedRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_bedService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $agentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBedListByAgentId($agentId)
    {
        $result = $this->_bedService->getBedListByAgentId($agentId);

        return response()->json($result);
    }
}
