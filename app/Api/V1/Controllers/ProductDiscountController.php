<?php
namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductDiscount\CreateProductDiscountRequest;
use App\Http\Requests\ProductDiscount\UpdateProductDiscountRequest;
use App\Http\Requests\ProductInclude\ProductDiscountRequest;
use App\Services\Contract\IProductDiscountService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductDiscountController extends Controller
{
    private $_productDiscountService;

    private $_request;

    private $_createProductDiscountRequest;

    private $_updateProductDiscountRequest;

    /**
     * ProductDiscountController constructor.
     * @param Request $request
     * @param IProductDiscountService $productDiscountService
     */
    public function __construct(Request $request, IProductDiscountService $productDiscountService)
    {
        $this->_request = $request;

        $this->_productDiscountService = $productDiscountService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new ProductDiscountRequest();

        $customRequest->agent_id = $this->_request->get('agent_id');
        $customRequest->product_id = $this->_request->get('product_id');
        $customRequest->open_date = $this->_request->get('open_date');
        $customRequest->close_date = $this->_request->get('close_date');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_productDiscountService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_productDiscountService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createProductDiscountRequest = new CreateProductDiscountRequest();

        $this->_createProductDiscountRequest->product_id = $this->_request->input('product_id');
        $this->_createProductDiscountRequest->open_date = $this->_request->input('open_date');
        $this->_createProductDiscountRequest->close_date = $this->_request->input('close_date');
        $this->_createProductDiscountRequest->discount = $this->_request->input('discount');
        $this->_createProductDiscountRequest->status = $this->_request->input('status');

        $result = $this->_productDiscountService->save($this->_createProductDiscountRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateProductDiscountRequest = new UpdateProductDiscountRequest();

        $this->_updateProductDiscountRequest->id = $this->_request->input('id');
        $this->_updateProductDiscountRequest->product_id = $this->_request->input('product_id');
        $this->_updateProductDiscountRequest->open_date = $this->_request->input('open_date');
        $this->_updateProductDiscountRequest->close_date = $this->_request->input('close_date');
        $this->_updateProductDiscountRequest->discount = $this->_request->input('discount');
        $this->_updateProductDiscountRequest->status = $this->_request->input('status');

        $result = $this->_productDiscountService->update($this->_updateProductDiscountRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_productDiscountService->delete($id);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByProductId($id)
    {
        $result = $this->_productDiscountService->getByProductId($id);

        return response()->json($result);
    }
}
