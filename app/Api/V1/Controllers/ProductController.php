<?php

namespace App\Api\V1\Controllers;

use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Requests\ProductImage\CreateProductImageRequest;
use App\Http\Requests\ProductTag\CreateProductTagRequest;
use App\Http\Requests\ProductVideo\CreateProductVideoRequest;
use App\Http\Requests\WayPoint\CreateWayPointRequest;
use App\Services\Contract\IProductService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    private $_productService;

    private $_request;

    private $_createProductRequest;

    private $_updateProductRequest;

    /**
     * ProductController constructor.
     * @param Request $request
     * @param IProductService $productService
     */
    public function __construct(Request $request, IProductService $productService)
    {
        $this->_request = $request;

        $this->_productService = $productService;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $customRequest = new ProductRequest();

        $customRequest->product_category_id = $this->_request->get('product_category_id');
        $customRequest->product_type_id = $this->_request->get('product_type_id');
        $customRequest->agent_id = $this->_request->get('agent_id');

        $pageRequest = new GenericPageRequest();

        $pageRequest->draw = $this->_request->get('draw');
        $pageRequest->start = $this->_request->get('start');
        $pageRequest->length = $this->_request->get('length');

        $order[0]['column'] = $this->_request->get('order')[0]['column'];
        $order[0]['dir'] = $this->_request->get('order')[0]['dir'];
        $pageRequest->order = $order[0];

        $columns[0]['name'] = $this->_request->get('columns')[0]['name'];
        $columns[1]['name'] = $this->_request->get('columns')[1]['name'];
        $columns[2]['name'] = $this->_request->get('columns')[2]['name'];
        $columns[3]['name'] = $this->_request->get('columns')[3]['name'];
        $pageRequest->columns = $columns;

        $pageRequest->search = $this->_request->get('search')['value'];

        $pageRequest->custom = $customRequest;

        $result = $this->_productService->getAll($pageRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail($id)
    {
        $result = $this->_productService->getDetail($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->_createProductRequest = new CreateProductRequest();

        $this->_createProductRequest->product_type_id = $this->_request->input('product_type_id');
        $this->_createProductRequest->product_category_id = $this->_request->input('product_category_id');
        $this->_createProductRequest->property_type_id = $this->_request->input('property_type_id');
        $this->_createProductRequest->agent_id = $this->_request->input('agent_id');
        $this->_createProductRequest->publish = $this->_request->input('publish');
        $this->_createProductRequest->title = $this->_request->input('title');
        $this->_createProductRequest->slug = $this->_request->input('slug');
        $this->_createProductRequest->overview = strip_tags($this->_request->input('overview'));
        $this->_createProductRequest->description = $this->_request->input('description');
        $this->_createProductRequest->featured_image_url = $this->_request->input('featured_image_url');
        $this->_createProductRequest->featured_video_url = $this->_request->input('featured_video_url');
        $this->_createProductRequest->include_brochure_url = $this->_request->input('include_brochure_url');
        $this->_createProductRequest->itinerary_brochure_url = $this->_request->input('itinerary_brochure_url');
        $this->_createProductRequest->lat = $this->_request->input('lat');
        $this->_createProductRequest->lng = $this->_request->input('lng');
        $this->_createProductRequest->address = $this->_request->input('address');
        $this->_createProductRequest->duration = $this->_request->input('duration');
        $this->_createProductRequest->min_age = $this->_request->input('min_age');
        $this->_createProductRequest->max_age = $this->_request->input('max_age');
        $this->_createProductRequest->price = $this->_request->input('price');
        $this->_createProductRequest->grade = $this->_request->input('grade');
        $this->_createProductRequest->discount = $this->_request->input('discount');
        $this->_createProductRequest->status = $this->_request->input('status');
        $this->_createProductRequest->product_tag = $this->_request->input('product_tag');
        $this->_createProductRequest->product_facility = $this->_request->input('product_facility');
        $this->_createProductRequest->product_room = $this->_request->input('product_room');
        $this->_createProductRequest->product_image = $this->_request->input('product_image');
        $this->_createProductRequest->product_video = $this->_request->input('product_video');
        $this->_createProductRequest->product_way_point = $this->_request->input('product_way_point');
        $this->_createProductRequest->product_include = $this->_request->input('product_include');
        $this->_createProductRequest->product_itinerary = $this->_request->input('product_itinerary');

        $result = $this->_productService->save($this->_createProductRequest);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $this->_updateProductRequest = new UpdateProductRequest();

        $this->_updateProductRequest->id = $this->_request->input('id');
        $this->_updateProductRequest->product_type_id = $this->_request->input('product_type_id');
        $this->_updateProductRequest->product_category_id = $this->_request->input('product_category_id');
        $this->_updateProductRequest->property_type_id = $this->_request->input('property_type_id');
        $this->_updateProductRequest->agent_id = $this->_request->input('agent_id');
        $this->_updateProductRequest->title = $this->_request->input('title');
        $this->_updateProductRequest->slug = $this->_request->input('slug');
        $this->_updateProductRequest->overview = strip_tags($this->_request->input('overview'));
        $this->_updateProductRequest->description = $this->_request->input('description');
        $this->_updateProductRequest->featured_image_url = $this->_request->input('featured_image_url');
        $this->_updateProductRequest->featured_video_url = $this->_request->input('featured_video_url');
        $this->_updateProductRequest->include_brochure_url = $this->_request->input('include_brochure_url');
        $this->_updateProductRequest->itinerary_brochure_url = $this->_request->input('itinerary_brochure_url');
        $this->_updateProductRequest->lat = $this->_request->input('lat');
        $this->_updateProductRequest->lng = $this->_request->input('lng');
        $this->_updateProductRequest->address = $this->_request->input('address');
        $this->_updateProductRequest->duration = $this->_request->input('duration');
        $this->_updateProductRequest->min_age = $this->_request->input('min_age');
        $this->_updateProductRequest->max_age = $this->_request->input('max_age');
        $this->_updateProductRequest->price = $this->_request->input('price');
        $this->_updateProductRequest->grade = $this->_request->input('grade');
        $this->_updateProductRequest->discount = $this->_request->input('discount');
        $this->_updateProductRequest->status = $this->_request->input('status');
        $this->_updateProductRequest->product_tag = $this->_request->input('product_tag');
        $this->_updateProductRequest->product_facility = $this->_request->input('product_facility');
        $this->_updateProductRequest->product_room = $this->_request->input('product_room');
        $this->_updateProductRequest->product_image = $this->_request->input('product_image');
        $this->_updateProductRequest->product_video = $this->_request->input('product_video');
        $this->_updateProductRequest->product_way_point = $this->_request->input('product_way_point');
        $this->_updateProductRequest->product_include = $this->_request->input('product_include');
        $this->_updateProductRequest->product_itinerary = $this->_request->input('product_itinerary');

        $result = $this->_productService->update($this->_updateProductRequest);

        return response()->json($result);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->_productService->delete($id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductByTitle()
    {
        $agent_id = $this->_request->get('agent_id');
        $product_type_id = $this->_request->get('product_type_id');
        $title = $this->_request->get('title');

        $result = $this->_productService->getProductByTitle($title, $agent_id, $product_type_id);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSlug()
    {
        $agent_id = $this->_request->get('agent_id');
        $title = $this->_request->get('title');

        $result = $this->_productService->getSlug($agent_id, $title);

        return response()->json($result);
    }
}
