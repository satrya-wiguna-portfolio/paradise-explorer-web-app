<?php
namespace App\Services\Implement;


use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;
use App\Http\Responses\BaseResponse;
use App\Repositories\Contract\IReservationPaypalResponseRepository;
use App\Services\Contract\IReservationPaypalResponseService;
use Exception;

class ReservationPaypalResponseService extends BaseService implements IReservationPaypalResponseService
{
    private $_reservationPaypalResponseRepository;

    /**
     * RegencyService constructor.
     * @param IReservationPaypalResponseRepository $reservationPaypalResponseRepository
     */
    public function __construct(IReservationPaypalResponseRepository $reservationPaypalResponseRepository)
    {
        $this->_reservationPaypalResponseRepository = $reservationPaypalResponseRepository;
    }

    /**
     * @param CreateReservationPaypalResponseRequest $request
     * @return BaseResponse
     */
    public function save(CreateReservationPaypalResponseRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $reservation_id = $this->_reservationPaypalResponseRepository->create($request);

            $this->_baseResponse->addSuccessMessage('Reservation paypal response added');
            $this->_baseResponse->_result = $reservation_id;

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

}