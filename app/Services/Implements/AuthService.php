<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Http\Requests\Auth\LoginAuthRequest;
use App\Http\Requests\Auth\RecoveryAuthRequest;
use App\Http\Requests\Auth\SignupAuthRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IAuthRepository;
use App\Repositories\Contract\IUserActivateRepository;
use App\Repositories\Contract\IUserRepository;
use App\Services\Contract\IAuthService;
use Dingo\Api\Exception\ValidationHttpException;
use Dingo\Api\Routing\Helpers;
use Exception;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService extends BaseService implements IAuthService
{
    use Helpers;

    private $_authRepository;

    private $_userRepository;

    private $_userActivateRepository;

    /**
     * AuthService constructor.
     * @param IAuthRepository $authRepository
     * @param IUserRepository $userRepository
     * @param IUserActivateRepository $userActivateRepository
     */
    public function __construct(IAuthRepository $authRepository, IUserRepository $userRepository, IUserActivateRepository $userActivateRepository)
    {
        $this->_authRepository = $authRepository;

        $this->_userRepository = $userRepository;

        $this->_userActivateRepository = $userActivateRepository;
    }

    /**
     * @param LoginAuthRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginAuthRequest $request)
    {
        $credentials = [
            'email' => $request->getEmail(),
            'password' => $request->getPassword()
        ];

        $validator = Validator::make($credentials, $request->rules());

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        } else {
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return $this->response->errorUnauthorized();
                }

                $user =  $this->_userRepository->skipCriteria()
                    ->with(['role'])
                    ->fetchFindSearch(['email' => $request->getEmail()])
                    ->first();

                $user_id = $user->id;
                $email = $user->email;
                $user_role = $user->role->first();
                $role_id = $user_role->id;
                $role = $user_role->role;

                if ($user->status != 1) {
                    return $this->response->errorInternal('Account is not active');
                }

                $this->_genericResponse = new GenericResponse();
                $this->_genericResponse->dto = compact('user_id', 'email', 'role_id', 'role', 'token');

                return $this->_genericResponse;

            } catch (JWTException $ex) {
                return $this->response->errorInternal('Could not create token');
            }
        }
    }

    /**
     * @param SignupAuthRequest $request
     * @return \Dingo\Api\Http\Response|void
     */
    public function signupByWeb(SignupAuthRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        $signup_key = Config::get('boilerplate.signup_fields');
        $signup_value = [
            $request->getEmail(),
            \Hash::make($request->getPassword())
        ];

        $credentials = array_combine($signup_key, $signup_value);
        $validator = Validator::make($credentials, Config::get('boilerplate.signup_fields_rules'));

        if ($validator->fails()) {
            $this->_baseResponse->_validator = $validator;
        } else {
            try {
                $user = $this->_authRepository->signup($credentials, $request->getRole()->id);

                $token = $this->_userActivateRepository->create($request->user_activate);

                $request = $user->toArray();
                $request['token'] = $token;

                $this->_baseResponse->addSuccessMessage("Success user registered, please check your email for activate!");

                $this->sendEmail('emails.signup', $request);

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;

    }

    /**
     * @param SignupAuthRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function signupByApi(SignupAuthRequest $request)
    {
        $signup_key = Config::get('boilerplate.signup_fields');
        $signup_value = [
            $request->getEmail(),
            \Hash::make($request->getPassword())
        ];

        $credentials = array_combine($signup_key, $signup_value);
        $validator = Validator::make($credentials, Config::get('boilerplate.signup_fields_rules'));

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        } else {
            try {
                $user = $this->_authRepository->signup($credentials, $request->getRole()->role_id);

                $token = $this->_userActivateRepository->create($request->user_activate);

                $request = $user->toArray();
                $request['token'] = $token;

                $this->_baseResponse->addSuccessMessage("Success user registered, please check your email for activate!");

                $this->sendEmail('emails.signup', $request);

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return  $this->response->created();

    }

    /**
     * @param RecoveryAuthRequest $request
     * @return \Dingo\Api\Http\Response|void
     */
    public function recovery(RecoveryAuthRequest $request)
    {
        $credentials = [
            'email' => $request->getEmail()
        ];

        $validator = Validator::make($credentials, $request->rules());

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        } else {
            $response = Password::sendResetLink($credentials, function (Message $message) {
                $message->subject(Config::get('boilerplate.recovery_email_subject'));
            });

            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return $this->response->noContent();

                case Password::INVALID_USER:
                    return $this->response->errorNotFound();
            }
        }
    }

//    /**
//     * @param ResetAuthRequest $request
//     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse|void
//     */
//    public function reset(ResetAuthRequest $request)
//    {
//        $credentials = [
//            'email' => $request->getEmail(),
//            'password' => $request->getPassword(),
//            'password_confirmation' => $request->getPasswordConfirmation(),
//            'token' => $request->getToken()
//        ];
//
//        $validator = Validator::make($credentials, $request->rules());
//
//        if ($validator->fails()) {
//            throw new ValidationHttpException($validator->errors()->all());
//        } else {
//            $response = Password::reset($credentials, function ($user, $password) {
//                $user->password = $password;
//                $user->save();
//            });
//
//            switch ($response) {
//                case Password::PASSWORD_RESET:
//                    if(Config::get('boilerplate.reset_token_release')) {
//                        return $this->login($request);
//                    }
//                    return $this->response->noContent();
//
//                default:
//                    return $this->response->errorInternal('Could not reset password');
//            }
//        }
//    }


    /**
     * @param $template
     * @param $request
     */
    private function sendEmail($template, $request)
    {
        $data = [
            'to' => $request['email'],
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Reply',
            'subject' => 'Activation',
            'model' => [
                'token' => $request['token'],
                'email' => $request['email']
            ]
        ];

        Event::fire(new SendEmailEvent($template, $data));
    }
}