<?php
namespace App\Services\Implement;


use App\Helper\PostHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductDiscount\CreateProductDiscountRequest;
use App\Http\Requests\ProductDiscount\UpdateProductDiscountRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IProductDiscountRepository;
use App\Repositories\Criterias\Implement\ProductDiscount\GetAllProductDiscountCriteria;
use App\Repositories\Criterias\Implement\ProductDiscount\GetDetailProductDiscountCriteria;
use App\Repositories\Criterias\Implement\ProductDiscount\GetProductDiscountWhereBetweenByOpenAndCloseCriteria;
use App\Repositories\Criterias\Implement\ProductDiscount\GetProductDiscountWhereEqualByAgentIdCriteria;
use App\Repositories\Criterias\Implement\ProductDiscount\GetProductDiscountWhereEqualByProductIdCriteria;
use App\Repositories\Criterias\Implement\ProductDiscount\GetProductDiscountWhereNotEqualByIdCriteria;
use App\Services\Contract\IProductDiscountService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class ProductDiscountService extends BaseService implements IProductDiscountService
{
    private $_productDiscountRepository;

    /**
     * ProductDiscountService constructor.
     * @param IProductDiscountRepository $productDiscountRepository
     */
    public function __construct(IProductDiscountRepository $productDiscountRepository)
    {
        $this->_productDiscountRepository = $productDiscountRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_productDiscountRepository->pushCriteria(new GetAllProductDiscountCriteria($pageRequest->getCustom()->open_date, $pageRequest->getCustom()->close_date))
            ->pushCriteria(new GetProductDiscountWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->pushCriteria(new GetProductDiscountWhereEqualByProductIdCriteria($pageRequest->getCustom()->product_id))
            ->orderBy('product_id')
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'product_id' => (int)$model->product_id,
                'product' => $model->product,
                'open_date' => date('F, d Y', strtotime($model->open_date)),
                'close_date' => date('F, d Y', strtotime($model->close_date)),
                'discount' => (float)$model->discount,
                'status' => (int)$model->status
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_productDiscountRepository->pushCriteria(new GetDetailProductDiscountCriteria())
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'agent_id' => (int)$model->agent_id,
            'product_id' => (int)$model->product_id,
            'open_date' => $model->open_date,
            'close_date' => $model->close_date,
            'discount' => (float)$model->discount,
            'status' => (int)$model->status
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateProductDiscountRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateProductDiscountRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $row = $this->_productDiscountRepository->pushCriteria(new GetProductDiscountWhereEqualByProductIdCriteria($request->product_id))
                    ->pushCriteria(new GetProductDiscountWhereBetweenByOpenAndCloseCriteria($request->open_date, $request->close_date))
                    ->fetchAll()->count();

                if ($row < 1) {
                    $this->_baseResponse->_result = $this->_productDiscountRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Product discount created");

                } else {
                    $this->_baseResponse->addErrorMessage('Product discount has already taken');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateProductDiscountRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateProductDiscountRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $row = $this->_productDiscountRepository->pushCriteria(new GetProductDiscountWhereEqualByProductIdCriteria($request->product_id))
                    ->pushCriteria(new GetProductDiscountWhereBetweenByOpenAndCloseCriteria($request->open_date, $request->close_date))
                    ->pushCriteria(new GetProductDiscountWhereNotEqualByIdCriteria($request->id))
                    ->fetchAll()->count();

                if ($row < 1) {
                    $this->_baseResponse->_result = $this->_productDiscountRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Product discount updated");

                } else {
                    $this->_baseResponse->addErrorMessage('Product discount has already taken');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_productDiscountRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Product discount deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getByProductId($id)
    {
        $models = $this->_productDiscountRepository->pushCriteria(new GetProductDiscountWhereEqualByProductIdCriteria($id))
            ->fetchAll();

        $output = [];

        foreach($models as $model) {
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => 'Discount: ' . $model->discount . '%',
                'start' => date('Y-m-d', strtotime($model->open_date)),
                'end' => date('Y-m-d', strtotime($model->close_date))
            ]);
        }

        return Collection::make($output);
    }
}