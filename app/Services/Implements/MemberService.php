<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Helper\DirectoryHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Member\CreateMemberRequest;
use App\Http\Requests\Member\UpdateMemberRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Libraries\Slim;
use App\Repositories\Contract\IMemberRepository;
use App\Repositories\Contract\IUserActivateRepository;
use App\Repositories\Contract\IUserRepository;
use App\Repositories\Criterias\Implement\Member\GetAllMemberCriteria;
use App\Repositories\Criterias\Implement\Member\GetDetailMemberCriteria;
use App\Repositories\Criterias\Implement\Member\GetMemberWhereEqualByCountryIdCriteria;
use App\Repositories\Criterias\Implement\Member\GetMemberWhereEqualByStateIdCriteria;
use App\Services\Contract\IMemberService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class MemberService extends BaseService implements IMemberService
{
    private $_memberRepository;

    private $_userRepository;

    private $_userActivateRepository;

    /**
     * MemberService constructor.
     * @param IMemberRepository $memberRepository
     * @param IUserRepository $userRepository
     * @param IUserActivateRepository $userActivateRepository
     */
    public function __construct(IMemberRepository $memberRepository, IUserRepository $userRepository, IUserActivateRepository $userActivateRepository)
    {
        $this->_memberRepository = $memberRepository;

        $this->_userRepository = $userRepository;

        $this->_userActivateRepository = $userActivateRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_memberRepository->pushCriteria(new GetAllMemberCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetMemberWhereEqualByCountryIdCriteria($pageRequest->getCustom()->country_id))
            ->pushCriteria(new GetMemberWhereEqualByStateIdCriteria($pageRequest->getCustom()->state_id))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'user_id' => (int)$model->user_id,
                'email' => $model->email,
                'title' => $model->title,
                'first_name' => $model->first_name,
                'last_name' => $model->last_name,
                'gender' => $model->gender,
                'country' => $model->country,
                'state' => $model->state
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_memberRepository->pushCriteria(new GetDetailMemberCriteria());

        $model = $model->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'user_id' => (int)$model->user_id,
            'title' => $model->title,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'gender' => $model->gender,
            'address' => $model->address,
            'country_id' => (int)$model->country_id,
            'country' => $model->country,
            'state_id' => (int)$model->state_id,
            'state' => $model->state,
            'city' => $model->city,
            'zip' => $model->zip,
            'image_url' => $model->image_url,
            'phone' => $model->phone
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateMemberRequest $request
     * @return BaseResponse
     */
    public function save(CreateMemberRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        if (!empty($request->user_id)) {
            $this->createMember($request);

        } else {
            $validator = Validator::make((array) $request->user, $request->user->rules());

            if ($validator->fails()) {
                $this->_baseResponse->addErrorMessage($validator->errors()->all());

            } else {
                try {
                    $request->user->password = \Hash::make($request->user->password);
                    $request->user_id = $this->_userRepository->create($request->user);

                    $this->createMember($request);

                    $token = $this->_userActivateRepository->create($request->user->user_activate);

                    $model = $this->_memberRepository->pushCriteria(new GetDetailMemberCriteria())
                        ->fetchFindBy('users.id', $request->user_id);

                    $data = $model->toArray();
                    $this->sendEmail('emails.signup', $data, $token);

                } catch (Exception $ex) {
                    $this->_baseResponse->addErrorMessage($ex->getMessage());

                }
            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateMemberRequest $request
     * @return BaseResponse
     */
    public function update(UpdateMemberRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_memberRepository->update($request);

                $images = Slim::getImagesByRequest([$request->getImageFile()]);

                if ($images) {
                    foreach ($images as $image) {
                        if (isset($image['output']['data'])) {
                            $name = $image['output']['name'];
                            $data = $image['output']['data'];
                            Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::MEMBER, false);
                        }
                    }
                }

                $this->_baseResponse->addSuccessMessage("Member updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_memberRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Member deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $userId
     * @return GenericResponse
     */
    public function getMemberByUserId($userId)
    {
        $model = $this->_memberRepository->skipCriteria()
            ->fetchFindBy('user_id', $userId, ['id', 'first_name', 'last_name']);

        $output = [];

        if ($model) {
            $output = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->first_name . ' ' . $model->last_name
            ]);
        }

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $request
     */
    private function createMember($request)
    {
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_memberRepository->create($request);

                $images = Slim::getImagesByRequest([$request->getImageFile()]);

                if ($images) {
                    foreach ($images as $image) {
                        if (isset($image['output']['data'])) {
                            $name = $image['output']['name'];
                            $data = $image['output']['data'];
                            Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::MEMBER, false);
                        }
                    }
                }

                $this->_baseResponse->addSuccessMessage("Member created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }
    }

    /**
     * @param $template
     * @param $request
     * @param $token
     */
    private function sendEmail($template, $request, $token)
    {
        $content = [
            'to' => $request['email'],
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Reply',
            'subject' => 'Activation Paradise Explorer',
            'model' => [
                'token' => $token,
                'email' => $request['email']
            ]
        ];

        Event::fire(new SendEmailEvent($template, $content));
    }
}