<?php
namespace App\Services\Implement;


use App\Http\Requests\Cart\AddCartRequest;
use App\Http\Responses\BaseResponse;
use App\Services\Contract\ICartService;
use Exception;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartService extends BaseService implements ICartService
{
    /**
     * @param $productTypeId
     * @param AddCartRequest $request
     * @return BaseResponse
     */
    public function add($productTypeId, AddCartRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $product = [
                'id' => $request->id,
                'name' => $request->name,
                'qty' => $request->qty,
                'price' => $request->price,
                'options' => $request->options
            ];

            if ($productTypeId != 2) {
                $this->_baseResponse->_result = Cart::instance('tour')->add($product);

            } else {
                $this->_baseResponse->_result = Cart::instance('hotel')->add($product);

            }

            $this->_baseResponse->addSuccessMessage("Item added to cart");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param $productTypeId
     * @param $rowId
     * @return BaseResponse
     */
    public function delete($productTypeId, $rowId)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            if ($productTypeId != 2) {
                $this->_baseResponse->_result = Cart::instance('tour')->remove($rowId);
            } else {
                $this->_baseResponse->_result = Cart::instance('hotel')->remove($rowId);
            }

            $this->_baseResponse->addSuccessMessage("Item deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

}