<?php
namespace App\Services\Implement;


use App\Http\Requests\BlogComment\CreateBlogCommentRequest;
use App\Http\Requests\BlogComment\UpdateBlogCommentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IBlogCommentRepository;
use App\Repositories\Criterias\Implement\BlogComment\GetAllBlogCommentCriteria;
use App\Repositories\Criterias\Implement\BlogComment\GetBlogCommentWhereEqualByBlogIdCriteria;
use App\Repositories\Criterias\Implement\BlogComment\GetBlogCommentWhereEqualByStatusCriteria;
use App\Services\Contract\IBlogCommentService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class BlogCommentService extends BaseService implements IBlogCommentService
{
    private $_blogCommentRepository;

    /**
     * BlogCommentService constructor.
     * @param IBlogCommentRepository $blogCommentRepository
     */
    public function __construct(IBlogCommentRepository $blogCommentRepository)
    {
        $this->_blogCommentRepository = $blogCommentRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_blogCommentRepository->select(['blog_comments.*', 'blogs.title AS title'])
            ->join('blogs', 'blogs.id', '=', 'blog_comments.blog_id')
            ->pushCriteria(new GetAllBlogCommentCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => $model->title,
                'comment' => $model->comment,
                'status' => (int)$model->status
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_blogCommentRepository->select(['blog_comments.*', 'blogs.title AS title'])
            ->join('blogs', 'blogs.id', '=', 'blog_comments.blog_id')
            ->skipCriteria()
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'title' => $model->title,
            'comment' => $model->comment,
            'rate' => $model->rate,
            'ip_address' => $model->ip_address,
            'browser' => $model->browser,
            'status' => (int)$model->status
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateBlogCommentRequest $request
     * @return BaseResponse
     */
    public function save(CreateBlogCommentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->_validator = $validator;

        } else {
            try {
                $this->_baseResponse->_result = $this->_blogCommentRepository->create($request);
                $this->_baseResponse->addSuccessMessage("Blog comment created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateBlogCommentRequest $request
     * @return BaseResponse
     */
    public function update(UpdateBlogCommentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_blogCommentRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Blog comment status updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_blogCommentRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Blog comment deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $blog_id
     * @return mixed
     */
    public function getBlogCommentByBlogId($blog_id)
    {
        $status = 1;
        $order = [
            'column' => 'created_at',
            'dir' => 'desc'
        ];

        $models = $this->_blogCommentRepository->pushCriteria(new GetBlogCommentWhereEqualByBlogIdCriteria($blog_id))
            ->pushCriteria(new GetBlogCommentWhereEqualByStatusCriteria($status))
            ->with(['user' => function ($q) {
                return $q->with(['member', 'agent', 'staff']);
            }])
            ->orderBy($order['column'], $order['dir'])
            ->fetchAll($reset = false);

        return $models;
    }

}