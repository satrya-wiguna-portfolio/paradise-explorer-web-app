<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Helper\DirectoryHelper;
use App\Http\Requests\Agent\CreateAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Libraries\Slim;
use App\Repositories\Contract\IAgentRepository;
use App\Repositories\Contract\IUserRepository;
use App\Repositories\Criterias\Implement\Agent\GetAgentWhereEqualByAgentTypeIdCriteria;
use App\Repositories\Criterias\Implement\Agent\GetAgentWhereEqualByCountryIdCriteria;
use App\Repositories\Criterias\Implement\Agent\GetAgentWhereLikeByCompanyCriteria;
use App\Repositories\Criterias\Implement\Agent\GetAgentWhereEqualByStateIdCriteria;
use App\Repositories\Criterias\Implement\Agent\GetAllAgentCriteria;
use App\Repositories\Criterias\Implement\Agent\GetDetailAgentCriteria;
use App\Services\Contract\IAgentService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class AgentService extends BaseService implements IAgentService
{
    private $_agentRepository;

    private $_userRepository;

    /**
     * AgentService constructor.
     * @param IAgentRepository $agentRepository
     * @param IUserRepository $userRepository
     */
    public function __construct(IAgentRepository $agentRepository, IUserRepository $userRepository)
    {
        $this->_agentRepository = $agentRepository;

        $this->_userRepository = $userRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_agentRepository->pushCriteria(new GetAllAgentCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetAgentWhereEqualByCountryIdCriteria($pageRequest->getCustom()->country_id))
            ->pushCriteria(new GetAgentWhereEqualByStateIdCriteria($pageRequest->getCustom()->state_id))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => $model->id,
                'user_id' => (int)$model->user_id,
                'email' => $model->email,
                'title' => $model->title,
                'first_name' => $model->first_name,
                'last_name' => $model->last_name,
                'company' => $model->company,
                'gender' => $model->gender,
                'country' => $model->country,
                'state' => $model->state
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_agentRepository->pushCriteria(new GetDetailAgentCriteria())
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'user_id' => (int)$model->user_id,
            'agent_type_id' => (int)$model->agent_type_id,
            'agent_type' => $model->agent_type,
            'title' => $model->title,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'gender' => $model->gender,
            'company' => $model->company,
            'address' => $model->address,
            'country_id' => (int)$model->country_id,
            'country' => $model->country,
            'state_id' => (int)$model->state_id,
            'state' => $model->state,
            'city' => $model->city,
            'zip' => $model->zip,
            'image_url' => $model->image_url,
            'phone' => $model->phone,
            'fax' => $model->fax
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateAgentRequest $request
     * @return BaseResponse
     */
    public function save(CreateAgentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        if (!empty($request->user_id)) {
            $this->createAgent($request);

        } else {
            $validator = Validator::make((array) $request->user, $request->user->rules());

            if ($validator->fails()) {
                $this->_baseResponse->addErrorMessage($validator->errors()->all());

            } else {
                try {
                    $request->user->password = \Hash::make($request->user->password);
                    $request->user_id = $this->_userRepository->create($request->user);

                    $this->createAgent($request);

                    $model = $this->_agentRepository->pushCriteria(new GetDetailAgentCriteria())
                        ->fetchFindby('users.id', $request->user_id);

                    $data = $model->toArray();
                    $this->sendEmail('emails.signup', $data);

                } catch (Exception $ex) {
                    $this->_baseResponse->addErrorMessage($ex->getMessage());

                }
            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateAgentRequest $request
     * @return BaseResponse
     */
    public function update(UpdateAgentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_agentRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Agent updated");

                $images = Slim::getImagesByRequest([$request->getImageFile()]);

                if ($images) {
                    foreach ($images as $image) {
                        if (isset($image['output']['data'])) {
                            $name = $image['output']['name'];
                            $data = $image['output']['data'];
                            Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::AGENT, false);
                        }
                    }
                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_agentRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Agent deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param null $agentTypeId
     * @param $company
     * @return GenericPageResponse
     */
    public function getAgentByTypeAndCompany($agentTypeId = null, $company)
    {

        $models = $this->_agentRepository->pushCriteria(new GetAgentWhereLikeByCompanyCriteria($company));

        if (isset($agentTypeId)) {
            $models = $models->pushCriteria(new GetAgentWhereEqualByAgentTypeIdCriteria($agentTypeId));
        }

        $models = $models->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'company' => $model->company
            ]);

        $this->_genericPageResponse = new GenericResponse();
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $userId
     * @return GenericResponse
     */
    public function getAgentByUserId($userId)
    {
        $model = $this->_agentRepository->skipCriteria()
            ->fetchFindBy('user_id', $userId, ['id', 'company']);

        $output = [];

        if ($model) {
            $output = new Laravel5DTO([
                'id' => (int)$model->id,
                'company' => $model->company
            ]);
        }

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $request
     */
    private function createAgent($request)
    {
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_agentRepository->create($request);

                $images = Slim::getImagesByRequest([$request->getImageFile()]);

                if ($images) {
                    foreach ($images as $image) {
                        if (isset($image['output']['data'])) {
                            $name = $image['output']['name'];
                            $data = $image['output']['data'];
                            Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::AGENT, false);
                        }
                    }
                }

                $this->_baseResponse->addSuccessMessage("Agent created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }
    }

    /**
     * @param $template
     * @param $data
     */
    private function sendEmail($template, $data)
    {
        $content = [
            'to' => $data['email'],
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Reply',
            'subject' => 'Activation Paradise Explorer',
            'model' => [
                'email' => base64_encode($data['email'])
            ]
        ];

        Event::fire(new SendEmailEvent($template, $content));
    }
}