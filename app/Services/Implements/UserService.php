<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\UserLog\UpdateUserActivateRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IUserActivateRepository;
use App\Repositories\Contract\IUserRepository;
use App\Repositories\Criterias\Implement\User\GetUserWhereEqualByStatusCriteria;
use App\Repositories\Criterias\Implement\User\GetUserWhereLikeByEmailCriteria;
use App\Repositories\Criterias\Implement\User\GetUserWhereByUserIdOfAgentCriteria;
use App\Repositories\Criterias\Implement\User\GetUserWhereByUserIdOfMemberCriteria;
use App\Repositories\Criterias\Implement\User\GetUserWhereByUserIdOfStaffCriteria;
use App\Services\Contract\IUserService;
use Dingo\Api\Routing\Helpers;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class UserService extends BaseService implements IUserService
{
    use Helpers;

    private $_userRepository;

    private $_userActivateRepository;

    /**
     * UserService constructor.
     * @param IUserRepository $userRepository
     * @param IUserActivateRepository $userActivateRepository
     */
    public function __construct(IUserRepository $userRepository, IUserActivateRepository $userActivateRepository)
    {
        $this->_userRepository = $userRepository;

        $this->_userActivateRepository = $userActivateRepository;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_userRepository->skipCriteria()
            ->with(['role'])
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'email' => $model->email,
            'handphone' => $model->handphone,
            'status' => (int)$model->status,
            'role' => $model->role
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param UpdateUserRequest $request
     * @return BaseResponse
     */
    public function update(UpdateUserRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                if (!empty($request->password)) {
                    $request->password = \Hash::make($request->password);
                }

                if ($request->status == 1) {
                    $model = $this->_userActivateRepository->skipCriteria()
                        ->fetchFindSearch([
                            ['email', '=', $request->email]
                        ]);

                    if ($model->count() > 0) {
                        $this->_userActivateRepository->delete($request->email);
                    }
                }

                $this->_baseResponse->_result = $this->_userRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Success user updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $email
     * @return GenericPageResponse
     */
    public function getUserByEmailMemberAgentAndStaff($email)
    {
        $models = $this->_userRepository->select(['users.*'])
            ->leftJoin('members', 'users.id', '=', 'members.user_id')
            ->leftJoin('agents', 'users.id', '=', 'agents.user_id')
            ->leftJoin('staffs', 'users.id', '=', 'staffs.user_id')
            ->pushCriteria(new GetUserWhereLikeByEmailCriteria($email))
            ->pushCriteria(new GetUserWhereByUserIdOfMemberCriteria())
            ->pushCriteria(new GetUserWhereByUserIdOfAgentCriteria())
            ->pushCriteria(new GetUserWhereByUserIdOfStaffCriteria())
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'email' => $model->email
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getUserInfo($id)
    {
        $model = $this->_userRepository->skipCriteria()
            ->with(['agent', 'member', 'staff'])
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'email' => $model->email,
            'handphone' => $model->handphone,
            'status' => (int)$model->status,
            'agent' => $model->agent,
            'member' => $model->member,
            'staff' => $model->staff
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $roleId
     * @param $userId
     * @return GenericResponse
     */
    public function getUserProfile($roleId, $userId)
    {
        $model = $this->_userRepository->skipCriteria();

        switch ($roleId) {
            case 1:
            case 2:
                $model = $model->with(['staff' => function ($query) {
                    $query->with(['country', 'state']);
                }, 'role'])->fetchFind($userId);

                if ($model->staff) {
                    $model->staff->country_id = (int)$model->staff->country_id;
                    $model->staff->state_id = (int)$model->staff->state_id;
                }

                $output = new Laravel5DTO([
                    'id' => (int)$model->id,
                    'email' => $model->email,
                    'handphone' => $model->handphone,
                    'status' => (int)$model->status,
                    'current_login' => $model->current_login,
                    'staff' => $model->staff,
                    'role' => $model->role
                ]);
                break;

            case 3:
                $model = $model->with(['agent' => function ($query) {
                    $query->with(['country', 'state']);
                }, 'role'])->fetchFind($userId);

                $model->agent->country_id = (int)$model->agent->country_id;
                $model->agent->state_id = (int)$model->agent->state_id;

                $output = new Laravel5DTO([
                    'id' => (int)$model->id,
                    'email' => $model->email,
                    'handphone' => $model->handphone,
                    'status' => (int)$model->status,
                    'current_login' => $model->current_login,
                    'agent' => $model->agent,
                    'role' => $model->role
                ]);
                break;
        }

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $email
     * @return GenericResponse
     */
    public function getUserByEmailStatusAndAgent($email)
    {
        $status = 1;

        $models = $this->_userRepository->select(['users.*'])
            ->leftJoin('agents', 'users.id', '=', 'agents.user_id')
            ->pushCriteria(new GetUserWhereLikeByEmailCriteria($email))
            ->pushCriteria(new GetUserWhereEqualByStatusCriteria($status))
            ->pushCriteria(new GetUserWhereByUserIdOfAgentCriteria())
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'email' => $model->email
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $token
     * @param $email
     * @return BaseResponse
     */
    public function activate($token, $email)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $model = $this->_userActivateRepository->skipCriteria()
                ->fetchFindSearch([
                    ['email', '=', $email],
                    ['token', '=', $token]
                ]);

            if ($model->count() > 0) {
                $this->_userActivateRepository->delete($email);

                $this->_baseResponse->_result = $this->_userRepository->activate($email);
                $this->_baseResponse->addSuccessMessage("Success user activated");
            } else {
                return $this->response->errorInternal('Account is not available');
            }

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateUserActivateRequest $request
     * @return BaseResponse|void
     */
    public function email(UpdateUserActivateRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $model = $this->_userActivateRepository->skipCriteria()
                ->fetchFindSearch([
                    ['email', '=', $request->email]
                ]);

            if ($model->count() > 0) {
                $token = $this->_userActivateRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Success send link activated");

                $data = ['email' => $request->email];

                $this->sendEmail('emails.signup', $data, $token);

            } else {
                return $this->response->errorInternal('Account is not available');
            }

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param $template
     * @param $request
     * @param $token
     */
    private function sendEmail($template, $request, $token)
    {
        $data = [
            'to' => $request['email'],
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Reply',
            'subject' => 'Activation Paradise Explorers',
            'model' => [
                'token' => $token,
                'email' => $request['email']
            ]
        ];

        Event::fire(new SendEmailEvent($template, $data));
    }
}