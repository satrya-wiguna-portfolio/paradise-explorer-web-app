<?php
namespace App\Services\Implement;


use App\Helper\DirectoryHelper;
use App\Http\Requests\BlogCategory\CreateDestinationRequest;
use App\Http\Requests\BlogCategory\UpdateDestinationRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Libraries\Slim;
use App\Repositories\Contract\IDestinationRepository;
use App\Repositories\Criterias\Implement\BlogCategory\GetDestinationWhereNotEqualByIdAndLikeByNameCriteria;
use App\Repositories\Criterias\Implement\BlogCategory\GetDestinationWhereEqualByParentIdCriteria;
use App\Repositories\Criterias\Implement\Destination\GetAllDestinationCriteria;
use App\Repositories\Criterias\Implement\Destination\GetDestinationByLimitCriteria;
use App\Repositories\Criterias\Implement\Destination\GetDestinationWhereNullByParentIdCriteria;
use App\Repositories\Criterias\Implement\Destination\GetDestinationByRandomCriteria;
use App\Services\Contract\IDestinationService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class DestinationService extends BaseService implements IDestinationService
{
    private $_destinationRepository;

    /**
     * DestinationService constructor.
     * @param IDestinationRepository $destinationRepository
     */
    public function __construct(IDestinationRepository $destinationRepository)
    {
        $this->_destinationRepository = $destinationRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_destinationRepository->pushCriteria(new GetAllDestinationCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetDestinationWhereEqualByParentIdCriteria($pageRequest->getCustom()->parent_id))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'parent_id' => (int)$model->parent_id,
                'parent' => $model->parent,
                'name' => $model->name,
                'image_url' => $model->image_url,
                'lat' => $model->lat,
                'lng' => $model->lng,
                'description' => $model->description,
                'child' => $model->child
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_destinationRepository->skipCriteria()
            ->with(['parent'])
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'parent_id' => (int)$model->parent_id ? $model->parent_id : null,
            'parent' => $model->parent ? $model->parent->name : null,
            'name' => $model->name,
            'image_url' => $model->image_url,
            'lat' => $model->lat,
            'lng' => $model->lng,
            'description' => $model->description
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateDestinationRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateDestinationRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByParentIdAndName = $this->_destinationRepository->skipCriteria()->fetchFindSearch([
                    ['parent_id', '=', $request->getParentId()],
                    ['name', 'REGEXP', '[[:<:]]' . $request->getName() . '[[:>:]]']
                ]);

                if ($modelByParentIdAndName->count() == 0) {
                    $this->_baseResponse->_result = $this->_destinationRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Destination created");

                    //Upload the image
                    $images = Slim::getImagesByRequest([$request->getImageFile()]);

                    if ($images) {
                        foreach ($images as $image) {
                            if (isset($image['output']['data'])) {
                                $name = $image['output']['name'];
                                $data = $image['output']['data'];
                                Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::DESTINATION, false);
                            }
                        }
                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Destination is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateDestinationRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateDestinationRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByIdParentIdAndName = $this->_destinationRepository->skipCriteria()->fetchFindSearch([
                    ['id', '<>', $request->getId()],
                    ['parent_id', '<>', $request->getParentId()],
                    ['name', 'REGEXP', '[[:<:]]' . $request->getName() . '[[:>:]]']
                ]);

                if ($modelByIdParentIdAndName->count() == 0) {
                    $this->_baseResponse->_result = $this->_destinationRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Destination updated");

                    $images = Slim::getImagesByRequest([$request->getImageFile()]);

                    if ($images) {
                        foreach ($images as $image) {
                            if (isset($image['output']['data'])) {
                                $name = $image['output']['name'];
                                $data = $image['output']['data'];
                                Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::DESTINATION, false);
                            }
                        }
                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Destination is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_destinationRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Destination deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @param $name
     * @return GenericPageResponse
     */
    public function getDestinationByName($id, $name)
    {
        $models = $this->_destinationRepository->pushCriteria(new GetDestinationWhereNotEqualByIdAndLikeByNameCriteria($id, $name))
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @return GenericResponse
     */
    public function getDestinationList()
    {
        $order = [
            'column' => 'name',
            'dir' => 'asc'
        ];

        $models = $this->_destinationRepository->skipCriteria()
            ->with(['parent'])
            ->orderBy($order['column'], $order['dir'])
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'parent_id' => $model->parent_id ? (int)$model->parent_id : null,
                'parent' => $model->parent ? $model->parent->name : null,
                'name' => $model->name,
                'image_url' => $model->image_url,
                'lat' => $model->lat,
                'lng' => $model->lng,
                'description' => $model->description
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @return GenericResponse
     */
    public function getDestinationHierarchy()
    {
        $models = $this->_destinationRepository->skipCriteria()
            ->fetchAll();

        $output = $this->builtHierarchy($models);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @return mixed
     */
    public function getRandomDestination()
    {
        $models = $this->_destinationRepository->pushCriteria(new GetDestinationByRandomCriteria())
            ->fetchAll();

        return $models;
    }

    /**
     * @param $limit
     * @return mixed
     */
    public function getRandomLimitDestination($limit)
    {
        $models = $this->_destinationRepository->pushCriteria(new GetDestinationByRandomCriteria())
            ->pushCriteria(new GetDestinationWhereNullByParentIdCriteria())
            ->pushCriteria(new GetDestinationByLimitCriteria($limit))
            ->fetchAll();

        return $models;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDestinationDetail($id)
    {
        $models = $this->_destinationRepository->skipCriteria()
            ->fetchFind($id);

        return $models;
    }

    /**
     * @param $models
     * @param null $parent
     * @return array
     */
    private function builtHierarchy($models, $parent = null)
    {
        $output = [];

        foreach($models as $model) {
            if ($model->parent_id == $parent) {

                $output[] = new Laravel5DTO([
                    'id' => (int)$model->id,
                    'parent_id' => $model->parent_id ? (int)$model->parent_id : null,
                    'parent' => $model->parent ? $model->parent->name : null,
                    'name' => $model->name,
                    'image_url' => $model->image_url,
                    'lat' => $model->lat,
                    'lng' => $model->lng,
                    'description' => $model->description,
                    'dto' => ($this->hasChildren($models, $model->id)) ? $this->builtHierarchy($models, $model->id) : null
                ]);
            }
        }

        return $output;
    }

    /**
     * @param $models
     * @param $id
     * @return bool
     */
    private function hasChildren($models, $id)
    {
        foreach ($models as $model) {
            if ($model->id == $id)
                return true;
        }

        return false;
    }
}