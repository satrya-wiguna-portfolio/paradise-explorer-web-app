<?php
namespace App\Services\Implement;


use App\Http\Requests\Currency\CreateCurrencyRequest;
use App\Http\Requests\Currency\SyncCurrencyRequest;
use App\Http\Requests\Currency\UpdateCurrencyRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\ICurrencyRepository;
use App\Repositories\Criterias\Implement\Currency\GetCurrencyWhereEqualByStatusCriteria;
use App\Services\Contract\ICurrencyService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class CurrencyService extends BaseService implements ICurrencyService
{
    private $_currencyRepository;

    /**
     * CurrencyService constructor.
     * @param ICurrencyRepository $currencyRepository
     */
    public function __construct(ICurrencyRepository $currencyRepository)
    {
        $this->_currencyRepository = $currencyRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_currencyRepository->skipCriteria()
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name,
                'code' => $model->code,
                'symbol' => $model->symbol,
                'format' => $model->format,
                'exchange_rate' => $model->exchange_rate,
                'active' => (int)$model->active
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_currencyRepository->skipCriteria()->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'name' => $model->name,
            'code' => $model->code,
            'symbol' => $model->symbol,
            'format' => $model->format,
            'exchange_rate' => $model->exchange_rate,
            'active' => (int)$model->active
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateCurrencyRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateCurrencyRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_currencyRepository->create($request);

                if (!strpos($this->_baseResponse->_result, 'exists') && !strpos($this->_baseResponse->_result, 'not found')) {
                    $this->_baseResponse->addSuccessMessage($this->_baseResponse->_result);
                } else {
                    $this->_baseResponse->addErrorMessage($this->_baseResponse->_result);
                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateCurrencyRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateCurrencyRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_currencyRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Currency updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $code
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($code)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_currencyRepository->delete($code);
            $this->_baseResponse->addSuccessMessage("Currency deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param SyncCurrencyRequest $request
     * @return BaseResponse
     */
    public function sync(SyncCurrencyRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_currencyRepository->sync($request);
                $this->_baseResponse->addSuccessMessage($this->_baseResponse->_result);

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @return static
     */
    public function getActiveCurrency()
    {
        $status = 1;

        $models = $this->_currencyRepository->pushCriteria(new GetCurrencyWhereEqualByStatusCriteria($status))
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name,
                'code' => $model->code,
                'symbol' => $model->symbol,
                'format' => $model->format,
                'exchange_rate' => $model->exchange_rate,
                'active' => (int)$model->active
            ]);

        return Collection::make($output);
    }
}