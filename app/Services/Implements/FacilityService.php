<?php
namespace App\Services\Implement;


use App\Helper\DirectoryHelper;
use App\Http\Requests\Facility\CreateFacilityRequest;
use App\Http\Requests\Facility\UpdateFacilityRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Libraries\Slim;
use App\Repositories\Contract\IFacilityRepository;
use App\Repositories\Criterias\Implement\Facility\GetAllFacilityCriteria;
use App\Repositories\Criterias\Implement\Facility\GetFacilityWhereLikeByNameCriteria;
use App\Services\Contract\IFacilityService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class FacilityService extends BaseService implements IFacilityService
{
    private $_facilityRepository;

    /**
     * FacilityService constructor.
     * @param IFacilityRepository $facilityRepository
     */
    public function __construct(IFacilityRepository $facilityRepository)
    {
        $this->_facilityRepository = $facilityRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_facilityRepository->pushCriteria(new GetAllFacilityCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'facility' => $model->facility,
                'image_url' => $model->image_url
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_facilityRepository->skipCriteria()
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'facility' => $model->facility,
            'image_url' => $model->image_url
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateFacilityRequest $request
     * @return BaseResponse
     */
    public function save(CreateFacilityRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByFacility = $this->_facilityRepository->skipCriteria()->fetchFindSearch([
                    ['facility', 'REGEXP', '[[:<:]]' . $request->getFacility() . '[[:>:]]']
                ]);

                if ($modelByFacility->count() == 0) {
                    $this->_baseResponse->_result = $this->_facilityRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Facility created");

                    //Upload the image
                    $images = Slim::getImagesByRequest([$request->getImageFile()]);

                    if ($images) {
                        foreach ($images as $image) {
                            if (isset($image['output']['data'])) {
                                $name = $image['output']['name'];
                                $data = $image['output']['data'];
                                Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::FACILITY, false);
                            }
                        }
                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Facility is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateFacilityRequest $request
     * @return BaseResponse
     */
    public function update(UpdateFacilityRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByIdAndFacility = $this->_facilityRepository->skipCriteria()->fetchFindSearch([
                    ['id', '<>', $request->getId()],
                    ['facility', 'REGEXP', '[[:<:]]' . $request->getFacility() . '[[:>:]]']
                ]);

                if ($modelByIdAndFacility->count() == 0) {
                    $this->_baseResponse->_result = $this->_facilityRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Facility updated");

                    $images = Slim::getImagesByRequest([$request->getImageFile()]);

                    if ($images) {
                        foreach ($images as $image) {
                            if (isset($image['output']['data'])) {
                                $name = $image['output']['name'];
                                $data = $image['output']['data'];
                                Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::FACILITY, false);
                            }
                        }
                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Facility is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_facilityRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Facility deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $name
     * @return GenericPageResponse
     */
    public function getFacilityByName($name)
    {
        $models = $this->_facilityRepository->pushCriteria(new GetFacilityWhereLikeByNameCriteria($name))
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'facility' => $model->facility,
                'image_url' => $model->image_url
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }
}