<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Helper\DirectoryHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Staff\CreateStaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Libraries\Slim;
use App\Repositories\Contract\IStaffRepository;
use App\Repositories\Contract\IUserRepository;
use App\Repositories\Criterias\Implement\Agent\GetAllStaffCriteria;
use App\Repositories\Criterias\Implement\Agent\GetDetailStaffCriteria;
use App\Repositories\Criterias\Implement\Agent\GetStaffWhereEqualByDistrictIdCriteria;
use App\Repositories\Criterias\Implement\Agent\GetStaffWhereEqualByProvinceIdCriteria;
use App\Repositories\Criterias\Implement\Agent\GetStaffWhereEqualByRegencyIdCriteria;
use App\Services\Contract\IStaffService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class StaffService extends BaseService implements IStaffService
{
    private $_staffRepository;

    private $_userRepository;

    /**
     * StaffService constructor.
     * @param IStaffRepository $staffRepository
     * @param IUserRepository $userRepository
     */
    public function __construct(IStaffRepository $staffRepository, IUserRepository $userRepository)
    {
        $this->_staffRepository = $staffRepository;

        $this->_userRepository = $userRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_staffRepository->pushCriteria(new GetAllStaffCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetStaffWhereEqualByProvinceIdCriteria($pageRequest->getCustom()->province_id))
            ->pushCriteria(new GetStaffWhereEqualByRegencyIdCriteria($pageRequest->getCustom()->regency_id))
            ->pushCriteria(new GetStaffWhereEqualByDistrictIdCriteria($pageRequest->getCustom()->district_id))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'user_id' => (int)$model->user_id,
                'email' => $model->email,
                'first_name' => $model->first_name,
                'last_name' => $model->last_name,
                'gender' => $model->gender,
                'province' => $model->province,
                'regency' => $model->regency,
                'district' => $model->district
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_staffRepository->pushCriteria(new GetDetailStaffCriteria());

        $model = $model->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'user_id' => (int)$model->user_id,
            'nik' => $model->nik,
            'title' => $model->title,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'gender' => $model->gender,
            'address' => $model->address,
            'province_id' => (int)$model->province_id,
            'province' => $model->province,
            'regency_id' => (int)$model->regency_id,
            'regency' => $model->regency,
            'district_id' => (int)$model->district_id,
            'district' => $model->district,
            'village' => $model->village,
            'zip' => $model->zip,
            'image_url' => $model->image_url,
            'phone' => $model->phone
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateStaffRequest $request
     * @return BaseResponse
     */
    public function save(CreateStaffRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        if (!empty($request->user_id)) {
            $this->createStaff($request);

        } else {
            $validator = Validator::make((array) $request->user, $request->user->rules());

            if ($validator->fails()) {
                $this->_baseResponse->addErrorMessage($validator->errors()->all());

            } else {
                try {
                    $request->user->password = \Hash::make($request->user->password);
                    $request->user_id = $this->_userRepository->create($request->user);

                    $this->createStaff($request);

                    $model = $this->_staffRepository->pushCriteria(new GetDetailStaffCriteria())
                        ->fetchFindBy('users.id', $request->user_id);

                    $data = $model->toArray();
                    $this->sendEmail('emails.signup', $data);

                } catch (Exception $ex) {
                    $this->_baseResponse->addErrorMessage($ex->getMessage());

                }
            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateStaffRequest $request
     * @return BaseResponse
     */
    public function update(UpdateStaffRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_staffRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Staff updated");

                $images = Slim::getImagesByRequest([$request->getImageFile()]);

                if ($images) {
                    foreach ($images as $image) {
                        if (isset($image['output']['data'])) {
                            $name = $image['output']['name'];
                            $data = $image['output']['data'];
                            Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::STAFF, false);
                        }
                    }
                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_staffRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Staff deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $userId
     * @return GenericResponse
     */
    public function getStaffByUserId($userId)
    {
        $model = $this->_staffRepository->skipCriteria()
            ->fetchFindBy('user_id', $userId, ['id', 'first_name', 'last_name']);

        $output = [];

        if ($model) {
            $output = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name
            ]);
        }

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $request
     */
    private function createStaff($request)
    {
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_staffRepository->create($request);

                $images = Slim::getImagesByRequest([$request->getImageFile()]);

                if ($images) {
                    foreach ($images as $image) {
                        if (isset($image['output']['data'])) {
                            $name = $image['output']['name'];
                            $data = $image['output']['data'];
                            Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::STAFF, false);
                        }
                    }
                }

                $this->_baseResponse->addSuccessMessage("Staff created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }
    }

    /**
     * @param $template
     * @param $data
     */
    private function sendEmail($template, $data)
    {
        $content = [
            'to' => $data['email'],
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Reply',
            'subject' => 'Activation Paradise Explorer',
            'model' => [
                'email' => base64_encode($data['email'])
            ]
        ];

        Event::fire(new SendEmailEvent($template, $content));
    }
}