<?php
namespace App\Services\Implement;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\PropertyType\CreatePropertyTypeRequest;
use App\Http\Requests\PropertyType\UpdatePropertyTypeRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IPropertyTypeRepository;
use App\Repositories\Criterias\Implement\PropertyType\GetAllPropertyTypeCriteria;
use App\Services\Contract\IPropertyTypeService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class PropertyTypeService extends BaseService implements IPropertyTypeService
{
    private $_propertyTypeRepository;

    /**
     * PropertyTypeService constructor.
     * @param IPropertyTypeRepository $propertyTypeRepository
     */
    public function __construct(IPropertyTypeRepository $propertyTypeRepository)
    {
        $this->_propertyTypeRepository = $propertyTypeRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_propertyTypeRepository->pushCriteria(new GetAllPropertyTypeCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name,
                'description' => $model->description
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_propertyTypeRepository->skipCriteria()->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'name' => $model->name,
            'description' => $model->description
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreatePropertyTypeRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreatePropertyTypeRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByRole = $this->_propertyTypeRepository->skipCriteria()->fetchFindSearch([
                    ['name', '=', $request->getName()]
                ]);

                if ($modelByRole->count() == 0) {
                    $this->_baseResponse->_result = $this->_propertyTypeRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Property type created");

                } else {
                    $this->_baseResponse->addErrorMessage('Property type is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdatePropertyTypeRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdatePropertyTypeRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByIdAndRole = $this->_propertyTypeRepository->skipCriteria()->fetchFindSearch([
                    ['id', '<>', $request->getId()],
                    ['name', '=', $request->getName()]
                ]);

                if ($modelByIdAndRole->count() == 0) {
                    $this->_baseResponse->_result = $this->_propertyTypeRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Property type updated");

                } else {
                    $this->_baseResponse->addErrorMessage('Property type is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_propertyTypeRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Property type deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @return GenericResponse
     */
    public function getPropertyTypeList()
    {
        $models = $this->_propertyTypeRepository->skipCriteria();
        $models = $models->orderBy('name', 'asc');
        $models = $models->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }
}