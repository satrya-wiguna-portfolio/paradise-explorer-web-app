<?php
namespace App\Services\Implement;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Room\CheckRoomAvailableRequest;
use App\Http\Requests\Room\CreateRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IProductRepository;
use App\Repositories\Contract\IRoomAllotmentRepository;
use App\Repositories\Contract\IRoomRepository;
use App\Repositories\Criterias\Implement\Product\GetRoomWhereLikeByTitleCriteria;
use App\Repositories\Criterias\Implement\Room\GetAllRoomCriteria;
use App\Repositories\Criterias\Implement\Room\GetRoomWhereEqualByAgentIdCriteria;
use App\Repositories\Criterias\Implement\Room\GetRoomWhereEqualByProductIdCriteria;
use App\Services\Contract\IRoomService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class RoomService extends BaseService implements IRoomService
{
    private $_roomRepository;

    private $_roomAllotmentRepository;

    private $_productRepository;

    /**
     * RoomService constructor.
     * @param IRoomRepository $roomRepository
     * @param IRoomAllotmentRepository $roomAllotmentRepository
     */
    public function __construct(IRoomRepository $roomRepository, IRoomAllotmentRepository $roomAllotmentRepository)
    {
        $this->_roomRepository = $roomRepository;

        $this->_roomAllotmentRepository = $roomAllotmentRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_roomRepository->pushCriteria(new GetAllRoomCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetRoomWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->pushCriteria(new GetRoomWhereEqualByProductIdCriteria($pageRequest->getCustom()->product_id))
            ->with(['product'])
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'product_id' => (int)$model->product_id,
                'product' => $model->product,
                'title' => $model->title,
                'adult' => $model->adult,
                'children' => $model->children,
                'price' => (float)$model->price
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_roomRepository->skipCriteria()
            ->with(['product', 'bed', 'facility', 'room_image'])
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'product_id' => (int)$model->product_id,
            'product' => $model->product,
            'title' => $model->title,
            'description' => $model->description,
            'adult' => $model->adult,
            'children' => $model->children,
            'max' => $model->max,
            'footage' => $model->footage,
            'price' => (float)$model->price,
            'option' => $model->option,
            'policy' => $model->policy,
            'bed' => $model->bed,
            'image' => $model->room_image,
            'room_facility' => $model->facility
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateRoomRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateRoomRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_roomRepository->create($request);
                $this->_baseResponse->addSuccessMessage("Room created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateRoomRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateRoomRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_roomRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Room updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_roomRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Room deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $agentId
     * @return GenericResponse
     */
    public function getRoomListByAgentId($agentId)
    {
        $order = [
            'column' => 'title',
            'dir' => 'asc'
        ];

        $models = $this->_roomRepository->skipCriteria()
            ->select(['rooms.*', 'products.agent_id AS agent_id'])
            ->join('products', 'rooms.product_id', '=', 'products.id')
            ->orderBy($order['column'], $order['dir'])
            ->fetchFindAllBy('agent_id', $agentId);

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'agent_id' => (int)$model->agent_id,
                'title' => $model->title
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $title
     * @param $agent_id
     * @param $product_id
     * @return GenericPageResponse
     */
    public function getRoomByTitle($title, $agent_id, $product_id)
    {
        $models = $this->_roomRepository->select(['rooms.*', 'products.agent_id AS agent_id'])
            ->join('products', 'rooms.product_id', '=', 'products.id')
            ->pushCriteria(new GetRoomWhereLikeByTitleCriteria($title))
            ->pushCriteria(new GetRoomWhereEqualByAgentIdCriteria($agent_id))
            ->pushCriteria(new GetRoomWhereEqualByProductIdCriteria($product_id))
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => $model->title
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param CheckRoomAvailableRequest $request
     * @return GenericResponse
     */
    public function checkRoomAvailability(CheckRoomAvailableRequest $request)
    {
        $models = DB::select('CALL spCheckRoomAvailability(' . $request->id . ', "' . $request->check_in . '", "' . $request->check_out . '")');

        $output = [];

        foreach($models as $model) {
            $inject = $this->_roomRepository->skipCriteria()
                ->with(['facility', 'bed', 'room_image'])
                ->fetchFind($model->id);

            $facility = $inject->facility;
            $bed = $inject->bed;
            $room_image = $inject->room_image;

            if ($request->discount != 0) {
                if ($model->room_discount != 0) {
                    $price_discount = $model->price - ((($request->discount + $model->room_discount) / 100) * $model->price);
                } else {
                    $price_discount = $model->price - (($request->discount / 100) * $model->price);
                }
            } else {
                if ($model->room_discount != 0) {
                    $price_discount = $model->price - (($model->room_discount / 100) * $model->price);
                } else {
                    $price_discount = $model->price;
                }
            }

            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => $model->title,
                'description' => $model->description,
                'adult' => (int)$model->adult,
                'child' => (int)$model->children,
                'max' => (int)$model->max,
                'footage' => (int)$model->footage,
                'price' => (float)$model->price,
                'price_convert' => currency($model->price, currency()->config('default'), Session::get('currency')),
                'price_discount' => (float)$price_discount,
                'price_discount_convert' => currency($price_discount, currency()->config('default'), Session::get('currency')),
                'room_discount' => (float)$model->room_discount,
                'option' => $model->option,
                'policy' => $model->policy,
                'long_stay' => $model->long_stay,
                'room_available_by_date' => $model->room_available_by_date,
                'room_available_by_quantity' => $model->room_available_by_quantity,
                'facility' => $facility,
                'bed' => $bed,
                'room_image' => $room_image
            ]);
        }

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;

    }
}