<?php
namespace App\Services\Implement;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductCategory\CreateProductCategoryRequest;
use App\Http\Requests\ProductCategory\UpdateProductCategoryRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IProductCategoryRepository;
use App\Repositories\Criterias\Implement\ProductCategory\GetAllProductCategoryCriteria;
use App\Repositories\Criterias\Implement\ProductCategory\GetProductCategoryWhereEqualByProductTypeIdCriteria;
use App\Services\Contract\IProductCategoryService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class ProductCategoryService extends BaseService implements IProductCategoryService
{
    private $_productCategoryRepository;

    /**
     * ProductCategoryService constructor.
     * @param IProductCategoryRepository $productCategoryRepository
     */
    public function __construct(IProductCategoryRepository $productCategoryRepository)
    {
        $this->_productCategoryRepository = $productCategoryRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_productCategoryRepository->pushCriteria(new GetAllProductCategoryCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetProductCategoryWhereEqualByProductTypeIdCriteria($pageRequest->getCustom()->product_type_id))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'type' => $model->type,
                'category' => $model->category,
                'description' => $model->description
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_productCategoryRepository->skipCriteria()
            >fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'product_type_id' => (int)$model->product_type_id,
            'category' => $model->category,
            'description' => $model->description
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateProductCategoryRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateProductCategoryRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_productCategoryRepository->create($request);
                $this->_baseResponse->addSuccessMessage("Product category created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateProductCategoryRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateProductCategoryRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_productCategoryRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Product category updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_productCategoryRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Product category deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $product_type_id
     * @return GenericResponse
     */
    public function getProductCategoryList($product_type_id)
    {
        $order = [
            'column' => 'category',
            'dir' => 'asc'
        ];

        $models = $this->_productCategoryRepository->skipCriteria()
            ->orderBy($order['column'], $order['dir'])
            ->fetchFindAllBy('product_type_id', $product_type_id);

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'category' => $model->category,
                'description' => $model->description
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProductCategoryByProductTypeId($id)
    {
        $order = [
            'column' => 'category',
            'dir' => 'asc'
        ];

        $models = $this->_productCategoryRepository->skipCriteria()
            ->orderBy($order['column'], $order['dir'])
            ->fetchFindAllBy('product_type_id', $id);

        return $models;
    }
}