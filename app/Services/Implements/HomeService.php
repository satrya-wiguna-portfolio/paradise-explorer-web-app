<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Http\Responses\BaseResponse;
use App\Services\Contract\IHomeService;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;

class HomeService extends BaseService implements IHomeService
{
    /**
     * @return BaseResponse
     */
    public function testSendEmail()
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $request = [
                'email' => 'satrya@freshcms.net'
            ];

            $this->sendEmail('emails.test', $request);
            $this->_baseResponse->addSuccessMessage('Email sent');

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param $template
     * @param $request
     */
    private function sendEmail($template, $request)
    {
        $data = [
            'to' => $request['email'],
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Replay',
            'subject' => 'Activation',
            'model' => [
                'email' => $request['email']
            ]
        ];

        Event::fire(new SendEmailEvent($template, $data));
    }
}