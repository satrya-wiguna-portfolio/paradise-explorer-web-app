<?php
namespace App\Services\Implement;


use App\Http\Requests\AgentType\CreateAgentTypeRequest;
use App\Http\Requests\AgentType\UpdateAgentTypeRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IAgentTypeRepository;
use App\Repositories\Criterias\Implement\AgentType\GetAllAgentTypeCriteria;
use App\Services\Contract\IAgentTypeService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class AgentTypeService extends BaseService implements IAgentTypeService
{
    private $_agentTypeRepository;

    /**
     * AgentTypeService constructor.
     * @param IAgentTypeRepository $agentTypeRepository
     */
    public function __construct(IAgentTypeRepository $agentTypeRepository)
    {
        $this->_agentTypeRepository = $agentTypeRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_agentTypeRepository->pushCriteria(new GetAllAgentTypeCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name,
                'description' => $model->description
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_agentTypeRepository->skipCriteria()
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'name' => $model->name,
            'description' => $model->description
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateAgentTypeRequest $request
     * @return BaseResponse
     */
    public function save(CreateAgentTypeRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByName = $this->_agentTypeRepository->skipCriteria()->fetchFindSearch([
                    ['name', 'REGEXP', '[[:<:]]' . $request->getName() . '[[:>:]]']
                ]);

                if ($modelByName->count() == 0) {
                    $this->_baseResponse->_result = $this->_agentTypeRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Agent type created");

                } else {
                    $this->_baseResponse->addErrorMessage('Agent type is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateAgentTypeRequest $request
     * @return BaseResponse
     */
    public function update(UpdateAgentTypeRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByIdAndName = $this->_agentTypeRepository->skipCriteria()->fetchFindSearch([
                    ['id', '<>', $request->getId()],
                    ['name', 'REGEXP', '[[:<:]]' . $request->getName() . '[[:>:]]']
                ]);

                if ($modelByIdAndName->count() == 0) {
                    $this->_baseResponse->_result = $this->_agentTypeRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Agent type updated");

                } else {
                    $this->_baseResponse->addErrorMessage('Agent type is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_agentTypeRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Success agent type deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @return GenericResponse
     */
    public function getAgentTypeList()
    {
        $order = [
            'column' => 'name',
            'dir' => 'asc'
        ];

        $models = $this->_agentTypeRepository->skipCriteria()
            ->orderBy($order['column'], $order['dir'])
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'name' => $model->name,
                'description' => $model->description
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }
}