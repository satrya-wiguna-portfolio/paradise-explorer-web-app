<?php
namespace App\Services\Implement;


use App\Http\Requests\ReservationDokuResponse\CreateReservationDokuResponseRequest;
use App\Http\Responses\BaseResponse;
use App\Repositories\Contract\IReservationDokuResponseRepository;
use App\Services\Contract\IReservationDokuResponseService;
use Exception;

class ReservationDokuResponseService extends BaseService implements IReservationDokuResponseService
{
    private $_reservationDokuResponseRepository;

    /**
     * RegencyService constructor.
     * @param IReservationDokuResponseRepository $reservationDokuResponseRepository
     */
    public function __construct(IReservationDokuResponseRepository $reservationDokuResponseRepository)
    {
        $this->_reservationDokuResponseRepository = $reservationDokuResponseRepository;
    }

    /**
     * @param CreateReservationDokuResponseRequest $request
     * @return BaseResponse
     */
    public function save(CreateReservationDokuResponseRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $reservation_id = $this->_reservationDokuResponseRepository->create($request);

            $this->_baseResponse->addSuccessMessage('Reservation doku response added');
            $this->_baseResponse->_result = $reservation_id;

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }
}