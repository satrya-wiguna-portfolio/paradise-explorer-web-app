<?php
namespace App\Services\Implement;


use App\Events\SendEmailEvent;
use App\Http\Requests\Reservation\CreateReservationRequest;
use App\Http\Requests\Reservation\UpdateReservationRequest;
use App\Http\Responses\BaseResponse;
use App\Repositories\Contract\IReservationProductRepository;
use App\Repositories\Contract\IReservationProductRoomRepository;
use App\Repositories\Contract\IReservationRepository;
use App\Repositories\Contract\IUserRepository;
use App\Services\Contract\IReservationService;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;

class ReservationService extends BaseService implements IReservationService
{
    private $_reservationRepository;

    private $_reservationProductRepository;

    private $_reservationProductRoomRepository;

    private $_userRepository;

    /**
     * RegencyService constructor.
     * @param IReservationRepository $reservationRepository
     * @param IReservationProductRepository $reservationProductRepository
     * @param IReservationProductRoomRepository $reservationProductRoomRepository
     * @param IUserRepository $userRepository
     */
    public function __construct(IReservationRepository $reservationRepository, IReservationProductRepository $reservationProductRepository, IReservationProductRoomRepository $reservationProductRoomRepository, IUserRepository $userRepository)
    {
        $this->_reservationRepository = $reservationRepository;

        $this->_reservationProductRepository = $reservationProductRepository;

        $this->_reservationProductRoomRepository = $reservationProductRoomRepository;

        $this->_userRepository = $userRepository;
    }

    /**
     * @param CreateReservationRequest $request
     * @return BaseResponse
     */
    public function save(CreateReservationRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $reservation_id = $this->_reservationRepository->create($request);

            $this->_baseResponse->addSuccessMessage('Reservation added');

            if (isset($request->products)) {
                if ($request->products->count() > 0) {
                    foreach ($request->products as $product) {
                        $product->reservation_id = $reservation_id;

                        $reservation_product_id = $this->_reservationProductRepository->create($product);

                        $this->_baseResponse->addSuccessMessage('Reservation product added');

                        if (isset($product->rooms)) {
                            if ($product->rooms->count() > 0) {
                                foreach ($product->rooms as $room) {
                                    $room->reservation_product_id = $reservation_product_id;

                                    $this->_reservationProductRoomRepository->create($room);

                                    $this->_baseResponse->addSuccessMessage('Reservation product room added');
                                }
                            }
                        }
                    }
                }
            }

            $this->_baseResponse->_result = $reservation_id;

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReservationDetail($id)
    {
        $model = $this->_reservationRepository->skipCriteria()->fetchFind($id);

        return $model;
    }

    /**
     * @param UpdateReservationRequest $request
     * @return BaseResponse
     */
    public function updateStatus(UpdateReservationRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_reservationRepository->updateStatus($request);

            $this->_baseResponse->addSuccessMessage('Reservation status updated');

            if ($request->getStatus() == 1) {
                if ($request->getPaymentId() == 3) {
                    // Get reservation detail
                    $reservationModel = $this->_reservationRepository->skipCriteria()
                        ->with(['user', 'country', 'state', 'payment', 'currency', 'reservation_product' => function ($query) {
                            $query->with(['reservation_product_room', 'product_type']);
                        }])->fetchFind($request->getId());

                    $this->sendEmail('emails.billing', $reservationModel);

                }

            } else {
                // Get reservation detail
                $reservationModel = $this->_reservationRepository->skipCriteria()
                    ->with(['user', 'country', 'state', 'payment', 'currency', 'reservation_product' => function ($query) {
                        $query->with(['reservation_product_room', 'product_type']);
                    }, 'reservation_paypal_response', 'reservation_doku_response'])->fetchFind($request->getId());

                $this->sendEmail('emails.invoice', $reservationModel);

            }

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param $template
     * @param $request
     */
    private function sendEmail($template, $request)
    {
        $data = [
            'to' => $request->user->email,
            'from_address' => Config::get('mail.username'),
            'from_name' => 'No Reply',
            'subject' => 'ID #' . $request->invoice_number,
            'model' => [
                'invoice_number' => $request->invoice_number,
                'invoice_paid_date' => $request->invoice_paid_date,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'gender' => $request->gender,
                'address' => $request->address,
                'country' => $request->country,
                'state' => $request->state,
                'city' => $request->city,
                'zip' => $request->zip,
                'payment' => $request->payment,
                'currency' => $request->currency,
                'reservation_product' => $request->reservation_product
            ]
        ];

        Event::fire(new SendEmailEvent($template, $data));
    }
}