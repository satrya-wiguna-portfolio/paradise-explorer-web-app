<?php
namespace App\Services\Implement;


use App\Helper\PostHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IProductRepository;
use App\Repositories\Contract\IReservationProductRepository;
use App\Repositories\Contract\IRoomAllotmentRepository;
use App\Repositories\Criterias\Implement\Product\GetAllProductCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereEqualByAgentIdCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereEqualByProductCategoryIdCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereEqualByProductTypeIdCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereEqualByStatusCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereInByIdCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereLikeByTitleCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductByLimitCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereNotEqualByProductTypeIdCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductByRandomCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductByHaverseinFormulaCriteria;
use App\Repositories\Criterias\Implement\Product\GetProductWhereGreaterLowerOrBetweenByAgeCriteria;
use App\Repositories\Criterias\Implement\ReservationProduct\GetReservationProductGroubByProductIdCriteria;
use App\Repositories\Criterias\Implement\ReservationProduct\GetReservationProductWhereEqualByProductTypeIdCriteria;
use App\Repositories\Criterias\Implement\ReservationProduct\GetReservationProductWhereEqualByStatusCriteria;
use App\Services\Contract\IProductService;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class ProductService extends BaseService implements IProductService
{
    private $_productRepository;

    private $_roomAllotmentRepository;

    private $_reservationProductRepository;

    /**
     * ProductService constructor.
     * @param IProductRepository $productRepository
     * @param IRoomAllotmentRepository $roomAllotmentRepository
     * @param IReservationProductRepository $reservationProductRepository
     */
    public function __construct(IProductRepository $productRepository, IRoomAllotmentRepository $roomAllotmentRepository, IReservationProductRepository $reservationProductRepository)
    {
        $this->_productRepository = $productRepository;

        $this->_roomAllotmentRepository = $roomAllotmentRepository;

        $this->_reservationProductRepository = $reservationProductRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_productRepository->pushCriteria(new GetAllProductCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetProductWhereEqualByProductTypeIdCriteria($pageRequest->getCustom()->product_type_id))
            ->pushCriteria(new GetProductWhereEqualByProductCategoryIdCriteria($pageRequest->getCustom()->product_category_id))
            ->pushCriteria(new GetProductWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->with(['product_type', 'product_category', 'agent'])
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'product_type' => $model->product_type,
                'product_category' => $model->product_category,
                'agent' => $model->agent,
                'publish' => date('F, d Y', strtotime($model->publish)),
                'title' => $model->title,
                'status' => (int)$model->status
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_productRepository->skipCriteria()
            ->with(['product_tag', 'facility', 'room', 'product_image', 'product_video', 'product_way_point', 'product_include', 'product_itinerary'])
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'product_type_id' => (int)$model->product_type_id,
            'product_category_id' => ($model->product_category_id != 0) ? (int)$model->product_category_id : null,
            'property_type_id' => (int)$model->property_type_id,
            'agent_id' => (int)$model->agent_id,
            'publish' => $model->publish,
            'title' => $model->title,
            'slug' => $model->slug,
            'overview' => $model->overview,
            'description' => $model->description,
            'featured_image_url' => $model->featured_image_url,
            'featured_video_url' => $model->featured_video_url,
            'include_brochure_url' => $model->include_brochure_url,
            'itinerary_brochure_url' => $model->itinerary_brochure_url,
            'lat' => $model->lat,
            'lng' => $model->lng,
            'address' => $model->address,
            'duration' => (int)$model->duration,
            'min_age' => (int)$model->min_age,
            'max_age' => (int)$model->max_age,
            'price' => (float)$model->price,
            'grade' => (int)$model->grade,
            'discount' => (float)$model->discount,
            'status' => (int)$model->status,
            'product_tag' => $model->product_tag,
            'product_facility' => $model->facility,
            'room' => $model->room,
            'product_image' => $model->product_image,
            'product_video' => $model->product_video,
            'product_way_point' => $model->product_way_point,
            'product_include' => $model->product_include,
            'product_itinerary' => $model->product_itinerary
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateProductRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateProductRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByAgentIdAndSlug = $this->_productRepository->skipCriteria()->fetchFindSearch([
                    ['agent_id', '=', $request->getAgentId()],
                    ['slug', '=', $request->getSlug()]
                ]);

                if ($modelByAgentIdAndSlug->count() == 0) {
                    $this->_baseResponse->_result = $this->_productRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Product created");

                } else {
                    $this->_baseResponse->addErrorMessage('Product is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateProductRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateProductRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByIdAgentIdAndSlug = $this->_productRepository->skipCriteria()->fetchFindSearch([
                    ['id', '<>', $request->getId()],
                    ['agent_id', '=', $request->getAgentId()],
                    ['slug', '=', $request->getSlug()]
                ]);

                if ($modelByIdAgentIdAndSlug->count() == 0) {
                    $this->_baseResponse->_result = $this->_productRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Product updated");

                } else {
                    $this->_baseResponse->addErrorMessage('Product is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_productRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Product deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $title
     * @param $agent_id
     * @param $product_type_id
     * @return GenericPageResponse
     */
    public function getProductByTitle($title, $agent_id, $product_type_id)
    {
        $status = 1;

        $models = $this->_productRepository->pushCriteria(new GetProductWhereLikeByTitleCriteria($title))
            ->pushCriteria(new GetProductWhereEqualByAgentIdCriteria($agent_id))
            ->pushCriteria(new GetProductWhereEqualByProductTypeIdCriteria($product_type_id))
            ->pushCriteria(new GetProductWhereEqualByStatusCriteria($status))
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => $model->title
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $agent_id
     * @param $name
     * @return mixed
     */
    public function getSlug($agent_id, $name)
    {
        $model = $this->_productRepository->model();

        $slug = SlugService::createSlug(new $model, 'slug', $name, ['agent_id' => $agent_id]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->dto = compact('slug');

        return $this->_genericResponse;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getProduct(GenericPageRequest $pageRequest)
    {
        $models = $this->_productRepository->pushCriteria(new GetProductByHaverseinFormulaCriteria($pageRequest->getCustom()->lat, $pageRequest->getCustom()->lng))
            ->pushCriteria(new GetProductWhereEqualByProductTypeIdCriteria($pageRequest->getCustom()->product_type_id))
            ->pushCriteria(new GetProductWhereEqualByProductCategoryIdCriteria($pageRequest->getCustom()->product_category_id))
            ->pushCriteria(new GetProductWhereGreaterLowerOrBetweenByAgeCriteria($pageRequest->getCustom()->age))
            ->with(['product_type', 'product_category', 'agent', 'facility', 'product_comment']);

//        If product type is tour
        if ($pageRequest->getCustom()->product_type_id == 1) {
            $models = $models->with(['product_discount' => function($q) {
                return $q->whereDate("open_date", "<=", date("Y-m-d"))
                    ->whereDate("close_date", ">=", date("Y-m-d"));
            }]);
        }

//        If product type is hotel
        if ($pageRequest->getCustom()->product_type_id == 2) {
            $models = $models->with(['room' => function ($q) {
                return $q->with(['room_discount' => function($r) {
                    return $r->whereDate("open_date", "<=", date("Y-m-d"))
                        ->whereDate("close_date", ">=", date("Y-m-d"));
                }]);
            }]);
        }

        $models = $models->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir'])
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'product_type_id' => (int)$model->product_type_id,
                'product_type' => $model->product_type,
                'product_category_id' => (int)$model->product_category_id,
                'product_category' => $model->product_category,
                'property_type_id' => (int)$model->property_type_id,
                'property_type' => $model->property_type,
                'agent_id' => $model->agent_id,
                'agent' => $model->agent,
                'facility' => $model->facility,
                'title' => $model->title,
                'overview' => $model->overview,
                'description' => $model->description,
                'featured_image_url' => $model->featured_image_url,
                'featured_video_url' => $model->featured_video_url,
                'lat' => $model->lat,
                'lng' => $model->lng,
                'address' => $model->address,
                'duration' => $model->duration,
                'min_age' => $model->min_age,
                'max_age' => $model->max_age,
                'price' => (float)$model->price,
                'grade' => (int)$model->grade,
                'discount' => (float)$model->discount,
                'product_discount' => $model->product_discount,
                'room' => $model->room,
                'product_comment' => $model->product_comment
            ]);

        return Collection::make($output);
    }

    /**
     * @param $id
     * @return static
     */
    public function getProductDetail($id)
    {
        $model = $this->_productRepository->skipCriteria()
            ->with(['product_tag', 'facility', 'room', 'product_image', 'product_video', 'product_way_point', 'product_include', 'product_itinerary', 'product_comment'])
            ->fetchFind($id);

        return $model;
    }

    /**
     * @param $product_type_id
     * @return mixed
     */
    public function getRandomLimitProductByProductTypeId($product_type_id)
    {
        $limit = 10;

        $model = $this->_productRepository->pushCriteria(new GetProductWhereNotEqualByProductTypeIdCriteria($product_type_id))
            ->pushCriteria(new GetProductByRandomCriteria())
            ->pushCriteria(new GetProductByLimitCriteria($limit))
            ->with(['product_type', 'product_category', 'agent', 'facility']);

            if ($product_type_id == 1) {
                $model = $model->with(['product_discount' => function($q) {
                    return $q->whereDate("open_date", "<=", date("Y-m-d"))
                        ->whereDate("close_date", ">=", date("Y-m-d"));
                }]);
            }

            if ($product_type_id == 2) {
                $model = $model->with(['room' => function ($q) {
                    return $q->with(['room_discount' => function($r) {
                        return $r->whereDate("open_date", "<=", date("Y-m-d"))
                            ->whereDate("close_date", ">=", date("Y-m-d"));
                    }]);
                }]);
            }

            $model = $model->fetchAll();

        $output = [];

        foreach($model as $item)
            $output[] = new Laravel5DTO([
                'id' => (int)$item->id,
                'product_type_id' => (int)$item->product_type_id,
                'product_type' => $item->product_type,
                'product_category_id' => (int)$item->product_category_id,
                'product_category' => $item->product_category,
                'property_type_id' => (int)$item->property_type_id,
                'property_type' => $item->property_type,
                'agent_id' => $item->agent_id,
                'agent' => $item->agent,
                'facility' => $item->facility,
                'title' => $item->title,
                'overview' => $item->overview,
                'description' => $item->description,
                'featured_image_url' => $item->featured_image_url,
                'featured_video_url' => $item->featured_video_url,
                'lat' => $item->lat,
                'lng' => $item->lng,
                'address' => $item->address,
                'duration' => $item->duration,
                'min_age' => $item->min_age,
                'max_age' => $item->max_age,
                'price' => (float)$item->price,
                'grade' => (int)$item->grade,
                'discount' => (float)$item->discount,
                'product_discount' => $item->product_discount,
                'room' => $item->room
            ]);

        return Collection::make($output);
    }

    /**
     * @return mixed
     */
    public function getMostPopularTour()
    {
        $productTypeId = 1;
        $status = 2;

        $reservationProductModels = $this->_reservationProductRepository->pushCriteria(new GetReservationProductGroubByProductIdCriteria())
            ->pushCriteria(new GetReservationProductWhereEqualByStatusCriteria($status))
            ->pushCriteria(new GetReservationProductWhereEqualByProductTypeIdCriteria($productTypeId))
            ->fetchAll();

        $productModels = $this->_productRepository->pushCriteria(new GetProductWhereInByIdCriteria($reservationProductModels))
            ->with(['product_type', 'product_category', 'agent', 'facility', 'product_discount' => function($q) {
                return $q->whereDate("open_date", "<=", date("Y-m-d"))
                    ->whereDate("close_date", ">=", date("Y-m-d"));
            }])
            ->fetchAll();

        $output = [];

        foreach($productModels as $productModel) {
            $price_before_discount = null;
            $price_after_discount = null;
            $discount_total = 0;

            if ($productModel->discount != 0) {
                if (count($productModel->product_discount) != 0) {
                    $price_before_discount = currency($productModel->price, currency()->config('default'), Session::get('currency'));
                    $price_after_discount = currency($productModel->price - (((PostHelper::minProductDiscount($productModel->product_discount) + $productModel->discount) / 100) * $productModel->price), currency()->config('default'), Session::get('currency'));
                    $discount_total = PostHelper::minProductDiscount($productModel->product_discount) + $productModel->discount;
                } else {
                    $price_before_discount = currency($productModel->price, currency()->config('default'), Session::get('currency'));
                    $price_after_discount = currency($productModel->price - (($productModel->discount / 100) * $productModel->price), currency()->config('default'), Session::get('currency'));
                    $discount_total = $productModel->discount;
                }
            } else {
                if (count($productModel->product_discount) != 0) {
                    $price_before_discount = currency($productModel->price, currency()->config('default'), Session::get('currency'));
                    $price_after_discount = currency($productModel->price - ((PostHelper::minProductDiscount($productModel->product_discount) / 100) * $productModel->price), currency()->config('default'), Session::get('currency'));
                    $discount_total = PostHelper::minProductDiscount($productModel->product_discount);
                } else {
                    $price_after_discount = currency($productModel->price, currency()->config('default'), Session::get('currency'));
                }
            }

            $output[] = new Laravel5DTO([
                'id' => (int)$productModel->id,
                'product_type_id' => (int)$productModel->product_type_id,
                'product_type' => $productModel->product_type,
                'product_category_id' => (int)$productModel->product_category_id,
                'product_category' => $productModel->product_category,
                'agent_id' => $productModel->agent_id,
                'agent' => $productModel->agent,
                'facility' => $productModel->facility,
                'title' => $productModel->title,
                'featured_image_url' => $productModel->featured_image_url,
                'price' => (float)$productModel->price,
                'grade' => (int)$productModel->grade,
                'discount' => (float)$productModel->discount,
                'product_discount' => PostHelper::minProductDiscount($productModel->product_discount),

//                Additional
                'price_before_discount' => $price_before_discount,
                'price_after_discount' => $price_after_discount,
                'discount_total' => (float)$discount_total
            ]);
        }

        return Collection::make($output);
    }

    /**
     * @return mixed
     */
    public function getMostPopularHotel()
    {
        $productTypeId = 2;
        $status = 2;

        $reservationProductModels = $this->_reservationProductRepository->pushCriteria(new GetReservationProductGroubByProductIdCriteria())
            ->pushCriteria(new GetReservationProductWhereEqualByStatusCriteria($status))
            ->pushCriteria(new GetReservationProductWhereEqualByProductTypeIdCriteria($productTypeId))
            ->fetchAll();

        $productModels = $this->_productRepository->pushCriteria(new GetProductWhereInByIdCriteria($reservationProductModels))
            ->with(['product_type', 'product_category', 'property_type', 'agent', 'facility', 'room' => function($q) {
                return $q->with(['room_discount' => function($r) {
                    return $r->whereDate("open_date", "<=", date("Y-m-d"))
                        ->whereDate("close_date", ">=", date("Y-m-d"));
                }]);
            }])
            ->fetchAll();

        $output = [];

        foreach($productModels as $productModel) {
            $price_before_discount = null;
            $price_after_discount = null;
            $discount_total = 0;

            if ($productModel->discount != 0) {
                if (count($productModel->product_discount) != 0) {
                    $price_before_discount = currency(PostHelper::maxRoomPrice($productModel->room), currency()->config('default'), Session::get('currency'));
                    $price_after_discount = currency(PostHelper::maxRoomPrice($productModel->room) - (((PostHelper::minRoomDiscount($productModel->room) + $productModel->discount) / 100) * PostHelper::maxRoomPrice($productModel->room)), currency()->config('default'), Session::get('currency'));
                    $discount_total = PostHelper::minRoomDiscount($productModel->room) + $productModel->discount;
                } else {
                    $price_before_discount = currency(PostHelper::maxRoomPrice($productModel->room), currency()->config('default'), Session::get('currency'));
                    $price_after_discount = currency(PostHelper::maxRoomPrice($productModel->room) - (($productModel->discount / 100) * PostHelper::maxRoomPrice($productModel->room)), currency()->config('default'), Session::get('currency'));
                    $discount_total = $productModel->discount;
                }
            } else {
                if (count($productModel->product_discount) != 0) {
                    $price_before_discount = currency(PostHelper::maxRoomPrice($productModel->room), currency()->config('default'), Session::get('currency'));
                    $price_after_discount = currency(PostHelper::maxRoomPrice($productModel->room) - ((PostHelper::minRoomDiscount($productModel->room) / 100) * PostHelper::maxRoomPrice($productModel->room)), currency()->config('default'), Session::get('currency'));
                    $discount_total = PostHelper::minRoomDiscount($productModel->room);
                } else {
                    $price_after_discount = currency(PostHelper::maxRoomPrice($productModel->room), currency()->config('default'), Session::get('currency'));
                }
            }

            $output[] = new Laravel5DTO([
                'id' => (int)$productModel->id,
                'product_type_id' => (int)$productModel->product_type_id,
                'product_type' => $productModel->product_type,
                'product_category_id' => (int)$productModel->product_category_id,
                'product_category' => $productModel->product_category,
                'property_type_id' => (int)$productModel->property_type_id,
                'property_type' => $productModel->property_type,
                'agent_id' => $productModel->agent_id,
                'agent' => $productModel->agent,
                'facility' => $productModel->facility,
                'title' => $productModel->title,
                'featured_image_url' => $productModel->featured_image_url,
                'price' => (float)$productModel->price,
                'grade' => (int)$productModel->grade,
                'discount' => (float)$productModel->discount,
                'room' => $productModel->room,

//                Additional
                'room_discount' => PostHelper::minRoomDiscount($productModel->room),
                'price_before_discount' => $price_before_discount,
                'price_after_discount' => $price_after_discount,
                'discount_total' => $discount_total
            ]);
        }

        return Collection::make($output);
    }
}