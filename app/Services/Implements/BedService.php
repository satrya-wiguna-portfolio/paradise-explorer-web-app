<?php
namespace App\Services\Implement;


use App\Http\Requests\Bed\CreateBedRequest;
use App\Http\Requests\Bed\UpdateBedRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IBedRepository;
use App\Repositories\Criterias\Implement\Bed\GetAllBedCriteria;
use App\Repositories\Criterias\Implement\Bed\GetBedWhereEqualByAgentIdCriteria;
use App\Services\Contract\IBedService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class BedService extends BaseService implements IBedService
{
    private $_bedRepository;

    /**
     * BedService constructor.
     * @param IBedRepository $bedRepository
     */
    public function __construct(IBedRepository $bedRepository)
    {
        $this->_bedRepository = $bedRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_bedRepository->pushCriteria(new GetAllBedCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetBedWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->with(['agent'])
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'agent' => $model->agent,
                'name' => $model->name,
                'description' => $model->description
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_bedRepository->skipCriteria()
            ->with(['agent'])
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'agent_id' => (int)$model->agent_id,
            'name' => $model->name,
            'description' => $model->description
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateBedRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateBedRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_bedRepository->create($request);
                $this->_baseResponse->addSuccessMessage("Bed created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateBedRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateBedRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_bedRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Bed updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_bedRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Bed deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $agentId
     * @return GenericResponse
     */
    public function getBedListByAgentId($agentId)
    {
        $order = [
            'column' => 'agent_id',
            'dir' => 'asc'
        ];

        $models = $this->_bedRepository->skipCriteria()
            ->orderBy($order['column'], $order['dir'])
            ->fetchFindAllBy('agent_id', $agentId);

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'agent_id' => (int)$model->agent_id,
                'name' => $model->name
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }
}