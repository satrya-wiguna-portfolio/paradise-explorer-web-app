<?php
namespace App\Services\Implement;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\RoomAllotment\CreateRoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\UpdateRoomAllotmentRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IRoomAllotmentRepository;
use App\Repositories\Criterias\Implement\RoomAllotment\GetAllRoomAllotmentCriteria;
use App\Repositories\Criterias\Implement\RoomAllotment\GetDetailRoomAllotmentCriteria;
use App\Repositories\Criterias\Implement\RoomAllotment\GetRoomAllotmentWhereBetweenByFromAndToCriteria;
use App\Repositories\Criterias\Implement\RoomAllotment\GetRoomAllotmentWhereEqualByAgentIdCriteria;
use App\Repositories\Criterias\Implement\RoomAllotment\GetRoomAllotmentWhereEqualByDate;
use App\Repositories\Criterias\Implement\RoomAllotment\GetRoomAllotmentWhereEqualByProductIdCriteria;
use App\Repositories\Criterias\Implement\RoomAllotment\GetRoomAllotmentWhereEqualByRoomIdCriteria;
use App\Repositories\Criterias\Implement\RoomAllotment\GetRoomAllotmentWhereNotEqualByIdCriteria;
use App\Services\Contract\IRoomAllotmentService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class RoomAllotmentService extends BaseService implements IRoomAllotmentService
{
    private $_roomAllotmentRepository;

    /**
     * roomAllotmentService constructor.
     * @param IRoomAllotmentRepository $roomAllotmentRepository
     */
    public function __construct(IRoomAllotmentRepository $roomAllotmentRepository)
    {
        $this->_roomAllotmentRepository = $roomAllotmentRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_roomAllotmentRepository->pushCriteria(new GetAllRoomAllotmentCriteria())
            ->pushCriteria(new GetRoomAllotmentWhereBetweenByFromAndToCriteria($pageRequest->getCustom()->from, $pageRequest->getCustom()->to))
            ->pushCriteria(new GetRoomAllotmentWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->pushCriteria(new GetRoomAllotmentWhereEqualByProductIdCriteria($pageRequest->getCustom()->product_id))
            ->pushCriteria(new GetRoomAllotmentWhereEqualByRoomIdCriteria($pageRequest->getCustom()->room_id))
            ->orderBy('room_id')
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'room_id' => (int)$model->room_id,
                'room' => $model->room,
                'date' => date('F, d Y', strtotime($model->date)),
                'allotment' => (int)$model->allotment,
                'lock' => (int)$model->lock,
                'sold' => (int)$model->sold,
                'balance' => (int)$model->balance,
                'price' => (float)$model->price
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_roomAllotmentRepository->pushCriteria(new GetDetailRoomAllotmentCriteria())->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'agent_id' => (int)$model->agent_id,
            'product_id' => (int)$model->product_id,
            'room_id' => (int)$model->room_id,
            'date' => $model->date,
            'allotment' => (int)$model->allotment,
            'lock' => (int)$model->lock,
            'sold' => (int)$model->sold,
            'balance' => (int)$model->balance,
            'price' => (float)$model->price,
            'status' => (int)$model->status
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateRoomAllotmentRequest $request
     * @return BaseResponse
     */
    public function save(CreateRoomAllotmentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                if ($request->to) {
                    $reduction = ((abs(strtotime($request->to) - strtotime($request->from))) / (60 * 60 * 24));

                    for ($i = 0; $i <= $reduction; $i++) {
                        $date = date('Y-m-d', strtotime('+'. $i .' days', strtotime($request->from)));

                        $row = $this->_roomAllotmentRepository->pushCriteria(new GetRoomAllotmentWhereEqualByRoomIdCriteria($request->room_id))
                            ->pushCriteria(new GetRoomAllotmentWhereEqualByDate($date))
                            ->fetchAll()->count();

                        if ($row < 1) {
                            $request->date = $date;

                            $this->_roomAllotmentRepository->create($request);

                        } else {
                            $this->_baseResponse->addErrorMessage('Room allotment has already taken');

                        }
                    }

                    $this->_baseResponse->addSuccessMessage("Room allotment created");

                } else {
                    $date = date('Y-m-d', strtotime($request->from));

                    $row = $this->_roomAllotmentRepository->pushCriteria(new GetRoomAllotmentWhereEqualByRoomIdCriteria($request->room_id))
                        ->pushCriteria(new GetRoomAllotmentWhereEqualByDate($date))
                        ->fetchAll()->count();

                    if ($row < 1) {
                        $request->date = $date;

                        $this->_baseResponse->_result = $this->_roomAllotmentRepository->create($request);
                        $this->_baseResponse->addSuccessMessage("Room allotment created");

                    } else {
                        $this->_baseResponse->addErrorMessage('Room allotment has already taken');

                    }
                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateRoomAllotmentRequest $request
     * @return BaseResponse
     */
    public function update(UpdateRoomAllotmentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $row = $this->_roomAllotmentRepository->pushCriteria(new GetRoomAllotmentWhereEqualByRoomIdCriteria($request->room_id))
                    ->pushCriteria(new GetRoomAllotmentWhereEqualByDate(date('Y-m-d', strtotime($request->date))))
                    ->pushCriteria(new GetRoomAllotmentWhereNotEqualByIdCriteria($request->id))
                    ->fetchAll()->count();

                if ($row < 1) {
                    if ($request->allotment >= ($request->lock + $request->sold)) {
                        $request->balance = $request->allotment - ($request->lock + $request->sold);

                        $this->_baseResponse->_result = $this->_roomAllotmentRepository->update($request);
                        $this->_baseResponse->addSuccessMessage("Room allotment updated");

                    } else {
                        $this->_baseResponse->addErrorMessage('Room allotment is too lowest');

                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Room allotment has already taken');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param RoomAllotmentRequest $request
     * @return BaseResponse
     */
    public function roomLock(RoomAllotmentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            DB::statement(DB::raw('CALL spRoomLock("' . $request->from . '",
            "' . $request->to . '",
            "' . $request->client_id . '",
            "' . $request->cart_item_id . '",
            ' . $request->room_id . ',
            ' . $request->lock . ');'));

            $this->_baseResponse->addSuccessMessage("Room allotment locked");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param RoomAllotmentRequest $request
     * @return BaseResponse
     */
    public function roomUnlock(RoomAllotmentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            DB::statement(DB::raw('CALL spRoomUnlock("' . $request->client_id . '",
            "' . $request->cart_item_id . '");'));

            $this->_baseResponse->addSuccessMessage("Room allotment unlocked");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_roomAllotmentRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Room allotment deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getByRoomId($id)
    {
        $models = $this->_roomAllotmentRepository->pushCriteria(new GetRoomAllotmentWhereEqualByRoomIdCriteria($id))
            ->with(['room'])
            ->fetchAll();

        $output = [];

        foreach($models as $model) {
            $title = ($model->balance > 1) ? 'Available: ' . $model->balance . ' Rooms' : 'Available: ' . $model->balance . ' Room';
            $title .= ($model->allotment > 1) ? ' of Total: ' . $model->allotment . ' Rooms' : ' of Total: ' . $model->allotment . ' Room';

            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => $title,
                'start' => date('Y-m-d', strtotime($model->date))
            ]);
        }

        return Collection::make($output);
    }

    /**
     * @param $id
     * @return static
     */
    public function getLockByRoomId($id)
    {
        $models = $this->_roomAllotmentRepository->pushCriteria(new GetRoomAllotmentWhereEqualByRoomIdCriteria($id))
            ->with(['room'])
            ->fetchAll();

        $output = [];

        foreach($models as $model) {
            if ($model->lock > 0) {
                $title = ($model->lock > 1) ? 'Room locked: ' . $model->lock . ' Rooms)' : 'Room locked: ' . $model->lock . ' Room)';

                $output[] = new Laravel5DTO([
                    'id' => (int)$model->id,
                    'title' => $title,
                    'start' => date('Y-m-d', strtotime($model->date))
                ]);
            }
        }

        return Collection::make($output);
    }
}