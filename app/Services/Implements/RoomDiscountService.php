<?php
namespace App\Services\Implement;


use App\Helper\PostHelper;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\RoomDiscount\CreateRoomDiscountRequest;
use App\Http\Requests\RoomDiscount\UpdateRoomDiscountRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IRoomDiscountRepository;
use App\Repositories\Criterias\Implement\RoomDiscount\GetRoomDiscountWhereBetweenByOpenAndCloseCriteria;
use App\Repositories\Criterias\Implement\RoomDiscount\GetAllRoomDiscountCriteria;
use App\Repositories\Criterias\Implement\RoomDiscount\GetDetailRoomDiscountCriteria;
use App\Repositories\Criterias\Implement\RoomDiscount\GetRoomDiscountWhereEqualByAgentIdCriteria;
use App\Repositories\Criterias\Implement\RoomDiscount\GetRoomDiscountWhereEqualByProductIdCriteria;
use App\Repositories\Criterias\Implement\RoomDiscount\GetRoomDiscountWhereEqualByRoomIdCriteria;
use App\Repositories\Criterias\Implement\RoomDiscount\GetRoomDiscountWhereNotEqualByIdCriteria;
use App\Services\Contract\IRoomDiscountService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class RoomDiscountService extends BaseService implements IRoomDiscountService
{
    private $_roomDiscountRepository;

    /**
     * roomDiscountService constructor.
     * @param IRoomDiscountRepository $roomDiscountRepository
     */
    public function __construct(IRoomDiscountRepository $roomDiscountRepository)
    {
        $this->_roomDiscountRepository = $roomDiscountRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_roomDiscountRepository->pushCriteria(new GetAllRoomDiscountCriteria($pageRequest->getCustom()->open_date, $pageRequest->getCustom()->close_date))
            ->pushCriteria(new GetRoomDiscountWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->pushCriteria(new GetRoomDiscountWhereEqualByProductIdCriteria($pageRequest->getCustom()->product_id))
            ->pushCriteria(new GetRoomDiscountWhereEqualByRoomIdCriteria($pageRequest->getCustom()->room_id))
            ->orderBy('room_id')
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'room_id' => (int)$model->room_id,
                'room' => $model->room,
                'open_date' => date('F, d Y', strtotime($model->open_date)),
                'close_date' => date('F, d Y', strtotime($model->close_date)),
                'discount' => (float)$model->discount,
                'status' => (int)$model->status
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_roomDiscountRepository->pushCriteria(new GetDetailRoomDiscountCriteria())->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'agent_id' => (int)$model->agent_id,
            'product_id' => (int)$model->product_id,
            'room_id' => (int)$model->room_id,
            'open_date' => $model->open_date,
            'close_date' => $model->close_date,
            'discount' => (float)$model->discount,
            'status' => (int)$model->status
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateRoomDiscountRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function save(CreateRoomDiscountRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $row = $this->_roomDiscountRepository->pushCriteria(new GetRoomDiscountWhereEqualByRoomIdCriteria($request->room_id))
                    ->pushCriteria(new GetRoomDiscountWhereBetweenByOpenAndCloseCriteria($request->open_date, $request->close_date))
                    ->fetchAll()->count();

                if ($row < 1) {
                    $this->_baseResponse->_result = $this->_roomDiscountRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Room discount created");

                } else {
                    $this->_baseResponse->addErrorMessage('Room discount has already taken');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateRoomDiscountRequest $request
     * @return \App\Http\Responses\BaseResponse
     */
    public function update(UpdateRoomDiscountRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $row = $this->_roomDiscountRepository->pushCriteria(new GetRoomDiscountWhereEqualByRoomIdCriteria($request->room_id))
                    ->pushCriteria(new GetRoomDiscountWhereBetweenByOpenAndCloseCriteria($request->open_date, $request->close_date))
                    ->pushCriteria(new GetRoomDiscountWhereNotEqualByIdCriteria($request->id))
                    ->fetchAll()->count();

                if ($row < 1) {
                    $this->_baseResponse->_result = $this->_roomDiscountRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Room discount updated");

                } else {
                    $this->_baseResponse->addErrorMessage('Room discount has already taken');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return \App\Http\Responses\BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_roomDiscountRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Room discount deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getByRoomId($id)
    {
        $models = $this->_roomDiscountRepository->pushCriteria(new GetRoomDiscountWhereEqualByRoomIdCriteria($id))
            ->fetchAll();

        $output = [];

        foreach($models as $model) {
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'title' => 'Discount: ' . $model->discount . '%',
                'start' => date('Y-m-d', strtotime($model->open_date)),
                'end' => date('Y-m-d', strtotime($model->close_date))
            ]);
        }

        return Collection::make($output);
    }

}