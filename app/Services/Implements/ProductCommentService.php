<?php
namespace App\Services\Implement;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductComment\CreateProductCommentRequest;
use App\Http\Requests\ProductComment\UpdateProductCommentRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IProductCommentRepository;
use App\Repositories\Criterias\Implement\ProductComment\GetAllProductCommentCriteria;
use App\Repositories\Criterias\Implement\ProductComment\GetProductCommentWhereEqualByAgentIdCriteria;
use App\Repositories\Criterias\Implement\ProductComment\GetProductCommentWhereEqualByProductIdCriteria;
use App\Repositories\Criterias\Implement\ProductComment\GetProductCommentWhereEqualByStatusCriteria;
use App\Services\Contract\IProductCommentService;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class ProductCommentService extends BaseService implements IProductCommentService
{

    private $_productCommentRepository;

    /**
     * ProductCommentService constructor.
     * @param IProductCommentRepository $productCommentRepository
     */
    public function __construct(IProductCommentRepository $productCommentRepository)
    {
        $this->_productCommentRepository = $productCommentRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_productCommentRepository->select(['product_comments.*', 'products.title AS title'])
            ->join('products', 'products.id', '=', 'product_comments.product_id')
            ->join('users', 'users.id', '=', 'product_comments.user_id')
            ->pushCriteria(new GetAllProductCommentCriteria($pageRequest->getSearch()))
            ->pushCriteria(new GetProductCommentWhereEqualByAgentIdCriteria($pageRequest->getCustom()->agent_id))
            ->with(['user' => function($q) {
                return $q->with(['agent']);
            }])
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'agent' => $model->user->agent->name,
                'title' => $model->title,
                'comment' => $model->comment,
                'status' => (int)$model->status
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_productCommentRepository->select(['product_comments.*', 'products.title AS title'])
            ->join('products', 'products.id', '=', 'product_comments.product_id')
            ->skipCriteria()
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'title' => $model->title,
            'comment' => $model->comment,
            'rate' => $model->rate,
            'ip_address' => $model->ip_address,
            'browser' => $model->browser,
            'status' => (int)$model->status
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateProductCommentRequest $request
     * @return BaseResponse
     */
    public function save(CreateProductCommentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_productCommentRepository->create($request);
                $this->_baseResponse->addSuccessMessage("Product comment created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateProductCommentRequest $request
     * @return BaseResponse
     */
    public function update(UpdateProductCommentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_productCommentRepository->update($request);
                $this->_baseResponse->addSuccessMessage("Product comment status updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_productCommentRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Product comment deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getProductCommentByProductId($product_id)
    {
        $status = 1;
        $order = [
            'column' => 'created_at',
            'dir' => 'desc'
        ];

        $models = $this->_productCommentRepository->pushCriteria(new GetProductCommentWhereEqualByProductIdCriteria($product_id))
            ->pushCriteria(new GetProductCommentWhereEqualByStatusCriteria($status))
            ->with(['user' => function ($q) {
                return $q->with(['member', 'agent', 'staff']);
            }])
            ->orderBy($order['column'], $order['dir'])
            ->fetchAll($reset = false);

        return $models;
    }
}