<?php
namespace App\Services\Implement;


use App\Helper\DirectoryHelper;
use App\Http\Requests\Payment\CreatePaymentRequest;
use App\Http\Requests\Payment\UpdatePaymentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Libraries\Slim;
use App\Repositories\Contract\ICurrencyRepository;
use App\Repositories\Contract\IPaymentRepository;
use App\Repositories\Contract\IReservationRepository;
use App\Repositories\Criterias\Implement\Payment\GetAllPaymentCriteria;
use App\Repositories\Criterias\Implement\Payment\GetPaymentWhereEqualByStatusCriteria;
use App\Services\Contract\IPaymentService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;
use Netshell\Paypal\Facades\Paypal;

class PaymentService extends BaseService implements IPaymentService
{
    private $_paymentRepository;

    private $_reservationRepository;

    private $_currencyRepository;

    private $_apiContext;

    /**
     * PaymentService constructor.
     * @param IPaymentRepository $paymentRepository
     * @param IReservationRepository $reservationRepository
     * @param ICurrencyRepository $currencyRepository
     */
    public function __construct(IPaymentRepository $paymentRepository, IReservationRepository $reservationRepository, ICurrencyRepository $currencyRepository)
    {
        $this->_paymentRepository = $paymentRepository;

        $this->_reservationRepository = $reservationRepository;

        $this->_currencyRepository = $currencyRepository;

        $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret')
        );

        $this->_apiContext->setConfig([
            'mode' => config('services.paypal.mode'),
            'service.EndPoint' => config('services.paypal.end_point'),
            'http.ConnectionTimeOut' => config('services.paypal.time_out'),
            'log.LogEnabled' => config('services.paypal.log'),
            'log.FileName' => config('services.paypal.file'),
            'log.LogLevel' => config('services.paypal.level')
        ]);
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_paymentRepository->pushCriteria(new GetAllPaymentCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'payment' => $model->payment,
                'description' => $model->description,
                'image_url' => $model->image_url,
                'status' => $model->status
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_paymentRepository->skipCriteria()
            ->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'payment' => $model->payment,
            'description' => $model->description,
            'image_url' => $model->image_url,
            'status' => $model->status
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreatePaymentRequest $request
     * @return BaseResponse
     */
    public function save(CreatePaymentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByPayment = $this->_paymentRepository->skipCriteria()->fetchFindSearch([
                    ['payment', 'REGEXP', '[[:<:]]' . $request->getPayment() . '[[:>:]]']
                ]);

                if ($modelByPayment->count() == 0) {
                    $this->_baseResponse->_result = $this->_paymentRepository->create($request);
                    $this->_baseResponse->addSuccessMessage("Payment created");

                    //Upload the image
                    $images = Slim::getImagesByRequest([$request->getImageFile()]);

                    if ($images) {
                        foreach ($images as $image) {
                            if (isset($image['output']['data'])) {
                                $name = $image['output']['name'];
                                $data = $image['output']['data'];
                                Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::PAYMENT, false);
                            }
                        }
                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Payment is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdatePaymentRequest $request
     * @return BaseResponse
     */
    public function update(UpdatePaymentRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $modelByIdAndPayment = $this->_paymentRepository->skipCriteria()->fetchFindSearch([
                    ['id', '<>', $request->getId()],
                    ['payment', 'REGEXP', '[[:<:]]' . $request->getPayment() . '[[:>:]]']
                ]);

                if ($modelByIdAndPayment->count() == 0) {
                    $this->_baseResponse->_result = $this->_paymentRepository->update($request);
                    $this->_baseResponse->addSuccessMessage("Payment updated");

                    $images = Slim::getImagesByRequest([$request->getImageFile()]);

                    if ($images) {
                        foreach ($images as $image) {
                            if (isset($image['output']['data'])) {
                                $name = $image['output']['name'];
                                $data = $image['output']['data'];
                                Slim::saveFile($data, $name, public_path() . '/' . DirectoryHelper::PAYMENT, false);
                            }
                        }
                    }

                } else {
                    $this->_baseResponse->addErrorMessage('Payment is already exists');

                }

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_paymentRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("Payment deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @return GenericPageResponse
     */
    public function getPayment()
    {
        $status = 1;

        $models = $this->_paymentRepository->pushCriteria(new GetPaymentWhereEqualByStatusCriteria($status))
            ->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'payment' => $model->payment,
                'description' => $model->description,
                'image_url' => $model->image_url,
                'status' => (int)$model->status,
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $reservationId
     * @return GenericResponse
     */
    public function payByPaypal($reservationId)
    {
        $this->_baseResponse = new BaseResponse();

        $items = [];
        $total = 0;

        $modelReservation = $this->_reservationRepository->skipCriteria()
            ->with(['user', 'reservation_product' => function($q) {
                return $q->with(['reservation_product_room']);
            }])
            ->fetchFind($reservationId);

        // Force to pay with USD
        $currencyModel = $this->_currencyRepository->skipCriteria()
            ->fetchFind($modelReservation->currency_id);

        $forceCurrencyModel = $this->_currencyRepository->skipCriteria()
            ->fetchFind(2);

        $payer = PayPal::payer();
        $payer->setPaymentMethod('paypal');

        foreach ($modelReservation->reservation_product as $product) {
            if (Collection::make($product->reservation_product_room)->count() > 0) {
                foreach ($product->reservation_product_room as $room) {
                    if ($currencyModel->code != $forceCurrencyModel->code) {
                        $price = $room->price * $forceCurrencyModel->exchange_rate;
                        $subTotal = round((($room->price - ((($room->room_discount + $product->discount) / 100) * $room->price)) * $forceCurrencyModel->exchange_rate) * $room->quantity, 2);
                    } else {
                        $price = $room->price;
                        $subTotal = round(($room->price - ((($room->room_discount + $product->discount) / 100) * $room->price)) * $room->quantity, 2);
                    }

                    $item = PayPal::item();
                    $item->setName($product->title . '(' . $room->title . ')');

                    $totalDiscount = $product->discount + $room->room_discount;
                    $description = $totalDiscount . '% x ' . $price . ' ' . $forceCurrencyModel->code . ' x ' . $room->quantity . ' x ' . $product->quantity;;

                    $item->setDescription($description);
                    $item->setCurrency($forceCurrencyModel->code)
                        ->setQuantity($product->quantity)
                        ->setPrice($subTotal);

                    $total = $total + ($subTotal * $product->quantity);
                    array_push($items, $item);
                }
            } else {
                if ($currencyModel->code != $forceCurrencyModel->code) {
                    $price = $product->price * $forceCurrencyModel->exchange_rate;
                    $subTotal = round(($product->price - (($product->discount / 100) * $product->price)) * $forceCurrencyModel->exchange_rate, 2);
                } else {
                    $price = $product->price;
                    $subTotal = round(($product->price - (($product->discount / 100) * $product->price)), 2);
                }

                $item = PayPal::item();
                $item->setName($product->title);

                $totalDiscount = $product->discount;
                $description = $totalDiscount . '% x ' . $price . ' ' . $forceCurrencyModel->code . ' x ' . $product->quantity;

                $item->setDescription($description);
                $item->setCurrency($forceCurrencyModel->code)
                    ->setQuantity($product->quantity)
                    ->setPrice($subTotal);

                $total = $total + ($subTotal * $product->quantity);
                array_push($items, $item);
            }
        }

        $itemList = PayPal::itemList();
        $itemList->setItems($items);

        $amount = PayPal::amount();
        $amount->setCurrency($forceCurrencyModel->code)
            ->setTotal(round($total, 2));

        $transaction = PayPal::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Description')
            ->setInvoiceNumber($modelReservation->invoice_number);

        $redirectUrls = PayPal::redirectUrls();
        $redirectUrls->setReturnUrl(url('checkout/getPaypalSuccess/'. $reservationId))
            ->setCancelUrl(url('checkout/getPaypalCancel/'. $reservationId));

        $payment = PayPal::payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);
        $this->_baseResponse->_result = $response->links[1]->href;

        return $this->_baseResponse;
    }

    public function payByDoku($reservationId)
    {
        // TODO: Implement payWithDoku() method.
    }

    /**
     * @param $reservationId
     * @return BaseResponse
     */
    public function payByBankTransfer($reservationId)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = url('checkout/getBankTransfer/' . $reservationId);

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    /**
     * @param CreateReservationPaypalResponseRequest $request
     * @return mixed
     */
    public function getPaypalSuccess(CreateReservationPaypalResponseRequest $request)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $payment = PayPal::getById($request->payment_id, $this->_apiContext);

            $paymentExecution = PayPal::PaymentExecution();
            $paymentExecution->setPayerId($request->payer_id);

            $this->_baseResponse->addSuccessMessage('Payment execution success');

            $this->_baseResponse->_result = $payment->execute($paymentExecution, $this->_apiContext);

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());

        }

        return $this->_baseResponse;
    }

    public function getPaypalCancel()
    {

    }

    public function getDokuSuccess()
    {
        // TODO: Implement getDokuSuccess() method.
    }

    public function getDokuCancel()
    {
        // TODO: Implement getDokuCancel() method.
    }

    public function getBankTransferSuccess()
    {
        // TODO: Implement getBankTransferSuccess() method.
    }


}