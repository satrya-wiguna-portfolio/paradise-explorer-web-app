<?php
namespace App\Services\Implement;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\State\CreateStateRequest;
use App\Http\Requests\State\UpdateStateRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IStateRepository;
use App\Repositories\Criterias\Implement\State\GetAllStateCriteria;
use App\Services\Contract\IStateService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class StateService extends BaseService implements IStateService
{
    private $_stateRepository;

    /**
     * StateService constructor.
     * @param IStateRepository $stateRepository
     */
    public function __construct(IStateRepository $stateRepository)
    {
        $this->_stateRepository = $stateRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_stateRepository->pushCriteria(new GetAllStateCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();

        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'country' => $model->country,
                'name' => $model->name
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_stateRepository->skipCriteria();
        $model = $model->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'country_id' => (int)$model->country_id,
            'name' => $model->name
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateStateRequest $request
     * @return BaseResponse
     */
    public function save(CreateStateRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_stateRepository->create($request);
                $this->_baseResponse->addSuccessMessage("State created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateStateRequest $request
     * @return BaseResponse
     */
    public function update(UpdateStateRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_stateRepository->update($request);
                $this->_baseResponse->addSuccessMessage("State updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_stateRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("State deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @return GenericResponse
     */
    public function getStateList()
    {
        $models = $this->_stateRepository->skipCriteria();
        $models = $models->orderBy('name', 'asc');
        $models = $models->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'country_id' => (int)$model->country_id,
                'name' => $model->name
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getStateListByCountry($id)
    {
        $models = $this->_stateRepository->skipCriteria();
        $models = $models->orderBy('name', 'asc');
        $models = $models->fetchFindAllBy('country_id', $id);

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'country_id' => (int)$model->country_id,
                'name' => $model->name
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

}