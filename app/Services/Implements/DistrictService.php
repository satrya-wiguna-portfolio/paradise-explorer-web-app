<?php
namespace App\Services\Implement;


use App\Http\Requests\District\CreateDistrictRequest;
use App\Http\Requests\District\UpdateDistrictRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\GenericPageResponse;
use App\Http\Responses\GenericResponse;
use App\Repositories\Contract\IDistrictRepository;
use App\Repositories\Criterias\Implement\District\GetAllDistrictCriteria;
use App\Services\Contract\IDistrictService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Kumuwai\DataTransferObject\Laravel5DTO;

class DistrictService extends BaseService implements IDistrictService
{
    private $_districtRepository;

    /**
     * DistrictService constructor.
     * @param IDistrictRepository $districtRepository
     */
    public function __construct(IDistrictRepository $districtRepository)
    {
        $this->_districtRepository = $districtRepository;
    }

    /**
     * @param GenericPageRequest $pageRequest
     * @return GenericPageResponse
     */
    public function getAll(GenericPageRequest $pageRequest)
    {
        $models = $this->_districtRepository->pushCriteria(new GetAllDistrictCriteria($pageRequest->getSearch()))
            ->orderBy($pageRequest->columns[$pageRequest->order['column']]['name'], $pageRequest->order['dir']);

        $all = $models->fetchAll($reset = false)->count();
        $models = $models->offsetPagination($pageRequest->getLength(), $pageRequest->getStart());

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'regency' => $model->regency,
                'name' => $model->name
            ]);

        $this->_genericPageResponse = new GenericPageResponse();
        $this->_genericPageResponse->setDraw($pageRequest->draw);
        $this->_genericPageResponse->setRecordsTotal($all);
        $this->_genericPageResponse->setRecordsFiltered($all);
        $this->_genericPageResponse->setDto(Collection::make($output));

        return $this->_genericPageResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDetail($id)
    {
        $model = $this->_districtRepository->skipCriteria();
        $model = $model->fetchFind($id);

        $output = new Laravel5DTO([
            'id' => (int)$model->id,
            'regency_id' => (int)$model->regency_id,
            'name' => $model->name
        ]);


        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param CreateDistrictRequest $request
     * @return BaseResponse
     */
    public function save(CreateDistrictRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_districtRepository->create($request);
                $this->_baseResponse->addSuccessMessage("District created");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param UpdateDistrictRequest $request
     * @return BaseResponse
     */
    public function update(UpdateDistrictRequest $request)
    {
        $this->_baseResponse = new BaseResponse();
        $validator = Validator::make((array) $request, $request->rules());

        if ($validator->fails()) {
            $this->_baseResponse->addErrorMessage($validator->errors()->all());

        } else {
            try {
                $this->_baseResponse->_result = $this->_districtRepository->update($request);
                $this->_baseResponse->addSuccessMessage("District updated");

            } catch (Exception $ex) {
                $this->_baseResponse->addErrorMessage($ex->getMessage());

            }
        }

        return $this->_baseResponse;
    }

    /**
     * @param $id
     * @return BaseResponse
     */
    public function delete($id)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            $this->_baseResponse->_result = $this->_districtRepository->delete($id);
            $this->_baseResponse->addSuccessMessage("District deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    public function getDistrictList()
    {
        $models = $this->_districtRepository->skipCriteria();
        $models = $models->orderBy('name', 'asc');
        $models = $models->fetchAll();

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'regency_id' => (int)$model->regency_id,
                'name' => $model->name
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }

    /**
     * @param $id
     * @return GenericResponse
     */
    public function getDistrictListByRegency($id)
    {
        $models = $this->_districtRepository->skipCriteria();
        $models = $models->orderBy('name', 'asc');
        $models = $models->fetchFindAllBy('regency_id', $id);

        $output = [];

        foreach($models as $model)
            $output[] = new Laravel5DTO([
                'id' => (int)$model->id,
                'regency_id' => (int)$model->regency_id,
                'name' => $model->name
            ]);

        $this->_genericResponse = new GenericResponse();
        $this->_genericResponse->setDto(Collection::make($output));

        return $this->_genericResponse;
    }
}