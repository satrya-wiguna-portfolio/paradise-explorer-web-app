<?php
namespace App\Services\Implement;


use App\Http\Responses\BaseResponse;
use App\Repositories\Contract\IRoomAllotmentLogRepository;
use App\Services\Contract\IRoomAllotmentLogService;
use Exception;
use Illuminate\Support\Facades\DB;

class RoomAllotmentLogService extends BaseService implements IRoomAllotmentLogService
{
    private $_roomAllotmentLogRepository;

    /**
     * RoomAllotmentLogService constructor.
     * @param IRoomAllotmentLogRepository $roomAllotmentLogRepository
     */
    public function __construct(IRoomAllotmentLogRepository $roomAllotmentLogRepository)
    {
        $this->_roomAllotmentLogRepository = $roomAllotmentLogRepository;
    }

    /**
     * @param $clientId
     * @param $cartItemId
     * @return BaseResponse
     */
    public function refresh($clientId, $cartItemId)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            DB::statement(DB::raw('CALL spRefreshRoomAllotmentLog("' . $clientId . '", "' . $cartItemId . '");'));

            $this->_baseResponse->addSuccessMessage("Room allotment log refreshed");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }

    /**
     * @param $clientId
     * @param $cartItemId
     * @return BaseResponse
     */
    public function clear($clientId, $cartItemId)
    {
        $this->_baseResponse = new BaseResponse();

        try {
            DB::statement(DB::raw('CALL spClearRoomAllotmentLog("' . $clientId . '", "' . $cartItemId . '");'));

            $this->_baseResponse->addSuccessMessage("Room allotment log deleted");

        } catch (Exception $ex) {
            $this->_baseResponse->addErrorMessage($ex->getMessage());
        }

        return $this->_baseResponse;
    }
}