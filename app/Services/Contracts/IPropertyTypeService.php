<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\PropertyType\CreatePropertyTypeRequest;
use App\Http\Requests\PropertyType\UpdatePropertyTypeRequest;

interface IPropertyTypeService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreatePropertyTypeRequest $request
     * @return mixed
     */
    public function save(CreatePropertyTypeRequest $request);

    /**
     * @param UpdatePropertyTypeRequest $request
     * @return mixed
     */
    public function update(UpdatePropertyTypeRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function getPropertyTypeList();
}