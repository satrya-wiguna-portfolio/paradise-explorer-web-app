<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\State\CreateStateRequest;
use App\Http\Requests\State\UpdateStateRequest;

interface IStateService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateStateRequest $request
     * @return mixed
     */
    public function save(CreateStateRequest $request);

    /**
     * @param UpdateStateRequest $request
     * @return mixed
     */
    public function update(UpdateStateRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function getStateList();

    /**
     * @param $id
     * @return mixed
     */
    public function getStateListByCountry($id);
}