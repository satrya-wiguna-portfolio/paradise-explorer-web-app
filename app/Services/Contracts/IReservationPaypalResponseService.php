<?php
namespace App\Services\Contract;


use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;

interface IReservationPaypalResponseService
{
    /**
     * @param CreateReservationPaypalResponseRequest $request
     * @return mixed
     */
    public function save(CreateReservationPaypalResponseRequest $request);
}