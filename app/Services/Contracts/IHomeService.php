<?php
namespace App\Services\Contract;


interface IHomeService
{
    /**
     * @return mixed
     */
    public function testSendEmail();
}