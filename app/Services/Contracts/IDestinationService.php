<?php
namespace App\Services\Contract;


use App\Http\Requests\BlogCategory\CreateDestinationRequest;
use App\Http\Requests\BlogCategory\UpdateDestinationRequest;
use App\Http\Requests\GenericPageRequest;

interface IDestinationService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateDestinationRequest $request
     * @return mixed
     */
    public function save(CreateDestinationRequest $request);

    /**
     * @param UpdateDestinationRequest $request
     * @return mixed
     */
    public function update(UpdateDestinationRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @param $name
     * @return mixed
     */
    public function getDestinationByName($id, $name);

    /**
     * @return mixed
     */
    public function getDestinationList();

    /**
     * @return mixed
     */
    public function getDestinationHierarchy();

    /**
     * @return mixed
     */
    public function getRandomDestination();

    /**
     * @param $limit
     * @return mixed
     */
    public function getRandomLimitDestination($limit);

    /**
     * @param $id
     * @return mixed
     */
    public function getDestinationDetail($id);
}