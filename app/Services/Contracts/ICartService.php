<?php
namespace App\Services\Contract;


use App\Http\Requests\Cart\AddCartRequest;

interface ICartService
{
    /**
     * @param $productTypeId
     * @param AddCartRequest $request
     * @return mixed
     */
    public function add($productTypeId, AddCartRequest $request);

    /**
     * @param $productTypeId
     * @param $rowId
     * @return mixed
     */
    public function delete($productTypeId, $rowId);
}