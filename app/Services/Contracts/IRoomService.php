<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Room\CheckRoomAvailableRequest;
use App\Http\Requests\Room\CreateRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;

interface IRoomService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateRoomRequest $request
     * @return mixed
     */
    public function save(CreateRoomRequest $request);

    /**
     * @param UpdateRoomRequest $request
     * @return mixed
     */
    public function update(UpdateRoomRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $agentId
     * @return mixed
     */
    public function getRoomListByAgentId($agentId);

    /**
     * @param $title
     * @param $agent_id
     * @param $product_id
     * @return mixed
     */
    public function getRoomByTitle($title, $agent_id, $product_id);

    /**
     * @param CheckRoomAvailableRequest $request
     * @return mixed
     */
    public function checkRoomAvailability(CheckRoomAvailableRequest $request);
}