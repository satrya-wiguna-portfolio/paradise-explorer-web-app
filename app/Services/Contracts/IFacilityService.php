<?php
namespace App\Services\Contract;


use App\Http\Requests\Facility\CreateFacilityRequest;
use App\Http\Requests\Facility\UpdateFacilityRequest;
use App\Http\Requests\GenericPageRequest;

interface IFacilityService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateFacilityRequest $request
     * @return mixed
     */
    public function save(CreateFacilityRequest $request);

    /**
     * @param UpdateFacilityRequest $request
     * @return mixed
     */
    public function update(UpdateFacilityRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $name
     * @return mixed
     */
    public function getFacilityByName($name);
}