<?php
namespace App\Services\Contract;


use App\Http\Requests\ReservationDokuResponse\CreateReservationDokuResponseRequest;

interface IReservationDokuResponseService
{
    /**
     * @param CreateReservationDokuResponseRequest $request
     * @return mixed
     */
    public function save(CreateReservationDokuResponseRequest $request);
}