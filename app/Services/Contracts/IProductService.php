<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;

interface IProductService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateProductRequest $request
     * @return mixed
     */
    public function save(CreateProductRequest $request);

    /**
     * @param UpdateProductRequest $request
     * @return mixed
     */
    public function update(UpdateProductRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $title
     * @param $agent_id
     * @param $product_type_id
     * @return mixed
     */
    public function getProductByTitle($title, $agent_id, $product_type_id);

    /**
     * @param $agent_id
     * @param $name
     * @return mixed
     */
    public function getSlug($agent_id, $name);

    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getProduct(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getProductDetail($id);

    /**
     * @param $product_type_id
     * @return mixed
     */
    public function getRandomLimitProductByProductTypeId($product_type_id);

    /**
     * @return mixed
     */
    public function getMostPopularTour();

    /**
     * @return mixed
     */
    public function getMostPopularHotel();

}