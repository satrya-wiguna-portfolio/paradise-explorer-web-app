<?php
namespace App\Services\Contract;


interface IRoomAllotmentLogService
{
    /**
     * @param $clientId
     * @param $cartItemId
     * @return mixed
     */
    public function refresh($clientId, $cartItemId);

    /**
     * @param $clientId
     * @param $cartItemId
     * @return mixed
     */
    public function clear($clientId, $cartItemId);
}