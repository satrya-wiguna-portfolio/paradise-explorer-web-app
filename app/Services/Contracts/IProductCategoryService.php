<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductCategory\CreateProductCategoryRequest;
use App\Http\Requests\ProductCategory\UpdateProductCategoryRequest;

interface IProductCategoryService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateProductCategoryRequest $request
     * @return mixed
     */
    public function save(CreateProductCategoryRequest $request);

    /**
     * @param UpdateProductCategoryRequest $request
     * @return mixed
     */
    public function update(UpdateProductCategoryRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $product_type_id
     * @return mixed
     */
    public function getProductCategoryList($product_type_id);

    /**
     * @param $id
     * @return mixed
     */
    public function getProductCategoryByProductTypeId($id);
}