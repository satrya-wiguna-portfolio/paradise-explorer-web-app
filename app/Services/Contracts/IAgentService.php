<?php
namespace App\Services\Contract;


use App\Http\Requests\Agent\CreateAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;
use App\Http\Requests\GenericPageRequest;

interface IAgentService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateAgentRequest $request
     * @return mixed
     */
    public function save(CreateAgentRequest $request);

    /**
     * @param UpdateAgentRequest $request
     * @return mixed
     */
    public function update(UpdateAgentRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $agentTypeId
     * @param $email
     * @return mixed
     */
    public function getAgentByTypeAndCompany($agentTypeId = null, $email);

    /**
     * @param $userId
     * @return mixed
     */
    public function getAgentByUserId($userId);
}