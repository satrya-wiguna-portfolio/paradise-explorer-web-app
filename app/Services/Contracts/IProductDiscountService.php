<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ProductDiscount\CreateProductDiscountRequest;
use App\Http\Requests\ProductDiscount\UpdateProductDiscountRequest;

interface IProductDiscountService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateProductDiscountRequest $request
     * @return mixed
     */
    public function save(CreateProductDiscountRequest $request);

    /**
     * @param UpdateProductDiscountRequest $request
     * @return mixed
     */
    public function update(UpdateProductDiscountRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getByProductId($id);
}