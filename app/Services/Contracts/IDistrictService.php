<?php
namespace App\Services\Contract;


use App\Http\Requests\District\CreateDistrictRequest;
use App\Http\Requests\District\UpdateDistrictRequest;
use App\Http\Requests\GenericPageRequest;

interface IDistrictService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateDistrictRequest $request
     * @return mixed
     */
    public function save(CreateDistrictRequest $request);

    /**
     * @param UpdateDistrictRequest $request
     * @return mixed
     */
    public function update(UpdateDistrictRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function getDistrictList();

    /**
     * @param $id
     * @return mixed
     */
    public function getDistrictListByRegency($id);
}