<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Regency\CreateRegencyRequest;
use App\Http\Requests\Regency\UpdateRegencyRequest;

interface IRegencyService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateRegencyRequest $request
     * @return mixed
     */
    public function save(CreateRegencyRequest $request);

    /**
     * @param UpdateRegencyRequest $request
     * @return mixed
     */
    public function update(UpdateRegencyRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function getRegencyList();

    /**
     * @param $id
     * @return mixed
     */
    public function getRegencyListByProvince($id);
}