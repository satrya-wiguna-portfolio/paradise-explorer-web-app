<?php
namespace App\Services\Contract;


use App\Http\Requests\Payment\CreatePaymentRequest;
use App\Http\Requests\Payment\UpdatePaymentRequest;
use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;

interface IPaymentService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreatePaymentRequest $request
     * @return mixed
     */
    public function save(CreatePaymentRequest $request);

    /**
     * @param UpdatePaymentRequest $request
     * @return mixed
     */
    public function update(UpdatePaymentRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function getPayment();

    /**
     * @param $reservationId
     * @return mixed
     */
    public function payByPaypal($reservationId);

    /**
     * @param $reservationId
     * @return mixed
     */
    public function payByDoku($reservationId);

    /**
     * @param $reservationId
     * @return mixed
     */
    public function payByBankTransfer($reservationId);

    /**
     * @param CreateReservationPaypalResponseRequest $request
     * @return mixed
     */
    public function getPaypalSuccess(CreateReservationPaypalResponseRequest $request);

    /**
     * @return mixed
     */
    public function getPaypalCancel();

    /**
     * @return mixed
     */
    public function getDokuSuccess();

    /**
     * @return mixed
     */
    public function getDokuCancel();

    /**
     * @return mixed
     */
    public function getBankTransferSuccess();

}