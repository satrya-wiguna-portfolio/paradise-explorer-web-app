<?php
namespace App\Services\Contract;


use App\Http\Requests\Reservation\CreateReservationRequest;
use App\Http\Requests\Reservation\UpdateReservationRequest;

interface IReservationService
{
    /**
     * @param CreateReservationRequest $request
     * @return mixed
     */
    public function save(CreateReservationRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function getReservationDetail($id);

    /**
     * @param UpdateReservationRequest $request
     * @return mixed
     */
    public function updateStatus(UpdateReservationRequest $request);
}