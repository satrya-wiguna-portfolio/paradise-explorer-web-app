<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\RoomAllotment\CreateRoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\UpdateRoomAllotmentRequest;

interface IRoomAllotmentService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateRoomAllotmentRequest $request
     * @return mixed
     */
    public function save(CreateRoomAllotmentRequest $request);

    /**
     * @param UpdateRoomAllotmentRequest $request
     * @return mixed
     */
    public function update(UpdateRoomAllotmentRequest $request);

    /**
     * @param RoomAllotmentRequest $request
     * @return mixed
     */
    public function roomLock(RoomAllotmentRequest $request);

    /**
     * @param RoomAllotmentRequest $request
     * @return mixed
     */
    public function roomUnlock(RoomAllotmentRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $room_id
     * @return mixed
     */
    public function getByRoomId($room_id);

    /**
     * @param $room_id
     * @return mixed
     */
    public function getLockByRoomId($room_id);

}