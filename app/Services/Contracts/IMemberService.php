<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Member\CreateMemberRequest;
use App\Http\Requests\Member\UpdateMemberRequest;

interface IMemberService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateMemberRequest $request
     * @return mixed
     */
    public function save(CreateMemberRequest $request);

    /**
     * @param UpdateMemberRequest $request
     * @return mixed
     */
    public function update(UpdateMemberRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $userId
     * @return mixed
     */
    public function getMemberByUserId($userId);
}