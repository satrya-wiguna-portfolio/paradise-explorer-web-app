<?php
namespace App\Services\Contract;


use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\UserLog\UpdateUserActivateRequest;

interface IUserService
{
    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update(UpdateUserRequest $request);

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByEmailMemberAgentAndStaff($email);

    /**
     * @param $id
     * @return mixed
     */
    public function getUserInfo($id);

    /**
     * @param $roleId
     * @param $userId
     * @return mixed
     */
    public function getUserProfile($roleId, $userId);

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByEmailStatusAndAgent($email);

    /**
     * @param $token
     * @param $email
     * @return mixed
     */
    public function activate($token, $email);

    /**
     * @param UpdateUserActivateRequest $request
     * @return mixed
     */
    public function email(UpdateUserActivateRequest $request);
}