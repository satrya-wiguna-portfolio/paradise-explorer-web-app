<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\RoomDiscount\CreateRoomDiscountRequest;
use App\Http\Requests\RoomDiscount\UpdateRoomDiscountRequest;

interface IRoomDiscountService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateRoomDiscountRequest $request
     * @return mixed
     */
    public function save(CreateRoomDiscountRequest $request);

    /**
     * @param UpdateRoomDiscountRequest $request
     * @return mixed
     */
    public function update(UpdateRoomDiscountRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getByRoomId($id);
}