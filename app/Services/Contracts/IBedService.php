<?php
namespace App\Services\Contract;


use App\Http\Requests\Bed\CreateBedRequest;
use App\Http\Requests\Bed\UpdateBedRequest;
use App\Http\Requests\GenericPageRequest;

interface IBedService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateBedRequest $request
     * @return mixed
     */
    public function save(CreateBedRequest $request);

    /**
     * @param UpdateBedRequest $request
     * @return mixed
     */
    public function update(UpdateBedRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $agentId
     * @return mixed
     */
    public function getBedListByAgentId($agentId);
}