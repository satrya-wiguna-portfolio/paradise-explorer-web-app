<?php
namespace App\Services\Contract;


use App\Http\Requests\GenericPageRequest;
use App\Http\Requests\Staff\CreateStaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;

interface IStaffService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateStaffRequest $request
     * @return mixed
     */
    public function save(CreateStaffRequest $request);

    /**
     * @param UpdateStaffRequest $request
     * @return mixed
     */
    public function update(UpdateStaffRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $userId
     * @return mixed
     */
    public function getStaffByUserId($userId);
}