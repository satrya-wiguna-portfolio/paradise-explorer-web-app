<?php
namespace App\Services\Contract;


use App\Http\Requests\Country\CreateCountryRequest;
use App\Http\Requests\Country\UpdateCountryRequest;
use App\Http\Requests\GenericPageRequest;

interface ICountryService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateCountryRequest $request
     * @return mixed
     */
    public function save(CreateCountryRequest $request);

    /**
     * @param UpdateCountryRequest $request
     * @return mixed
     */
    public function update(UpdateCountryRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function getCountryList();
}