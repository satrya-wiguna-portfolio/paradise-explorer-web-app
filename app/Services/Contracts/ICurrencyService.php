<?php
namespace App\Services\Contract;


use App\Http\Requests\Currency\CreateCurrencyRequest;
use App\Http\Requests\Currency\SyncCurrencyRequest;
use App\Http\Requests\Currency\UpdateCurrencyRequest;
use App\Http\Requests\GenericPageRequest;

interface ICurrencyService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateCurrencyRequest $request
     * @return mixed
     */
    public function save(CreateCurrencyRequest $request);

    /**
     * @param UpdateCurrencyRequest $request
     * @return mixed
     */
    public function update(UpdateCurrencyRequest $request);

    /**
     * @param $code
     * @return mixed
     */
    public function delete($code);

    /**
     * @param SyncCurrencyRequest $request
     * @return mixed
     */
    public function sync(SyncCurrencyRequest $request);

    /**
     * @return mixed
     */
    public function getActiveCurrency();
}