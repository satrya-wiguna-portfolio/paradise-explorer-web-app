<?php
namespace App\Services\Contract;



use App\Http\Requests\AgentType\CreateAgentTypeRequest;
use App\Http\Requests\AgentType\UpdateAgentTypeRequest;
use App\Http\Requests\GenericPageRequest;

interface IAgentTypeService
{
    /**
     * @param GenericPageRequest $pageRequest
     * @return mixed
     */
    public function getAll(GenericPageRequest $pageRequest);

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * @param CreateAgentTypeRequest $request
     * @return mixed
     */
    public function save(CreateAgentTypeRequest $request);

    /**
     * @param UpdateAgentTypeRequest $request
     * @return mixed
     */
    public function update(UpdateAgentTypeRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}