<?php
namespace App\Repositories\Criterias\Implement\BlogComment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllBlogCommentCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllBlogTagCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_keyword)) {
            $model = $model->where('blog_comments.comment', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('blogs.title', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}