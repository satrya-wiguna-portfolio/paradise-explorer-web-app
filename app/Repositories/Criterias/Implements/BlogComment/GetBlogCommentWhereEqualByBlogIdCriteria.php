<?php
namespace App\Repositories\Criterias\Implement\BlogComment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogCommentWhereEqualByBlogIdCriteria extends BaseCriteria
{
    private $_blog_id;

    /**
     * GetBlogWhereEqualByStatusCriteria constructor.
     * @param $blogId
     */
    public function __construct($blogId)
    {
        $this->_blog_id = $blogId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_blog_id)) {
            $model = $model->where('blog_id', $this->_blog_id);
        }

        return $model;
    }
}