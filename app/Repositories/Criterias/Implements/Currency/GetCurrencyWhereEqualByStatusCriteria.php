<?php
namespace App\Repositories\Criterias\Implement\Currency;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetCurrencyWhereEqualByStatusCriteria extends BaseCriteria
{
    private $_active;

    /**
     * GetCurrencyWhereEqualByStatusCriteria constructor.
     * @param $active
     */
    public function __construct($active)
    {
        $this->_active = $active;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_status)) {
            $model = $model->where('active', $this->_active);
        }

        return $model;
    }

}