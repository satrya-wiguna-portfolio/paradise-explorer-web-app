<?php
namespace App\Repositories\Criterias\Implement\District;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllDistrictCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllDistrictCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['districts.*', 'regencies.name as regency'])
            ->join('regencies', 'districts.regency_id', '=', 'regencies.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('districts.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('regencies.name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}