<?php
namespace App\Repositories\Criterias\Implement\Blog;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogWherePivotEqualByBlogTagIdCriteria extends BaseCriteria
{
    private $_blog_tag_id;

    /**
     * GetBlogWherePivotEqualByBlogTagIdCriteria constructor.
     * @param $blogTagId
     */
    public function __construct($blogTagId)
    {
        $this->_blog_tag_id = $blogTagId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_blog_tag_id)) {
            $model = $model->with(['blog_tag' => function($query) {
                return $query->wherePivot('blog_tag_id', $this->_blog_tag_id);
            }]);
        } else {
            $model = $model->with(['blog_tag']);
        }

        return $model;
    }
}