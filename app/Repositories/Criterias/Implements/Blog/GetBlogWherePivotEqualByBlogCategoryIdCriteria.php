<?php
namespace App\Repositories\Criterias\Implement\Blog;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogWherePivotEqualByBlogCategoryIdCriteria extends BaseCriteria
{
    private $_blog_category_id;

    /**
     * GetBlogWherePivotEqualByBlogCategoryIdCriteria constructor.
     * @param $blogCategoryId
     */
    public function __construct($blogCategoryId)
    {
        $this->_blog_category_id = $blogCategoryId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_blog_category_id)) {
            $model = $model->with(['blog_category' => function($query) {
                return $query->wherePivot('blog_category_id', $this->_blog_category_id);
            }]);
        } else {
            $model = $model->with(['blog_category']);
        }

        return $model;
    }
}