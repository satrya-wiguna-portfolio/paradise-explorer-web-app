<?php
namespace App\Repositories\Criterias\Implement\Blog;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogByLimitCriteria extends BaseCriteria
{
    private $_limit;

    /**
     * GetBlogByLimitCriteria constructor.
     * @param $limit
     */
    public function __construct($limit)
    {
        $this->_limit = $limit;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_limit)) {
            $model = $model->take($this->_limit);
        }

        return $model;
    }
}