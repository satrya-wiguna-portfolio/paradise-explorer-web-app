<?php
namespace App\Repositories\Criterias\Implement\RoomAllotment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomAllotmentWhereBetweenByFromAndToCriteria extends BaseCriteria
{
    private $_from;

    private $_to;

    /**
     * GetRoomAllotmentWhereBetweenByFromAndToCriteria constructor.
     * @param $from
     * @param $to
     */
    public function __construct($from, $to)
    {
        $this->_from = $from;

        $this->_to = $to;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_from) && !empty($this->_to)) {
            $model = $model->whereRaw("(room_allotments.date BETWEEN '" . $this->_from . "' AND '" . $this->_to . "')");
        }

        return $model;
    }
}