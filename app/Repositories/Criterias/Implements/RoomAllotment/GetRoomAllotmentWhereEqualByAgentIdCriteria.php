<?php
namespace App\Repositories\Criterias\Implement\RoomAllotment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomAllotmentWhereEqualByAgentIdCriteria extends BaseCriteria
{
    private $_agent_id;

    /**
     * GetRoomAllotmentWhereEqualByAgentIdCriteria constructor.
     * @param $agentId
     */
    public function __construct($agentId)
    {
        $this->_agent_id = $agentId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if ($this->_agent_id) {
            $model = $model->where('agent_id', $this->_agent_id);
        }

        return $model;
    }
}