<?php
namespace App\Repositories\Criterias\Implement\RoomAllotment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomAllotmentWhereEqualByDate extends BaseCriteria
{
    private $_date;

    /**
     * GetRoomAllotmentWhereEqualByDate constructor.
     * @param $date
     */
    public function __construct($date)
    {
        $this->_date = $date;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_date)) {
            $model = $model->whereDate('room_allotments.date', '=', $this->_date);
        }

        return $model;
    }
}