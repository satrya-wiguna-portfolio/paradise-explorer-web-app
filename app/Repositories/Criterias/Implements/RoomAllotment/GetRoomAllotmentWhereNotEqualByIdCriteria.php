<?php
namespace App\Repositories\Criterias\Implement\RoomAllotment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomAllotmentWhereNotEqualByIdCriteria extends BaseCriteria
{
    private $_id;

    /**
     * GetRoomAllotmentWhereNotEqualByIdCriteria constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->_id = $id;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_id)) {
            $model = $model->where('room_allotments.id', '<>',  $this->_id);
        }

        return $model;
    }
}