<?php
namespace App\Repositories\Criterias\Implement\RoomAllotment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomAllotmentWhereEqualByRoomIdCriteria extends BaseCriteria
{
    private $_room_id;

    /**
     * GetRoomAllotmentWhereEqualByRoomIdCriteria constructor.
     * @param $roomId
     */
    public function __construct($roomId)
    {
        $this->_room_id = $roomId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_room_id)) {
            $model = $model->where('room_allotments.room_id', $this->_room_id);
        }

        return $model;
    }
}