<?php
namespace App\Repositories\Criterias\Implement\RoomAllotment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllRoomAllotmentCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['room_allotments.*', 'rooms.title as room', 'rooms.product_id as product_id', 'products.agent_id AS agent_id'])
            ->join('rooms', 'room_allotments.room_id', '=', 'rooms.id')
            ->join('products', 'rooms.product_id', '=', 'products.id');

        return $model;
    }
}