<?php
namespace App\Repositories\Criterias\Implement\Destination;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDestinationByRandomCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->inRandomOrder()
            ->take(4);

        return $model;
    }
}