<?php
namespace App\Repositories\Criterias\Implement\BlogCategory;

use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDestinationWhereEqualByParentIdCriteria extends BaseCriteria
{
    private $_parent_id;

    /**
     * GetDestinationWhereEqualByParentIdCriteria constructor.
     * @param $parentId
     */
    public function __construct($parentId)
    {
        $this->_parent_id = $parentId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_parent_id)) {
            $model = $model->where('parent_id', ($this->_parent_id <> 0) ? $this->_parent_id : null);
        }

        return $model;
    }
}