<?php
namespace App\Repositories\Criterias\Implement\Facility;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetFacilityWhereLikeByNameCriteria extends BaseCriteria
{
    private $_name;

    /**
     * GetFacilityWhereLikeByNameCriteria constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->_name = $name;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_name)) {
            $model = $model->where('facility', 'LIKE', '%' . $this->_name . '%');
        }

        return $model;
    }
}