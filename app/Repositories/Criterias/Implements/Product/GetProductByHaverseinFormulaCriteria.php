<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;
use Illuminate\Support\Facades\DB;

class GetProductByHaverseinFormulaCriteria extends BaseCriteria
{
    private $_lat;

    private $_lng;

    private $_distance = 25;

    //private $_radius = 25;

    /**
     * GetProductByHaverseinFormulaCriteria constructor.
     * @param $lat
     * @param $lng
     */
    public function __construct($lat, $lng)
    {
        $this->_lat = $lat;

        $this->_lng = $lng;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_lat) && !empty($this->_lng)) {
            $model = $model->select(DB::raw('products.*, (3959 * acos(cos(radians(' . $this->_lat . ')) * cos(radians(lat)) * cos(radians(lng) - radians(' . $this->_lng . ')) + sin(radians(' . $this->_lat . ')) * sin(radians(lat)))) AS distance'))
                ->having('distance', '<', $this->_distance)
                ->where('products.status', 1);
        } else {
            $model = $model->select(DB::raw('products.*'))
                ->where('products.status', 1);
        }


        //69.0 for miles
        //111.045 for km
        /*$model = $model->select(DB::raw('products.*, p.distance_unit
            * DEGREES(ACOS(COS(RADIANS(p.lat_point))
            * COS(RADIANS(products.lat))
            * COS(RADIANS(p.lng_point) - RADIANS(products.lng))
            + SIN(RADIANS(p.lat_point))
            * SIN(RADIANS(products.lat)))) AS distance'))
            ->join(DB::raw('(SELECT '. $this->_lat .' AS lat_point, '. $this->_lng .' AS lng_point, '. $this->_radius .' AS radius, 69.0 AS distance_unit) AS p'), 1, '=', 1)
            ->whereBetween('products.lat', ['p.lat_point  - (p.radius / p.distance_unit)', 'p.lat_point  + (p.radius / p.distance_unit)'])
            ->whereBetween('products.lng', ['p.lng_point - (p.radius / (p.distance_unit * COS(RADIANS(p.lng_point))))', 'p.lng_point + (p.radius / (p.distance_unit * COS(RADIANS(p.lng_point))))'])
            ->where('products.status', 1);*/

        return $model;
    }
}