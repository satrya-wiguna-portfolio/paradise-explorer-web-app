<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductWhereInByIdCriteria extends BaseCriteria
{
    private $_ids;

    /**
     * GetAllProvinceCriteria constructor.
     * @param $ids
     */
    public function __construct($ids)
    {
        $this->_ids = $ids;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $product_ids = [];

        foreach($this->_ids->toArray() as $id) {
            array_push($product_ids, $id['product_id']);
        }

        $model = $model->whereIn('id', $product_ids);

        return $model;
    }
}