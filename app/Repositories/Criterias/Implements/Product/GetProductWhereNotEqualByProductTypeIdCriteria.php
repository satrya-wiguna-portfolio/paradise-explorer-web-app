<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductWhereNotEqualByProductTypeIdCriteria extends BaseCriteria
{
    private $_product_type_id;

    /**
     * GetProductWhereNotEqualByProductTypeIdCriteria constructor.
     * @param $productTypeId
     */
    public function __construct($productTypeId)
    {
        $this->_product_type_id = $productTypeId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_product_type_id)) {
            $model = $model->where('products.product_type_id', '<>', $this->_product_type_id);
        }

        return $model;
    }
}