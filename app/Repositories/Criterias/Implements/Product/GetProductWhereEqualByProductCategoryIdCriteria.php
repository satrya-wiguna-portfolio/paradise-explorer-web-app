<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductWhereEqualByProductCategoryIdCriteria extends BaseCriteria
{
    private $_product_category_id;

    /**
     * GetProductWhereEqualByProductCategoryIdCriteria constructor.
     * @param $productCategoryId
     */
    public function __construct($productCategoryId)
    {
        $this->_product_category_id = $productCategoryId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_product_category_id)) {
            $model = $model->where('products.product_category_id', $this->_product_category_id);
        }

        return $model;
    }
}