<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllProductCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllProductCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['products.*'])
            ->join('agents', 'products.agent_id', '=', 'agents.id')
            ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
            ->leftJoin('product_categories', 'products.product_category_id', '=', 'product_categories.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('products.title', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}