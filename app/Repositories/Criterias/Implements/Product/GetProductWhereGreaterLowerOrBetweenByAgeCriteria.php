<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductWhereGreaterLowerOrBetweenByAgeCriteria extends BaseCriteria
{
    private $_option;

    /**
     * GetProductWhereGreaterLowerOrBetweenByAgeCriteria constructor.
     * @param $option
     */
    public function __construct($option)
    {
        $this->_option = $option;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_option)) {
            $age = $this->getAgeOptionValue($this->_option);

            if (($this->_option == 1)) {
                $model = $model->where('products.min_age', '<', $age)
                    ->where('products.max_age', '<', $age);
            } else if ($this->_option == 6) {
                $model = $model->where('products.min_age', '>', $age)
                    ->where('products.max_age', '>', $age);
            } else {
                $model = $model->whereBetween('products.min_age', array($age))
                    ->whereBetween('products.max_age', array($age));
            }
        }

        return $model;
    }

    /**
     * @param $option
     * @return array|int
     */
    private function getAgeOptionValue($option)
    {
        $value = 0;

        switch($option) {
            case 1:
                $value = 20;
                break;

            case 2:
                $value = [20, 29];
                break;

            case 3:
                $value = [30, 39];
                break;

            case 4:
                $value = [40, 49];
                break;

            case 5:
                $value = [50, 59];
                break;

            case 6:
                $value = 59;
                break;

            default:
                $value = [1, 70];
                break;
        }

        return $value;
    }
}