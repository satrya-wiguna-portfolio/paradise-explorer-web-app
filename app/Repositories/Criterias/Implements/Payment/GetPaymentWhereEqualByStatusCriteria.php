<?php
namespace App\Repositories\Criterias\Implement\Payment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetPaymentWhereEqualByStatusCriteria extends BaseCriteria
{
    private $_status;

    /**
     * GetPaymentWhereEqualByStatusCriteria constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->_status = $status;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_status)) {
            $model = $model->where('payments.status', $this->_status);
        }

        return $model;
    }
}