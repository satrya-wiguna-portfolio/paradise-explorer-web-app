<?php
namespace App\Repositories\Criterias\Implement\Payment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllPaymentCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllFacilityCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_keyword)) {
            $model = $model->where('payment', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}