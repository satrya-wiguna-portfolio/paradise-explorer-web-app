<?php
namespace App\Repositories\Criterias\Implement\Payment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetPaymentWhereLikeByNameCriteria extends BaseCriteria
{
    private $_name;

    /**
     * GetPaymentWhereLikeByNameCriteria constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->_name = $name;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_name)) {
            $model = $model->where('payment', 'LIKE', '%' . $this->_name . '%');
        }

        return $model;
    }
}