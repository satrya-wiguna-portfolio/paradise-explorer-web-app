<?php
namespace App\Repositories\Criterias\Implement\ProductTag;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductTagWhereEqualByNameCriteria extends BaseCriteria
{
    private $_name;

    /**
     * GetProductTagWhereEqualByNameCriteria constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->_name = $name;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_name)) {
            $model = $model->where('name', 'LIKE', '%' . $this->_name . '%');
        }

        return $model;
    }
}