<?php
namespace App\Repositories\Criterias\Implement\State;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllStateCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllStateCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['states.*', 'countries.name as country'])
            ->join('countries', 'states.country_id', '=', 'countries.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('states.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('countries.name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}