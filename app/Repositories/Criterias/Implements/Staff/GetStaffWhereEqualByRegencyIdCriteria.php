<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetStaffWhereEqualByRegencyIdCriteria extends BaseCriteria
{
    private $_regency_id;

    /**
     * GetStaffWhereEqualByRegencyIdCriteria constructor.
     * @param $regencyId
     */
    public function __construct($regencyId)
    {
        $this->_regency_id = $regencyId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_regency_id)) {
            $model = $model->where('staffs.regency_id', $this->_regency_id);
        }

        return $model;
    }
}