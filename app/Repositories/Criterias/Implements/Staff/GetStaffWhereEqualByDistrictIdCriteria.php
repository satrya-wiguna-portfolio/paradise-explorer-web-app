<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetStaffWhereEqualByDistrictIdCriteria extends BaseCriteria
{
    private $_district_id;

    /**
     * GetStaffWhereEqualByDistrictIdCriteria constructor.
     * @param $districtId
     */
    public function __construct($districtId)
    {
        $this->_district_id = $districtId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_district_id)) {
            $model = $model->where('staffs.district_id', $this->_district_id);
        }

        return $model;
    }
}