<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDetailStaffCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['staffs.*', 'users.email as email', 'provinces.name as province', 'regencies.name as regency', 'districts.name as district'])
            ->join('users', 'staffs.user_id', '=', 'users.id')
            ->join('provinces', 'staffs.province_id', '=', 'provinces.id')
            ->join('regencies', 'staffs.regency_id', '=', 'regencies.id')
            ->join('districts', 'staffs.district_id', '=', 'districts.id');

        return $model;
    }
}