<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetStaffWhereEqualByProvinceIdCriteria extends BaseCriteria
{
    private $_province_id;

    /**
     * GetStaffWhereEqualByProvinceIdCriteria constructor.
     * @param $provinceId
     */
    public function __construct($provinceId)
    {
        $this->_province_id = $provinceId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_province_id)) {
            $model = $model->where('staffs.province_id', $this->_province_id);
        }

        return $model;
    }
}