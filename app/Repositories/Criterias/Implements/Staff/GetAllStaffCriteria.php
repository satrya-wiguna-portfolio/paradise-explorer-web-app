<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllStaffCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllStaffCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['staffs.*', 'users.email as email', 'provinces.name as province', 'regencies.name as regency', 'districts.name as district'])
            ->join('users', 'staffs.user_id', '=', 'users.id')
            ->join('provinces', 'staffs.province_id', '=', 'provinces.id')
            ->join('regencies', 'staffs.regency_id', '=', 'regencies.id')
            ->join('districts', 'staffs.district_id', '=', 'districts.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('staffs.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('staffs.gender', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('staffs.city', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('staffs.zip', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('staffs.phone', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('users.email', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('provinces.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('regencies.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('districts.name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}