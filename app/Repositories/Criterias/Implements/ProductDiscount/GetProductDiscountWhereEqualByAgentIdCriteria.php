<?php
namespace App\Repositories\Criterias\Implement\ProductDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductDiscountWhereEqualByAgentIdCriteria extends BaseCriteria
{
    private $_agent_id;

    /**
     * GetProductDiscountWhereEqualByAgentIdCriteria constructor.
     * @param $agentId
     */
    public function __construct($agentId)
    {
        $this->_agent_id = $agentId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if ($this->_agent_id) {
            $model = $model->where('products.agent_id', $this->_agent_id);
        }

        return $model;
    }
}