<?php
namespace App\Repositories\Criterias\Implement\ProductDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDetailProductDiscountCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['product_discounts.*', 'products.agent_id as agent_id'])
            ->join('products', 'product_discounts.product_id', '=', 'products.id');

        return $model;
    }
}