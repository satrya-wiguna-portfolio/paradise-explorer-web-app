<?php
namespace App\Repositories\Criterias\Implement\ProductDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductDiscountWhereBetweenByOpenAndCloseCriteria extends BaseCriteria
{
    private $_open_date;

    private $_close_date;

    /**
     * GetProductDiscountWhereBetweenByOpenAndCloseCriteria constructor.
     * @param $openDate
     * @param $closeDate
     */
    public function __construct($openDate, $closeDate)
    {
        $this->_open_date = $openDate;

        $this->_close_date = $closeDate;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_open_date) && !empty($this->_close_date)) {
            $model = $model->whereRaw("(product_discounts.open_date BETWEEN '" . $this->_open_date . "' AND '" . $this->_close_date .
                "' OR product_discounts.close_date BETWEEN '" . $this->_open_date . "' AND '" . $this->_close_date ."')");
        }

        return $model;
    }
}