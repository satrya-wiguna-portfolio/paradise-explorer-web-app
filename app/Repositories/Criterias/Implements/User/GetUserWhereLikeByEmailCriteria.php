<?php
namespace App\Repositories\Criterias\Implement\User;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetUserWhereLikeByEmailCriteria extends BaseCriteria
{
    private $_email;

    /**
     * GetUserWhereLikeByEmailCriteria constructor.
     * @param $email
     */
    public function __construct($email)
    {
        $this->_email = $email;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_keyword)) {
            $model = $model->where('users.email', 'LIKE', '%' . $this->_email . '%');
        }

        return $model;
    }
}