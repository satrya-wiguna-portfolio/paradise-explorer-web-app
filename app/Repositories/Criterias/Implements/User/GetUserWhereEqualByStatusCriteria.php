<?php
namespace App\Repositories\Criterias\Implement\User;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetUserWhereEqualByStatusCriteria extends BaseCriteria
{
    private $_status;

    /**
     * GetUserWhereEqualByStatusCriteria constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->_status = $status;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_status)) {
            $model = $model->where('status', $this->_status);
        }

        return $model;
    }
}