<?php
namespace App\Repositories\Criterias\Implement\User;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetUserWhereByUserIdOfStaffCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->where('staffs.user_id');

        return $model;
    }
}