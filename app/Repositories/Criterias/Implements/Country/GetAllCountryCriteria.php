<?php
namespace App\Repositories\Criterias\Implement\Country;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllCountryCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllCountryCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_keyword)) {
            $model = $model->where('code', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}