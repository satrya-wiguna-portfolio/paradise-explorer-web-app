<?php
namespace App\Repositories\Criterias\Implement\ProductComment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllProductCommentCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllProductCommentCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_keyword)) {
            $model = $model->where('product_comments.comment', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('products.title', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}