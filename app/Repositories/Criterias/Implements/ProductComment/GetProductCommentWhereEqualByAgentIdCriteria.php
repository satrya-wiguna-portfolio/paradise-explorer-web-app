<?php
namespace App\Repositories\Criterias\Implement\ProductComment;


use App\Models\Agent;
use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductCommentWhereEqualByAgentIdCriteria extends BaseCriteria
{
    private $_agent_id;

    /**
     * GetProductCommentWhereEqualByAgentIdCriteria constructor.
     * @param $agentId
     */
    public function __construct($agentId)
    {
        $this->_agent_id = $agentId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if ($this->_agent_id) {
            $agentId = $this->_agent_id;

            $model = $model->where('user_id', function($q) use ($agentId) {
                return $q->select(['id'])
                    ->from(with(new Agent)->getTable())
                    ->where('agent_id', $agentId);
            });
        }

        return $model;
    }
}