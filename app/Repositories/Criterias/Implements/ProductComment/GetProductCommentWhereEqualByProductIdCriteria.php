<?php
namespace App\Repositories\Criterias\Implement\ProductComment;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductCommentWhereEqualByProductIdCriteria extends BaseCriteria
{
    private $_product_id;

    /**
     * GetProductCommentWhereEqualByProductIdCriteria constructor.
     * @param $productId
     */
    public function __construct($productId)
    {
        $this->_product_id = $productId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_product_id)) {
            $model = $model->where('product_id', $this->_product_id);
        }

        return $model;
    }
}