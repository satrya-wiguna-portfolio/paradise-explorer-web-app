<?php
namespace App\Repositories\Criterias\Implement\Room;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllRoomCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllProductTagCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['rooms.*', 'products.agent_id AS agent_id'])
            ->join('products', 'rooms.product_id', '=', 'products.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('title', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}