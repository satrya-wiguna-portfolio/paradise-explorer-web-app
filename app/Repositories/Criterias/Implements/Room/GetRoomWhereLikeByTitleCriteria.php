<?php
namespace App\Repositories\Criterias\Implement\Product;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomWhereLikeByTitleCriteria extends BaseCriteria
{
    private $_title;

    /**
     * GetRoomWhereLikeByTitleCriteria constructor.
     * @param $title
     */
    public function __construct($title)
    {
        $this->_title = $title;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_title)) {
            $model = $model->where('rooms.title', 'LIKE', '%' . $this->_title . '%');
        }

        return $model;
    }
}