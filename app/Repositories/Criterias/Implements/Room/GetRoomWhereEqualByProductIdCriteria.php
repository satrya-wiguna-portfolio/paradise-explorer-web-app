<?php
namespace App\Repositories\Criterias\Implement\Room;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomWhereEqualByProductIdCriteria extends BaseCriteria
{
    private $_product_id;

    /**
     * GetRoomWhereEqualByAgentIdCriteria constructor.
     * @param $productId
     */
    public function __construct($productId)
    {
        $this->_product_id = $productId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_product_id)) {
            $model = $model->where('rooms.product_id', $this->_product_id);
        }

        return $model;
    }
}