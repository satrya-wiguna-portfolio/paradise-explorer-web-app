<?php
namespace App\Repositories\Criterias\Implement\Room;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomWhereEqualByAgentIdCriteria extends BaseCriteria
{
    private $_agent_id;

    /**
     * GetRoomWhereEqualByAgentIdCriteria constructor.
     * @param $agentId
     */
    public function __construct($agentId)
    {
        $this->_agent_id = $agentId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_agent_id) || ($this->_agent_id <> 0)) {
            $model = $model->where('agent_id', $this->_agent_id);
        }

        return $model;
    }
}