<?php
namespace App\Repositories\Criterias\Implement\AgentType;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllAgentTypeCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllAgentTypeCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_keyword)) {
            $model = $model->where('name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}