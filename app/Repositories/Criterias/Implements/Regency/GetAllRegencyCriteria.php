<?php
namespace App\Repositories\Criterias\Implement\Regency;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllRegencyCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllRegencyCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['regencies.*', 'provinces.name as province'])
            ->join('provinces', 'regencies.province_id', '=', 'provinces.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('regencies.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('provinces.name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}