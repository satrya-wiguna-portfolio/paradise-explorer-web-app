<?php
namespace App\Repositories\Criterias\Implement\ProductCategory;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllProductCategoryCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllProductTypeCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['product_categories.*', 'product_types.type as type'])
            ->join('product_types', 'product_categories.product_type_id', '=', 'product_types.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('product_categories.category', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('product_types.type', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}