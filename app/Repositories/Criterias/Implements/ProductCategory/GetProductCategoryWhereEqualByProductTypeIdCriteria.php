<?php
namespace App\Repositories\Criterias\Implement\ProductCategory;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetProductCategoryWhereEqualByProductTypeIdCriteria extends BaseCriteria
{
    private $_product_type_id;

    /**
     * GetProductCategoryWhereEqualByProductTypeIdCriteria constructor.
     * @param $productTypeId
     */
    public function __construct($productTypeId)
    {
        $this->_product_type_id = $productTypeId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_product_type_id)) {
            $model = $model->where('product_type_id', $this->_product_type_id);
        }

        return $model;
    }
}