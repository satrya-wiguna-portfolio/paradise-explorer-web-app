<?php
namespace App\Repositories\Criterias\Implement\BlogTag;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogTagByRandomCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->inRandomOrder();

        return $model;
    }
}