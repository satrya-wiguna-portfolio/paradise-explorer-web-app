<?php
namespace App\Repositories\Criterias\Implement\BlogTag;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogTagWhereEqualByStatusCriteria extends BaseCriteria
{
    private $_status;

    /**
     * GetBlogTagWhereEqualByStatusCriteria constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->_status = $status;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if ($this->_status) {
            $model = $model->where('blogs.status', $this->_status);
        }

        return $model;
    }
}