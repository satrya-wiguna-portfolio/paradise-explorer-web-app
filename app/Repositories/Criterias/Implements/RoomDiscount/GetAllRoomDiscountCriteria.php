<?php
namespace App\Repositories\Criterias\Implement\RoomDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllRoomDiscountCriteria extends BaseCriteria
{
    private $_open_date;

    private $_close_date;

    /**
     * GetAllProductDiscountCriteria constructor.
     * @param $openDate
     * @param $closeDate
     */
    public function __construct($openDate, $closeDate)
    {
        $this->_open_date = $openDate;

        $this->_close_date = $closeDate;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['room_discounts.*', 'rooms.title AS room', 'rooms.product_id AS product_id', 'products.agent_id AS agent_id'])
            ->join('rooms', 'room_discounts.room_id', '=', 'rooms.id')
            ->join('products', 'rooms.product_id', '=', 'products.id');

        if (!empty($this->_open_date) && !empty($this->_close_date)) {
            $model = $model->whereRaw("(room_discounts.open_date BETWEEN '" . $this->_open_date . "' AND '" . $this->_close_date .
                "' OR room_discounts.close_date BETWEEN '" . $this->_open_date . "' AND '" . $this->_close_date ."')");
        }

        return $model;
    }
}