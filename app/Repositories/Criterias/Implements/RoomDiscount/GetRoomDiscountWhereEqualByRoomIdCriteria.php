<?php
namespace App\Repositories\Criterias\Implement\RoomDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomDiscountWhereEqualByRoomIdCriteria extends BaseCriteria
{
    private $_room_id;

    /**
     * GetRoomDiscountWhereEqualByRoomIdCriteria constructor.
     * @param $roomId
     */
    public function __construct($roomId)
    {
        $this->_room_id = $roomId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_room_id)) {
            $model = $model->where('room_discounts.room_id', $this->_room_id);
        }

        return $model;
    }
}