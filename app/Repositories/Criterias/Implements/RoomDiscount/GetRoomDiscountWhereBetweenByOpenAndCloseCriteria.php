<?php
namespace App\Repositories\Criterias\Implement\RoomDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomDiscountWhereBetweenByOpenAndCloseCriteria extends BaseCriteria
{
    private $_open_date;

    private $_close_date;

    /**
     * GetRoomDiscountWhereBetweenByOpenAndCloseCriteria constructor.
     * @param $openDate
     * @param $closeDate
     */
    public function __construct($openDate, $closeDate)
    {
        $this->_open_date = $openDate;

        $this->_close_date = $closeDate;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_open_date) && !empty($this->_close_date)) {
            $model = $model->whereRaw("(room_discounts.open_date BETWEEN '" . $this->_open_date . "' AND '" . $this->_close_date .
                "' OR room_discounts.close_date BETWEEN '" . $this->_open_date . "' AND '" . $this->_close_date ."')");
        }

        return $model;
    }
}