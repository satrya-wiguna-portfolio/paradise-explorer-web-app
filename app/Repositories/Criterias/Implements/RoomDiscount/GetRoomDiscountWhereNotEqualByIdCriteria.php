<?php
namespace App\Repositories\Criterias\Implement\RoomDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetRoomDiscountWhereNotEqualByIdCriteria extends BaseCriteria
{
    private $_id;

    /**
     * GetRoomDiscountWhereNotEqualByIdCriteria constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->_id = $id;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_id)) {
            $model = $model->where('room_discounts.id', '<>',  $this->_id);
        }

        return $model;
    }
}