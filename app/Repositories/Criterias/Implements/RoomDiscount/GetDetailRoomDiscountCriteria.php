<?php
namespace App\Repositories\Criterias\Implement\RoomDiscount;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDetailRoomDiscountCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['room_discounts.*', 'rooms.title AS room', 'rooms.product_id AS product_id', 'products.agent_id as agent_id'])
            ->join('rooms', 'room_discounts.room_id', '=', 'rooms.id')
            ->join('products', 'rooms.product_id', '=', 'products.id');

        return $model;
    }
}