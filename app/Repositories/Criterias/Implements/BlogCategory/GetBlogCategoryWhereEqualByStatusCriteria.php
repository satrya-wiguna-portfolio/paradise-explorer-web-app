<?php
namespace App\Repositories\Criterias\Implement\BlogCategory;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogCategoryWhereEqualByStatusCriteria extends BaseCriteria
{
    private $_status;

    /**
     * GetBlogCategoryWhereEqualByStatusCriteria constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->_status = $status;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if ($this->_status) {
            $model = $model->with(['blog' => function($query) {
                return $query->where('status', $this->_status);
            }]);
        }

        return $model;
    }
}