<?php
namespace App\Repositories\Criterias\Implement\BlogCategory;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetBlogCategoryWhereNotEqualByIdAndLikeByNameCriteria extends BaseCriteria
{
    private $_id;

    private $_name;

    /**
     * GetBlogCategoryWhereNotEqualByIdAndLikeByNameCriteria constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->_id = $id;

        $this->_name = $name;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_id) && !empty($this->_name)) {
            $model = $model->where('id', '<>', $this->_id)
                ->where('name', 'LIKE', '%' . $this->_name . '%');
        }

        return $model;
    }
}