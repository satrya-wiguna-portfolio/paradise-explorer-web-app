<?php
namespace App\Repositories\Criterias\Implement\Member;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllMemberCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllMemberCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['members.*', 'users.email as email', 'countries.name as country', 'states.name as state'])
            ->join('users', 'members.user_id', '=', 'users.id')
            ->join('countries', 'members.country_id', '=', 'countries.id')
            ->join('states', 'members.state_id', '=', 'states.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('members.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('members.gender', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('members.city', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('members.zip', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('members.phone', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('users.email', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('countries.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('states.name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}