<?php
namespace App\Repositories\Criterias\Implement\Member;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetMemberWhereEqualByCountryIdCriteria extends BaseCriteria
{
    private $_country_id;

    /**
     * GetMemberWhereEqualByCountryIdCriteria constructor.
     * @param $countryId
     */
    public function __construct($countryId)
    {
        $this->_country_id = $countryId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_country_id)) {
            $model = $model->where('members.country_id', $this->_country_id);
        }

        return $model;
    }
}