<?php
namespace App\Repositories\Criterias\Implement\Member;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDetailMemberCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['members.*', 'users.email as email', 'countries.name as country', 'states.name as state'])
            ->join('users', 'members.user_id', '=', 'users.id')
            ->join('countries', 'members.country_id', '=', 'countries.id')
            ->join('states', 'members.state_id', '=', 'states.id');

        return $model;
    }
}