<?php
namespace App\Repositories\Criterias\Implement\Member;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetMemberWhereEqualByStateIdCriteria extends BaseCriteria
{
    private $_state_id;

    /**
     * GetMemberWhereEqualByStateIdCriteria constructor.
     * @param $stateId
     */
    public function __construct($stateId)
    {
        $this->_state_id = $stateId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_state_id)) {
            $model = $model->where('members.state_id', $this->_state_id);
        }

        return $model;
    }
}