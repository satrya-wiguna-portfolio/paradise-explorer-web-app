<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAgentWhereEqualByAgentTypeIdCriteria extends BaseCriteria
{
    private $_agent_type_id;

    /**
     * GetAgentWhereEqualByCountryIdCriteria constructor.
     * @param $agentTypeId
     */
    public function __construct($agentTypeId)
    {
        $this->_agent_type_id = $agentTypeId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_agent_type_id)) {
            $model = $model->where('agents.agent_type_id', $this->_agent_type_id);
        }

        return $model;
    }
}