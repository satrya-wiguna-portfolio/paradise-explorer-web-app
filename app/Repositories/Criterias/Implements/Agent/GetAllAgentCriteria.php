<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAllAgentCriteria extends BaseCriteria
{
    private $_keyword;

    /**
     * GetAllAgentCriteria constructor.
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->_keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['agents.*', 'users.email as email', 'countries.name as country', 'states.name as state'])
            ->join('users', 'agents.user_id', '=', 'users.id')
            ->join('countries', 'agents.country_id', '=', 'countries.id')
            ->join('states', 'agents.state_id', '=', 'states.id');

        if (!empty($this->_keyword)) {
            $model = $model->where('agents.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('agents.gender', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('agents.contact', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('agents.city', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('agents.zip', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('agents.phone', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('users.email', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('countries.name', 'LIKE', '%' . $this->_keyword . '%')
                ->orWhere('states.name', 'LIKE', '%' . $this->_keyword . '%');
        }

        return $model;
    }
}