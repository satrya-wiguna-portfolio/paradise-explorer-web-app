<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetDetailAgentCriteria extends BaseCriteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(['agents.*', 'agent_types.name as agent_type', 'users.email as email', 'countries.name as country', 'states.name as state'])
            ->join('agent_types', 'agents.agent_type_id', '=', 'agent_types.id')
            ->join('users', 'agents.user_id', '=', 'users.id')
            ->join('countries', 'agents.country_id', '=', 'countries.id')
            ->join('states', 'agents.state_id', '=', 'states.id');

        return $model;
    }
}