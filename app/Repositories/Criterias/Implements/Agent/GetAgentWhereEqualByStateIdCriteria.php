<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAgentWhereEqualByStateIdCriteria extends BaseCriteria
{
    private $_state_id;

    /**
     * GetAgentWhereEqualByStateIdCriteria constructor.
     * @param $stateId
     */
    public function __construct($stateId)
    {
        $this->_state_id = $stateId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_state_id)) {
            $model = $model->where('agents.state_id', $this->_state_id);
        }

        return $model;
    }
}