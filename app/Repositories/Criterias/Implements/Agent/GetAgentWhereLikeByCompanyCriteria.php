<?php
namespace App\Repositories\Criterias\Implement\Agent;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetAgentWhereLikeByCompanyCriteria extends BaseCriteria
{
    private $_company;

    /**
     * GetAgentWhereLikeByCompanyCriteria constructor.
     * @param $company
     */
    public function __construct($company)
    {
        $this->_company = $company;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (!empty($this->_company)) {
            $model = $model->where('company', 'LIKE', '%' . $this->_company . '%');
        }

        return $model;
    }
}