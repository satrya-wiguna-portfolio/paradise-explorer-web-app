<?php
namespace App\Repositories\Criterias\Implement\ReservationProduct;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;

class GetReservationProductWhereEqualByProductTypeIdCriteria extends BaseCriteria
{
    private $_productTypeId;

    /**
     * GetAllProvinceCriteria constructor.
     * @param $productTypeId
     */
    public function __construct($productTypeId)
    {
        $this->_productTypeId = $productTypeId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->where('product_type_id', $this->_productTypeId);

        return $model;
    }
}