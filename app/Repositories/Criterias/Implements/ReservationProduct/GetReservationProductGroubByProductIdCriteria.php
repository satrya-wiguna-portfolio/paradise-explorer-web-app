<?php
namespace App\Repositories\Criterias\Implement\ReservationProduct;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;
use Illuminate\Support\Facades\DB;

class GetReservationProductGroubByProductIdCriteria extends BaseCriteria
{
    /**
     * GetAllProvinceCriteria constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $model = $model->select(DB::raw('product_id, COUNT(*) as quantity'))
            ->groupBy('product_id')
            ->orderBy('quantity', 'desc')
            ->take(6);

        return $model;
    }
}