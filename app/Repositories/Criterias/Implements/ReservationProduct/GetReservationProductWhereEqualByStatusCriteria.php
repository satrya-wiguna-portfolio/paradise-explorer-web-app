<?php
namespace App\Repositories\Criterias\Implement\ReservationProduct;


use App\Repositories\Contract\IRepository as Repository;
use App\Repositories\Criterias\Implement\BaseCriteria;
use Illuminate\Support\Facades\DB;

class GetReservationProductWhereEqualByStatusCriteria extends BaseCriteria
{
    private $_status;

    /**
     * GetAllProvinceCriteria constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->_status = $status;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $status = $this->_status;

        $model = $model->whereIn('reservation_id', function($q) use($status) {
            return $q->select(DB::raw('id FROM reservations'))
                ->where('status', $status);
        });

        return $model;
    }
}