<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductCategory\CreateProductCategoryRequest;
use App\Http\Requests\ProductCategory\UpdateProductCategoryRequest;
use App\Repositories\Contract\IProductCategoryRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductCategoryRepository extends BaseRepository implements IProductCategoryRepository
{
    /**
     * ProductCategoryRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductCategory';
    }

    /**
     * @param CreateProductCategoryRequest $request
     * @return int|mixed
     */
    public function create(CreateProductCategoryRequest $request)
    {
        $productCategory = $this->_model;

        $productCategory->product_type_id = $request->getProductTypeId();
        $productCategory->category = $request->getCategory();
        $productCategory->description = $request->getDescription();
        $productCategory->created_at = Carbon::now();

        return $productCategory->save() ? $productCategory->id : 0;
    }

    /**
     * @param UpdateProductCategoryRequest $request
     * @return int
     */
    public function update(UpdateProductCategoryRequest $request)
    {
        $productCategory = $this->_model->find($request->getId());

        $productCategory->product_type_id = $request->getProductTypeId();
        $productCategory->category = $request->getCategory();
        $productCategory->description = $request->getDescription();
        $productCategory->updated_at = Carbon::now();

        return $productCategory->save() ? $productCategory->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productCategory = $this->_model->whereId($id);

        return $productCategory->delete();
    }
}