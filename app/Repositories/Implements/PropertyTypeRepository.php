<?php
namespace App\Repositories\Implement;


use App\Http\Requests\PropertyType\CreatePropertyTypeRequest;
use App\Http\Requests\PropertyType\UpdatePropertyTypeRequest;
use App\Repositories\Contract\IPropertyTypeRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class PropertyTypeRepository extends BaseRepository implements IPropertyTypeRepository
{
    /**
     * PropertyTypeRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\PropertyType';
    }

    /**
     * @param CreatePropertyTypeRequest $request
     * @return int|mixed
     */
    public function create(CreatePropertyTypeRequest $request)
    {
        $role = $this->_model;

        $role->name = $request->getName();
        $role->description = $request->getDescription();
        $role->created_at = Carbon::now();

        return $role->save() ? $role->id : 0;
    }

    /**
     * @param UpdatePropertyTypeRequest $request
     * @return int
     */
    public function update(UpdatePropertyTypeRequest $request)
    {
        $role = $this->_model->find($request->getId());

        $role->name = $request->getName();
        $role->description = $request->getDescription();
        $role->updated_at = Carbon::now();

        return $role->save() ? $role->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $role = $this->_model->whereId($id);

        return $role->delete();
    }
}