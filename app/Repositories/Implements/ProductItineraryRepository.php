<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductItinerary\CreateProductItineraryRequest;
use App\Http\Requests\ProductItinerary\UpdateProductItineraryRequest;
use App\Repositories\Contract\IProductItineraryRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductItineraryRepository extends BaseRepository implements IProductItineraryRepository
{
    /**
     * ItineraryRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Itinerary';
    }

    /**
     * @param CreateProductItineraryRequest $request
     * @return int|mixed
     */
    public function create(CreateProductItineraryRequest $request)
    {
        $productItinerary = $this->_model;

        $productItinerary->product_id = $request->getProductId();
        $productItinerary->title = $request->getTitle();
        $productItinerary->sub_title = $request->getSubTitle();
        $productItinerary->description = $request->getDescription();
        $productItinerary->order = $request->getOrder();
        $productItinerary->created_at = Carbon::now();

        return $productItinerary->save() ? $productItinerary->id : 0;
    }

    /**
     * @param UpdateProductItineraryRequest $request
     * @return int
     */
    public function update(UpdateProductItineraryRequest $request)
    {
        $productItinerary = $this->_model->find($request->getId());

        $productItinerary->product_id = $request->getProductId();
        $productItinerary->title = $request->getTitle();
        $productItinerary->sub_title = $request->getSubTitle();
        $productItinerary->description = $request->getDescription();
        $productItinerary->order = $request->getOrder();
        $productItinerary->updated_at = Carbon::now();

        return $productItinerary->save() ? $productItinerary->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productItinerary = $this->_model->whereId($id);

        return $productItinerary->delete();
    }
}