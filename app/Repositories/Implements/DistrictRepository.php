<?php
namespace App\Repositories\Implement;


use App\Http\Requests\District\CreateDistrictRequest;
use App\Http\Requests\District\UpdateDistrictRequest;
use App\Repositories\Contract\IDistrictRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class DistrictRepository extends BaseRepository implements IDistrictRepository
{
    /**
     * DistrictRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\District';
    }

    /**
     * @param CreateDistrictRequest $request
     * @return int|mixed
     */
    public function create(CreateDistrictRequest $request)
    {
        $district = $this->_model;

        $district->regency_id = $request->getRegencyId();
        $district->name = $request->getName();
        $district->created_at = Carbon::now();

        return $district->save() ? $district->id : 0;
    }

    /**
     * @param UpdateDistrictRequest $request
     * @return int
     */
    public function update(UpdateDistrictRequest $request)
    {
        $district = $this->_model->find($request->getId());

        $district->regency_id = $request->getRegencyId();
        $district->name = $request->getName();
        $district->updated_at = Carbon::now();

        return $district->save() ? $district->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $district = $this->_model->whereId($id);

        return $district->delete();
    }
}