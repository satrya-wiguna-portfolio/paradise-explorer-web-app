<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ReservationDokuResponse\CreateReservationDokuResponseRequest;
use App\Repositories\Contract\IReservationDokuResponseRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReservationDokuResponseRepository extends BaseRepository implements IReservationDokuResponseRepository
{
    /**
     * ReservationDokuResponseRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\ReservationDokuResponse';
    }

    /**
     * @param bool $reset
     * @param CreateReservationDokuResponseRequest $request
     * @return int|mixed
     */
    public function create(CreateReservationDokuResponseRequest $request, $reset = true)
    {
        $reservationDokuResponse = $this->_model;

        $reservationDokuResponse->reservation_id = $request->getReservationId();
        $reservationDokuResponse->created_at = Carbon::now();

        $result = $reservationDokuResponse->save() ? $reservationDokuResponse->id : 0;

        return $result;
    }
}