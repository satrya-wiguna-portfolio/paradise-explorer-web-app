<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Payment\CreatePaymentRequest;
use App\Http\Requests\Payment\UpdatePaymentRequest;
use App\Repositories\Contract\IPaymentRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class PaymentRepository extends BaseRepository implements IPaymentRepository
{
    /**
     * PaymentRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Payment';
    }

    /**
     * @param CreatePaymentRequest $request
     * @return int|mixed
     */
    public function create(CreatePaymentRequest $request)
    {
        $payment = $this->_model;

        $payment->payment = $request->getPayment();
        $payment->description = $request->getDescription();
        $payment->image_url = $request->getImageUrl();
        $payment->created_at = Carbon::now();

        return $payment->save() ? $payment->id : 0;
    }

    /**
     * @param UpdatePaymentRequest $request
     * @return int
     */
    public function update(UpdatePaymentRequest $request)
    {
        $payment = $this->_model->find($request->getId());

        $payment->payment = $request->getPayment();
        $payment->description = $request->getDescription();
        $payment->image_url = $request->getImageUrl();
        $payment->status = $request->getStatus();
        $payment->updated_at = Carbon::now();

        return $payment->save() ? $payment->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $payment = $this->_model->whereId($id);

        return $payment->delete();
    }
}