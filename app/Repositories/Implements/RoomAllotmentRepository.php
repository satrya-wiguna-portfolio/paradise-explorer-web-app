<?php
namespace App\Repositories\Implement;


use App\Http\Requests\RoomAllotment\CreateRoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\UpdateRoomAllotmentRequest;
use App\Repositories\Contract\IRoomAllotmentRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RoomAllotmentRepository extends BaseRepository implements  IRoomAllotmentRepository
{
    /**
     * RoomDiscountRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\RoomAllotment';
    }

    /**
     * @param CreateRoomAllotmentRequest $request
     * @return int|mixed
     */
    public function create(CreateRoomAllotmentRequest $request)
    {
        $roomAllotment = $this->_model;

        $roomAllotment->room_id = $request->getRoomId();
        $roomAllotment->date = $request->getDate();
        $roomAllotment->allotment = $request->getAllotment();
        $roomAllotment->lock = $request->getLock();
        $roomAllotment->sold = $request->getSold();
        $roomAllotment->balance = $request->getBalance();
        $roomAllotment->price = $request->getPrice();
        $roomAllotment->created_at = Carbon::now();

        return $roomAllotment->save() ? $roomAllotment->id : 0;
    }

    /**
     * @param UpdateRoomAllotmentRequest $request
     * @return int
     */
    public function update(UpdateRoomAllotmentRequest $request)
    {
        $roomAllotment = $this->_model->find($request->getId());

        $roomAllotment->room_id = $request->getRoomId();
        $roomAllotment->date = $request->getDate();
        $roomAllotment->allotment = $request->getAllotment();
        $roomAllotment->lock = $request->getLock();
        $roomAllotment->sold = $request->getSold();
        $roomAllotment->balance = $request->getBalance();
        $roomAllotment->price = $request->getPrice();
        $roomAllotment->status = $request->getStatus();
        $roomAllotment->updated_at = Carbon::now();

        return $roomAllotment->save() ? $roomAllotment->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $roomAllotment = $this->_model->whereId($id);

        return $roomAllotment->delete();
    }
}