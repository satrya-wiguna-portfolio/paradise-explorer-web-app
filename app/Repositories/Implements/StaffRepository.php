<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Staff\CreateStaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;
use App\Repositories\Contract\IStaffRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class StaffRepository extends BaseRepository implements IStaffRepository
{
    /**
     * StaffRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Staff';
    }

    /**
     * @param CreateStaffRequest $request
     * @return int|mixed
     */
    public function create(CreateStaffRequest $request)
    {
        $staff = $this->_model;

        $staff->user_id = $request->getUserId();
        $staff->nik = $request->getNik();
        $staff->title = $request->getTitle();
        $staff->first_name = $request->getFirstName();
        $staff->last_name = $request->getLastName();
        $staff->gender = $request->getGender();
        $staff->address = $request->getAddress();
        $staff->province_id = $request->getProvinceId();
        $staff->regency_id = $request->getRegencyId();
        $staff->district_id = $request->getDistrictId();
        $staff->village = $request->getVillage();
        $staff->zip = $request->getZip();
        $staff->image_url = $request->getImageUrl();
        $staff->phone = $request->getPhone();
        $staff->created_at = Carbon::now();

        return $staff->save() ? $staff->id : 0;
    }

    /**
     * @param UpdateStaffRequest $request
     * @return int
     */
    public function update(UpdateStaffRequest $request)
    {
        $staff = $this->_model->find($request->getId());

        $staff->user_id = $request->getUserId();
        $staff->nik = $request->getNik();
        $staff->title = $request->getTitle();
        $staff->first_name = $request->getFirstName();
        $staff->last_name = $request->getLastName();
        $staff->gender = $request->getGender();
        $staff->address = $request->getAddress();
        $staff->province_id = $request->getProvinceId();
        $staff->regency_id = $request->getRegencyId();
        $staff->district_id = $request->getDistrictId();
        $staff->village = $request->getVillage();
        $staff->zip = $request->getZip();
        $staff->image_url = $request->getImageUrl();
        $staff->phone = $request->getPhone();
        $staff->updated_at = Carbon::now();

        return $staff->save() ? $staff->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $staff = $this->_model->whereId($id);

        return $staff->delete();
    }
}