<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductImage\CreateProductImageRequest;
use App\Http\Requests\ProductImage\UpdateProductImageRequest;
use App\Repositories\Contract\IProductImageRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductImageRepository extends BaseRepository implements IProductImageRepository
{
    /**
     * ProductImageRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductImage';
    }

    /**
     * @param CreateProductImageRequest $request
     * @return int|mixed
     */
    public function create(CreateProductImageRequest $request)
    {
        $productImage = $this->_model;

        $productImage->product_id = $request->getProductId();
        $productImage->url = $request->getUrl();
        $productImage->title = $request->getTitle();
        $productImage->caption = $request->getCaption();
        $productImage->alt = $request->getAlt();
        $productImage->description = $request->getDescription();
        $productImage->created_at = Carbon::now();

        return $productImage->save() ? $productImage->id : 0;
    }

    /**
     * @param UpdateProductImageRequest $request
     * @return int
     */
    public function update(UpdateProductImageRequest $request)
    {
        $productImage = $this->_model->find($request->getId());

        $productImage->product_id = $request->getProductId();
        $productImage->url = $request->getUrl();
        $productImage->title = $request->getTitle();
        $productImage->caption = $request->getCaption();
        $productImage->alt = $request->getAlt();
        $productImage->description = $request->getDescription();
        $productImage->updated_at = Carbon::now();

        return $productImage->save() ? $productImage->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productImage = $this->_model->whereId($id);

        return $productImage->delete();
    }
}