<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Agent\CreateAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;
use App\Repositories\Contract\IAgentRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class AgentRepository extends BaseRepository implements IAgentRepository
{
    /**
     * AgentRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Agent';
    }

    /**
     * @param CreateAgentRequest $request
     * @return int|mixed
     */
    public function create(CreateAgentRequest $request)
    {
        $agent = $this->_model;

        $agent->user_id = $request->getUserId();
        $agent->agent_type_id = $request->getAgentTypeId();
        $agent->title = $request->getTitle();
        $agent->first_name = $request->getFirstName();
        $agent->last_name = $request->getLastName();
        $agent->gender = $request->getGender();
        $agent->company = $request->getCompany();
        $agent->address = $request->getAddress();
        $agent->country_id = $request->getCountryId();
        $agent->state_id = $request->getStateId();
        $agent->city = $request->getCity();
        $agent->zip = $request->getZip();
        $agent->image_url = $request->getImageUrl();
        $agent->phone = $request->getPhone();
        $agent->fax = $request->getFax();
        $agent->created_at = Carbon::now();

        return $agent->save() ? $agent->id : 0;
    }

    /**
     * @param UpdateAgentRequest $request
     * @return int
     */
    public function update(UpdateAgentRequest $request)
    {
        $agent = $this->_model->find($request->getId());

        $agent->user_id = $request->getUserId();
        $agent->agent_type_id = $request->getAgentTypeId();
        $agent->title = $request->getTitle();
        $agent->first_name = $request->getFirstName();
        $agent->last_name = $request->getLastName();
        $agent->gender = $request->getGender();
        $agent->company = $request->getCompany();
        $agent->address = $request->getAddress();
        $agent->country_id = $request->getCountryId();
        $agent->state_id = $request->getStateId();
        $agent->city = $request->getCity();
        $agent->zip = $request->getZip();
        $agent->image_url = $request->getImageUrl();
        $agent->phone = $request->getPhone();
        $agent->fax = $request->getFax();
        $agent->updated_at = Carbon::now();

        return $agent->save() ? $agent->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $agent = $this->_model->whereId($id);

        return $agent->delete();
    }

}