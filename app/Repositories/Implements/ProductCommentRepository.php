<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductComment\CreateProductCommentRequest;
use App\Http\Requests\ProductComment\UpdateProductCommentRequest;
use App\Repositories\Contract\IProductCommentRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class ProductCommentRepository extends BaseRepository implements IProductCommentRepository
{
    /**
     * ProductCommentRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductComment';
    }

    /**
     * @param CreateProductCommentRequest $request
     * @return int|mixed
     */
    public function create(CreateProductCommentRequest $request)
    {
        $productComment = $this->_model;

        $productComment->blog_id = $request->getProductId();
        $productComment->user_id = $request->getUserId();
        $productComment->comment = $request->getComment();
        $productComment->rate = $request->getRate();
        $productComment->ip_address = $request->getIpAddress();
        $productComment->browser = $request->getBrowser();
        $productComment->approved = $request->getStatus();
        $productComment->created_at = Carbon::now();

        return $productComment->save() ? $productComment->id : 0;
    }

    /**
     * @param UpdateProductCommentRequest $request
     * @return int
     */
    public function update(UpdateProductCommentRequest $request)
    {
        $productComment = $this->_model->find($request->getId());

        $productComment->approved = $request->getStatus();
        $productComment->updated_at = Carbon::now();

        return $productComment->save() ? $productComment->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productComment = $this->_model->whereId($id);

        return $productComment->delete();
    }

}