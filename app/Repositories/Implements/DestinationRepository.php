<?php
namespace App\Repositories\Implement;


use App\Http\Requests\BlogCategory\CreateDestinationRequest;
use App\Http\Requests\BlogCategory\UpdateDestinationRequest;
use App\Repositories\Contract\IDestinationRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class DestinationRepository extends BaseRepository implements IDestinationRepository
{
    /**
     * DestinationRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Destination';
    }

    /**
     * @param CreateDestinationRequest $request
     * @return int|mixed
     */
    public function create(CreateDestinationRequest $request)
    {
        $destination = $this->_model;

        $destination->parent_id = $request->getParentId();
        $destination->name = $request->getName();
        $destination->image_url = $request->getImageUrl();
        $destination->lat = $request->getLat();
        $destination->lng = $request->getLng();
        $destination->description = $request->getDescription();
        $destination->created_at = Carbon::now();

        return $destination->save() ? $destination->id : 0;
    }

    /**
     * @param UpdateDestinationRequest $request
     * @return int
     */
    public function update(UpdateDestinationRequest $request)
    {
        $destination = $this->_model->find($request->getId());

        $destination->parent_id = $request->getParentId();
        $destination->name = $request->getName();
        $destination->image_url = $request->getImageUrl();
        $destination->lat = $request->getLat();
        $destination->lng = $request->getLng();
        $destination->description = $request->getDescription();
        $destination->updated_at = Carbon::now();

        return $destination->save() ? $destination->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $destination = $this->_model->whereId($id);

        return $destination->delete();
    }
}