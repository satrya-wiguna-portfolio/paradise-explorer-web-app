<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ReservationProduct\CreateReservationProductRequest;
use App\Repositories\Contract\IReservationProductRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReservationProductRepository extends BaseRepository implements IReservationProductRepository
{
    /**
     * ReservationRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\ReservationProduct';
    }

    /**
     * @param bool $reset
     * @param CreateReservationProductRequest $request
     * @return int|mixed
     */
    public function create(CreateReservationProductRequest $request, $reset = true)
    {
        $reservationProduct = $this->_model;

        $reservationProduct->reservation_id = $request->getReservationId();
        $reservationProduct->product_id = $request->getProductId();
        $reservationProduct->title = $request->getTitle();
        $reservationProduct->agent_id = $request->getAgentId();
        $reservationProduct->product_type_id = $request->getProductTypeId();
        $reservationProduct->price = $request->getPrice();
        $reservationProduct->discount = $request->getDiscount();
        $reservationProduct->product_discount = $request->getProductDiscount();
        $reservationProduct->quantity = $request->getQuantity();
        $reservationProduct->total = $request->getTotal();
        $reservationProduct->check_in = $request->getCheckIn();
        $reservationProduct->check_out = $request->getCheckOut();
        $reservationProduct->departure = $request->getDeparture();
        $reservationProduct->adult = $request->getAdult();
        $reservationProduct->child = $request->getChild();

        $reservationProduct->created_at = Carbon::now();

        if ($reservationProduct->save()) {
            if ($reset)
                $this->resetModel();

            return $reservationProduct->id;
        } else {
            return 0;
        }
    }
}