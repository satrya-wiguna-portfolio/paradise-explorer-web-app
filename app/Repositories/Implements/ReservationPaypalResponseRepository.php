<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;
use App\Repositories\Contract\IReservationPaypalResponseRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReservationPaypalResponseRepository extends BaseRepository implements IReservationPaypalResponseRepository
{
    /**
     * ReservationPaypalResponseRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\ReservationPaypalResponse';
    }

    /**
     * @param CreateReservationPaypalResponseRequest $request
     * @return int|mixed
     */
    public function create(CreateReservationPaypalResponseRequest $request)
    {
        $reservationPaypalResponse = $this->_model;

        $reservationPaypalResponse->reservation_id = $request->getReservationId();
        $reservationPaypalResponse->payment_id = $request->getPaymentId();
        $reservationPaypalResponse->payer_id = $request->getPayerId();
        $reservationPaypalResponse->token = $request->getToken();
        $reservationPaypalResponse->intent = $request->getIntent();
        $reservationPaypalResponse->state = $request->getState();
        $reservationPaypalResponse->cart = $request->getCart();
        $reservationPaypalResponse->created_at = Carbon::now();

        $result = $reservationPaypalResponse->save() ? $reservationPaypalResponse->id : 0;

        return $result;
    }
}