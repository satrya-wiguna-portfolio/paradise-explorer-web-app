<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Province\CreateProvinceRequest;
use App\Http\Requests\Province\UpdateProvinceRequest;
use App\Repositories\Contract\IProvinceRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProvinceRepository extends BaseRepository implements IProvinceRepository
{
    /**
     * ProvinceRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Province';
    }

    /**
     * @param CreateProvinceRequest $request
     * @return int|mixed
     */
    public function create(CreateProvinceRequest $request)
    {
        $province = $this->_model;

        $province->name = $request->getName();
        $province->created_at = Carbon::now();

        return $province->save() ? $province->id : 0;
    }

    /**
     * @param UpdateProvinceRequest $request
     * @return int
     */
    public function update(UpdateProvinceRequest $request)
    {
        $province = $this->_model->find($request->getId());

        $province->name = $request->getName();
        $province->updated_at = Carbon::now();

        return $province->save() ? $province->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $province = $this->_model->whereId($id);

        return $province->delete();
    }
}