<?php
namespace App\Repositories\Implement;


use App\Http\Requests\RoomDiscount\CreateRoomDiscountRequest;
use App\Http\Requests\RoomDiscount\UpdateRoomDiscountRequest;
use App\Repositories\Contract\IRoomDiscountRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RoomDiscountRepository extends BaseRepository implements IRoomDiscountRepository
{
    /**
     * RoomDiscountRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\RoomDiscount';
    }

    /**
     * @param CreateRoomDiscountRequest $request
     * @return int|mixed
     */
    public function create(CreateRoomDiscountRequest $request)
    {
        $roomDiscount = $this->_model;

        $roomDiscount->room_id = $request->getRoomId();
        $roomDiscount->open_date = $request->getOpenDate();
        $roomDiscount->close_date = $request->getCloseDate();
        $roomDiscount->discount = $request->getDiscount();
        $roomDiscount->status = $request->getStatus();
        $roomDiscount->created_at = Carbon::now();

        return $roomDiscount->save() ? $roomDiscount->id : 0;
    }

    /**
     * @param UpdateRoomDiscountRequest $request
     * @return int
     */
    public function update(UpdateRoomDiscountRequest $request)
    {
        $roomDiscount = $this->_model->find($request->getId());

        $roomDiscount->room_id = $request->getRoomId();
        $roomDiscount->open_date = $request->getOpenDate();
        $roomDiscount->close_date = $request->getCloseDate();
        $roomDiscount->discount = $request->getDiscount();
        $roomDiscount->status = $request->getStatus();
        $roomDiscount->updated_at = Carbon::now();

        return $roomDiscount->save() ? $roomDiscount->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $roomDiscount = $this->_model->whereId($id);

        return $roomDiscount->delete();
    }

}