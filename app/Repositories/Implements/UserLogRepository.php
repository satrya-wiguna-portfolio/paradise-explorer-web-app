<?php
namespace App\Repositories\Implement;


use App\Http\Requests\UserLog\CreateUserLogRequest;
use App\Http\Requests\UserLog\UpdateUserLogRequest;
use App\Repositories\Contract\IUserLogRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class UserLogRepository extends BaseRepository implements IUserLogRepository
{
    /**
     * UserLogRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\UserLog';
    }

    /**
     * @param CreateUserLogRequest $request
     * @return int|mixed
     */
    public function create(CreateUserLogRequest $request)
    {
        $userLog = $this->_model;

        $userLog->user_id = $request->getUserId();
        $userLog->log = $request->getLog();
        $userLog->ip_address = $request->getIpAddress();
        $userLog->browser = $request->getBrowser();
        $userLog->created_at = Carbon::now();

        return $userLog->save() ? $userLog->id : 0;
    }

    /**
     * @param UpdateUserLogRequest $request
     * @return int
     */
    public function update(UpdateUserLogRequest $request)
    {
        $userLog = $this->_model->find($request->getId());

        $userLog->user_id = $request->getUserId();
        $userLog->log = $request->getLog();
        $userLog->ip_address = $request->getIpAddress();
        $userLog->browser = $request->getBrowser();
        $userLog->updated_at = Carbon::now();

        return $userLog->save() ? $userLog->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $userLog = $this->_model->whereId($id);

        return $userLog->delete();
    }
}