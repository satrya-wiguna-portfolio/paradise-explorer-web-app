<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductInclude\CreateProductIncludeRequest;
use App\Http\Requests\ProductInclude\UpdateProductIncludeRequest;
use App\Repositories\Contract\IProductIncludeRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductIncludeRepository extends BaseRepository implements IProductIncludeRepository
{
    /**
     * ProductIncludeRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductInclude';
    }

    /**
     * @param CreateProductIncludeRequest $request
     * @return int|mixed
     */
    public function create(CreateProductIncludeRequest $request)
    {
        $productInclude = $this->_model;

        $productInclude->product_id = $request->getProductId();
        $productInclude->title = $request->getTitle();
        $productInclude->sub_title = $request->getSubTitle();
        $productInclude->description = $request->getDescription();
        $productInclude->order = $request->getOrder();
        $productInclude->created_at = Carbon::now();

        return $productInclude->save() ? $productInclude->id : 0;
    }

    /**
     * @param UpdateProductIncludeRequest $request
     * @return int
     */
    public function update(UpdateProductIncludeRequest $request)
    {
        $productInclude = $this->_model->find($request->getId());

        $productInclude->product_id = $request->getProductId();
        $productInclude->title = $request->getTitle();
        $productInclude->sub_title = $request->getSubTitle();
        $productInclude->description = $request->getDescription();
        $productInclude->order = $request->getOrder();
        $productInclude->updated_at = Carbon::now();

        return $productInclude->save() ? $productInclude->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productInclude = $this->_model->whereId($id);

        return $productInclude->delete();
    }
}