<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Member\CreateMemberRequest;
use App\Http\Requests\Member\UpdateMemberRequest;
use App\Repositories\Contract\IMemberRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class MemberRepository extends BaseRepository implements IMemberRepository
{
    /**
     * MemberRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Member';
    }

    /**
     * @param CreateMemberRequest $request
     * @return int|mixed
     */
    public function create(CreateMemberRequest $request)
    {
        $member = $this->_model;

        $member->user_id = $request->getUserId();
        $member->title = $request->getTitle();
        $member->first_name = $request->getFirstName();
        $member->last_name = $request->getLastName();
        $member->gender = $request->getGender();
        $member->address = $request->getAddress();
        $member->country_id = $request->getCountryId();
        $member->state_id = $request->getStateId();
        $member->city = $request->getCity();
        $member->zip = $request->getZip();
        $member->image_url = $request->getImageUrl();
        $member->phone = $request->getPhone();
        $member->created_at = Carbon::now();

        return $member->save() ? $member->id : 0;
    }

    /**
     * @param UpdateMemberRequest $request
     * @return int
     */
    public function update(UpdateMemberRequest $request)
    {
        $member = $this->_model->find($request->getId());

        $member->user_id = $request->getUserId();
        $member->title = $request->getTitle();
        $member->first_name = $request->getFirstName();
        $member->last_name = $request->getLastName();
        $member->gender = $request->getGender();
        $member->address = $request->getAddress();
        $member->country_id = $request->getCountryId();
        $member->state_id = $request->getStateId();
        $member->city = $request->getCity();
        $member->zip = $request->getZip();
        $member->image_url = $request->getImageUrl();
        $member->phone = $request->getPhone();
        $member->updated_at = Carbon::now();

        return $member->save() ? $member->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $member = $this->_model->whereId($id);

        return $member->delete();
    }
}