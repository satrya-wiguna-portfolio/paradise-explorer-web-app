<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Regency\CreateRegencyRequest;
use App\Http\Requests\Regency\UpdateRegencyRequest;
use App\Repositories\Contract\IRegencyRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RegencyRepository extends BaseRepository implements IRegencyRepository
{
    /**
     * RegencyRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Regency';
    }

    /**
     * @param CreateRegencyRequest $request
     * @return int|mixed
     */
    public function create(CreateRegencyRequest $request)
    {
        $regency = $this->_model;

        $regency->province_id = $request->getProvinceId();
        $regency->name = $request->getName();
        $regency->created_at = Carbon::now();

        return $regency->save() ? $regency->id : 0;
    }

    /**
     * @param UpdateRegencyRequest $request
     * @return int
     */
    public function update(UpdateRegencyRequest $request)
    {
        $regency = $this->_model->find($request->getId());

        $regency->province_id = $request->getProvinceId();
        $regency->name = $request->getName();
        $regency->updated_at = Carbon::now();

        return $regency->save() ? $regency->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $regency = $this->_model->whereId($id);

        return $regency->delete();
    }
}