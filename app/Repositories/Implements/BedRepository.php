<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Bed\CreateBedRequest;
use App\Http\Requests\Bed\UpdateBedRequest;
use App\Repositories\Contract\IBedRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class BedRepository extends BaseRepository implements IBedRepository
{
    /**
     * BedRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Bed';
    }

    /**
     * @param CreateBedRequest $request
     * @return int|mixed
     */
    public function create(CreateBedRequest $request)
    {
        $bed = $this->_model;

        $bed->agent_id = $request->getAgentId();
        $bed->name = $request->getName();
        $bed->description = $request->getDescription();
        $bed->created_at = Carbon::now();

        return $bed->save() ? $bed->id : 0;
    }

    /**
     * @param UpdateBedRequest $request
     * @return int
     */
    public function update(UpdateBedRequest $request)
    {
        $bed = $this->_model->find($request->getId());

        $bed->agent_id = $request->getAgentId();
        $bed->name = $request->getName();
        $bed->description = $request->getDescription();
        $bed->updated_at = Carbon::now();

        return $bed->save() ? $bed->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $bed = $this->_model->whereId($id);

        return $bed->delete();
    }
}