<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ReservationProductRoom\CreateReservationProductRoomRequest;
use App\Repositories\Contract\IReservationProductRoomRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReservationProductRoomRepository extends BaseRepository implements IReservationProductRoomRepository
{
    /**
     * ReservationRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\ReservationProductRoom';
    }

    /**
     * @param bool $reset
     * @param CreateReservationProductRoomRequest $request
     * @return int|mixed
     */
    public function create(CreateReservationProductRoomRequest $request, $reset = true)
    {
        $reservationProductRoom = $this->_model;

        $reservationProductRoom->reservation_product_id = $request->getReservationProductId();
        $reservationProductRoom->room_id = $request->getRoomId();
        $reservationProductRoom->title = $request->getTitle();
        $reservationProductRoom->price = $request->getPrice();
        $reservationProductRoom->room_discount = $request->getRoomDiscount();
        $reservationProductRoom->quantity = $request->getQuantity();
        $reservationProductRoom->total = $request->getTotal();

        $reservationProductRoom->created_at = Carbon::now();

        if ($reservationProductRoom->save()) {
            if ($reset)
                $this->resetModel();

            return $reservationProductRoom->id;
        } else {
            return 0;
        }
    }
}