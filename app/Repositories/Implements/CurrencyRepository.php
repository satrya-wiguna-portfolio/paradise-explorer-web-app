<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Currency\CreateCurrencyRequest;
use App\Http\Requests\Currency\SyncCurrencyRequest;
use App\Http\Requests\Currency\UpdateCurrencyRequest;
use App\Repositories\Contract\ICurrencyRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;

class CurrencyRepository extends BaseRepository implements ICurrencyRepository
{
    /**
     * RoleRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Currency';
    }

    /**
     * @param CreateCurrencyRequest $request
     * @return mixed
     */
    public function create(CreateCurrencyRequest $request)
    {
        Artisan::call('currency:manage', [
            'action' => 'add',
            'currency' => $request->code
        ]);

        return Artisan::output();
    }

    /**
     * @param UpdateCurrencyRequest $request
     * @return int
     */
    public function update(UpdateCurrencyRequest $request)
    {
        $currency = $this->_model->find($request->getId());

        $currency->name = $request->getName();
        $currency->symbol = $request->getSymbol();
        $currency->format = $request->getPattern();
        $currency->exchange_rate = $request->getExchangeRate();
        $currency->active = $request->getActive();
        $currency->updated_at = Carbon::now();

        return $currency->save() ? $currency->id : 0;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function delete($code)
    {
        return Artisan::call('currency:manage', [
            'action' => 'delete',
            'currency' => $code
        ]);
    }

    /**
     * @param SyncCurrencyRequest $request
     * @return mixed
     */
    public function sync(SyncCurrencyRequest $request)
    {
        if (!empty($request->code)) {
            Artisan::call('currency:manage', [
                'action' => 'update',
                'currency' => $request->code,
                '--openexchangerates' => true
            ]);

            return Artisan::output();
        }

        Artisan::call('currency:update', [
            '--openexchangerates' => true
        ]);

        return Artisan::output();
    }

}