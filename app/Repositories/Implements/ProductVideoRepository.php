<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductVideo\CreateProductVideoRequest;
use App\Http\Requests\ProductVideo\UpdateProductVideoRequest;
use App\Repositories\Contract\IProductVideoRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductVideoRepository extends BaseRepository implements IProductVideoRepository
{
    /**
     * ProductVideoRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductVideo';
    }

    /**
     * @param CreateProductVideoRequest $request
     * @return int|mixed
     */
    public function create(CreateProductVideoRequest $request)
    {
        $productVideo = $this->_model;

        $productVideo->product_id = $request->getProductId();
        $productVideo->url = $request->getUri();
        $productVideo->title = $request->getTitle();
        $productVideo->description = $request->getDescription();
        $productVideo->created_at = Carbon::now();

        return $productVideo->save() ? $productVideo->id : 0;
    }

    /**
     * @param UpdateProductVideoRequest $request
     * @return int
     */
    public function update(UpdateProductVideoRequest $request)
    {
        $productVideo = $this->_model->find($request->getId());

        $productVideo->product_id = $request->getProductId();
        $productVideo->url = $request->getUri();
        $productVideo->title = $request->getTitle();
        $productVideo->description = $request->getDescription();
        $productVideo->updated_at = Carbon::now();

        return $productVideo->save() ? $productVideo->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productVideo = $this->_model->whereId($id);

        return $productVideo->delete();
    }
}