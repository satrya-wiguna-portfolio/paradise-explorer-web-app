<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Country\CreateCountryRequest;
use App\Http\Requests\Country\UpdateCountryRequest;
use App\Repositories\Contract\ICountryRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class CountryRepository extends BaseRepository implements ICountryRepository
{
    /**
     * CountryRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Country';
    }

    /**
     * @param CreateCountryRequest $request
     * @return int|mixed
     */
    public function create(CreateCountryRequest $request)
    {
        $country = $this->_model;

        $country->code = $request->getCode();
        $country->name = $request->getName();
        $country->created_at = Carbon::now();

        return $country->save() ? $country->id : 0;
    }

    /**
     * @param UpdateCountryRequest $request
     * @return int
     */
    public function update(UpdateCountryRequest $request)
    {
        $country = $this->_model->find($request->getId());

        $country->code = $request->getCode();
        $country->name = $request->getName();
        $country->updated_at = Carbon::now();

        return $country->save() ? $country->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $country = $this->_model->whereId($id);

        return $country->delete();
    }
}