<?php
namespace App\Repositories\Implement;


use App\Http\Requests\AgentType\CreateAgentTypeRequest;
use App\Http\Requests\AgentType\UpdateAgentTypeRequest;
use App\Repositories\Contract\IAgentTypeRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class AgentTypeRepository extends BaseRepository implements IAgentTypeRepository
{
    /**
     * AgentTypeRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\AgentType';
    }

    /**
     * @param CreateAgentTypeRequest $request
     * @return int|mixed
     */
    public function create(CreateAgentTypeRequest $request)
    {
        $agentType = $this->_model;

        $agentType->name = $request->getName();
        $agentType->description = $request->getDescription();
        $agentType->created_at = Carbon::now();

        return $agentType->save() ? $agentType->id : 0;
    }

    /**
     * @param UpdateAgentTypeRequest $request
     * @return int
     */
    public function update(UpdateAgentTypeRequest $request)
    {
        $agentType = $this->_model->find($request->getId());

        $agentType->name = $request->getName();
        $agentType->description = $request->getDescription();
        $agentType->updated_at = Carbon::now();

        return $agentType->save() ? $agentType->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $agentType = $this->_model->whereId($id);

        return $agentType->delete();
    }
}