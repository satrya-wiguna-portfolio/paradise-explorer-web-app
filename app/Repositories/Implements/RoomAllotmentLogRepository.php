<?php
namespace App\Repositories\Implement;


use App\Repositories\Contract\IRoomAllotmentLogRepository;
use Illuminate\Support\Collection;

class RoomAllotmentLogRepository extends BaseRepository implements IRoomAllotmentLogRepository
{
    /**
     * RoomAllotmentLogRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\RoomAllotmentLog';
    }
}