<?php
/**
 * Created by PhpStorm.
 * User: satryawiguna
 * Date: 1/3/18
 * Time: 3:04 PM
 */

namespace App\Repositories\Implement;


use App\Http\Requests\Room\CreatePriceDetailRequest;
use App\Http\Requests\Room\UpdatePriceDetailRequest;
use App\Repositories\Contract\IPriceDetailRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class PriceDetailRepository extends BaseRepository implements IPriceDetailRepository
{
    /**
     * RoomRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\PriceDetail';
    }

    public function create(CreatePriceDetailRequest $request)
    {
        $room = $this->_model;

        $room->product_id = $request->getProductId();
        $room->room_id = $request->getRoomId();
        $room->price_adult = $request->getPriceAdult();
        $room->price_child = $request->getPriceChild();
        $room->price_infant = $request->getPriceChild();
        $room->created_at = Carbon::now();

        $result =  $room->save() ? $room->id : 0;
        
        return $result;
    }

    public function update(UpdatePriceDetailRequest $request)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }


}