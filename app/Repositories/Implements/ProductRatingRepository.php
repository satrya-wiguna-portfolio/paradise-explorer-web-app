<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductRating\CreateProductRatingRequest;
use App\Http\Requests\ProductRating\UpdateProductRatingRequest;
use App\Repositories\Contract\IProductRatingRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductRatingRepository extends BaseRepository implements IProductRatingRepository
{
    /**
     * ProductRatingRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_model = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductRating';
    }

    /**
     * @param CreateProductRatingRequest $request
     * @return int|mixed
     */
    public function create(CreateProductRatingRequest $request)
    {
        $productRating = $this->_model;

        $productRating->product_id = $request->getProductId();
        $productRating->user_id = $request->getUserId();
        $productRating->rate = $request->getRate();
        $productRating->comment = $request->getComment();
        $productRating->created_at = Carbon::now();

        return $productRating->save() ? $productRating->id : 0;
    }

    /**
     * @param UpdateProductRatingRequest $request
     * @return int
     */
    public function update(UpdateProductRatingRequest $request)
    {
        $productRating = $this->_model->find($request->getId());

        $productRating->product_id = $request->getProductId();
        $productRating->user_id = $request->getUserId();
        $productRating->rate = $request->getRate();
        $productRating->comment = $request->getComment();
        $productRating->updated_at = Carbon::now();

        return $productRating->save() ? $productRating->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productRating = $this->_model->whereId($id);

        return $productRating->delete();
    }
}