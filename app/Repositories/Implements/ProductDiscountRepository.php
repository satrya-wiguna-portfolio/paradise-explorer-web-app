<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductDiscount\CreateProductDiscountRequest;
use App\Http\Requests\ProductDiscount\UpdateProductDiscountRequest;
use App\Repositories\Contract\IProductDiscountRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductDiscountRepository extends BaseRepository implements IProductDiscountRepository
{
    /**
     * ProductDiscountRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductDiscount';
    }

    /**
     * @param CreateProductDiscountRequest $request
     * @return int|mixed
     */
    public function create(CreateProductDiscountRequest $request)
    {
        $productDiscount = $this->_model;

        $productDiscount->product_id = $request->getProductId();
        $productDiscount->open_date = $request->getOpenDate();
        $productDiscount->close_date = $request->getCloseDate();
        $productDiscount->discount = $request->getDiscount();
        $productDiscount->status = $request->getStatus();
        $productDiscount->created_at = Carbon::now();

        return $productDiscount->save() ? $productDiscount->id : 0;
    }

    /**
     * @param UpdateProductDiscountRequest $request
     * @return int
     */
    public function update(UpdateProductDiscountRequest $request)
    {
        $productDiscount = $this->_model->find($request->getId());

        $productDiscount->product_id = $request->getProductId();
        $productDiscount->open_date = $request->getOpenDate();
        $productDiscount->close_date = $request->getCloseDate();
        $productDiscount->discount = $request->getDiscount();
        $productDiscount->status = $request->getStatus();
        $productDiscount->updated_at = Carbon::now();

        return $productDiscount->save() ? $productDiscount->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productDiscount = $this->_model->whereId($id);

        return $productDiscount->delete();
    }
}