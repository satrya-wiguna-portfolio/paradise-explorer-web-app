<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Room\CreateRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Repositories\Contract\IRoomRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RoomRepository extends BaseRepository implements IRoomRepository
{
    /**
     * RoomRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Room';
    }

    /**
     * @param CreateRoomRequest $request
     * @return int|mixed
     */
    public function create(CreateRoomRequest $request)
    {
        $room = $this->_model;

        $room->product_id = $request->getProductId();
        $room->title = $request->getTitle();
        $room->description = $request->getDescription();
        $room->adult = $request->getAdult();
        $room->children = $request->getChildren();
        $room->max = $request->getMax();
        $room->footage = $request->getFootage();
        $room->price = $request->getPrice();
        $room->option = $request->getOption();
        $room->policy = $request->getPolicy();
        $room->created_at = Carbon::now();

        $result =  $room->save() ? $room->id : 0;

        $room->bed()->attach($request->getBed());
        $room->facility()->attach($request->getRoomFacility());

        $room->room_image()->createMany($request->getRoomImage());

        return $result;
    }

    /**
     * @param UpdateRoomRequest $request
     * @return int
     */
    public function update(UpdateRoomRequest $request)
    {
        $room = $this->_model->find($request->getId());

        $room->product_id = $request->getProductId();
        $room->title = $request->getTitle();
        $room->description = $request->getDescription();
        $room->adult = $request->getAdult();
        $room->children = $request->getChildren();
        $room->max = $request->getMax();
        $room->footage = $request->getFootage();
        $room->price = $request->getPrice();
        $room->option = $request->getOption();
        $room->policy = $request->getPolicy();
        $room->updated_at = Carbon::now();

        $result = $room->save() ? $room->id : 0;

        $room->bed()->sync($request->getBed());
        $room->facility()->sync($request->getRoomFacility());

        $room->sync_room_image($request->getRoomImage());

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $room = $this->_model->findOrFail($id);

        $room->bed()->detach();
        $room->facility()->detach();

        return $room->delete();
    }
}