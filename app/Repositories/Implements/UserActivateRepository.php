<?php
namespace App\Repositories\Implement;


use App\Http\Requests\UserLog\CreateUserActivateRequest;
use App\Http\Requests\UserLog\UpdateUserActivateRequest;
use App\Repositories\Contract\IUserActivateRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class UserActivateRepository extends BaseRepository implements IUserActivateRepository
{
    /**
     * UserLogRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\UserActivate';
    }

    /**
     * @param CreateUserActivateRequest $request
     * @return int|mixed
     */
    public function create(CreateUserActivateRequest $request)
    {
        $userActivate = $this->_model;

        $userActivate->email = $request->getEmail();
        $userActivate->token = $request->getToken();
        $userActivate->created_at = Carbon::now();

        return $userActivate->save() ? $userActivate->token : null;
    }

    /**
     * @param UpdateUserActivateRequest $request
     * @return int
     */
    public function update(UpdateUserActivateRequest $request)
    {
        $userActivate = $this->_model->where('email', $request->getEmail())->first();

        $userActivate->token = $request->getToken();
        $userActivate->created_at = Carbon::now();

        return $userActivate->save() ? $userActivate->token : null;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function delete($email)
    {
        $userActivate = $this->_model->where('email', $email);

        return $userActivate->delete();
    }
}