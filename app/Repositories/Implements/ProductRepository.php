<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Repositories\Contract\IProductRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductRepository extends BaseRepository implements IProductRepository
{
    /**
     * ProductRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Product';
    }

    /**
     * @param CreateProductRequest $request
     * @return int|mixed
     */
    public function create(CreateProductRequest $request)
    {
        $product = $this->_model;

        $product->product_type_id = $request->getProductTypeId();
        $product->product_category_id = $request->getProductCategoryId();
        $product->property_type_id = $request->getPropertyTypeId();
        $product->agent_id = $request->getAgentId();
        $product->publish = $request->getPublish();
        $product->title = $request->getTitle();
        $product->slug = $request->getSlug();
        $product->overview = $request->getOverview();
        $product->description = $request->getDescription();
        $product->featured_image_url = $request->getFeaturedImageUrl();
        $product->featured_video_url = $request->getFeaturedVideoUrl();
        $product->include_brochure_url = $request->getIncludeBrochureUrl();
        $product->itinerary_brochure_url = $request->getItineraryBrochureUrl();
        $product->lat = $request->getLat();
        $product->lng = $request->getLng();
        $product->address = $request->getAddress();
        $product->duration = $request->getDuration();
        $product->min_age = $request->getMinAge();
        $product->max_age = $request->getMaxAge();
        $product->price = $request->getPrice();
        $product->grade = $request->getGrade();
        $product->discount = $request->getDiscount();
        $product->status = $request->getStatus();
        $product->created_at = Carbon::now();

        $result = $product->save() ? $product->id : 0;

        $product->product_tag()->attach($request->getProductTag());
        $product->facility()->attach($request->getProductFacility());

        $product->product_image()->createMany($request->getProductImage());
        $product->product_video()->createMany($request->getProductVideo());
        $product->product_way_point()->createMany($request->getProductWayPoint());
        $product->product_include()->createMany($request->getProductInclude());
        $product->product_itinerary()->createMany($request->getProductItinerary());

        return $result;
    }

    /**
     * @param UpdateProductRequest $request
     * @return int
     */
    public function update(UpdateProductRequest $request)
    {
        $product = $this->_model->find($request->getId());

        $product->product_type_id = $request->getProductTypeId();
        $product->product_category_id = $request->getProductCategoryId();
        $product->property_type_id = $request->getPropertyTypeId();
        $product->agent_id = $request->getAgentId();
        $product->title = $request->getTitle();
        $product->slug = $request->getSlug();
        $product->overview = $request->getOverview();
        $product->description = $request->getDescription();
        $product->featured_image_url = $request->getFeaturedImageUrl();
        $product->featured_video_url = $request->getFeaturedVideoUrl();
        $product->include_brochure_url = $request->getIncludeBrochureUrl();
        $product->itinerary_brochure_url = $request->getItineraryBrochureUrl();
        $product->lat = $request->getLat();
        $product->lng = $request->getLng();
        $product->address = $request->getAddress();
        $product->duration = $request->getDuration();
        $product->min_age = $request->getMinAge();
        $product->max_age = $request->getMaxAge();
        $product->price = $request->getPrice();
        $product->grade = $request->getGrade();
        $product->discount = $request->getDiscount();
        $product->status = $request->getStatus();
        $product->updated_at = Carbon::now();

        $result = $product->save() ? $product->id : 0;

        $product->product_tag()->sync($request->getProductTag());
        $product->facility()->sync($request->getProductFacility());

        $product->sync_product_image($request->getProductImage());
        $product->sync_product_video($request->getProductVideo());
        $product->sync_product_way_point($request->getProductWayPoint());
        $product->sync_product_include($request->getProductInclude());
        $product->sync_product_itinerary($request->getProductItinerary());

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $product = $this->_model->findOrFail($id);

        $product->product_tag()->detach();
        $product->facility()->detach();

        return $product->delete();
    }
}