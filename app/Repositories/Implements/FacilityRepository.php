<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Facility\CreateFacilityRequest;
use App\Http\Requests\Facility\UpdateFacilityRequest;
use App\Repositories\Contract\IFacilityRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class FacilityRepository extends BaseRepository implements IFacilityRepository
{
    /**
     * FacilityRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\Facility';
    }

    /**
     * @param CreateFacilityRequest $request
     * @return int|mixed
     */
    public function create(CreateFacilityRequest $request)
    {
        $facility = $this->_model;

        $facility->facility = $request->getFacility();
        $facility->image_url = $request->getImageUrl();
        $facility->created_at = Carbon::now();

        return $facility->save() ? $facility->id : 0;
    }

    /**
     * @param UpdateFacilityRequest $request
     * @return int
     */
    public function update(UpdateFacilityRequest $request)
    {
        $facility = $this->_model->find($request->getId());

        $facility->facility = $request->getFacility();
        $facility->image_url = $request->getImageUrl();
        $facility->updated_at = Carbon::now();

        return $facility->save() ? $facility->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $facility = $this->_model->whereId($id);

        return $facility->delete();
    }
}