<?php
namespace App\Repositories\Implement;


use App\Http\Requests\State\CreateStateRequest;
use App\Http\Requests\State\UpdateStateRequest;
use App\Repositories\Contract\IStateRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class StateRepository extends BaseRepository implements IStateRepository
{
    /**
     * StateRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\State';
    }

    /**
     * @param CreateStateRequest $request
     * @return int|mixed
     */
    public function create(CreateStateRequest $request)
    {
        $state = $this->_model;

        $state->country_id = $request->getCountryId();
        $state->name = $request->getName();
        $state->created_at = Carbon::now();

        return $state->save() ? $state->id : 0;
    }

    /**
     * @param UpdateStateRequest $request
     * @return int
     */
    public function update(UpdateStateRequest $request)
    {
        $state = $this->_model->find($request->getId());

        $state->country_id = $request->getCountryId();
        $state->name = $request->getName();
        $state->updated_at = Carbon::now();

        return $state->save() ? $state->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $state = $this->_model->whereId($id);

        return $state->delete();
    }
}