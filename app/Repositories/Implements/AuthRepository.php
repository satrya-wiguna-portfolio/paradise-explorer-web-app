<?php
namespace App\Repositories\Implement;


use App\Repositories\Contract\IAuthRepository;

class AuthRepository extends BaseRepository implements IAuthRepository
{
    /**
     * AuthRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\User';
    }

    /**
     * @param array $data
     * @param $role_id
     * @return static
     */
    public function signup(array $data, $role_id)
    {
        $this->_model->unguard();

        $user = $this->_model->create($data);
        $user->role()->attach([$role_id]);

        $this->_model->reguard();

        return $user;
    }

}