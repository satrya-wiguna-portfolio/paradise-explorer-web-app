<?php
namespace App\Repositories\Implement;


use App\Http\Requests\Reservation\CreateReservationRequest;
use App\Http\Requests\Reservation\UpdateReservationRequest;
use App\Repositories\Contract\IReservationRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReservationRepository extends BaseRepository implements IReservationRepository
{
    /**
     * ReservationRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Reservation';
    }

    /**
     * @param bool $reset
     * @param CreateReservationRequest $request
     * @return int|mixed
     */
    public function create(CreateReservationRequest $request, $reset = true)
    {
        $reservation = $this->_model;

        $reservation->invoice_number = $request->getInvoiceNumber();
        $reservation->invoice_release_date = $request->getInvoiceReleaseDate();
        $reservation->user_id = $request->getUserId();
        $reservation->title = $request->getTitle();
        $reservation->first_name = $request->getFirstname();
        $reservation->last_name = $request->getLastName();
        $reservation->gender = $request->getGender();
        $reservation->address = $request->getAddress();
        $reservation->country_id = $request->getCountryId();
        $reservation->state_id = $request->getStateId();
        $reservation->city = $request->getCity();
        $reservation->zip = $request->getZip();
        $reservation->phone = $request->getPhone();
        $reservation->handphone = $request->getHandphone();
        $reservation->payment_id = $request->getPaymentId();
        $reservation->currency_id = $request->getCurrencyId();
        $reservation->exchange_rate = $request->getExchangeRate();

        $reservation->created_at = Carbon::now();

        if ($reservation->save()) {
            if ($reset)
                $this->resetModel();

            $result = $reservation->id;
        } else {
            $result = 0;
        }

        return $result;
    }

    /**
     * @param UpdateReservationRequest $request
     * @param bool $reset
     * @return int
     */
    public function updateStatus(UpdateReservationRequest $request, $reset = true)
    {
        $reservation = $this->_model->find($request->getId());

        if ($request->getStatus() == 2) {
            $reservation->invoice_paid_date = $request->getInvoicePaidDate();
        }

        $reservation->status = $request->getStatus();
        $reservation->updated_at = Carbon::now();

        if ($reservation->save()) {
            if ($reset)
                $this->resetModel();

            $result = $reservation->id;
        } else {
            $result = 0;
        }

        return $result;
    }
}