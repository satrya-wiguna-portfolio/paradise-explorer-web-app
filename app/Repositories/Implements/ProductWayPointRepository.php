<?php
namespace App\Repositories\Implement;


use App\Http\Requests\ProductWayPoint\CreateProductWayPointRequest;
use App\Http\Requests\ProductWayPoint\UpdateProductWayPointRequest;
use App\Repositories\Contract\IProductWayPointRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ProductWayPointRepository extends BaseRepository implements IProductWayPointRepository
{
    /**
     * WayPointRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_criteria = new Collection();
    }

    /**
     *
     */
    public function model()
    {
        return 'App\Models\ProductWayPoint';
    }

    /**
     * @param CreateProductWayPointRequest $request
     * @return int|mixed
     */
    public function create(CreateProductWayPointRequest $request)
    {
        $productWayPoint = $this->_model;

        $productWayPoint->product_id = $request->getProductId();
        $productWayPoint->title = $request->getTitle();
        $productWayPoint->address = $request->getAddress();
        $productWayPoint->lat = $request->getLat();
        $productWayPoint->lng = $request->getLng();
        $productWayPoint->order = $request->getOrder();
        $productWayPoint->created_at = Carbon::now();

        return $productWayPoint->save() ? $productWayPoint->id : 0;
    }

    /**
     * @param UpdateProductWayPointRequest $request
     * @return int
     */
    public function update(UpdateProductWayPointRequest $request)
    {
        $productWayPoint = $this->_model->find($request->getId());

        $productWayPoint->product_id = $request->getProductId();
        $productWayPoint->title = $request->getTitle();
        $productWayPoint->address = $request->getAddress();
        $productWayPoint->lat = $request->getLat();
        $productWayPoint->lng = $request->getLng();
        $productWayPoint->order = $request->getOrder();
        $productWayPoint->updated_at = Carbon::now();

        return $productWayPoint->save() ? $productWayPoint->id : 0;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $productWayPoint = $this->_model->whereId($id);

        return $productWayPoint->delete();
    }
}