<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Province\CreateProvinceRequest;
use App\Http\Requests\Province\UpdateProvinceRequest;

interface IProvinceRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProvinceRequest $request
     * @return mixed
     */
    public function create(CreateProvinceRequest $request);

    /**
     * @param UpdateProvinceRequest $request
     * @return mixed
     */
    public function update(UpdateProvinceRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}