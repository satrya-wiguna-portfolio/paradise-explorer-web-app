<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductCategory\CreateProductCategoryRequest;
use App\Http\Requests\ProductCategory\UpdateProductCategoryRequest;

interface IProductCategoryRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductCategoryRequest $request
     * @return mixed
     */
    public function create(CreateProductCategoryRequest $request);

    /**
     * @param UpdateProductCategoryRequest $request
     * @return mixed
     */
    public function update(UpdateProductCategoryRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}