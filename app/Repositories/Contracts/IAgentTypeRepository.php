<?php
namespace App\Repositories\Contract;


use App\Http\Requests\AgentType\CreateAgentTypeRequest;
use App\Http\Requests\AgentType\UpdateAgentTypeRequest;

interface IAgentTypeRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateAgentTypeRequest $request
     * @return mixed
     */
    public function create(CreateAgentTypeRequest $request);

    /**
     * @param UpdateAgentTypeRequest $request
     * @return mixed
     */
    public function update(UpdateAgentTypeRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}