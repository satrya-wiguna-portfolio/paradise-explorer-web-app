<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Regency\CreateRegencyRequest;
use App\Http\Requests\Regency\UpdateRegencyRequest;

interface IRegencyRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateRegencyRequest $request
     * @return mixed
     */
    public function create(CreateRegencyRequest $request);

    /**
     * @param UpdateRegencyRequest $request
     * @return mixed
     */
    public function update(UpdateRegencyRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}