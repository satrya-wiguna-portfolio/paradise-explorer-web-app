<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Currency\CreateCurrencyRequest;
use App\Http\Requests\Currency\SyncCurrencyRequest;
use App\Http\Requests\Currency\UpdateCurrencyRequest;

interface ICurrencyRepository extends IRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateCurrencyRequest $request
     * @return mixed
     */
    public function create(CreateCurrencyRequest $request);

    /**
     * @param UpdateCurrencyRequest $request
     * @return mixed
     */
    public function update(UpdateCurrencyRequest $request);

    /**
     * @param $code
     * @return mixed
     */
    public function delete($code);

    /**
     * @param SyncCurrencyRequest $request
     * @return mixed
     */
    public function sync(SyncCurrencyRequest $request);
}