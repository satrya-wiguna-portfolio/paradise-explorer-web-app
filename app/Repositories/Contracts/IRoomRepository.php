<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Room\CreateRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;

interface IRoomRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateRoomRequest $request
     * @return mixed
     */
    public function create(CreateRoomRequest $request);

    /**
     * @param UpdateRoomRequest $request
     * @return mixed
     */
    public function update(UpdateRoomRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}