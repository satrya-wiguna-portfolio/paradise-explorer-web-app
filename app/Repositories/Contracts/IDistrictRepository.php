<?php
namespace App\Repositories\Contract;


use App\Http\Requests\District\CreateDistrictRequest;
use App\Http\Requests\District\UpdateDistrictRequest;

interface IDistrictRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateDistrictRequest $request
     * @return mixed
     */
    public function create(CreateDistrictRequest $request);

    /**
     * @param UpdateDistrictRequest $request
     * @return mixed
     */
    public function update(UpdateDistrictRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}