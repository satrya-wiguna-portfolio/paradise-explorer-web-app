<?php
namespace App\Repositories\Contract;


interface IAuthRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param array $data
     * @param $role_id
     * @return mixed
     */
    public function signup(array $data, $role_id);
}