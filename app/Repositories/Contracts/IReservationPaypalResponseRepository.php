<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ReservationPaypalResponse\CreateReservationPaypalResponseRequest;

interface IReservationPaypalResponseRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateReservationPaypalResponseRequest $request
     * @return mixed
     */
    public function create(CreateReservationPaypalResponseRequest $request);
}