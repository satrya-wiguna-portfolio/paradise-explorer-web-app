<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Payment\CreatePaymentRequest;
use App\Http\Requests\Payment\UpdatePaymentRequest;

interface IPaymentRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreatePaymentRequest $request
     * @return mixed
     */
    public function create(CreatePaymentRequest $request);

    /**
     * @param UpdatePaymentRequest $request
     * @return mixed
     */
    public function update(UpdatePaymentRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}