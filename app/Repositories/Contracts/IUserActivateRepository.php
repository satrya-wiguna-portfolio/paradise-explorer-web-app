<?php
namespace App\Repositories\Contract;


use App\Http\Requests\UserLog\CreateUserActivateRequest;
use App\Http\Requests\UserLog\UpdateUserActivateRequest;

interface IUserActivateRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateUserActivateRequest $request
     * @return mixed
     */
    public function create(CreateUserActivateRequest $request);

    /**
     * @param UpdateUserActivateRequest $request
     * @return mixed
     */
    public function update(UpdateUserActivateRequest $request);

    /**
     * @param $email
     * @return mixed
     */
    public function delete($email);
}