<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ReservationProduct\CreateReservationProductRequest;

interface IReservationProductRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param bool $reset
     * @param CreateReservationProductRequest $request
     * @return mixed
     */
    public function create(CreateReservationProductRequest $request, $reset = true);
}