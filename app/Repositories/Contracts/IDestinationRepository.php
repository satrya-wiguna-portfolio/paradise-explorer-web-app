<?php
namespace App\Repositories\Contract;


use App\Http\Requests\BlogCategory\CreateDestinationRequest;
use App\Http\Requests\BlogCategory\UpdateDestinationRequest;

interface IDestinationRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateDestinationRequest $request
     * @return mixed
     */
    public function create(CreateDestinationRequest $request);

    /**
     * @param UpdateDestinationRequest $request
     * @return mixed
     */
    public function update(UpdateDestinationRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}