<?php
namespace App\Repositories\Contract;


use App\Http\Requests\UserLog\CreateUserLogRequest;
use App\Http\Requests\UserLog\UpdateUserLogRequest;

interface IUserLogRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateUserLogRequest $request
     * @return mixed
     */
    public function create(CreateUserLogRequest $request);

    /**
     * @param UpdateUserLogRequest $request
     * @return mixed
     */
    public function update(UpdateUserLogRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}