<?php
namespace App\Repositories\Contract;


use App\Http\Requests\RoomAllotment\CreateRoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\RoomAllotmentRequest;
use App\Http\Requests\RoomAllotment\UpdateRoomAllotmentRequest;

interface IRoomAllotmentRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateRoomAllotmentRequest $request
     * @return mixed
     */
    public function create(CreateRoomAllotmentRequest $request);

    /**
     * @param UpdateRoomAllotmentRequest $request
     * @return mixed
     */
    public function update(UpdateRoomAllotmentRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}