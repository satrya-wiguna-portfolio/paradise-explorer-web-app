<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductRating\CreateProductRatingRequest;
use App\Http\Requests\ProductRating\UpdateProductRatingRequest;

interface IProductRatingRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductRatingRequest $request
     * @return mixed
     */
    public function create(CreateProductRatingRequest $request);

    /**
     * @param UpdateProductRatingRequest $request
     * @return mixed
     */
    public function update(UpdateProductRatingRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}