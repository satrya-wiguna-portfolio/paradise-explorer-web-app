<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Staff\CreateStaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;

interface IStaffRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateStaffRequest $request
     * @return mixed
     */
    public function create(CreateStaffRequest $request);

    /**
     * @param UpdateStaffRequest $request
     * @return mixed
     */
    public function update(UpdateStaffRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}