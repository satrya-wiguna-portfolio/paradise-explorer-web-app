<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Reservation\CreateReservationRequest;
use App\Http\Requests\Reservation\UpdateReservationRequest;

interface IReservationRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param bool $reset
     * @param CreateReservationRequest $request
     * @return mixed
     */
    public function create(CreateReservationRequest $request, $reset = true);

    /**
     * @param UpdateReservationRequest $request
     * @param bool $reset
     * @return mixed
     */
    public function updateStatus(UpdateReservationRequest $request, $reset = true);
}