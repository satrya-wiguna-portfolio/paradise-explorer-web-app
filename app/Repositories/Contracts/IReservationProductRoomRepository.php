<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ReservationProductRoom\CreateReservationProductRoomRequest;

interface IReservationProductRoomRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param bool $reset
     * @param CreateReservationProductRoomRequest $request
     * @return mixed
     */
    public function create(CreateReservationProductRoomRequest $request, $reset = true);
}