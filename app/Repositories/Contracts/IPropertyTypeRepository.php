<?php
namespace App\Repositories\Contract;


use App\Http\Requests\PropertyType\CreatePropertyTypeRequest;
use App\Http\Requests\PropertyType\UpdatePropertyTypeRequest;

interface IPropertyTypeRepository extends IRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreatePropertyTypeRequest $request
     * @return mixed
     */
    public function create(CreatePropertyTypeRequest $request);

    /**
     * @param UpdatePropertyTypeRequest $request
     * @return mixed
     */
    public function update(UpdatePropertyTypeRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}