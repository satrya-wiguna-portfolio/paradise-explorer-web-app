<?php
namespace App\Repositories\Contract;


use App\Http\Requests\State\CreateStateRequest;
use App\Http\Requests\State\UpdateStateRequest;

interface IStateRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateStateRequest $request
     * @return mixed
     */
    public function create(CreateStateRequest $request);

    /**
     * @param UpdateStateRequest $request
     * @return mixed
     */
    public function update(UpdateStateRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}