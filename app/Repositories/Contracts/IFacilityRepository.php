<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Facility\CreateFacilityRequest;
use App\Http\Requests\Facility\UpdateFacilityRequest;

interface IFacilityRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateFacilityRequest $request
     * @return mixed
     */
    public function create(CreateFacilityRequest $request);

    /**
     * @param UpdateFacilityRequest $request
     * @return mixed
     */
    public function update(UpdateFacilityRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}