<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductInclude\CreateProductIncludeRequest;
use App\Http\Requests\ProductInclude\UpdateProductIncludeRequest;

interface IProductIncludeRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductIncludeRequest $request
     * @return mixed
     */
    public function create(CreateProductIncludeRequest $request);

    /**
     * @param UpdateProductIncludeRequest $request
     * @return mixed
     */
    public function update(UpdateProductIncludeRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}