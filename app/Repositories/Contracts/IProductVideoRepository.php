<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductVideo\CreateProductVideoRequest;
use App\Http\Requests\ProductVideo\UpdateProductVideoRequest;

interface IProductVideoRepository
{
    /**
     * @return mixed
     */
    public function model();
    
    /**
     * @param CreateProductVideoRequest $request
     * @return mixed
     */
    public function create(CreateProductVideoRequest $request);

    /**
     * @param UpdateProductVideoRequest $request
     * @return mixed
     */
    public function update(UpdateProductVideoRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}