<?php
namespace App\Repositories\Contract;


use App\Http\Requests\BlogTag\CreateBlogTagRequest;
use App\Http\Requests\BlogTag\UpdateBlogTagRequest;

interface IBlogTagRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateBlogTagRequest $request
     * @return mixed
     */
    public function create(CreateBlogTagRequest $request);

    /**
     * @param UpdateBlogTagRequest $request
     * @return mixed
     */
    public function update(UpdateBlogTagRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}