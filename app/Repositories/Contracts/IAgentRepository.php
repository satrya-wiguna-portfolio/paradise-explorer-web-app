<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Agent\CreateAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;

interface IAgentRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateAgentRequest $request
     * @return mixed
     */
    public function create(CreateAgentRequest $request);

    /**
     * @param UpdateAgentRequest $request
     * @return mixed
     */
    public function update(UpdateAgentRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}