<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Member\CreateMemberRequest;
use App\Http\Requests\Member\UpdateMemberRequest;

interface IMemberRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateMemberRequest $request
     * @return mixed
     */
    public function create(CreateMemberRequest $request);

    /**
     * @param UpdateMemberRequest $request
     * @return mixed
     */
    public function update(UpdateMemberRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}