<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductItinerary\CreateProductItineraryRequest;
use App\Http\Requests\ProductItinerary\UpdateProductItineraryRequest;

interface IProductItineraryRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductItineraryRequest $request
     * @return mixed
     */
    public function create(CreateProductItineraryRequest $request);

    /**
     * @param UpdateProductItineraryRequest $request
     * @return mixed
     */
    public function update(UpdateProductItineraryRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}