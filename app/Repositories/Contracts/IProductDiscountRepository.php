<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductDiscount\CreateProductDiscountRequest;
use App\Http\Requests\ProductDiscount\UpdateProductDiscountRequest;

interface IProductDiscountRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductDiscountRequest $request
     * @return mixed
     */
    public function create(CreateProductDiscountRequest $request);

    /**
     * @param UpdateProductDiscountRequest $request
     * @return mixed
     */
    public function update(UpdateProductDiscountRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}   