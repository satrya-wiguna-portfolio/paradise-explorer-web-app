<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ReservationDokuResponse\CreateReservationDokuResponseRequest;

interface IReservationDokuResponseRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param bool $reset
     * @param CreateReservationDokuResponseRequest $request
     * @return mixed
     */
    public function create(CreateReservationDokuResponseRequest $request, $reset = true);
}