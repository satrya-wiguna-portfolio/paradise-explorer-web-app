<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Country\CreateCountryRequest;
use App\Http\Requests\Country\UpdateCountryRequest;

interface ICountryRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateCountryRequest $request
     * @return mixed
     */
    public function create(CreateCountryRequest $request);

    /**
     * @param UpdateCountryRequest $request
     * @return mixed
     */
    public function update(UpdateCountryRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}