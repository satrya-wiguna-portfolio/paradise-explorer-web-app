<?php
namespace App\Repositories\Contract;


use App\Http\Requests\Bed\CreateBedRequest;
use App\Http\Requests\Bed\UpdateBedRequest;

interface IBedRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateBedRequest $request
     * @return mixed
     */
    public function create(CreateBedRequest $request);

    /**
     * @param UpdateBedRequest $request
     * @return mixed
     */
    public function update(UpdateBedRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}