<?php
namespace App\Repositories\Contract;


use App\Http\Requests\RoomDiscount\CreateRoomDiscountRequest;
use App\Http\Requests\RoomDiscount\UpdateRoomDiscountRequest;

interface IRoomDiscountRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateRoomDiscountRequest $request
     * @return mixed
     */
    public function create(CreateRoomDiscountRequest $request);

    /**
     * @param UpdateRoomDiscountRequest $request
     * @return mixed
     */
    public function update(UpdateRoomDiscountRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}