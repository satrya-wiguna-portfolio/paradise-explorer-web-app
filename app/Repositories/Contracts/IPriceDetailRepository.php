<?php
/**
 * Created by PhpStorm.
 * User: satryawiguna
 * Date: 1/3/18
 * Time: 2:40 PM
 */

namespace App\Repositories\Contract;


use App\Http\Requests\Room\CreatePriceDetailRequest;
use App\Http\Requests\Room\UpdatePriceDetailRequest;

interface IPriceDetailRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreatePriceDetailRequest $request
     * @return mixed
     */
    public function create(CreatePriceDetailRequest $request);

    /**
     * @param UpdatePriceDetailRequest $request
     * @return mixed
     */
    public function update(UpdatePriceDetailRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}