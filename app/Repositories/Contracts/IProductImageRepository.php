<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductImage\CreateProductImageRequest;
use App\Http\Requests\ProductImage\UpdateProductImageRequest;

interface IProductImageRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductImageRequest $request
     * @return mixed
     */
    public function create(CreateProductImageRequest $request);

    /**
     * @param UpdateProductImageRequest $request
     * @return mixed
     */
    public function update(UpdateProductImageRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}