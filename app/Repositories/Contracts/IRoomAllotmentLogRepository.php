<?php
namespace App\Repositories\Contract;


interface IRoomAllotmentLogRepository
{
    /**
     * @return mixed
     */
    public function model();

}