<?php
namespace App\Repositories\Contract;


use App\Http\Requests\ProductWayPoint\CreateProductWayPointRequest;
use App\Http\Requests\ProductWayPoint\UpdateProductWayPointRequest;

interface IProductWayPointRepository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param CreateProductWayPointRequest $request
     * @return mixed
     */
    public function create(CreateProductWayPointRequest $request);

    /**
     * @param UpdateProductWayPointRequest $request
     * @return mixed
     */
    public function update(UpdateProductWayPointRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}