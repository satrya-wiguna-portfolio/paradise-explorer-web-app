'use strict';

app.service('Session', function() {
	this.create = function(data) {
		this.userId = data.dto.user_id;
		this.email = data.dto.email;
		this.roleId = data.dto.role_id;
		this.role = data.dto.role;
		this.token = data.dto.token;
	};

	this.destroy = function() {
		this.userId = null;
		this.email = null;
		this.roleId = null;
		this.role = null;
		this.token = null;
	};
	
	return this;
});