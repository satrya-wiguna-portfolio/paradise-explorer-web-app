<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckRoomAvailabilityStoreProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spCheckRoomAvailability');
        DB::unprepared('CREATE PROCEDURE `spCheckRoomAvailability` (
            IN product_id INT,
            IN check_in DATETIME,
            IN check_out DATETIME
        )
        BEGIN
            DECLARE long_stay INT;

            SET long_stay = TO_DAYS(check_out) - TO_DAYS(check_in);

            SELECT
                rooms.id, rooms.title, rooms.description, rooms.adult, rooms.children, rooms.max, rooms.footage,
                (
                -- GET MAX PRICE FROM ROOM OR ROOM ALLOTMENT
                    SELECT
                        IF (COALESCE(MAX(room_allotments.price), 0) = 0, rooms.price, COALESCE(MAX(room_allotments.price), 0))
                    FROM
                        room_allotments
                    WHERE
                        (CONVERT(room_allotments.date, DATE) BETWEEN check_in AND check_out)
                        AND
                        room_allotments.room_id = rooms.id
                        AND
                        room_allotments.deleted_at is NULL
                ) AS price,
                (
                -- GET MIN DISCOUNT FROM ROOM DISCOUNT
                    SELECT
                        MIN(room_discounts.discount)
                    FROM
                        room_discounts
                    WHERE
                        ((CONVERT(room_discounts.open_date, DATE) <= check_in) OR (CONVERT(room_discounts.close_date, DATE) >= check_out))
                        AND
                        room_discounts.room_id = rooms.id
                        AND
                        room_discounts.deleted_at is NULL
                ) AS room_discount,
                rooms.option, rooms.policy, long_stay,
                (
                -- GET ROOM AVAILABLE FROM ROOM ALLOTMENT BY DATE AND STATUS ACTIVE
                    SELECT
                        IF ((COUNT(*) - 1) <= 0, 0, (COUNT(*) - 1))
                    FROM
                        room_allotments
                    WHERE
                        (CONVERT(room_allotments.date, DATE) BETWEEN check_in AND check_out)
                        AND
                        room_allotments.room_id = rooms.id
                        AND
                        room_allotments.balance <> 0
                        AND
                        room_allotments.status = 1
                        AND
                        room_allotments.deleted_at is NULL
                ) AS room_available_by_date,
                (
                -- GET ROOM AVAILABLE FROM ROOM ALLOTMENT BY QUANTITY AND STATUS ACTIVE
                    SELECT
                        MIN(room_allotments.balance)
                    FROM
                        room_allotments
                    WHERE
                        (CONVERT(room_allotments.date, DATE) BETWEEN check_in AND check_out)
                        AND
                        room_allotments.room_id = rooms.id
                        AND
                        room_allotments.balance <> 0
                        AND
                        room_allotments.status = 1
                        AND
                        room_allotments.deleted_at is NULL
                ) AS room_available_by_quantity
            FROM
                rooms
            WHERE
                rooms.product_id = product_id
                AND
                rooms.deleted_at is NULL;

        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spCheckRoomAvailability');
    }
}
