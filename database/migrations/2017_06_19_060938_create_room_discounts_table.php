<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_discounts', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('room_id');
            $table->timestamp('open_date');
            $table->timestamp('close_date');
            $table->float('discount', 5, 2);
            $table->tinyInteger('status', false, false)->default(1)->comment('0: not active, 1: active');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_discounts');
    }
}
