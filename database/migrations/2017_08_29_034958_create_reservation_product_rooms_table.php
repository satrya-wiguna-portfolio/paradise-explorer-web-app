<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationProductRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_product_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('reservation_product_id');
            $table->unsignedBigInteger('room_id');
            $table->string('title');
            $table->float('price', 10, 2)->default(0)->nullable();
            $table->float('room_discount', 5, 2)->nullable()->default(0);
            $table->tinyInteger('quantity', false, false)->default(0);
            $table->float('total', 10, 2)->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('reservation_product_id')->references('id')->on('reservation_products');
            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_product_rooms');
    }
}
