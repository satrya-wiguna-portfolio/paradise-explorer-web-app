<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClearRoomAllotmentLogStoreProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spClearRoomAllotmentLog');
        DB::unprepared('CREATE PROCEDURE `spClearRoomAllotmentLog` (
            IN client_id VARCHAR(15),
            IN cart_item_id VARCHAR(35)
        )
        BEGIN

            -- DELETE ROOM ALLOTMENT LOGS
            SET @SQL = concat("
                DELETE FROM
                    room_allotment_logs
                WHERE
                    room_allotment_logs.client_id = client_id
                    AND
                    room_allotment_logs.cart_item_id IN (" , cart_item_id , ")
                    AND
                    room_allotment_logs.deleted_at is NULL;
            ");

            PREPARE statement FROM @SQL;
            EXECUTE statement;
        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spClearRoomAllotmentLog');
    }
}
