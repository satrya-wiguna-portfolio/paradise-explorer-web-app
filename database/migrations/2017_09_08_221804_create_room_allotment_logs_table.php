<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomAllotmentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_allotment_logs', function (Blueprint $table) {
            $table->string('client_id', 15);
            $table->string('cart_item_id', 35);
            $table->unsignedBigInteger('room_allotment_id');
            $table->tinyInteger('lock', false, false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_allotment_id')->references('id')->on('room_allotments')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_allotment_logs');
    }
}
