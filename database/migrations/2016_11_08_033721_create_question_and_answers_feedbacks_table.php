<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionAndAnswersFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_and_answers_feedbacks', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_and_answers_id');
            $table->timestamp('date');
            $table->string('ip_address');
            $table->enum('feedback', ['help', 'unhelp']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('question_and_answers_id')->references('id')->on('question_and_answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question_and_answers_feedbacks');
    }
}
