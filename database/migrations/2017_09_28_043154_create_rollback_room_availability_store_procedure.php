<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRollbackRoomAvailabilityStoreProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spRollbackRoomAvailability');
        DB::unprepared('CREATE PROCEDURE `spRollbackRoomAvailability` ()
        BEGIN

            -- UPDATE ROOM ALLOTMENTS
            UPDATE
                room_allotments INNER JOIN room_allotment_logs
                ON room_allotments.id = room_allotment_logs.room_allotment_id
            SET
                room_allotments.lock = room_allotments.lock - room_allotment_logs.lock,
                room_allotments.balance = room_allotments.balance + room_allotment_logs.lock
            WHERE
                TIMESTAMPDIFF(MINUTE, IF(room_allotment_logs.updated_at is NULL, room_allotment_logs.created_at, room_allotment_logs.updated_at), NOW()) >= 5;

            -- UPDATE ROOM ALLOTMENT LOGS
            DELETE FROM
                room_allotment_logs
            WHERE
                TIMESTAMPDIFF(MINUTE, IF(room_allotment_logs.updated_at is NULL, room_allotment_logs.created_at, room_allotment_logs.updated_at), NOW()) >= 5;

        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spRollbackRoomAvailability');
    }
}
