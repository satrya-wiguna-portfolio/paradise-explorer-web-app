<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('agent_type_id');
            $table->enum('title', ['mr', 'mrs'])->nullable();
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('company', 100)->nullable();
            $table->string('address');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('state_id');
            $table->string('city');
            $table->string('zip', 5)->nullable();
            $table->string('image_url')->nullable();
            $table->string('phone', 15);
            $table->string('fax', 15)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('agent_type_id')->references('id')->on('agent_types');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('state_id')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agents');
    }
}
