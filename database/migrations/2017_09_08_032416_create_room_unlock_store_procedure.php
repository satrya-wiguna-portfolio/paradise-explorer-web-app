<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomUnlockStoreProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spRoomUnlock');
        DB::unprepared('CREATE PROCEDURE `spRoomUnlock` (
            IN client_id VARCHAR(15),
            IN cart_item_id VARCHAR(35)
        )
        BEGIN

            -- UPDATE ROOM ALLOTMENTS
            UPDATE
                room_allotments INNER JOIN room_allotment_logs
                ON room_allotments.id = room_allotment_logs.room_allotment_id
            SET
                room_allotments.lock = room_allotments.lock - room_allotment_logs.lock,
                room_allotments.balance = room_allotments.balance + room_allotment_logs.lock
            WHERE
                room_allotments.id IN (
                    SELECT
                        room_allotment_id
                    FROM
                        room_allotment_logs
                    WHERE
                        room_allotment_logs.client_id = client_id COLLATE utf8_unicode_ci
                        AND
                        room_allotment_logs.cart_item_id = cart_item_id COLLATE utf8_unicode_ci
                        AND
                        room_allotment_logs.deleted_at is NULL
                )
                AND
                room_allotments.deleted_at is NULL;


            -- DELETE ROOM ALLOTMENT LOGS
            DELETE FROM
                room_allotment_logs
            WHERE
                room_allotment_logs.client_id = client_id COLLATE utf8_unicode_ci
                AND
                room_allotment_logs.cart_item_id = cart_item_id COLLATE utf8_unicode_ci
                AND
                room_allotment_logs.deleted_at is NULL;

        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spRoomUnlock');
    }
}
