<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRollbackRoomAvailabilityEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP EVENT IF EXISTS evRollbackRoomAvailability');
        DB::unprepared('CREATE EVENT IF NOT EXISTS `evRollbackRoomAvailability`
            ON SCHEDULE EVERY 1 MINUTE
            ON COMPLETION PRESERVE
            DO
              CALL spRollbackRoomAvailability();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evRollbackRoomAvailability');
    }
}
