<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomLockStoreProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spRoomLock');
        DB::unprepared('CREATE PROCEDURE `spRoomLock` (
            IN check_in DATETIME,
            IN check_out DATETIME,
            IN client_id VARCHAR(15),
            IN cart_item_id VARCHAR(35),
            IN room_id INT,
            IN qty INT
        )
        BEGIN

            -- INSERT ROOM ALLOTMENT LOGS
            INSERT INTO room_allotment_logs
                    (room_allotment_logs.client_id,
                    room_allotment_logs.cart_item_id,
                    room_allotment_logs.room_allotment_id,
                    room_allotment_logs.lock,
                    room_allotment_logs.created_at)
            SELECT
                client_id,
                cart_item_id,
                room_allotments.id,
                qty,
                NOW()
            FROM
                room_allotments
            WHERE
                (CONVERT(room_allotments.date, DATE) BETWEEN check_in AND check_out)
                AND
                room_allotments.room_id = room_id
                AND
                room_allotments.deleted_at is NULL;


            -- UDATE ROOM ALLOTMENTS
            UPDATE
                room_allotments
            SET
                room_allotments.lock = room_allotments.lock + qty,
                room_allotments.balance = room_allotments.balance - qty
            WHERE
                (CONVERT(room_allotments.date, DATE) BETWEEN check_in AND check_out)
                AND
                room_allotments.room_id = room_id
                AND
                room_allotments.deleted_at is NULL;
        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS spRoomLock');
    }
}
