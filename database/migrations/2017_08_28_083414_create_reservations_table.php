<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_number', 25)->unique();
            $table->date('invoice_release_date');
            $table->dateTime('invoice_paid_date')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->enum('title', ['mr', 'mrs'])->nullable();
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('address')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('state_id')->nullable();
            $table->string('city')->nullable();
            $table->string('zip', 5)->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('handphone', 15)->nullable();
            $table->unsignedInteger('payment_id');
            $table->unsignedInteger('currency_id');
            $table->string('exchange_rate');
            $table->tinyInteger('status', false, false)->default(0)->comment('0:pending, 1:unpaid, 2:paid, 3:success, 4:cancel');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
