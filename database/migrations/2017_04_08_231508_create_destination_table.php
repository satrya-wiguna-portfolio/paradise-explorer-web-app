<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinations', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id', false, false)->unsigned()->nullable();
            $table->string('name');
            $table->string('image_url')->nullable();
            $table->float('lat', 10, 6);
            $table->float('lng', 10, 6);
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parent_id')->references('id')->on('destinations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('destinations');
    }
}
