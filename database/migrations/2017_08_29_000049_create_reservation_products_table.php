<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('reservation_id');
            $table->unsignedBigInteger('product_id');
            $table->string('title');
            $table->unsignedBigInteger('agent_id');
            $table->unsignedInteger('product_type_id');
            $table->float('price', 10, 2)->default(0)->nullable();
            $table->float('discount', 5, 2)->nullable()->default(0);
            $table->float('product_discount', 5, 2)->nullable()->default(0);
            $table->tinyInteger('quantity', false, false)->default(0)->nullable();
            $table->float('total', 10, 2)->default(0)->nullable();
            $table->date('check_in')->nullable();
            $table->date('check_out')->nullable();
            $table->date('departure')->nullable();
            $table->tinyInteger('adult', false, false)->default(0);
            $table->tinyInteger('child', false, false)->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->foreign('product_type_id')->references('id')->on('product_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_products');
    }
}
