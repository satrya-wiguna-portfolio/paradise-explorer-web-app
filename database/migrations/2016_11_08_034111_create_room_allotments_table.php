<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomAllotmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_allotments', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('room_id');
            $table->dateTime('date');
            $table->tinyInteger('allotment', false, false)->default(0);
            $table->tinyInteger('balance', false, false)->default(0);
            $table->tinyInteger('lock', false, false)->default(0);
            $table->tinyInteger('sold', false, false)->default(0);
            $table->float('price', 10, 2)->default(0)->nullable();
            $table->tinyInteger('status', false, false)->default(1)->comment('0: not active, 1: active');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_allotments');
    }
}
