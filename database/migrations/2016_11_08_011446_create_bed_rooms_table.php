<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bed_rooms', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('bed_id');
            $table->unsignedBigInteger('room_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bed_id')->references('id')->on('beds');
            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bed_rooms');
    }
}
