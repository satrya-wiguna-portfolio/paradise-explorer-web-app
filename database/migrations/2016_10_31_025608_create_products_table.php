<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_type_id');
            $table->unsignedInteger('product_category_id')->nullable();
            $table->unsignedInteger('property_type_id')->nullable();
            $table->unsignedBigInteger('agent_id');
            $table->dateTime('publish');
            $table->string('title');
            $table->string('slug');
            $table->text('overview')->nullable();
            $table->text('description')->nullable();
            $table->string('featured_image_url')->nullable();
            $table->string('featured_video_url')->nullable();
            $table->string('include_brochure_url')->nullable();
            $table->string('itinerary_brochure_url')->nullable();
            $table->float('lat', 10, 6);
            $table->float('lng', 10, 6);
            $table->string('address')->nullable();
            $table->string('duration', 50)->nullable();
            $table->tinyInteger('min_age')->default(0);
            $table->tinyInteger('max_age')->default(0);
            $table->float('price', 10, 2)->default(0)->nullable();
            $table->tinyInteger('grade')->default(0)->nullable();
            $table->float('discount', 5, 2)->nullable();
            $table->tinyInteger('status', false, false)->default(0)->comment('0: draft, 1: publish, 2: pendding');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('product_type_id')->references('id')->on('product_types');
            $table->foreign('product_category_id')->references('id')->on('product_categories');
            $table->foreign('property_type_id')->references('id')->on('property_types');
            $table->foreign('agent_id')->references('id')->on('agents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
