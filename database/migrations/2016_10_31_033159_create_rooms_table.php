<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->tinyInteger('adult', false, false)->default(0);
            $table->tinyInteger('children', false, false)->default(0);
            $table->tinyInteger('max', false, false)->default(0);
            $table->float('footage', 5, 2)->default(0);
            $table->float('price', 10, 2)->default(0);
            $table->text('option')->nullable();
            $table->text('policy')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }
}
