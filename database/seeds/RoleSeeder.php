<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['role' => 'super', 'description' => 'description of super role'],
            ['role' => 'admin', 'description' => 'description of admin role'],
            ['role' => 'agent', 'description' => 'description of agent role'],
            ['role' => 'member', 'description' => 'description of member role'],
            ['role' => 'guest', 'description' => 'description of guest role']
        ]);
    }
}
