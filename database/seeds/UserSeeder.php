<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert user table records
        DB::table('users')->insert([
            [ 'email' => 'admin@paradise-explorers.id', 'password' => \Hash::make('d3f4ult'), 'handphone' => +628113808231, 'status' => '1']
        ]);
    }
}
