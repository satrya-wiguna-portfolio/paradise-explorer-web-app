<?php

use Illuminate\Database\Seeder;

class AgentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert agent type table records
        DB::table('agent_types')->insert([
            ['name' => 'Tour','description' => 'Description of tour agent type'],
            ['name' => 'Hotel','description' => 'Description of hotel agent type']
        ]);
    }
}
