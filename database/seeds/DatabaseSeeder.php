<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        Model::unguard();

        ini_set('memory_limit','512M');

        //Delete table records
        DB::table('states')->delete();
        DB::table('countries')->delete();

        DB::table('districts')->delete();
        DB::table('regencies')->delete();
        DB::table('provinces')->delete();

        DB::table('user_roles')->delete();
        DB::table('roles')->delete();
        DB::table('users')->delete();

        DB::table('agent_types')->delete();
        DB::table('product_types')->delete();


        //Call seeder
        $this->call('CountrySeeder');
        $this->call('StateSeeder');

        $this->call('ProvinceSeeder');
        $this->call('RegencySeeder');
        $this->call('DistrictSeeder');

        $this->call('AgentTypeSeeder');
        $this->call('ProductTypeSeeder');

        $this->call('RoleSeeder');
        $this->call('UserSeeder');
        $this->call('UserRoleSeeder');
    }
}
