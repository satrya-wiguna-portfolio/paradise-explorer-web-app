<?php

use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert product type table records
        DB::table('product_types')->insert([
            ['type' => 'Tour','description' => 'Description of tour product type'],
            ['type' => 'Hotel','description' => 'Description of hotel product type']
        ]);
    }
}
